<?php

namespace App\Exports;

use App\Models\Docente;
use Maatwebsite\Excel\Concerns\FromCollection;

class pruebaExcel implements FromCollection
{
    public function collection()
    {
        return Docente::all();
    }
}
?>