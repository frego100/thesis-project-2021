<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\Curso;
use App\Models\Docente;
use App\Models\Estudiante;
use App\Models\EscuelaSemestre;
use App\Models\CursoSemestre;
use App\Models\CursoGrupo;
use App\Models\CursoEstudiante;
use App\Models\Semestre;
use App\Models\Escuela;

class UsersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $curso_id = $request->curso_id;
        $semestre_id = $request->semestre_id;
        return User::all();
    }
}
?>