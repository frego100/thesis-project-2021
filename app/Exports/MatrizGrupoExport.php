<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;

class MatrizGrupoExport implements FromView, WithCustomStartCell
{
    use Exportable;

    protected $escuela;
    protected $semestre;
    protected $curso;
    protected $cursogrupo;
    protected $docente;

    function __construct($escuela,$semestre,$curso,$cursogrupo,$docente,$cursomatrizs,$cursoestudiantes,$matrizdetalles) {
        $this->escuela = $escuela;
        $this->semestre = $semestre;
        $this->curso = $curso;
        $this->cursogrupo = $cursogrupo;
        $this->docente = $docente;
        $this->cursomatrizs = $cursomatrizs;
        $this->cursoestudiantes = $cursoestudiantes;
        $this->matrizdetalles = $matrizdetalles;
    }

    public function view(): View
    {
        $escuela=$this->escuela;
        $semestre=$this->semestre;
        $curso=$this->curso;
        $cursogrupo=$this->cursogrupo;
        $docente=$this->docente;
        $cursomatrizs=$this->cursomatrizs;
        $cursoestudiantes=$this->cursoestudiantes;
        $matrizdetalles=$this->matrizdetalles;

        error_log("PRUEBA EXPORTAR MATRIZ GRUPO");
        return view('export.exportMatrizGrupo', compact('escuela','semestre','curso','cursogrupo','docente','cursomatrizs','cursoestudiantes','matrizdetalles'));
    }

    public function startCell(): string
    {
        return 'B3';
    }
}
?>