<?php

namespace Dompdf;

use Illuminate\Http\Request;
use Dompdf\Dompdf;

class ExportarPDF extends Controller
{
	public function pruebaPDF(Request $request)
	{
		$pdf = PDF::loadView('export.exportEjemplo');
        return $pdf->download('prueba_pdf.pdf');
	}
}
?>