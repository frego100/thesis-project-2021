<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;

class EsquemaExport implements FromView, WithStyles, WithCustomStartCell
{
    use Exportable;

    protected $escuela;
    protected $semestre;
    protected $curso;
    protected $docente;
    protected $cursosemestre;

    function __construct($escuela,$semestre,$curso,$docente,$cursosemestre) {
        $this->escuela = $escuela;
        $this->semestre = $semestre;
        $this->curso = $curso;
        $this->docente = $docente;
        $this->cursosemestre = $cursosemestre;
    }

    public function view(): View
    {
        $escuela=$this->escuela;
        $semestre=$this->semestre;
        $curso=$this->curso;
        $docente=$this->docente;
        $cursosemestre=$this->cursosemestre;
        error_log("PRUEBAEXPORTAR");
        error_log($this->curso);
        return view('export.exportEsquema', compact('escuela','semestre','curso','docente','cursosemestre'));
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getColumnDimension('D')->setVisible(false);;
    }

    public function startCell(): string
    {
        return 'B3';
    }
}
?>