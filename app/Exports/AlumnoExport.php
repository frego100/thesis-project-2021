<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;

class AlumnoExport implements FromView, WithStyles, WithCustomStartCell
{
    use Exportable;

    protected $escuela;
    protected $semestre;
    protected $curso;
    protected $cursogrupo;
    protected $docente;

    function __construct($escuela,$semestre,$curso,$cursogrupo,$docente) {
        $this->escuela = $escuela;
        $this->semestre = $semestre;
        $this->curso = $curso;
        $this->cursogrupo = $cursogrupo;
        $this->docente = $docente;
    }

    public function view(): View
    {
        $escuela=$this->escuela;
        $semestre=$this->semestre;
        $curso=$this->curso;
        $cursogrupo=$this->cursogrupo;
        $docente=$this->docente;
        error_log("PRUEBAEXPORTAR");
        error_log($this->curso);
        return view('export.exportAlumno', compact('escuela','semestre','curso','cursogrupo','docente'));
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getColumnDimension('D')->setVisible(false);;
    }

    public function startCell(): string
    {
        return 'B3';
    }
}
?>