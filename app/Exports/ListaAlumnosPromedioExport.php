<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Facades\Excel;

use Maatwebsite\Excel\Concerns\FromCollection;

class ListaAlumnosPromedioExport implements FromView, WithTitle
{
    use Exportable;

    protected $escuela;
    protected $semestre;
    protected $curso;
    protected $docente;
    protected $grupo;
    protected $reporte;
    protected $esquemas;
    protected $cursomatriz;
    protected $matrizdetalle;

    function __construct($escuela,$semestre,$curso,$docente,$grupo,$reporte,$esquemas,$cursomatriz,$matrizdetalle) {
        $this->escuela = $escuela;
        $this->semestre = $semestre;
        $this->curso = $curso;
        $this->docente = $docente;
        $this->grupo = $grupo;
        $this->reporte = $reporte;
        $this->esquemas = $esquemas;
        $this->cursomatriz = $cursomatriz;
        $this->matrizdetalle = $matrizdetalle;
    }

    public function view(): View
    {
        $escuela=$this->escuela;
        $semestre=$this->semestre;
        $curso=$this->curso;
        $docente=$this->docente;
        $grupo=$this->grupo;
        $reporte=$this->reporte;
        $esquemas = $this->esquemas;
        $cursomatriz=$this->cursomatriz;
        $matrizdetalle = $this->matrizdetalle;
        $cont = 0;
        return view('export.exportListaAlumnosPromedio', compact('escuela','semestre','curso','docente','grupo','reporte','esquemas','cursomatriz','cont','matrizdetalle'));
    }

    public function title(): string
    {
        return 'Promedio';
    }
}
?>