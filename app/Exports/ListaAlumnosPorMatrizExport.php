<?php

namespace App\Exports;

use App\Models\Criterio;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Facades\Excel;

use Maatwebsite\Excel\Concerns\FromCollection;

class ListaAlumnosPorMatrizExport implements FromView, WithTitle, WithStyles
{
    use Exportable;

    protected $escuela;
    protected $semestre;
    protected $curso;
    protected $docente;
    protected $grupo;
    protected $reporte;
    protected $esquemas;
    protected $criterios;
    protected $cursomatriz;
    protected $matrizdetalle;

    function __construct($escuela,$semestre,$curso,$docente,$grupo,$reporte,$esquemas,$criterios,$cursomatriz,$matrizdetalle) {
        $this->escuela = $escuela;
        $this->semestre = $semestre;
        $this->curso = $curso;
        $this->docente = $docente;
        $this->grupo = $grupo;
        $this->reporte = $reporte;
        $this->esquemas = $esquemas;
        $this->criterios = $criterios;
        $this->cursomatriz = $cursomatriz;
        $this->matrizdetalle = $matrizdetalle;
    }

    public function view(): View
    {

        $criterios_ids = [];

        $escuela=$this->escuela;
        $semestre=$this->semestre;
        $curso=$this->curso;
        $docente=$this->docente;
        $grupo=$this->grupo;
        $reporte=$this->reporte;
        $esquemas = $this->esquemas;
        $criterios = $this->criterios;
        $matrizdetalle = $this->matrizdetalle;
        $cont = 0;

        foreach($criterios as $criterio){
            array_push($criterios_ids,$criterio->id);
        }

        array_unique($criterios_ids);

        $criteriost = Criterio::whereIn('id',$criterios_ids)->where('estado','A')->get();

        foreach($criteriost as $cri){
            error_log("CRITERIOS REPORTEEEEEEEEEEEEEEEEEEEEEEEE");
            error_log($cri->codigo);
        }
        return view('export.exportListaAlumnos', compact('escuela','semestre','curso','docente','grupo','reporte','esquemas','criterios','criteriost','cont','matrizdetalle'));
    }

    public function title(): string
    {
        return $this->cursomatriz->matriz;
    }

    public function styles(Worksheet $sheet)
    {
        $columna = 68;
        $columna = $columna + sizeof($this->criterios);
        $sheet->getStyle("C11:".(chr($columna-1))."11")->getAlignment()->setTextRotation(90);
        // $sheet->cell("C11:AZ11",function($cell){
        //     $cell->setValignment('center');
        // });
    }

    // public function map($escuela): array
    // {
    //         return [
    //         'B22' => '=C13*D13'
    //         ];
    
    // }
}
?>