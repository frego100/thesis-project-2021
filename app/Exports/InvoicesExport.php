<?php
namespace App\Exports;

use App\Invoice;
use Maatwebsite\Excel\Concerns\FromCollection;

class InvoicesExport implements FromCollection, WithCustomStartCell

{
    public function collection()
    {
        return Invoice::all();
    }
    public function startCell(): string
    {
        return 'A1';
        // Universidad Nacional de San Agustín de Arequipa
        //A2 - Escuela Profesional de Ingeniería de Sistemas
        //A3 - semestre
        //A4- Curso:....
        //A5-Docente:...
    }
}
?>