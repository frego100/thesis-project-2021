<?php

namespace App\Exports;

class ObjetoReporte{

    public $estudiante;
    public $notas;

    function __construct($estudiante,$notas) {
        $this->estudiante = $estudiante;
        $this->notas = $notas;
    }
}