<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\FromCollection;

class ListaAlumnosExport implements WithMultipleSheets
{
    use Exportable;

    protected $escuela;
    protected $semestre;
    protected $curso;
    protected $docente;
    protected $grupo;
    protected $reporte;
    protected $esquemasreporte;
    protected $criteriosreporte;
    protected $cursomatrizreporte;
    protected $matrizsheet;

    function __construct($escuela,$semestre,$curso,$docente,$grupo,$reporte,$esquemasreporte,$criteriosreporte,$cursomatrizreporte,$matrizsheet) {
        $this->escuela = $escuela;
        $this->semestre = $semestre;
        $this->curso = $curso;
        $this->docente = $docente;
        $this->grupo = $grupo;
        $this->reporte = $reporte;
        $this->esquemasreporte = $esquemasreporte;
        $this->criteriosreporte = $criteriosreporte;
        $this->cursomatrizreporte = $cursomatrizreporte;
        $this->matrizsheet = $matrizsheet;
    }

    public function sheets(): array
    {
        $sheets = [];
        $reporteporMatriz = [];

        error_log('REPORTE ERRORRRRRRRRRRRRRRRRRRRRRRRRRRRR');
        error_log(sizeof($this->reporte));

        // foreach($this->estudiantes as $est){
        //     foreach($est->notas as $notas){
        //         $re = new ObjetoReporte($est,$notas);
        //         array_push($reporteporMatriz,$re);
        //     }
        //     array_push($reporte,$reporteporMatriz);
        // }

        // foreach($reporte as $rep){
        //     error_log("REPORTEEEEEEEEEEEEEEEEEEEEEE: ");
        //     error_log($rep);
        // }

        // foreach($this->esquemasreporte as $esqr){
        //     error_log("SEPARACIONPORARRIBAAAAAAAAAAAAA");
        //     foreach($esqr as $e){
        //         error_log("CRITERIOREPORTEEEEEEEEEEEEEEEEEEE");
        //         error_log($e);
        //     }
        //     error_log("SEPARACIONNNNNNNNNNNNNNNN");
        // }

        for($i=0; $i<sizeof($this->reporte); $i++){
            // error_log("CRITERIOREPORTEEEEEEEEEEEEEEEEEEE");
            // if($this->esquemasreporte[$i]!=null)
            // error_log($this->esquemasreporte[$i][0]);
            $sheets[] = new ListaAlumnosPorMatrizExport($this->escuela,$this->semestre,$this->curso,$this->docente,$this->grupo,$this->reporte[$i],$this->esquemasreporte[$i],$this->criteriosreporte[$i],$this->cursomatrizreporte[$i],$this->matrizsheet[$i]);
        }

        $sheets[] = new ListaAlumnosPromedioExport($this->escuela,$this->semestre,$this->curso,$this->docente,$this->grupo,$this->reporte[0],$this->esquemasreporte,$this->cursomatrizreporte,$this->matrizsheet);

        return $sheets;
    }
}
?>