<?php

namespace App\Http\Controllers;

use App\Exports\pruebaExcel;
use Maatwebsite\Excel\Facades\Excel;

class PruebaExcelController extends Controller 
{
    public function view(): View
    {
        return view('user.docente.crearMatriz', [
            'invoices' => Invoice::all()
        ]);
    }
    
    public function export() 
    {
        return Excel::download(new pruebaExcel, 'docente.xlsx');
    }
}