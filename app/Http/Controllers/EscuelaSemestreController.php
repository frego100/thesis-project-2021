<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EscuelaSemestre;
use App\Models\Escuela;
use App\Models\Semestre;
use App\Models\CursoSemestre;

class EscuelaSemestreController extends Controller
{
    //
    public function index()
    {
        $escuelasemestres = EscuelaSemestre::with(['escuela','semestre'])->where('estado','A')->get();
        return view('escuelasemestre.index',compact('escuelasemestres'));
    }
    public function create()
    {
        $escuelas=Escuela::all();
        $semestres = Semestre::all();
        return view('escuelasemestre.create',compact('semestres','escuelas'));
    }
    public function show($id)
    {
        $escuelasemestre = EscuelaSemestre::find($id);
        return view('escuelasemestre.show',compact('escuelasemestre'));
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['escuela_id']= $request['escuela'];
        $input['semestre_id']= $request['semestre'];
        $escuelasemestre = EscuelaSemestre::create($input);

        return view('escuelasemestre.index')->with('message','insertado correctamente');
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $escuelasemestre = EscuelaSemestre::find($id);
        $escuelasemestre->update($input);

        return view('escuelasemestre.index')->with('message','insertado correctamente');
    }
    public function edit($id)
    {
        $escuelas=Escuela::all();
        $semestres = Semestre::all();
        $escuelasemestre = EscuelaSemestre::findOrFail($id);
        return view('escuelasemestre.edit', compact('escuelasemestre','escuelas','semestres'));
    }
    public function delete($id)
    {
        $escuelasemestre = EscuelaSemestre::find($id);

        $cursosemestre = CursoSemestre::where('escuelasemestre_id',$id)->where('estado','A')->get();

        if(sizeof($cursosemestre) == 0){
            $escuelasemestre->estado = 'D';
            $escuelasemestre->save();
            return redirect('escuelasemestres')->with(['success' => 'Se elimino exitosamente']);
        }

        return redirect('escuelasemestres')->with(['error' => 'No se puede eliminar escuelasemestre porque tiene relacion con '. sizeof($cursosemestre).' registros']);
    }
}
