<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CursoGrupo;
use App\Models\CursoSemestre;
use App\Models\Docente;
use App\Models\CursoEstudiante;

class CursoGrupoController extends Controller
{
    public function index()
    {   
        $cursogrupos=CursoGrupo::with(['cursosemestre','docente'])->where('estado','A')->get();

        return view("cursogrupo.index",compact('cursogrupos'));
    }

    public function create()
    {
        $cursosemestres=CursoSemestre::all();
        $docentes=Docente::all();
        return view('cursogrupo.create', compact('cursosemestres','docentes'));
    }
  
    public function store(Request $request)
    {
        $input = $request->all();
        $input['cursosemestre_id'] = $request['cursosemestre'];
        $input['docente_id'] = $request['docente'];
        $cursogrupo = CursoGrupo::create($input);
        return redirect('cursogrupos')->with('message', 'Guardado Exitosamente');
    }

    public function show($id)
    {
        $cursogrupo=CursoGrupo::find($id);
        return view('cursogrupo.show', compact('cursogrupo'));
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $input['cursosemestre_id'] = $request['cursosemestre'];
        $input['docente_id'] = $request['docente'];
        $cursogrupo = CursoGrupo::find($id);
        $cursogrupo->update($input);
        return redirect('cursogrupos')->with('message', 'Actualizado!');
    }

    public function edit($id)
    {
        $cursosemestres=CursoSemestre::all();
        $docentes=Docente::all();
        $cursogrupo = CursoGrupo::findOrFail($id);
        return view('cursogrupo.edit', compact('cursosemestres','docentes','cursogrupo'));
    }

    public function delete($id)
    {
        $cursogrupo = CursoGrupo::find($id);

        $cursoestudiante = CursoEstudiante::where('cursogrupo_id',$id)->where('estado','A')->get();

        if(sizeof($cursoestudiante) == 0){
            $cursogrupo->estado = 'D';
            $cursogrupo->save();
            return redirect('cursogrupos')->with(['success' => 'Se elimino exitosamente']);
        }

        return redirect('cursogrupos')->with(['error' => 'No se puede eliminar cursogrupo porque tiene relacion con '. sizeof($cursoestudiante).' registros']);
        
    }
    
    //ajax
    public function insertarCursoGrupo(Request $request)
    {
        $input = $request->all();
        $input['grupo'] = $request->grupo;
        $input['coordinador'] = $request->coordinador;
        $input['cursosemestre_id'] = $request->cursosemestre_id;
        $input['docente_id'] = $request->docente_id;
        $cursogrupo = CursoGrupo::create($input);
        return 1;
    }

    public function eliminarCursoGrupo(Request $request)
    {
        $id = $request->id;
        $cursogrupo = CursoGrupo::findOrFail($id);

        $cursoestudiante = CursoEstudiante::where('cursogrupo_id',$id)->where('estado','A')->get();

        if(sizeof($cursoestudiante) == 0){
            $cursogrupo->estado = 'D';
            $cursogrupo->save();
            return 0;
        }

        return sizeof($cursoestudiante);
    }

    public function editarCursoGrupo(Request $request)
    {
        $id = $request->id;
        $cursogrupo = CursoGrupo::findOrFail($id);
        $cursogrupo->grupo = $request->grupo;
        $cursogrupo->docente_id = $request->docente_id;
        $cursogrupo->save();
        return 1;
    }

    public function editarCursoGrupoCoordinador(Request $request)
    {
        $id = $request->id;
        $cursogrupo = CursoGrupo::findOrFail($id);
        $otrocursogrupo = CursoGrupo::where('cursosemestre_id',$cursogrupo->cursosemestre_id)
            ->where('coordinador','1')
            ->first();

        $cursogrupo->coordinador = 1;
        $otrocursogrupo->coordinador = 0;
        $otrocursogrupo->save();
        $cursogrupo->save();
        return 1;
    }
}
