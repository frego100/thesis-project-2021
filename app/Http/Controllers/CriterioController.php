<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Criterio;
use App\Models\MatrizDetalle;
use App\Models\ResultadosEstudiante;

class CriterioController extends Controller
{
    //
    public function index()
    {

        $criterios = Criterio::with(['resultadoestudiante'])->where('estado','A')->get();        
        return view('criterio.index',compact('criterios'));

    }
    public function create()
    {
        $resultadosestudiantes=ResultadosEstudiante::all();
        return view('criterio.create',compact('resultadosestudiantes'));
    }
  
    public function store(Request $request)
    {
        $input = $request->all();

        $input['resultadoestudiante_id'] = $request['resultadosestudiante'];


        $criterio = Criterio::create($input);
        return redirect('criterios');
       // return \response()->json(['res' => true, 'message' =>'Guardado correctamente'], 200);
    }

    public function show($id)
    {
        $criterio=Criterio::find($id);
        return view('criterio.show',compact('criterio'));

    }

    
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $input['resultadoestudiante_id'] = $request['resultadoestudiante'];

        $criterio = Criterio::find($id);
        $criterio->update($input);

        //return \response()->json(['res' => true, 'message' =>'actualizado correctamente'], 200);
        return redirect('criterios')->with('message', 'Actualizado!');
    }

    public function edit($id)
    {
        $resultadosestudiantes=ResultadosEstudiante::all();
        $criterio = Criterio::findOrFail($id);
        return view('criterio.edit', compact('criterio','resultadosestudiantes'));
    }
    public function delete($id)
    {
        $criterio = Criterio::find($id);

        $matrizdetalle = MatrizDetalle::where('criterio_id',$id)->where('estado','A')->get();

        if(sizeof($matrizdetalle) == 0){
            $criterio->estado = 'D';
            $criterio->save();
            return redirect('criterios')->with(['success' => 'Se elimino exitosamente']);
        }

        return redirect('criterios')->with(['error' => 'No se puede eliminar criterio porque tiene relacion con '. sizeof($matrizdetalle).' registros']);

    }
        //ajax
    public function insertarCriterio(Request $request)
    {
        $input = $request->all();
        $input['resultadoestudiante_id'] = $request->resultadoestudiante_id;
        $input['codigo'] = $request->codigo;
        $input['criterio'] = $request->criterio;
        $input['insatisfactorio'] = $request->insatisfactorio;
        $input['enprogreso'] = $request->enprogreso;
        $input['satisfactorio'] = $request->satisfactorio;
        $input['sobresaliente'] = $request->sobresaliente;
        $criterio = Criterio::create($input);
        return 1;
    }
    public function eliminarCriterio(Request $request)
    {
        $id = $request->id;
        $criterio = Criterio::findOrFail($id);

        $matrizdetalle = MatrizDetalle::where('criterio_id',$id)->where('estado','A')->get();

        if(sizeof($matrizdetalle) == 0){
            $criterio->estado = 'D';
            $criterio->save();
            return 0;
        }

        return sizeof($matrizdetalle);
    }

    public function editarCriterio(Request $request)
    {
        $id = $request->id;
        $criterio = Criterio::findOrFail($id);
        $criterio->codigo = $request->codigo;
        $criterio->criterio = $request->criterio;
        $criterio->insatisfactorio = $request->insatisfactorio;
        $criterio->enproceso = $request->enproceso;
        $criterio->satisfactorio = $request->satisfactorio;
        $criterio->sobresaliente = $request->sobresaliente;
        $criterio->save();
        return 1;
    }
}
