<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CursoEstudiante;
use App\Models\Estudiante;
use App\Models\CursoGrupo;
use App\Models\EstudianteMatriz;

class CursoEstudianteController extends Controller
{
    public function index()
    {
        $cursoestudiantes = CursoEstudiante::with(['cursogrupo','estudiante'])->get();
        return view('cursoestudiante.index',compact('cursoestudiantes'));
    }
    public function create()
    {
        $cursogrupos=CursoGrupo::all();
        $estudiantes = Estudiante::all();
        return view('cursoestudiante.create',compact('cursogrupos','estudiantes'));
    }
    public function show($id)
    {
        $cursoestudiante = CursoEstudiante::find($id);
        return view('cursoestudiante.show',compact('cursoestudiante'));
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['cursogrupo_id']= $request['cursogrupo'];
        $input['estudiante_id']= $request['estudiante'];
        $cursoestudiante = CursoEstudiante::create($input);
        return redirect('cursoestudiantes')->with('message', 'Guardado Exitosamente');
    
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $cursoestudiante = CursoEstudiante::find($id);
        $cursoestudiante->update($input);

        return redirect('cursoestudiantes')->with('message','Actualizado Correctamente');
    }
    public function edit($id)
    {
        $cursogrupos = CursoGrupo::all();
        $estudiantes = Estudiante::all();
        $cursoestudiante = CursoEstudiante::findOrFail($id);
        return view('cursoestudiante.edit', compact('cursoestudiante','cursogrupos','estudiantes'));
    }
    public function delete($id)
    {
        $cursoestudiante = CursoEstudiante::find($id);

        $estudiantematriz = EstudianteMatriz::where('curso_estudiante_id',$id)->where('estado','A')->get();

        if(sizeof($estudiantematriz) == 0){
            $cursoestudiante->estado = 'D';
            $cursoestudiante->save();
            return redirect('cursoestudiantes')->with(['success' => 'Se elimino exitosamente']);
        }

        return redirect('cursoestudiantes')->with(['error' => 'No se puede eliminar cursoestudiante porque tiene relacion con '. sizeof($estudiantematriz).' registros']);
    }
    // para el ajax
    public function insertarAlumnoGrupo(Request $request)
    {
        $input = $request->all();
        $input['estudiante_id'] = $request->alumno_id;
        $input['cursogrupo_id'] = $request->cursogrupo_id;
        $cursoestudiante = CursoEstudiante::create($input);
        return 1;
    }
    public function eliminarAlumnoGrupo(Request $request)
    {
        $id = $request->id;
        $cursoestudiante = CursoEstudiante::findOrFail($id);

        $estudiantematriz = EstudianteMatriz::where('curso_estudiante_id',$id)->where('estado','A')->get();

        if(sizeof($estudiantematriz) == 0){
            $cursoestudiante->estado = 'D';
            $cursoestudiante->save();
            return 0;
        }

        return sizeof($estudiantematriz);
    }
}
