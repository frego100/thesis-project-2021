<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Curso;
use App\Models\CursoTipo;
use App\Models\Plan;
use App\Models\CursoSemestre;

class CursoController extends Controller
{
    //

    public function index()
    {
        $cursos=Curso::with(['plan','cursotipo','cursosemestres'])->where('estado','A')->get();
        //return $escuelas;
        return view('curso.index',compact('cursos'));
    }

    public function create()
    {
        $planes=Plan::all();
        $cursotipos=CursoTipo::all();
        return view('curso.create',compact('planes','cursotipos'));
    }
  
    public function store(Request $request)
    {
        $input = $request->all();
        $input['plan_id'] = $request['plan_id'];
        $input['cursotipo_id'] = $request['cursotipo_id'];
        $curso = Curso::create($input);
        return redirect('cursos');
       // return \response()->json(['res' => true, 'message' =>'Guardado correctamente'], 200);
    }


    public function show($id)
    {
        $curso=Curso::find($id);
        return view('curso.show',compact('curso'));
    }

    
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $input['plan_id'] = $request['plan'];
        $input['cursotipo_id'] = $request['cursotipo'];
        $curso = Curso::find($id);
        $curso->update($input);

        //return \response()->json(['res' => true, 'message' =>'actualizado correctamente'], 200);
        return redirect('cursos')->with('message', 'Actualizado!');
    }

    public function edit($id)
    {
        $planes=Plan::all();
        $cursotipos=CursoTipo::all();
        $curso = Curso::findOrFail($id);
        return view('curso.edit', compact('curso','planes','cursotipos'));
    }

    public function delete($id)
    {
        $curso = Curso::find($id);

        $cursosemestre = CursoSemestre::where('curso_id',$id)->where('estado','A')->get();

        if(sizeof($cursosemestre) == 0){
            $curso->estado = 'D';
            $curso->save();
            return redirect('cursos')->with(['success' => 'Se elimino exitosamente']);
        }

        return redirect('cursos')->with(['error' => 'No se puede eliminar curso porque tiene relacion con '. sizeof($cursosemestre).' registros']);
    }

        //ajax
        public function insertarCurso(Request $request)
        {
            $input = $request->all();
            $input['plan_id'] = $request->plan_id;
            $input['cursotipo_id'] = $request->cursotipo_id;
            $input['codigo'] = $request->codigo;
            $input['curso'] = $request->curso;
            $input['descripcion'] = $request->descripcion;
            $input['semestre'] = $request->semestre;
            $input['horasteoria'] = $request->horasteoria;
            $input['horaspractica'] = $request->horaspractica;
            $input['creditos'] = $request->creditos;
            $curso = Curso::create($input);
            return 1;
        }
        public function eliminarCurso(Request $request)
        {
            $id = $request->id;
            $curso = Curso::findOrFail($id);

            $cursosemestre = CursoSemestre::where('curso_id',$id)->where('estado','A')->get();

            if(sizeof($cursosemestre) == 0){
                $curso->estado = 'D';
                $curso->save();
                return 0;
            }

            return sizeof($cursosemestre);
        }
    
        public function editarCurso(Request $request)
        {
            $id = $request->id;
            $curso = Curso::findOrFail($id);
            error_log($curso);
            $curso->cursotipo_id = $request->cursotipo_id;
            $curso->codigo = $request->codigo;
            $curso->curso = $request->curso;
            $curso->descripcion = $request->descripcion;
            $curso->semestre = $request->semestre;
            $curso->horasteoria = $request->horasteoria;
            $curso->horaspractica = $request->horaspractica;
            $curso->creditos = $request->creditos;
            error_log($curso);
            $curso->save();
            return 1;
        }

    // public function action(Request $request){

    //     if($request->ajax()){

    //         if($request->action == 'Edit'){

    //             $data = array(
    //                 'plan_id' => $request-> plan_id,
    //                 'cursotipo_id' => $request-> cursotipo_id,
    //                 'codigo' => $request-> codigo,
    //                 'curso' => $request-> curso,
    //                 'semestre' => $request-> semestre,
    //                 'horasteoria' => $request-> horasteoria,
    //                 'horaspractica' => $request-> horaspractica,
    //                 'creditos' => $request-> creditos,

    //             );

    //             DB::table('Seval')
    //             ->where('id', $request->id)
    //             ->update($data);
    //         }

    //         if($request->action == 'delete')
    // 		{
    // 			DB::table('Seval')
    // 				->where('id', $request->id)
    // 				->delete();
    // 		}
    //         return response()->json($request);
    //     }

    // }
}
