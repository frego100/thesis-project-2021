<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Competencia;
use App\Models\CompetenciasRE;
use App\Models\Plan;

class CompetenciaController extends Controller
{
    //

    public function index()
    {
        $competencias=Competencia::with(['plan'])->where('estado','A')->get();
        //return $escuelas;
        return view('competencia.index',compact('competencias'));
    }

    public function create()
    {
        $planes=Plan::all();
        return view('competencia.create',compact('planes'));
    }
  
    public function store(Request $request)
    {
        $input = $request->all();
        $input['plan_id'] = $request['plan'];
        $competencia = Competencia::create($input);
        return redirect('competencias');
       // return \response()->json(['res' => true, 'message' =>'Guardado correctamente'], 200);
    }


    public function show($id)
    {
        $competencia=Competencia::find($id);
        return view('competencia.show',compact('competencia'));
    }

    
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $input['plan_id'] = $request['plan'];
        $competencia = Competencia::find($id);
        $competencia->update($input);

        //return \response()->json(['res' => true, 'message' =>'actualizado correctamente'], 200);
        return redirect('competencias')->with('message', 'Actualizado!');
    }

    public function edit($id)
    {
        $planes=Plan::all();
        $competencia = Competencia::findOrFail($id);
        return view('competencia.edit', compact('competencia','planes'));
    }

    public function delete($id)
    {
        $competencia = Competencia::find($id);

        $competenciares = CompetenciasRE::where('competencia_id',$id)->where('estado','A')->get();

        if(sizeof($competenciares) == 0){
            $competencia->estado = 'D';
            $competencia->save();
            return redirect('competencias')->with(['success' => 'Se elimino exitosamente']);
        }

        return redirect('competencias')->with(['error' => 'No se puede eliminar competencia porque tiene relacion con '. sizeof($competenciares).' registros']);
    }
    //ajax
    public function insertarCompetencia(Request $request)
    {
        $input = $request->all();
        $input['plan_id'] = $request->plan_id;
        $input['codigo'] = $request->codigo;
        $input['competencia'] = $request->competencia;
        $cursosemestre = Competencia::create($input);
        return 1;
    }
    public function eliminarCompetencia(Request $request)
    {
        $id = $request->id;
        $cursosemestre = Competencia::findOrFail($id);

        $competenciares = CompetenciasRE::where('competencia_id',$id)->where('estado','A')->get();

        if(sizeof($competenciares) == 0){
            $cursosemestre->estado = 'D';
            $cursosemestre->save();
            return 0;
        }

        return sizeof($competenciares);
    }

    public function editarCompetencia(Request $request)
    {
        $id = $request->id;
        $cursosemestre = Competencia::findOrFail($id);
        $cursosemestre->codigo = $request->codigo;
        $cursosemestre->competencia = $request->competencia;
        $cursosemestre->save();
        return 1;
    }
}
