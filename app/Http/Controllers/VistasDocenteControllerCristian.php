<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Imports\UsersImport;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

use App\Models\Curso;
use App\Models\Docente;
use App\Models\Estudiante;
use App\Models\EscuelaSemestre;
use App\Models\CursoSemestre;
use App\Models\CursoGrupo;
use App\Models\CursoEstudiante;
use App\Models\Semestre;
use App\Models\Escuela;

class VistasDocenteControllerCristian extends Controller
{
    public function cargalistaAlumnos(Request $request)
    {

        error_log("INICIO DE MATRIZ");
        $semestres_n=[];
        $escuelasemestres_n=[];
        $cursosemestres_n=[];
        $cursogrupos_n=[];

        $semestres=[];
        $escuelasemestres=[];
        $cursosemestres=[];
        $cursogrupos=[];
        $escuelas=[];
        $cursos = [];
        $grupos = [];

        $cargo;

        $selected_semestre = $request->semestre;
        $selected_curso = $request->curso;
        $selected_grupo = $request->grupo;

        error_log("VALORES SELECCIONADOS");
        error_log($selected_semestre);
        error_log($selected_curso);
        error_log($selected_grupo);
        error_log("FIN VALORES SELECCIONADOS");

        $micursosemestre;

        $idDocente = $request->session()->get('id');
        //error_log($idDocente);
        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('estado','A')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        error_log($cursogrupos_general);

        //Busca los semestres
        foreach($cursogrupos_general as $item)
        {
            array_push($semestres_n, $item->cursosemestre->escuelasemestre->semestre->id);
        }

        array_unique($semestres_n);
        $semestres = Semestre::whereIn('id',$semestres_n)->where('estado','A')->get();
        error_log("SEMESTRES");
        error_log($semestres);
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_semestre))
        {
            $selected_semestre = $semestres[0]->id;
        }
        //Busca las escuela-semestres
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre_id == $selected_semestre)
            {
                array_push($escuelasemestres_n, $item->cursosemestre->escuelasemestre->id);
                error_log("Nuevo dato en EscuelaSemestre_n");
                error_log($item->cursosemestre->escuelasemestre->id);
            }
        }

        array_unique($escuelasemestres_n);
        $escuelasemestres = EscuelaSemestre::whereIn('id',$escuelasemestres_n)->where('estado','A')->get();
        error_log("ESCUELA SEMESTRES");
        error_log($escuelasemestres);

        //Busca los curso-semestres
        foreach($cursogrupos_general as $item)
        {
            foreach($escuelasemestres as $es)
            {
                if($es->id == $item->cursosemestre->escuelasemestre_id)
                {
                    array_push($cursosemestres_n, $item->cursosemestre->id);
                    array_push($cursos, $item->cursosemestre->curso);
                }
            }
        }

        array_unique($cursosemestres_n);
        $cursosemestres = CursoSemestre::whereIn('id',$cursosemestres_n)
            ->where('estado','A')
            ->with(['curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();
        error_log("CURSO SEMESTRES");
        error_log($cursosemestres);

        //IF para evitar fallos con la entrada rapida
        if(empty($selected_curso))
        {
            $selected_curso = $cursosemestres[0]->curso->id;
            error_log("valor por defecto en selected_curso");
        }
        //Busca el curso semestre selecionado
        foreach($cursosemestres as $item)
        {
            if($item->curso_id == $selected_curso)
            {
                error_log("Selecciono curso semestre");
                $micursosemestre = $item;
                break;
            }
        }

        error_log("MI CURSO SEMESTRES");
        error_log($micursosemestre);

        //Busca los curso-grupos
        foreach($cursogrupos_general as $item)
        {
            if($micursosemestre->id == $item->cursosemestre_id)
                array_push($cursogrupos_n, $item->id);
        }

        array_unique($cursogrupos_n);

        //Busca coordinador
        $micurso = CursoGrupo::where('docente_id',$idDocente)
            ->where('cursosemestre_id',$micursosemestre->id)
            ->get();

        error_log("MI CURSO");
        error_log($micurso);
        //IF para evitar error con entrada GET
        if(empty($selected_grupo))
        {
            $selected_grupo = $cursogrupos[0]->id;
        }
        //Busca grupos y docente
        if($micurso[0]->coordinador == 1)
        {
            error_log("ES COODINADOR");
            $cursogrupos = CursoGrupo::where('cursosemestre_id',$micursosemestre->id)
                ->where('estado','A')
                ->get();
            $micursogrupo = CursoGrupo::where('id',$selected_grupo)
                ->where('estado','A')
                ->get();
            error_log("MI CURSO GRUPO SELECCIONADO");
            error_log($micursogrupo);
            $docente = Docente::where('id',$micursogrupo[0]->docente_id)->get();
            $cargo = "Coordinador";
        }
        else
        {
            error_log("NO ES COODINADOR");
            $cursogrupos = $micurso;
            $docente = Docente::where('id',$idDocente)->get();
            $cargo = "Docente";
        }

        error_log("CURSO GRUPOS");
        error_log($cursogrupos);

        error_log("DOCENTE");
        error_log($docente);

        //Busca los alumnos
        $grupoalumnos=CursoEstudiante::where('cursogrupo_id',$selected_grupo)->where('estado','A')
        ->with(['estudiante' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
        ->get();
        error_log($grupoalumnos);

        return view('user.docente.lista_alumnos',compact('semestres','cursos','cursogrupos','grupoalumnos','selected_semestre','selected_curso','selected_grupo','cargo','docente'));

    }

    //Funciones para carga AJAX
    public static function cargaGrupoEstudiantes(Request $request){
        $cursogrupo_id=$request->grupo_id;
        $grupoalumnos=CursoEstudiante::where('cursogrupo_id',$cursogrupo_id)->where('estado','A')
        ->with(['estudiante' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
        ->get();
        return response()->json
        ([
            'grupoalumnos' => $grupoalumnos
        ]);
    }

    public static function cargaListaEstudiantes(Request $request){
        $alumnos=Estudiante::where('estado','A')->get();
        return response()->json
        ([
            'listaAlumnos' => $alumnos
        ]);
    }
    public static function guardaListaEstudiantes(Request $request){
        $alumnos=Estudiante::where('estado','A')->get();
        return response()->json
        ([
            'listaAlumnos' => $alumnos
        ]);
    }
    public static function cargaCursosDocenteEspecifico(Request $request)
    {
        $semestre_id = $request->semestre_id;
        $verificar_coordinador = $request->coordinador;
        $cursos=[];
        $idDocente = $request->session()->get('id');

        if(empty($verificar_coordinador))
        {   
            $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
                ->where('estado','A')
                ->with(['cursosemestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.curso' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->get();
        }
        else
        {
            $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
                ->where('coordinador','1')
                ->where('estado','A')
                ->with(['cursosemestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.curso' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->get();
        }
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre->id == $semestre_id)
            {
                array_push($cursos, $item->cursosemestre->curso);
            }
        }

        return response()->json
        ([
            'cursos' => $cursos
        ]);
    }

    public static function cargaGruposDocenteEspecifico(Request $request)
    {
        error_log("AJAX DE CURSOS");
        $curso_id = $request->curso_id;
        $semestre_id = $request->semestre_id;
        $grupos=[];
        $idDocente = $request->session()->get('id');
        $micurso_n;
        $micursosemestre_n;
        $docente;
        $cargo;

        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('estado','A')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();


        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->curso->id == $curso_id && $item->cursosemestre->escuelasemestre->semestre->id == $semestre_id)
            {
                $micurso_n = $item->id;
                $micursosemestre_n = $item->cursosemestre->id;
                error_log("CURSO ID");
                error_log($micurso_n);
            }
        }

        $micurso = CursoGrupo::where('id',$micurso_n)
            ->get();

        if($micurso[0]->coordinador == 1)
        {
            error_log("ES COODINADOR");
            $grupos = CursoGrupo::where('cursosemestre_id',$micursosemestre_n)
                ->where('estado','A')->get();
            $docente = Docente::where('id',$grupos[0]->docente_id)->get();
            $cargo = "Coordinador";
        }
        else
        {
            error_log("NO ES COODINADOR");
            $grupos = $micurso;
            $docente = Docente::where('id',$idDocente)->get();
            $cargo = "Docente";
        }
        error_log("MI DOCENTE");
        error_log($docente);
        return response()->json
        ([
            'grupos' => $grupos,
            'docente' => $docente,
            'cargo' => $cargo
        ]);
    }

    public static function cargaDocenteEspecifico(Request $request)
    {
        error_log("CARGA DE DOCENTE");
        $grupo_id = $request->grupo_id;

        $micurso = CursoGrupo::where('id',$grupo_id)
            ->with(['docente' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();
        error_log("MI CURSO");
        error_log($micurso);

        $docente = $micurso[0]->docente;

        error_log("MI DOCENTE");
        error_log($docente);

        return response()->json
        ([
            'docente' => $docente
        ]);
    }
    //excel
    public function export() 
    {
        return Excel::download(new UsersExport, 'Registros de Alumnos.xlsx');
    }
    public function import() 
    {
        Excel::import(new UsersImport, 'users.xlsx');
        
        return redirect('/')->with('success', 'All good!');
    }
}