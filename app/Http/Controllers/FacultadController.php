<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Facultad;
use App\Models\Escuela;

class FacultadController extends Controller
{

    public function index()
    {   
        $facultades=Facultad::where('estado','A')->get();
        return view("facultad.index",compact('facultades'));
    }

    public function create()
    {
        return view('facultad.create');
    }
  
    public function store(Request $request)
    {
        $input = $request->all();
        $facultad = Facultad::create($input);
        return redirect('facultades')->with('message', 'Guardado Exitosamente');
    }

    public function show($id)
    {
        $facultad=Facultad::find($id);
        return view('facultad.show', compact('facultad'));
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $facultad = Facultad::find($id);
        $facultad->update($input);
        return redirect('facultades')->with('message', 'Actualizado!');
    }


    public function edit($id)
    {
        $facultad = Facultad::findOrFail($id);
        return view('facultad.edit', compact('facultad'));
    }

    public function delete($id)
    {
        $facultad = Facultad::find($id);

        $escuela = Escuela::where('facultad_id',$id)->where('estado','A')->get();

        if(sizeof($escuela) == 0){
            $facultad->estado = 'D';
            $facultad->save();
            return redirect('facultades')->with(['success' => 'Se elimino exitosamente']);
        }

        return redirect('facultades')->with(['error' => 'No se puede eliminar facultad porque tiene relacion con '. sizeof($escuela).' registros']);
    }
}
