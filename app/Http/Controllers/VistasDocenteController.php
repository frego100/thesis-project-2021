<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;

use App\Models\Docente;
use App\Models\CursoGrupo;
use App\Models\EscuelaSemestre;
use App\Models\CursoSemestre;
use App\Models\CursoEsquema;
use App\Models\CursoMatriz;
use App\Models\MatrizDetalle;
use App\Models\Semestre;
use App\Models\Escuela;
use App\Models\Estudiante;
use App\Models\EstudianteMatriz;
use App\Models\Criterio;
use App\Models\DatoMatriz;
use App\Models\ResultadosEstudiante;
use App\Models\Curso;
use App\Models\CursoEstudiante;

use App\Exports\ObjetoReporte;
use App\Exports\EsquemaExport;
use App\Exports\AlumnoExport;
use App\Exports\ListaAlumnosPorMatrizExport;
use App\Exports\ListaAlumnosExport;
use App\Imports\EsquemaImport;
use App\Imports\AlumnoImport;
use Maatwebsite\Excel\Facades\Excel;

use PDF;

class VistasDocenteController extends Controller
{
    public function cargaIndexDocentes(Request $request)
    {
        $semestres_n=[];
        $escuelasemestres_n=[];
        $cursosemestres_n=[];
        $cursogrupos_n=[];

        $semestres=[];
        $escuelasemestres=[];
        $cursosemestres=[];
        $cursogrupos=[];
        $escuelas=[];
        $cursos = [];
        $grupos = [];

        $cargo;

        $idDocente = $request->session()->get('id');
        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('estado','A')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        //Busca los semestres
        foreach($cursogrupos_general as $item)
        {
            array_push($semestres_n, $item->cursosemestre->escuelasemestre->semestre->id);
        }

        array_unique($semestres_n);
        $semestres = Semestre::whereIn('id',$semestres_n)->where('estado','A')->get();

        //Busca las escuela-semestres
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre_id == $semestres[0]->id)
            {
                array_push($escuelasemestres_n, $item->cursosemestre->escuelasemestre->id);
            }
        }

        array_unique($escuelasemestres_n);
        $escuelasemestres = EscuelaSemestre::whereIn('id',$escuelasemestres_n)->where('estado','A')->get();

        //Busca los curso-semestres
        foreach($cursogrupos_general as $item)
        {
            foreach($escuelasemestres as $es)
            {
                if($es->id == $item->cursosemestre->escuelasemestre_id)
                {
                    array_push($cursosemestres_n, $item->cursosemestre->id);
                    array_push($cursos, $item->cursosemestre->curso);
                }
            }
        }

        array_unique($cursosemestres_n);
        $cursosemestres = CursoSemestre::whereIn('id',$cursosemestres_n)
            ->where('estado','A')
            ->with(['curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        if(count($cursosemestres) == 0)
        {
        	return view('user.docente.error');
        }
        
        //Busca los curso-grupos
        foreach($cursogrupos_general as $item)
        {
            if($cursosemestres[0]->id == $item->cursosemestre_id)
                array_push($cursogrupos_n, $item->id);
        }

        array_unique($cursogrupos_n);

        //Busca coordinador
        $micurso = CursoGrupo::where('docente_id',$idDocente)
            ->where('cursosemestre_id',$cursosemestres[0]->id)
            ->get();

        //Busca grupos y docente
        if($micurso[0]->coordinador == 1)
        {
            $cursogrupos = CursoGrupo::where('cursosemestre_id',$cursosemestres[0]->id)
                ->where('estado','A')->get();
            $docente = Docente::where('id',$cursogrupos[0]->docente_id)->get();
            $cargo = "Coordinador";
        }
        else
        {
            $cursogrupos = $micurso;
            $docente = Docente::where('id',$idDocente)->get();
            $cargo = "Docente";
        }

        return view('user.docente.index',compact('semestres','cursos','cursogrupos','cargo'));
    }

    public function cargaVistaMatriz(Request $request)
    {
        $semestres_n=[];
        $escuelasemestres_n=[];
        $cursosemestres_n=[];
        $cursogrupos_n=[];

        $semestres=[];
        $escuelasemestres=[];
        $cursosemestres=[];
        $cursogrupos=[];
        $escuelas=[];
        $cursos = [];
        $grupos = [];
        $matrizdetalles = [];
        $cursoestudiantes = [];

        $cargo;

        $selected_semestre = $request->semestre;
        $selected_curso = $request->curso;
        $selected_grupo = $request->grupo;

        $micursosemestre;

        $idDocente = $request->session()->get('id');
        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('estado','A')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();


        //Busca los semestres
        foreach($cursogrupos_general as $item)
        {
            array_push($semestres_n, $item->cursosemestre->escuelasemestre->semestre->id);
        }

        array_unique($semestres_n);
        $semestres = Semestre::whereIn('id',$semestres_n)->where('estado','A')->get();

        //IF para evitar fallos con la entrada rapida
        if(empty($selected_semestre))
        {
            $selected_semestre = $semestres[0]->id;
        }
        //Busca las escuela-semestres
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre_id == $selected_semestre)
            {
                array_push($escuelasemestres_n, $item->cursosemestre->escuelasemestre->id);
            }
        }

        array_unique($escuelasemestres_n);
        $escuelasemestres = EscuelaSemestre::whereIn('id',$escuelasemestres_n)->where('estado','A')->get();

        //Busca los curso-semestres
        foreach($cursogrupos_general as $item)
        {
            foreach($escuelasemestres as $es)
            {
                if($es->id == $item->cursosemestre->escuelasemestre_id)
                {
                    array_push($cursosemestres_n, $item->cursosemestre->id);
                    array_push($cursos, $item->cursosemestre->curso);
                }
            }
        }

        array_unique($cursosemestres_n);
        $cursosemestres = CursoSemestre::whereIn('id',$cursosemestres_n)
            ->where('estado','A')
            ->with(['curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        //IF para evitar fallos con la entrada rapida
        if(empty($selected_curso))
        {
            $selected_curso = $cursosemestres[0]->curso->id;
        }
        //Busca el curso semestre selecionado
        foreach($cursosemestres as $item)
        {
            if($item->curso_id == $selected_curso)
            {
                $micursosemestre = $item;
                break;
            }
        }

        //Busca los curso-grupos
        foreach($cursogrupos_general as $item)
        {
            if($micursosemestre->id == $item->cursosemestre_id)
                array_push($cursogrupos_n, $item->id);
        }

        array_unique($cursogrupos_n);

        //Busca coordinador
        $micurso = CursoGrupo::where('docente_id',$idDocente)
            ->where('cursosemestre_id',$micursosemestre->id)
            ->get();

        //IF para evitar error con entrada GET
        if(empty($selected_grupo))
        {
            $selected_grupo = $cursogrupos[0]->id;
        }
        //Busca grupos y docente
        if($micurso[0]->coordinador == 1)
        {
            $cursogrupos = CursoGrupo::where('cursosemestre_id',$micursosemestre->id)
                ->where('estado','A')
                ->get();
            $micursogrupo = CursoGrupo::where('id',$selected_grupo)
                ->where('estado','A')
                ->get();
            $docente = Docente::where('id',$micursogrupo[0]->docente_id)->get();
            $cargo = "Coordinador";
        }
        else
        {
            $cursogrupos = $micurso;
            $docente = Docente::where('id',$idDocente)->get();
            $cargo = "Docente";
        }

        //Busca las matrices relacionadas al curso
        $cursomatrizs = CursoMatriz::where('cursosemestre_id',$micursosemestre->id)
            ->where('estado','A')
            ->get();

        $cursoestudiantes = CursoEstudiante::where('cursogrupo_id',$selected_grupo)
            ->where('estado','A')
            ->with(['estudiante' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        foreach ($cursoestudiantes as $ce)
        {
            $promedios = [];
            $promedio_final = 0;
            foreach ($cursomatrizs as $cm)
            {
                $promedio_actual = $this->promedioMatrizEstudiante($cm->id, $ce->id);
                array_push($promedios, $promedio_actual);
                $promedio_final = $promedio_final + ($promedio_actual*$cm->ponderacion/100);
            }
            array_push($promedios, $promedio_final);
            $ce->promedios = $promedios;
        }

        $escuelasemestres = EscuelaSemestre::where('semestre_id',$selected_semestre)->get();
        $cursosemestres = CursoSemestre::where('curso_id',$selected_curso)->get();

        foreach($escuelasemestres as $es){
            foreach($cursosemestres as $cs){
                if($es->id == $cs->escuelasemestre_id)
                    $escuelasemestre = $es;
            }
        }

        return view('user.docente.matriz',compact('semestres','cursos','cursogrupos','docente','selected_semestre','selected_curso','selected_grupo','cargo','cursomatrizs','cursoestudiantes','escuelasemestre'));
    }

    public function cargaEsquemaTrabajo(Request $request)
    {
        $semestres_n=[];
        $escuelasemestres_n=[];
        $cursosemestres_n=[];
        $cursogrupos_n=[];

        $semestres=[];
        $escuelasemestres=[];
        $cursosemestres=[];
        $cursogrupos=[];
        $escuelas=[];
        $cursos = [];
        $esquemas = [];

        $selected_semestre = $request->semestre;
        $selected_curso = $request->curso;

        $idDocente = $request->session()->get('id');
        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('coordinador','1')
            ->where('estado','A')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        //Busca los semestres
        foreach($cursogrupos_general as $item)
        {
            array_push($semestres_n, $item->cursosemestre->escuelasemestre->semestre->id);
        }

        array_unique($semestres_n);
        $semestres = Semestre::whereIn('id',$semestres_n)->where('estado','A')->get();

        //IF para evitar fallos con la entrada rapida
        if(empty($selected_semestre))
        {
            $selected_semestre = $semestres[0]->id;
        }
        //Busca las escuela-semestres
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre_id == $selected_semestre)
            {
                array_push($escuelasemestres_n, $item->cursosemestre->escuelasemestre->id);
            }
        }

        array_unique($escuelasemestres_n);
        $escuelasemestres = EscuelaSemestre::whereIn('id',$escuelasemestres_n)->where('estado','A')->get();

        //Busca los curso-semestres
        foreach($cursogrupos_general as $item)
        {
            foreach($escuelasemestres as $es)
            {
                if($es->id == $item->cursosemestre->escuelasemestre_id)
                {
                    array_push($cursosemestres_n, $item->cursosemestre->id);
                    array_push($cursos, $item->cursosemestre->curso);
                    if($item->cursosemestre->curso->id == $selected_curso)
                    {
                        $cursosemestres = $item->cursosemestre;
                    }
                }
            }
        }

        array_unique($cursosemestres_n);

        //Busca los curso-esquemas
        $esquemas = CursoEsquema::where('cursosemestre_id',$cursosemestres->id)
            ->where('estado','A')
            ->orderBy('codigo')
            ->get();

        return view('user.docente.esquema',compact('semestres','cursos','esquemas','selected_semestre','selected_curso','cursosemestres'));

    }

    public function cargaMatrizCabecera(Request $request)
    {
        $semestres_n=[];
        $escuelasemestres_n=[];
        $cursosemestres_n=[];
        $cursogrupos_n=[];

        $semestres=[];
        $escuelasemestres=[];
        $cursosemestres=[];
        $cursogrupos=[];
        $escuelas=[];
        $cursos = [];
        $esquemas = [];

        $selected_semestre = $request->semestre;
        $selected_curso = $request->curso;

        $idDocente = $request->session()->get('id');
        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('coordinador','1')
            ->where('estado','A')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        //Busca los semestres
        foreach($cursogrupos_general as $item)
        {
            array_push($semestres_n, $item->cursosemestre->escuelasemestre->semestre->id);
        }

        array_unique($semestres_n);
        $semestres = Semestre::whereIn('id',$semestres_n)->where('estado','A')->get();

        //IF para evitar fallos con la entrada rapida
        if(empty($selected_semestre))
        {
            $selected_semestre = $semestres[0]->id;
        }
        //Busca las escuela-semestres
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre_id == $selected_semestre)
            {
                array_push($escuelasemestres_n, $item->cursosemestre->escuelasemestre->id);
            }
        }

        array_unique($escuelasemestres_n);
        $escuelasemestres = EscuelaSemestre::whereIn('id',$escuelasemestres_n)->where('estado','A')->get();

        //Busca los curso-semestres
        foreach($cursogrupos_general as $item)
        {
            foreach($escuelasemestres as $es)
            {
                if($es->id == $item->cursosemestre->escuelasemestre_id)
                {
                    array_push($cursosemestres_n, $item->cursosemestre->id);
                    array_push($cursos, $item->cursosemestre->curso);
                    if($item->cursosemestre->curso->id == $selected_curso)
                    {
                        $cursosemestres = $item->cursosemestre;
                    }
                }
            }
        }

        array_unique($cursosemestres_n);

        //Busca los curso-esquemas
        $cursomatrizs = CursoMatriz::where('cursosemestre_id',$cursosemestres->id)
            ->where('estado','A')
            ->get();

        $contador = 1;
        foreach($cursomatrizs as $item)
        {
            $item->minumero = $contador;
            $contador = $contador + 1;
        }

        return view('user.docente.matrizCabecera',compact('semestres','cursos','cursomatrizs','selected_semestre','selected_curso','cursosemestres'));

    }

    public function cargaNotasAlumno(Request $request)
    {
        $semestres_n=[];
        $escuelasemestres_n=[];
        $cursosemestres_n=[];
        $cursogrupos_n=[];
        $cursomatrizs_n=[];
        $cursoesquemas_n=[];

        $semestres=[];
        $escuelasemestres=[];
        $cursosemestres=[];
        $cursogrupos=[];
        $escuelas=[];
        $cursos = [];
        $grupos = [];
        $matrizdetalles = [];
        $cursoestudiantes = [];

        $cargo;

        $promedio_final=0;
        $promedios = [];

        $selected_semestre = $request->semestre;
        $selected_curso = $request->curso;
        $selected_grupo = $request->grupo;
        $selected_alumno = $request->AlumnoID;
        $selected_matriz = $request->matriz;

        $micursosemestre;

        $idDocente = $request->session()->get('id');
        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('estado','A')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        //Busca los semestres
        foreach($cursogrupos_general as $item)
        {
            array_push($semestres_n, $item->cursosemestre->escuelasemestre->semestre->id);
        }

        array_unique($semestres_n);
        $semestres = Semestre::whereIn('id',$semestres_n)->where('estado','A')->get();

        //IF para evitar fallos con la entrada rapida
        if(empty($selected_semestre))
        {
            $selected_semestre = $semestres[0]->id;
        }
        //Busca las escuela-semestres
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre_id == $selected_semestre)
            {
                array_push($escuelasemestres_n, $item->cursosemestre->escuelasemestre->id);
            }
        }

        array_unique($escuelasemestres_n);
        $escuelasemestres = EscuelaSemestre::whereIn('id',$escuelasemestres_n)->where('estado','A')->get();

        //Busca los curso-semestres
        foreach($cursogrupos_general as $item)
        {
            foreach($escuelasemestres as $es)
            {
                if($es->id == $item->cursosemestre->escuelasemestre_id)
                {
                    array_push($cursosemestres_n, $item->cursosemestre->id);
                    array_push($cursos, $item->cursosemestre->curso);
                }
            }
        }

        array_unique($cursosemestres_n);
        $cursosemestres = CursoSemestre::whereIn('id',$cursosemestres_n)
            ->where('estado','A')
            ->with(['curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        //IF para evitar fallos con la entrada rapida
        if(empty($selected_curso))
        {
            $selected_curso = $cursosemestres[0]->curso->id;
        }
        //Busca el curso semestre selecionado
        foreach($cursosemestres as $item)
        {
            if($item->curso_id == $selected_curso)
            {
                $micursosemestre = $item;
                break;
            }
        }

        //Busca los curso-grupos
        foreach($cursogrupos_general as $item)
        {
            if($micursosemestre->id == $item->cursosemestre_id)
                array_push($cursogrupos_n, $item->id);
        }

        array_unique($cursogrupos_n);

        //Busca coordinador
        $micurso = CursoGrupo::where('docente_id',$idDocente)
            ->where('cursosemestre_id',$micursosemestre->id)
            ->get();

        //Busca grupos y docente
        if($micurso[0]->coordinador == 1)
        {
            $cursogrupos = CursoGrupo::where('cursosemestre_id',$micursosemestre->id)
                ->where('estado','A')
                ->get();
            //IF para evitar error con entrada GET
            if(empty($selected_grupo))
            {
                $selected_grupo = $cursogrupos[0]->id;
            }

            $micursogrupo = CursoGrupo::where('id',$selected_grupo)
                ->where('estado','A')
                ->get();
            $docente = Docente::where('id',$micursogrupo[0]->docente_id)->get();
            $cargo = "Coordinador";
        }
        else
        {
            $cursogrupos = $micurso;
            $docente = Docente::where('id',$idDocente)->get();
            $cargo = "Docente";
        }

        //IF para evitar error con entrada GET
        if(empty($selected_grupo))
        {
            $selected_grupo = $cursogrupos[0]->id;
        }
        //Buscar alumnos del curso seleccionado previamente
        $cursoestudiantes = CursoEstudiante::where('cursogrupo_id',$selected_grupo)
            ->where('estado','A')
            ->with(['estudiante' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        //Busca las matrices relacionadas al curso
        $cursomatrizs = CursoMatriz::where('cursosemestre_id',$micursosemestre->id)
            ->where('estado','A')
            ->get();

        //Busca los esquemas relacionadas al curso
        $cursoesquemas = CursoEsquema::where('cursosemestre_id',$micursosemestre->id)
            ->where('estado','A')
            ->orderBy('codigo')
            ->get();

        //IF para evitar error con entrada GET
        if(empty($selected_alumno))
        {
            $selected_alumno = $cursoestudiantes[0]->id;
        }

        //carga id's de las matrices
        foreach ($cursomatrizs as $item) 
        {
            array_push($cursomatrizs_n, $item->id);
            $promedios[$item->id] = $this->promedioMatrizEstudiante($item->id, $selected_alumno);
            $promedio_final = $promedio_final + $promedios[$item->id]*$item->ponderacion/100;
        }

        //carga id's de los esquemas
        foreach ($cursoesquemas as $item) 
        {
            $mis_notas = [];
            array_push($cursoesquemas_n, $item->id);
            $matrizdetalles = MatrizDetalle::where('cursoesquema_id',$item->id)
                ->where('estado','A')
                ->orderBy('cursomatriz_id')
                ->get();
            foreach ($matrizdetalles as $md)
            {
                $nota = EstudianteMatriz::where('matriz_detalle_id',$md->id)
                    ->where('estado','A')
                    ->where('curso_estudiante_id',$selected_alumno)
                    ->get();
                $mis_notas[$md->cursomatriz_id] = $nota;

            }
            $item->notas = $mis_notas;
        }

        $datomatrizs = DatoMatriz::where('curso_estudiante_id', $selected_alumno)
            ->where('curso_matriz_id', $cursomatrizs[0]->id)
            ->where('estado','A')
            ->get();

        return view('user.docente.docente_notas',compact('semestres','cursos','cursogrupos','docente','selected_semestre','selected_curso','selected_grupo','selected_alumno','selected_matriz','cargo','cursoesquemas','cursomatrizs','cursoestudiantes','matrizdetalles','cursomatrizs_n','cursoesquemas_n','promedios','promedio_final','datomatrizs'));
    }

    public function crearMatriz(Request $request)
    {
        $selected_semestre=$request->semestre;
        $selected_curso=$request->curso;
        $selected_matriz=$request->matriz;
        $semestres_n=[];
        $escuelasemestres_n=[];
        $cursosemestres_n=[];
        $cursogrupos_n=[];

        $semestres=[];
        $escuelasemestres=[];
        $cursosemestres=[];
        $cursogrupos=[];
        $escuelas=[];
        $cursos = [];
        $grupos = [];
        $matrizs=[];
        $matrizdetalle_general=[];
        $criterios=[];
        $cursoesquemas_n=[];
        $cursoesquemas=[];
        $cursoesquemas_seleccionados=[];
        $cursoesquemas_seleccionados_n=[];

        $suma_ponderacion = 0;

        $cargo;

        $idDocente = $request->session()->get('id');
        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('estado','A')->where('coordinador','1')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();


        //Busca los semestres
        foreach($cursogrupos_general as $item)
        {
            array_push($semestres_n, $item->cursosemestre->escuelasemestre->semestre->id);
        }

        array_unique($semestres_n);
        $semestres = Semestre::whereIn('id',$semestres_n)->where('estado','A')->get();

        if(empty($selected_semestre))
        {
            $selected_semestre = $semestres[0]->id;
        }

        //Busca las escuela-semestres
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre_id == $selected_semestre)
            {
                array_push($escuelasemestres_n, $item->cursosemestre->escuelasemestre->id);
            }
        }

        array_unique($escuelasemestres_n);
        $escuelasemestres = EscuelaSemestre::whereIn('id',$escuelasemestres_n)->where('estado','A')->get();

        //Busca los curso-semestres
        foreach($cursogrupos_general as $item)
        {
            foreach($escuelasemestres as $es)
            {
                if($es->id == $item->cursosemestre->escuelasemestre_id)
                {
                    array_push($cursosemestres_n, $item->cursosemestre->id);
                    array_push($cursos, $item->cursosemestre->curso);
                }
            }
        }

        array_unique($cursosemestres_n);
        $cursosemestres = CursoSemestre::whereIn('id',$cursosemestres_n)
            ->where('estado','A')
            ->with(['curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        if(empty($selected_curso))
        {
            $selected_curso = $cursosemestres[0]->curso->id;
        }
        //Busca el curso semestre selecionado
        foreach($cursosemestres as $item)
        {
            if($item->curso_id == $selected_curso)
            {
                $micursosemestre = $item;
                break;
            }
        }

        $matrizs=CursoMatriz::where('estado','A')->where('cursosemestre_id',$micursosemestre->id)->get();

        if(empty($selected_matriz) && sizeof($matrizs)>0)
        {
            $selected_matriz = $matrizs[0]->id;
        }

        $matrizdetalle_general=MatrizDetalle::where('estado','A')->where('cursomatriz_id',$selected_matriz)
            ->with(['cursoesquema' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        //Busca los criterios
        $curso = Curso::where('id',$selected_curso)->firstOrFail();
        $resultados = ResultadosEstudiante::where('plan_id',$curso->plan_id)->get();

        foreach($resultados as $res){
            $tempcrit = Criterio::where('resultadoestudiante_id',$res->id)->get();
            foreach($tempcrit as $crit){
                array_push($criterios,$crit);
            }
        }

        $cursoesquemas=CursoEsquema::where('cursosemestre_id',$micursosemestre->id)
            ->orderBy('codigo')
            ->get();

        foreach($cursoesquemas as $esquema){
            $temp=$matrizdetalle_general->where('cursoesquema.id',$esquema->id)->first();
            if(!empty($temp)){
                $esquema->ponderacion=$temp->ponderacion;
                $esquema->matrizdetalle_id=$temp->id;
                $suma_ponderacion = $suma_ponderacion + $temp->ponderacion;
            }else{
                $esquema->ponderacion='SIN PONDERACION';
                $esquema->matrizdetalle_id=0;
            }
        }

        return view('user.docente.crearMatriz',compact('semestres','cursos','criterios','matrizs','matrizdetalle_general','cursoesquemas','selected_semestre','selected_curso','selected_matriz','suma_ponderacion'));
    }

    public function cargalistaAlumnos(Request $request)
    {
        $semestres_n=[];
        $escuelasemestres_n=[];
        $cursosemestres_n=[];
        $cursogrupos_n=[];

        $semestres=[];
        $escuelasemestres=[];
        $cursosemestres=[];
        $cursogrupos=[];
        $escuelas=[];
        $cursos = [];
        $grupos = [];

        $cargo;

        $selected_semestre = $request->semestre;
        $selected_curso = $request->curso;
        $selected_grupo = $request->grupo;

        $micursosemestre;

        $idDocente = $request->session()->get('id');
        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('estado','A')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        //Busca los semestres
        foreach($cursogrupos_general as $item)
        {
            array_push($semestres_n, $item->cursosemestre->escuelasemestre->semestre->id);
        }

        array_unique($semestres_n);
        $semestres = Semestre::whereIn('id',$semestres_n)->where('estado','A')->get();
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_semestre))
        {
            $selected_semestre = $semestres[0]->id;
        }
        //Busca las escuela-semestres
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre_id == $selected_semestre)
            {
                array_push($escuelasemestres_n, $item->cursosemestre->escuelasemestre->id);
            }
        }

        array_unique($escuelasemestres_n);
        $escuelasemestres = EscuelaSemestre::whereIn('id',$escuelasemestres_n)->where('estado','A')->get();

        //Busca los curso-semestres
        foreach($cursogrupos_general as $item)
        {
            foreach($escuelasemestres as $es)
            {
                if($es->id == $item->cursosemestre->escuelasemestre_id)
                {
                    array_push($cursosemestres_n, $item->cursosemestre->id);
                    array_push($cursos, $item->cursosemestre->curso);
                }
            }
        }

        array_unique($cursosemestres_n);
        $cursosemestres = CursoSemestre::whereIn('id',$cursosemestres_n)
            ->where('estado','A')
            ->with(['curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        //IF para evitar fallos con la entrada rapida
        if(empty($selected_curso))
        {
            $selected_curso = $cursosemestres[0]->curso->id;
        }
        //Busca el curso semestre selecionado
        foreach($cursosemestres as $item)
        {
            if($item->curso_id == $selected_curso)
            {
                $micursosemestre = $item;
                break;
            }
        }

        //Busca los curso-grupos
        foreach($cursogrupos_general as $item)
        {
            if($micursosemestre->id == $item->cursosemestre_id)
                array_push($cursogrupos_n, $item->id);
        }

        array_unique($cursogrupos_n);

        //Busca coordinador
        $micurso = CursoGrupo::where('docente_id',$idDocente)
            ->where('cursosemestre_id',$micursosemestre->id)
            ->get();

        //IF para evitar error con entrada GET
        if(empty($selected_grupo))
        {
            $selected_grupo = $cursogrupos[0]->id;
        }
        //Busca grupos y docente
        if($micurso[0]->coordinador == 1)
        {
            $cursogrupos = CursoGrupo::where('cursosemestre_id',$micursosemestre->id)
                ->where('estado','A')
                ->get();
            $micursogrupo = CursoGrupo::where('id',$selected_grupo)
                ->where('estado','A')
                ->get();
            $docente = Docente::where('id',$micursogrupo[0]->docente_id)->get();
            $cargo = "Coordinador";
        }
        else
        {
            $cursogrupos = $micurso;
            $docente = Docente::where('id',$idDocente)->get();
            $cargo = "Docente";
        }

        //Busca los alumnos
        $grupoalumnos=CursoEstudiante::where('cursogrupo_id',$selected_grupo)->where('estado','A')
        ->with(['estudiante' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
        ->get();

        return view('user.docente.lista_alumnos',compact('semestres','cursos','cursogrupos','grupoalumnos','selected_semestre','selected_curso','selected_grupo','cargo','docente'));

    }

    //Funciones para carga AJAX
    public static function cargaGrupoEstudiantes(Request $request){
        $cursogrupo_id=$request->grupo_id;
        $grupoalumnos=CursoEstudiante::where('cursogrupo_id',$cursogrupo_id)->where('estado','A')
        ->with(['estudiante' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
        ->get();
        return response()->json
        ([
            'grupoalumnos' => $grupoalumnos
        ]);
    }

    public static function cargaListaEstudiantes(Request $request){
        $alumnos=Estudiante::where('estado','A')->get();
        return response()->json
        ([
            'listaAlumnos' => $alumnos
        ]);
    }
    public static function guardaListaEstudiantes(Request $request){
        $alumnos=Estudiante::where('estado','A')->get();
        return response()->json
        ([
            'listaAlumnos' => $alumnos
        ]);
    }
    public static function cargaCursosDocenteEspecificoC(Request $request)
    {
        $semestre_id = $request->semestre_id;
        $verificar_coordinador = $request->coordinador;
        $cursos=[];
        $idDocente = $request->session()->get('id');

        if(empty($verificar_coordinador))
        {   
            $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
                ->where('estado','A')
                ->with(['cursosemestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.curso' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->get();
        }
        else
        {
            $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
                ->where('coordinador','1')
                ->where('estado','A')
                ->with(['cursosemestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.curso' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->get();
        }
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre->id == $semestre_id)
            {
                array_push($cursos, $item->cursosemestre->curso);
            }
        }

        return response()->json
        ([
            'cursos' => $cursos
        ]);
    }

    public static function cargaDocenteEspecifico(Request $request)
    {
        $grupo_id = $request->grupo_id;

        $micurso = CursoGrupo::where('id',$grupo_id)
            ->with(['docente' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        $docente = $micurso[0]->docente;

        return response()->json
        ([
            'docente' => $docente
        ]);
    }

    public static function cargaCursosDocenteEspecificoW(Request $request)
    {
        $semestre_id = $request->semestre_id;
        $verificar_coordinador = $request->coordinador;
        $cursos=[];
        $idDocente = $request->session()->get('id');

        if(empty($verificar_coordinador))
        {   
            $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
                ->where('estado','A')->where('coordinador','1')
                ->with(['cursosemestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.curso' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->get();
        }
        else
        {
            $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
                ->where('coordinador','1')
                ->where('estado','A')
                ->with(['cursosemestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.curso' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->get();
        }
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre->id == $semestre_id)
            {
                array_push($cursos, $item->cursosemestre->curso);
            }
        }

        return response()->json
        ([
            'cursos' => $cursos
        ]);
    }

    //ajax
    public function insertarMatriz(Request $request)
    {
        $semestre_id=$request->semestre_id;
        $curso_id=$request->curso_id;

        $escuelasemestre=EscuelaSemestre::where('semestre_id',$semestre_id)->get();

        foreach($escuelasemestre as $item)
        {
            $cursosemestre=CursoSemestre::where('curso_id',$curso_id)->where('escuelasemestre_id',$item->id)->get();
            if(!$cursosemestre->isEmpty())
                break;
        }

        $input = $request->all();
        $input['matriz'] = $request->matriz;
        $input['ponderacion'] = $request->ponderacion;
        $input['cursosemestre_id'] = $cursosemestre[0]->id;
        $input['fechacalificacioninicio'] = $request->fechacalificacioninicio;
        $input['fechacalificacionfin'] = $request->fechacalificacionfin;

        $curso = CursoMatriz::create($input);
        return 1;
    }

    public static function cargaCursoSemestre(Request $request)
    {
        $cursoSemestre=CursoSemestre::where('Estado','A')->get();
        return response()->json
        ([
            'cursoSemestre' => $cursoSemestre
        ]);
    }

    public static function cargaMatriz(Request $request)
    {
        $curso_id = $request->curso_id;
        $semestre_id = $request->semestre_id;

        $matrizs=[];

        $escuelasemestre=EscuelaSemestre::where('semestre_id',$semestre_id)->get();

        foreach($escuelasemestre as $item)
        {
            $cursosemestre=CursoSemestre::where('curso_id',$curso_id)->where('escuelasemestre_id',$item->id)->get();
            if(!$cursosemestre->isEmpty())
                break;
        }

        if(sizeof($cursosemestre)>0){
            $matrizs=CursoMatriz::where('estado','A')->where('cursosemestre_id',$cursosemestre[0]->id)->get();
        }

        return response()->json
        ([
            'matrizs' => $matrizs
        ]);
        
    }

    public static function cargaSemestre(Request $request)
    {
        $semestres_n=[];
        $escuelasemestres_n=[];
        $escuelasemestres_n2=[];
        $cursosemestres_n=[];
        $cursogrupos_n=[];

        $semestres=[];
        $escuelasemestres=[];
        $cursosemestres=[];
        $cursogrupos=[];
        $escuelas=[];
        $cursos = [];
        $grupos = [];
        $matrizs=[];

        $cargo;
        
        $idDocente = $request->session()->get('id');
        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('estado','A')->where('coordinador','1')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        //Busca los semestres
        foreach($cursogrupos_general as $item)
        {
            array_push($semestres_n, $item->cursosemestre->escuelasemestre->semestre->id);
        }

        array_unique($semestres_n);
        $semestres = Semestre::whereIn('id',$semestres_n)->where('estado','A')->get();
        return response()->json
        ([
            'semestres' => $semestres
        ]);
    }

    public static function cargaCurso(Request $request)
    {
        $semestre_id=$request->semestre_id;
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre_id == $semestre_id)
            {
                array_push($escuelasemestres_n, $item->cursosemestre->escuelasemestre->id);
            }
        }

        array_unique($escuelasemestres_n);
        $escuelasemestres = EscuelaSemestre::whereIn('id',$escuelasemestres_n)->where('estado','A')->get();

        //Busca los curso-semestres
        foreach($cursogrupos_general as $item)
        {
            foreach($escuelasemestres as $es)
            {
                if($es->id == $item->cursosemestre->escuelasemestre_id)
                {
                    array_push($cursosemestres_n, $item->cursosemestre->id);
                    array_push($cursos, $item->cursosemestre->curso);
                }
            }
        }
        return response()->json
        ([
            'cursos' => $cursos
        ]);
    }

    public static function cargaResultado(Request $request)
    {

        // $criterios = Criterio::where('estado','A')->get();
        $matriz_id = $request->matriz_id;
        $curso_id = $request->curso_id;
        $criterios = [];

        $curso = Curso::where('id',$curso_id)->where('estado','A')->firstOrFail();
        $resultados = ResultadosEstudiante::where('plan_id',$curso->plan_id)->where('estado','A')->get();

        foreach($resultados as $res){
            $tempcrit = Criterio::where('resultadoestudiante_id',$res->id)->where('estado','A')->get();
            foreach($tempcrit as $crit){
                array_push($criterios,$crit);
            }
        }

        $criterios_n=[];

        $matrizdetalle_general=MatrizDetalle::where('estado','A')->where('cursomatriz_id',$matriz_id)
            ->with(['criterio' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['criterio.resultadoestudiante' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        return response()->json
        ([
            'criterios' => $criterios,
            'matrizdetalle_general' => $matrizdetalle_general
        ]);
        
    }

    public static function cargaEsquema(Request $request)
    {
        $matriz_id = $request->matriz_id;

        $semestre_id=$request->semestre_id;
        $curso_id=$request->curso_id;

        $escuelasemestre=EscuelaSemestre::where('semestre_id',$semestre_id)->where('estado','A')->get();

        foreach($escuelasemestre as $item)
        {
            $cursosemestre=CursoSemestre::where('curso_id',$curso_id)->where('escuelasemestre_id',$item->id)->where('estado','A')->get();
            if(!$cursosemestre->isEmpty())
                break;
        }

        $cursosemestres = CursoSemestre::where('id',$cursosemestre[0]->id)
            ->where('estado','A')
            ->with(['curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        $esquemas_n=[];

        $matrizdetalle_general=MatrizDetalle::where('estado','A')->where('cursomatriz_id',$matriz_id)
            ->with(['cursoesquema' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        $criterios = [];

        $curso = Curso::where('id',$curso_id)->where('estado','A')->firstOrFail();
        $resultados = ResultadosEstudiante::where('plan_id',$curso->plan_id)->where('estado','A')->get();

        foreach($resultados as $res){
            $tempcrit = Criterio::where('resultadoestudiante_id',$res->id)->where('estado','A')->get();
            foreach($tempcrit as $crit){
                array_push($criterios,$crit);
            }
        }

        $esquemas=CursoEsquema::where('cursosemestre_id',$cursosemestres[0]->id)->where('estado','A')->orderBy('codigo')->get();

        foreach($esquemas as $esquema){
            $temp=$matrizdetalle_general->where('cursoesquema.id',$esquema->id)->where('estado','A')->first();
            if(!empty($temp)){
                $esquema->ponderacion=$temp->ponderacion;
                $esquema->matrizdetalle_id=$temp->id;
            }else{
                $esquema->ponderacion='SIN PONDERACION';
                $esquema->matrizdetalle_id=0;
            }
        }

        return response()->json
        ([
            'esquemas' => $esquemas,
            'matrizdetalle_general' => $matrizdetalle_general,
            'criterios' => $criterios
        ]);
        
    }

    public function verificarEstadoSemestre(Request $request){
        $semestre_id = $request->semestre_id;
        $curso_id = $request->curso_id;

        $escuelasemestres = EscuelaSemestre::where('semestre_id',$semestre_id)->get();
        $cursosemestres = CursoSemestre::where('curso_id',$curso_id)->get();

        foreach($escuelasemestres as $es){
            foreach($cursosemestres as $cs){
                if($es->id == $cs->escuelasemestre_id)
                    $escuelasemestre = $es;
            }
        }

        return response()->json
        ([
            'escuelasemestre' => $escuelasemestre
        ]);
    }

    public function editarEsquema(Request $request)
    {
        $matrizdetalle_id=$request->matrizdet_id;
        $cursoesquema_id=$request->cursoesquema;
        $matriz_id=$request->matriz_id;
        $criterio_valor=$request->criterio_valor;
        $pond=$request->pond;

        if($criterio_valor==0){
            $id = $matrizdetalle_id;
            $mdetalleeliminar = MatrizDetalle::findOrFail($id);
            $mdetalleeliminar->estado = 'D';
            $mdetalleeliminar->save();
            return 1;
        }

        if($matrizdetalle_id == 0){
            if($pond=="SIN PONDERACION"){
                $pond1 = 0;
            }else{
                $pond1 = $pond;
            }
            $input = $request->all();
            $input['criterio_id'] = $criterio_valor;
            $input['cursomatriz_id'] = $matriz_id;
            $input['cursoesquema_id'] = $cursoesquema_id;
            $input['ponderacion'] = $pond1;
            $matriz_detalle = MatrizDetalle::create($input);
            return 1;
        }else{
            $id = $matrizdetalle_id;
            $mdetalle = MatrizDetalle::findOrFail($id);
            $mdetalle->criterio_id = $criterio_valor;
            $mdetalle->cursomatriz_id = $matriz_id;
            $mdetalle->cursoesquema_id = $cursoesquema_id;
            $mdetalle->ponderacion = $pond;
            $mdetalle->save();
            return 1;
        }
    }

    public static function exportarEsquema(Request $request){
        $curso_id = $request->curso;
        $semestre_id = $request->semestre;
        $cursosemestre_id = $request->cursoSemestre;

        $curso = Curso::where('id',$curso_id)->where('estado','A')->firstOrFail();
        $semestre = Semestre::where('id',$semestre_id)->where('estado','A')->firstOrFail();
        $cursosemestre = CursoSemestre::where('id',$cursosemestre_id)
        ->with(['escuelasemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
        ->get();

        $escuela = Escuela::where('id',$cursosemestre[0]->escuelasemestre->escuela_id)->where('estado','A')->firstOrFail();

        $cursogrupo = CursoGrupo::where('cursosemestre_id',$cursosemestre_id)->where('coordinador','1')->get();

        $docente = Docente::where('id',$cursogrupo[0]->docente_id)->where('estado','A')->firstOrFail();

        return Excel::download($exportar = new EsquemaExport($escuela,$semestre,$curso,$docente,$cursosemestre[0]), $semestre->anio.'-'.$semestre->semestre.'_'.$curso->codigo.'.xlsx');
    }

    public function importarEsquema(Request $request){

        if($request->file('esquemasexcel')->isValid()){
            $fileEsquemas = $request->file('esquemasexcel');

            $cursosemestre = $request->cursoSemestre;
            $curso = $request->cursoE;
            $semestre = $request->semestreE;
            $docente = $request->session()->get('id');

            $cursosemestreO = CursoSemestre::where('id',$cursosemestre)
            ->with(['escuelasemestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
            ->get();

            $escuela = Escuela::where('id',$cursosemestreO[0]->escuelasemestre->escuela_id)->where('estado','A')->firstOrFail();
            
            Excel::import(new EsquemaImport($escuela->id,$semestre,$curso,$docente,$cursosemestre,$request), $fileEsquemas);
            
            $url = redirect()->getUrlGenerator()->previous();

            // $modificar = $request->session()->put('mensaje','MENSAJEMODIFICADO');
            $mensaje = $request->session()->get('mensajeEsquema');
            $tipoMensaje = $request->session()->get('tipoMensajeEsquema');
        
            return redirect('docente/esquemas?semestre='.$semestre.'+&curso='.$curso.'&grupo=1')->with([$tipoMensaje => $mensaje]);
        
        } else {
            dd("UPS LO SENTIMOS ERROR");
        }
    }

    public static function exportarAlumno(Request $request){
        $curso_id = $request->curso;
        $semestre_id = $request->semestre;
        $cursogrupo_id = $request->grupo;

        $curso = Curso::where('id',$curso_id)->where('estado','A')->firstOrFail();
        $semestre = Semestre::where('id',$semestre_id)->where('estado','A')->firstOrFail();

        $cursogrupo = CursoGrupo::where('id',$cursogrupo_id)->where('estado','A')->firstOrFail();
        $cursosemestre = CursoSemestre::where('id',$cursogrupo->cursosemestre_id)
        ->with(['escuelasemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
        ->get();

        $escuela = Escuela::where('id',$cursosemestre[0]->escuelasemestre->escuela_id)->where('estado','A')->firstOrFail();

        $docente = Docente::where('id',$cursogrupo->docente_id)->where('estado','A')->firstOrFail();

        return Excel::download(new AlumnoExport($escuela,$semestre,$curso,$cursogrupo,$docente),$semestre->anio.'-'.$semestre->semestre.'_'.$curso->codigo.'_'.$cursogrupo->grupo.'.xlsx');
    }

    public function importarAlumno(Request $request){

        if($request->file('alumnosexcel')->isValid()){
            $fileAlumnos = $request->file('alumnosexcel');

            $curso = $request->cursoI;
            $semestre = $request->semestreI;
            $grupo = $request->grupoI;
            $docente = $request->docenteI;
            
            Excel::import(new AlumnoImport($semestre,$curso,$grupo,$docente,$request), $fileAlumnos);

            $mensaje = $request->session()->get('mensajeAlumno');
            $tipoMensaje = $request->session()->get('tipoMensajeAlumno');
        
            // return redirect($url)->with(['success' => 'La lista de alumnos ha sido ingresada']);
            return redirect('docente/alumnos?semestre='.$semestre.'+&curso='.$curso.'&grupo='.$grupo)->with([$tipoMensaje => $mensaje]);
        
        } else {
            dd("UPS LO SENTIMOS ERROR");
        }
    }

    public static function exportarLista(Request $request){
        $semestre_id = $request->semestreR;
        $curso_id = $request->cursoR;
        $cursogrupo_id = $request->grupoR;
        $docente_id = $request->docenteR;

        $matrizdetalles = [];
        $matrizsheet = [];
        $notas = [];
        $arreglonotas = [];
        $criterios = [];
        $esquemas = [];
        $criteriosreporte = [];
        $esquemasreporte = [];
        $cursomatrizreporte = [];

        $docente = Docente::where('id',$docente_id)->where('estado','A')->first();

        $semestre = Semestre::where('id',$semestre_id)->where('estado','A')->first();
        
        $curso = Curso::where('id',$curso_id)->where('estado','A')->first();

        $estudiantes = CursoEstudiante::where('cursogrupo_id',$cursogrupo_id)->where('estado','A')->get();

        $cursogrupo = CursoGrupo::where('id',$cursogrupo_id)->where('estado','A')->firstOrFail();

        $cursosemestre = CursoSemestre::where('id',$cursogrupo->cursosemestre_id)->where('estado','A')->first();

        $escuelasemestre = EscuelaSemestre::where('id',$cursosemestre->escuelasemestre_id)->where('estado','A')->first();

        $escuela = Escuela::where('id',$escuelasemestre->escuela_id)->where('estado','A')->first();

        // $esquemas = CursoEsquema::where('cursosemestre_id',$cursogrupo->cursosemestre_id)->where('estado','A')->get();

        $cursomatriz = CursoMatriz::where('cursosemestre_id',$cursogrupo->cursosemestre_id)->get();

        foreach($cursomatriz as $cm){
            $matrizdetalle = MatrizDetalle::where('cursomatriz_id',$cm->id)->where('estado','A')->orderBy('cursoesquema_id')
            ->with(['cursoesquema' => function ($query)
                {
                    $query->where('estado','A');
                }
            ])->get();
            $matrizdetalle = $matrizdetalle->sortBy('cursoesquema.codigo');
            array_push($matrizsheet,$matrizdetalle);
            if(sizeof($matrizdetalle)>0)
                array_push($cursomatrizreporte,$cm);
            foreach($matrizdetalle as $md){
                array_push($matrizdetalles,$md);
            }
        }

        foreach($estudiantes as $est){
            $Estudiante = Estudiante::where('id',$est->estudiante_id)->where('estado','A')->first();
            foreach($matrizsheet as $ms){
                foreach($ms as $mds){
                    $nota = EstudianteMatriz::where('curso_estudiante_id',$est->id)->where('matriz_detalle_id',$mds->id)->where('estado','A')->first();
                    array_push($notas,$nota);
                }
                if(sizeof($notas)>0){
                    array_push($arreglonotas,$notas);
                }
                $notas = [];
            }
            $est->datos = $Estudiante;
            $est->notas = $arreglonotas;
            $arreglonotas = [];
        }

        foreach($matrizsheet as $sheet){
            foreach($sheet as $md1){
                $criterio = Criterio::where('id',$md1->criterio_id)->where('estado','A')->first();
                $esquema = CursoEsquema::where('id',$md1->cursoesquema_id)->where('estado','A')->first();
                array_push($criterios,$criterio);
                array_push($esquemas,$esquema);
            }

            if(sizeof($criterios)>0)
                array_push($criteriosreporte,$criterios);

            if(sizeof($esquemas)>0)
                array_push($esquemasreporte,$esquemas);
            $criterios = [];
            $esquemas = [];
        }

        $reporteporMatriz = [];
        $reporte = [];

        if(sizeof($estudiantes)==0)
        return redirect('docente/matriz?semestre='.$semestre->id.'+&curso='.$curso->id.'&grupo='.$cursogrupo->id)->with(['error' => 'Debe haber alumnos matriculados para generar el reporte']);

        if(sizeof($estudiantes[0]->notas) == 0)
        return redirect('docente/matriz?semestre='.$semestre->id.'+&curso='.$curso->id.'&grupo='.$cursogrupo->id)->with(['error' => 'No se han registrado esquemas de evaluacion aun.']);

        for($i=0; $i<sizeof($estudiantes[0]->notas); $i++){
            foreach($estudiantes as $est){
                $re = new ObjetoReporte($est,$est->notas[$i]);
                array_push($reporteporMatriz,$re);
            }
            array_push($reporte,$reporteporMatriz);
            $reporteporMatriz = [];
        }

        return Excel::download(new ListaAlumnosExport($escuela,$semestre,$curso,$docente,$cursogrupo,$reporte,$esquemasreporte,$criteriosreporte,$cursomatrizreporte,$matrizsheet),'Registros de notas '.$semestre->anio.'-'.$semestre->semestre.'_'.$curso->curso.'_GRUPO_'.$cursogrupo->grupo.'_'.str_replace('/',' ',$docente->nombre).'.xlsx');
    }

public static function cargaMatrizAlumno(Request $request)
    {
        $cursomatriz_id = $request->cursomatriz_id;
        $cursoestudiante_id = $request->cursoestudiante_id;
        $matrizdetalles;

        //$micursosemestre_id;

        $cursoesquemas_n = [];

        $uncursomatriz = CursoMatriz::where('id',$cursomatriz_id)
            ->where('estado','A')
            ->get();

        $micursosemestre_id = $uncursomatriz[0]->cursosemestre_id;

        //Busca las matrices relacionadas al curso
        $cursomatrizs = CursoMatriz::where('cursosemestre_id',$micursosemestre_id)
            ->where('estado','A')
            ->get();

        //Busca los esquemas relacionadas al curso
        $cursoesquemas = CursoEsquema::where('cursosemestre_id',$micursosemestre_id)
            ->where('estado','A')
            ->orderBy('codigo')
            ->get();

        //carga id's de los esquemas
        foreach ($cursoesquemas as $item) 
        {
            $mis_notas = [];
            array_push($cursoesquemas_n, $item->id);
            $matrizdetalles = MatrizDetalle::where('cursoesquema_id',$item->id)
                /*
                ->with(['estudiantematrizs',function ($query) use($selected_alumno)
                {
                    $query->where('estado','A')
                        ->where('curso_estudiante_id',$selected_alumno);
                }
                ])
                */
                ->where('estado','A')
                ->orderBy('cursomatriz_id')
                ->get();
            foreach ($matrizdetalles as $md)
            {
                $nota = EstudianteMatriz::where('matriz_detalle_id',$md->id)
                    ->where('estado','A')
                    ->where('curso_estudiante_id',$cursoestudiante_id)
                    ->get();
                $mis_notas[$md->cursomatriz_id] = $nota;

            }
            $item->notas = $mis_notas;
        }

        /*
        
        $matrizdetalles = MatrizDetalle::where('cursomatriz_id',$cursomatriz_id)
            ->where('estado','A')
            ->orderBy('cursoesquema_id')
            ->with(['cursoesquema' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['estudiantematrizs' => function ($query) use($cursoestudiante_id)
            {
                $query->where('estado','A')
                    ->where('curso_estudiante_id',$cursoestudiante_id);
            }
            ])
            ->get();
        */
        return response()->json
        ([
            'cursoesquemas_n' => $cursoesquemas_n,
            'cursoesquemas' => $cursoesquemas
        ]);
    }

    public static function cargaCursosDocenteEspecifico(Request $request)
    {
        $semestre_id = $request->semestre_id;
        $verificar_coordinador = $request->coordinador;
        $escuela_id = $request->escuela_id;
        $cursos=[];
        $idDocente = $request->session()->get('id');

        if(empty($verificar_coordinador))
        {   
            $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
                ->where('estado','A')
                ->with(['cursosemestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.curso' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->get();
        }
        else
        {
            $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
                ->where('coordinador','1')
                ->where('estado','A')
                ->with(['cursosemestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.curso' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->get();
        }
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre->id == $semestre_id)
            {
                array_push($cursos, $item->cursosemestre->curso);
            }
        }

        $escuelasemestre = EscuelaSemestre::where('semestre_id',$semestre_id)->where('escuela_id',$escuela_id)->first();

        return response()->json
        ([
            'cursos' => $cursos,
            'escuelasemestre' => $escuelasemestre
        ]);
    }

    public static function cargaGruposDocenteEspecifico(Request $request)
    {
        $curso_id = $request->curso_id;
        $semestre_id = $request->semestre_id;
        $grupos=[];
        $idDocente = $request->session()->get('id');
        $micurso_n;
        $micursosemestre_n;
        $docente;
        $cargo;

        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('estado','A')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();


        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->curso->id == $curso_id && $item->cursosemestre->escuelasemestre->semestre->id == $semestre_id)
            {
                $micurso_n = $item->id;
                $micursosemestre_n = $item->cursosemestre->id;
            }
        }

        $micurso = CursoGrupo::where('id',$micurso_n)
            ->get();

        if($micurso[0]->coordinador == 1)
        {
            $grupos = CursoGrupo::where('cursosemestre_id',$micursosemestre_n)
                ->where('estado','A')->get();
            $docente = Docente::where('id',$grupos[0]->docente_id)->get();
            $cargo = "Coordinador";
        }
        else
        {
            $grupos = $micurso;
            $docente = Docente::where('id',$idDocente)->get();
            $cargo = "Docente";
        }
        return response()->json
        ([
            'grupos' => $grupos,
            'docente' => $docente,
            'cargo' => $cargo
        ]);
    }

    public static function cargaEsquemasCurso(Request $request)
    {
        $curso_id = $request->curso_id;
        $semestre_id = $request->semestre_id;
        $grupos=[];
        $idDocente = $request->session()->get('id');
        $micurso_n;
        $micursosemestre_n;
        $docente;

        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('coordinador','1')
            ->where('estado','A')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();


        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->curso->id == $curso_id && $item->cursosemestre->escuelasemestre->semestre->id == $semestre_id)
            {
                $micurso_n = $item->id;
                $micursosemestre_n = $item->cursosemestre->id;
            }
        }

        $micurso = CursoGrupo::where('id',$micurso_n)
            ->get();

        if($micurso[0]->coordinador == 1)
        {
            $grupos = CursoGrupo::where('cursosemestre_id',$micursosemestre_n)
                ->where('estado','A')->get();
            $docente = Docente::where('id',$grupos[0]->docente_id)->get();
        }

        $cursoesquemas = CursoEsquema::where('cursosemestre_id',$micursosemestre_n)
            ->where('estado','A')
            ->orderBy('codigo')
            ->get();

        return response()->json
        ([
            'cursoesquemas' => $cursoesquemas,
            'cursosemestre' => $micursosemestre_n
        ]);
    }

    public static function cargaCursoMatriz(Request $request)
    {
        $grupo_id = $request->grupo_id;

        $cursomatrizs_n = [];

        $cursogrupo = CursoGrupo::where('id',$grupo_id)
            ->where('estado','A')
            ->first();

        $cursomatrizs = CursoMatriz::where('cursosemestre_id',$cursogrupo->cursosemestre_id)
            ->where('estado','A')
            ->get();

        foreach ($cursomatrizs as $cm)
        {
            array_push($cursomatrizs_n, $cm->id);
        }

        return response()->json
        ([
            'cursomatrizs' => $cursomatrizs,
            'cursomatrizs_n' => $cursomatrizs_n
        ]);
    }

    public static function cargaMatrizDetalle(Request $request)
    {
        $cursomatriz_id = $request->cursomatriz_id;

        $matrizdetalles = MatrizDetalle::where('cursomatriz_id',$cursomatriz_id)
            ->where('estado','A')
            ->with(['criterio' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursoesquema' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        $matrizdetalles_sort = $matrizdetalles->sortBy(function ($item) 
        {
            return $item->cursoesquema->codigo;
        });
        $matrizdetalles_sort->values()->all();

        $new = $matrizdetalles_sort->flatten();

        return response()->json
        ([
            'matrizdetalles' => $new
        ]);
    }

    public static function cargaMatrizsCurso(Request $request)
    {
        $curso_id = $request->curso_id;
        $semestre_id = $request->semestre_id;
        $grupos=[];
        $idDocente = $request->session()->get('id');
        $micurso_n;
        $micursosemestre_n;
        $docente;

        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('coordinador','1')
            ->where('estado','A')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();


        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->curso->id == $curso_id && $item->cursosemestre->escuelasemestre->semestre->id == $semestre_id)
            {
                $micurso_n = $item->id;
                $micursosemestre_n = $item->cursosemestre->id;
            }
        }

        $micurso = CursoGrupo::where('id',$micurso_n)
            ->get();

        if($micurso[0]->coordinador == 1)
        {
            $grupos = CursoGrupo::where('cursosemestre_id',$micursosemestre_n)
                ->where('estado','A')->get();
            $docente = Docente::where('id',$grupos[0]->docente_id)->get();
        }

        $cursomatrizs = CursoMatriz::where('cursosemestre_id',$micursosemestre_n)
            ->where('estado','A')
            ->get();

        return response()->json
        ([
            'cursomatrizs' => $cursomatrizs,
            'cursosemestre' => $micursosemestre_n
        ]);
    }

    public static function cargaNotasDetalle(Request $request)
    {
        $cursomatriz_id = $request->cursomatriz_id;
        $cursogrupo_id = $request->cursogrupo_id;

        $matrizdetalles_n = [];

        $matrizdetalles = MatrizDetalle::where('cursomatriz_id',$cursomatriz_id)
            ->where('estado','A')
            //->orderBy('cursoesquema_id')
            ->get();

        $matrizdetalles = $matrizdetalles->sortBy(function ($item) 
        {
            return $item->cursoesquema->codigo;
        });

        $matrizdetalles->values()->all();

        $matrizdetalles_sort = $matrizdetalles->flatten();

        foreach($matrizdetalles_sort as $item)
        {
            array_push($matrizdetalles_n,$item->id);
        }

        //Buscar alumnos del curso seleccionado previamente
        $cursoestudiantes = CursoEstudiante::where('cursogrupo_id',$cursogrupo_id)
            ->where('estado','A')
            ->with(['estudiante' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['estudiantematrizs' => function ($query) use ($matrizdetalles_n)
            {
                $query->where('estado','A')
                    ->whereIn('matriz_detalle_id',$matrizdetalles_n);
            }
            ])
            ->get();

        foreach($cursoestudiantes as $ce)
        {
            $misNotas = [];
            foreach($ce->estudiantematrizs as $em)
            {
                $misNotas[$em->matriz_detalle_id] = $em; 
            }
            $ce->notas = $misNotas;
            $ce->promedio_matriz = VistasDocenteController::promedioMatrizEstudiante($cursomatriz_id,$ce->id);
        }

        return response()->json
        ([
            'cursoestudiantes' => $cursoestudiantes,
            'matrizdetalles_n' => $matrizdetalles_n
        ]);
    }

    public function cargaMatrizPromedio(Request $request)
    {
        $grupo_id = $request->grupo_id;
        $curso_estudiante_id = $request->curso_estudiante_id;

        $promedios = [];
        $promedio_final = 0;

        $cursogrupo = CursoGrupo::where('id',$grupo_id)
            ->where('estado','A')
            ->first();

        $cursomatrizs = CursoMatriz::where('cursosemestre_id',$cursogrupo->cursosemestre_id)
            ->where('estado','A')
            ->get();

        foreach ($cursomatrizs as $item)
        {
            $promedios[$item->id] = $this->promedioMatrizEstudiante($item->id, $curso_estudiante_id);
            $promedio_final = $promedio_final + $item->ponderacion*$promedios[$item->id]/100;
        }

        return response()->json
        ([
            'promedios' => $promedios,
            'promedio_final' => $promedio_final
        ]);
    }

    public function cargaMatrizPromedios(Request $request)
    {
        $cursogrupo_id = $request->cursogrupo_id;

        $cursogrupo = CursoGrupo::where('id',$cursogrupo_id)
            ->where('estado','A')
            ->first();
        $cursomatrizs = CursoMatriz::where('cursosemestre_id',$cursogrupo->cursosemestre_id)
            ->where('estado','A')
            ->get();

        $cursoestudiantes = CursoEstudiante::where('cursogrupo_id',$cursogrupo_id)
            ->where('estado','A')
            ->with(['estudiante' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        foreach ($cursoestudiantes as $ce)
        {
            $promedios = [];
            $promedio_final = 0;
            foreach ($cursomatrizs as $cm)
            {
                $promedio_actual = $this->promedioMatrizEstudiante($cm->id, $ce->id);
                array_push($promedios, $promedio_actual);
                $promedio_final = $promedio_final + ($promedio_actual*$cm->ponderacion/100);
            }
            array_push($promedios, $promedio_final);
            $ce->promedios = $promedios;
        }

        return response()->json
        ([
            'cursomatrizs' => $cursomatrizs,
            'cursoestudiantes' => $cursoestudiantes
        ]);
    }

    public function cargaDatoMatriz(Request $request)
    {
        $curso_matriz_id = $request->curso_matriz_id;
        $curso_estudiante_id = $request->curso_estudiante_id;

        $datomatriz = DatoMatriz::where('estado','A')
            ->where('curso_matriz_id',$curso_matriz_id)
            ->where('curso_estudiante_id',$curso_estudiante_id)
            ->get();

        return response()->json
        ([
            'datomatriz' => $datomatriz
        ]);
    }

    //funciones auxiliares

    public static function promedioMatrizEstudiante($curso_matriz_id, $curso_estudiante_id)
    {
        $promedio = 0.00;
        $notas = MatrizDetalle::where('estado','A')
            ->where('cursomatriz_id',$curso_matriz_id)
            ->with(['estudiantematrizs' => function ($query) use ($curso_estudiante_id)
            {
                $query->where('estado','A')
                ->where('curso_estudiante_id',$curso_estudiante_id);
            }
            ])
            ->get();

        foreach ($notas as $nota) 
        {
            if(isset($nota->estudiantematrizs[0]->nota))
            {
                $promedio = $promedio + ($nota->estudiantematrizs[0]->nota*$nota->ponderacion/100);
            }
        }

        return $promedio;
    }

    //funciones de exportacion de datos

    public static function exportarMatrizGrupo(Request $request)
    {
        $curso_id = $request->curso;
        $semestre_id = $request->semestre;
        $cursogrupo_id = $request->grupo;

        $curso = Curso::where('id',$curso_id)
            ->where('estado','A')
            ->with('plan.escuela')
            ->firstOrFail();

        //$escuela = Escuela::where('id',$cursosemestre[0]->escuelasemestre->escuela_id)->where('estado','A')->firstOrFail();
        $escuela = $curso->plan->escuela;

        $semestre = Semestre::where('id',$semestre_id)->where('estado','A')->firstOrFail();

        $cursogrupo = CursoGrupo::where('id',$cursogrupo_id)
            ->where('estado','A')
            ->firstOrFail();

        $docente = Docente::where('id',$cursogrupo->docente_id)->where('estado','A')->firstOrFail();

        $cursomatrizs = CursoMatriz::where('cursosemestre_id',$cursogrupo->cursosemestre_id)
            ->where('estado','A')
            ->firstOrFail();

        $cursoestudiantes = CursoEstudiante::where('cursogrupo_id',$cursogrupo_id)
            ->where('estado','A')
            ->with('estudiante')
            ->firstOrFail();

        $matrizdetalles = MatrizDetalle::where('cursomatriz_id', $cursomatrizs->id)
            ->with(['cursoesquema' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['estudiantematrizs' => function ($query) use($cursoestudiantes)
            {
                $query->where('estado','A')
                    ->where('curso_estudiante_id',$cursoestudiantes->id);
            }
            ])
            ->where('estado','A')
            ->orderBy('cursoesquema_id')
            ->get();

        /*
        return Excel::download(new AlumnoExport($escuela,$semestre,$curso,$cursogrupo,$docente), $semestre->anio.'-'.$semestre->semestre.'_'.$curso->codigo.'_'.$cursogrupo->grupo.'.xlsx');
        */

        return (new MatrizGrupoExport($escuela,$semestre,$curso,$cursogrupo,$docente,$cursomatrizs,$cursoestudiantes,$matrizdetalles))->download('CONT1 '.$semestre->anio.$semestre->semestre.' '.$curso->curso.' Grupo-'.$cursogrupo->grupo.'.pdf', \Maatwebsite\Excel\Excel::DOMPDF);
    }

    //reporte de pdf por alumno
    public function matrizAlumnoPDF(Request $request)
    {
        $curso_id = $request->curso;
        $semestre_id = $request->semestre;
        $cursogrupo_id = $request->grupo;
        $curso_estudiante_id = $request->alumno;
        $matriz_id = $request->matrizid;

        $curso = Curso::where('id',$curso_id)
            ->where('estado','A')
            ->with('plan.escuela')
            ->firstOrFail();

        $escuela = $curso->plan->escuela;

        $semestre = Semestre::where('id',$semestre_id)->where('estado','A')->firstOrFail();

        $cursogrupo = CursoGrupo::where('id',$cursogrupo_id)
            ->where('estado','A')
            ->firstOrFail();

        $docente = Docente::where('id',$cursogrupo->docente_id)->where('estado','A')->firstOrFail();

        $cursomatrizs = CursoMatriz::where('id',$matriz_id)
            ->where('estado','A')
            ->firstOrFail();

        $cursoestudiantes = CursoEstudiante::where('id',$curso_estudiante_id)
            ->where('estado','A')
            ->get();

        $matrizdetalles = MatrizDetalle::where('cursomatriz_id', $cursomatrizs->id)
            ->with(['cursoesquema' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['estudiantematrizs' => function ($query) use($cursoestudiantes)
            {
                $query->where('estado','A')
                    ->where('curso_estudiante_id',$cursoestudiantes[0]->id);
            }
            ])
            ->where('estado','A')
            ->orderBy('cursoesquema_id')
            ->get();

        $matrizdetalles = $matrizdetalles->sortBy(function ($item) 
        {
            return $item->cursoesquema->codigo;
        });

        $matrizdetalles->values()->all();

        $matrizdetalles_sort = $matrizdetalles->flatten();

        $cursoestudiantes[0]->matrizdetalles_sort = $matrizdetalles_sort;

        $cursoestudiantes[0]->promedio = $this->promedioMatrizEstudiante($matriz_id, $curso_estudiante_id);

        $datomatriz = DatoMatriz::where('estado','A')
            ->where('curso_matriz_id',$matriz_id)
            ->where('curso_estudiante_id',$curso_estudiante_id)
            ->get();

        $cursoestudiantes[0]->datomatriz = $datomatriz;
        //devolver vista
        //return view('export.exportMatrizGrupo',compact('escuela','semestre','curso','cursogrupo','docente','cursomatrizs','cursoestudiantes'));

        $pdf = PDF::loadView('export.exportMatrizGrupo',compact('escuela','semestre','curso','cursogrupo','docente','cursomatrizs','cursoestudiantes'));
        //$pdf->set_paper (‘a4′,’landscape’);

        //dercargar  DOMPDF
        return $pdf->download($semestre->anio.$semestre->semestre.' '.$curso->curso.' Grupo-'.$cursogrupo->grupo.' '.$cursomatrizs->matriz.' '.$cursoestudiantes[0]->estudiante->estudiante.'.pdf');
        
    }


    //reporte de pdf por grupo
    public function matrizGrupoPDF(Request $request)
    {
        $curso_id = $request->curso;
        $semestre_id = $request->semestre;
        $cursogrupo_id = $request->grupo;
        $matriz_id = $request->matrizid;

        $curso = Curso::where('id',$curso_id)
            ->where('estado','A')
            ->with('plan.escuela')
            ->firstOrFail();

        $escuela = $curso->plan->escuela;

        $semestre = Semestre::where('id',$semestre_id)->where('estado','A')->firstOrFail();

        $cursogrupo = CursoGrupo::where('id',$cursogrupo_id)
            ->where('estado','A')
            ->firstOrFail();

        $docente = Docente::where('id',$cursogrupo->docente_id)->where('estado','A')->firstOrFail();

        $cursomatrizs = CursoMatriz::where('id',$matriz_id)
            ->where('estado','A')
            ->firstOrFail();

        $cursoestudiantes = CursoEstudiante::where('cursogrupo_id',$cursogrupo_id)
            ->where('estado','A')
            ->get();

        foreach ($cursoestudiantes as $ce)
        {
            $matrizdetalles = MatrizDetalle::where('cursomatriz_id', $cursomatrizs->id)
                ->with(['cursoesquema' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['estudiantematrizs' => function ($query) use($ce)
                {
                    $query->where('estado','A')
                        ->where('curso_estudiante_id',$ce->id);
                }
                ])
                ->where('estado','A')
                ->orderBy('cursoesquema_id')
                ->get();

            $matrizdetalles = $matrizdetalles->sortBy(function ($item) 
            {
                return $item->cursoesquema->codigo;
            });

            $matrizdetalles->values()->all();

            $matrizdetalles_sort = $matrizdetalles->flatten();

            $ce->matrizdetalles_sort = $matrizdetalles_sort;

            $ce->promedio = $this->promedioMatrizEstudiante($matriz_id, $ce->id);

            $datomatriz = DatoMatriz::where('estado','A')
            ->where('curso_matriz_id',$matriz_id)
            ->where('curso_estudiante_id',$ce->id)
            ->get();

            $ce->datomatriz = $datomatriz;
        }

        //return view('export.exportMatrizGrupo',compact('escuela','semestre','curso','cursogrupo','docente','cursomatrizs','cursoestudiantes'));


        $pdf = PDF::loadView('export.exportMatrizGrupo',compact('escuela','semestre','curso','cursogrupo','docente','cursomatrizs','cursoestudiantes'));
        //$pdf->set_paper (‘a4′,’landscape’);

        return $pdf->download($semestre->anio.$semestre->semestre.' '.$curso->curso.' Grupo-'.$cursogrupo->grupo.' '.$cursomatrizs->matriz.'.pdf');
    }
}