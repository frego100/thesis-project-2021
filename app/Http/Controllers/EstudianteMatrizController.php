<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EstudianteMatriz;
use App\Models\MatrizDetalle;
use App\Models\CursoEstudiante;

class EstudianteMatrizController extends Controller
{
    public function index()
    {
        $estudiantematrizs = EstudianteMatriz::with(['matrizdetalle','cursoestudiante'])->get();
        return view('matrizestudiante.index',compact('estudiantematrizs'));
    }
    public function create()
    {
        $matrizdetalles=MatrizDetalle::all();
        $cursoestudiantes = CursoEstudiante::all();
        return view('matrizestudiante.create',compact('matrizdetalles','cursoestudiantes'));
    }
    public function show($id)
    {
        $estudiantematriz = EstudianteMatriz::find($id);
        return view('matrizestudiante.show',compact('estudiantematriz'));
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['matrizdetalle_id']= $request['matrizdetalle'];
        $input['cursoestudiante_id']= $request['cursoestudiante'];
        $estudiantematriz = EstudianteMatriz::create($input);

        return view('matrizestudiante.index')->with('message','insertado correctamente');
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $estudiantematriz = EstudianteMatriz::find($id);
        $estudiantematriz->update($input);

        return view('matrizestudiante.index')->with('message','insertado correctamente');
    }
    public function edit($id)
    {
        $matrizdetalles=MatrizDetalle::all();
        $cursoestudiantes = CursoEstudiante::all();
        $estudiantematriz = EstudianteMatriz::findOrFail($id);
        return view('matrizestudiante.edit', compact('estudiantematriz','matrizdetalles','cursoestudiantes'));
    }
    public function delete($id)
    {
        $estudiantematriz = EstudianteMatriz::find($id);
        $estudiantematriz->estado = 'D';
        $estudiantematriz->save();
        return view('matrizestudiante.index')->with('message','eliminado correctamente');
    }

    //determinar que hacer al insertar nota
    public function modificarNotaAlumno(Request $request)
    {
        $estudiante_matriz_id = $request->estudiante_matriz_id;
        if($estudiante_matriz_id == 0)
        {
            EstudianteMatrizController::insertarEstudianteMatriz($request);
        }
        else
        {
            EstudianteMatrizController::editarEstudianteMatriz($request);
        }
        return 1;
    }

    //ajax
    public function insertarEstudianteMatriz(Request $request)
    {
        $matrizdetalle = MatrizDetalle::where('cursomatriz_id', $request->cursomatriz_id)
            ->where('cursoesquema_id', $request->cursoesquema_id)->first();
        $input = $request->all();
        $input['matriz_detalle_id'] = $matrizdetalle->id;
        $input['curso_estudiante_id'] = $request->curso_estudiante_id;
        $input['nota'] = $request->nota;
        $estudiantematriz = EstudianteMatriz::create($input);
        return 1;
    }

    public function eliminarEstudianteMatriz(Request $request)
    {
        $id = $request->estudiante_matriz_id;
        $estudiantematriz = EstudianteMatriz::findOrFail($id);
        $estudiantematriz->estado = 'D';
        $estudiantematriz->save();
        return 1;
    }

    public function editarEstudianteMatriz(Request $request)
    {
        $id = $request->estudiante_matriz_id;
        $estudiantematriz = EstudianteMatriz::findOrFail($id);
        /*
        $estudiantematriz->matriz_detalle_id = $request->matriz_detalle_id;
        $estudiantematriz->curso_estudiante_id = $request->curso_estudiante_id;
        */
        $estudiantematriz->nota = $request->nota;
        $estudiantematriz->save();
        return 1;
    }
}
