<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CursoEsquema;
use App\Models\MatrizDetalle;
use App\Models\Curso;

class CursoEsquemaController extends Controller
{
    public function index()
    {   
        $cursoesquemas=CursoEsquema::with(['cursosemestre'])->where('estado','A')->get();
        return view("cursoesquema.index",compact('cursoesquemas'));
    }

    public function create()
    {
        $cursos = Curso::all()->where('estado','A');
        return view('cursoesquema.create', compact('cursos'));
    }
  
    public function store(Request $request)
    {
        $input = $request->all();
        $input['curso_id'] = $request['curso'];
        $cursoesquema = CursoEsquema::create($input);
        return redirect('cursoesquemas')->with('message', 'Guardado Exitosamente');
    }

    public function show($id)
    {
        $cursoesquema=CursoEsquema::find($id);
        return view('cursoesquema.show', compact('cursoesquema'));
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $input['curso_id'] = $request['curso'];
        $cursoesquema = CursoEsquema::find($id);
        $cursoesquema->update($input);
        return redirect('cursoesquemas')->with('message', 'Actualizado!');
    }


    public function edit($id)
    {
        $cursos = Curso::all();
        $cursoesquema = CursoEsquema::findOrFail($id);
        return view('cursoesquema.edit', compact('cursoesquema','cursos'));
    }

    public function delete($id)
    {
        $cursoesquema = CursoEsquema::find($id);
        $cursoesquema->estado = 'D';
        $cursoesquema->save();
        return redirect('cursoesquemas')->with('message', 'Eliminado!');
    }

    //ajax
    public function insertarCursoEsquema(Request $request)
    {
        $input = $request->all();
        $input['detalle'] = $request->detalle;
        $input['codigo'] = $request->codigo;
        $input['cursosemestre_id'] = $request->cursosemestre_id;
        $cursoesquema = CursoEsquema::create($input);
        return 1;
    }

    public function eliminarCursoEsquema(Request $request)
    {
        $id = $request->id;

        $cursoesquema = CursoEsquema::findOrFail($id);

        $matrizdetalle = MatrizDetalle::where('cursoesquema_id',$id)->where('estado','A')->get();


        if(sizeof($matrizdetalle) == 0){
            $cursoesquema->estado = 'D';
            $cursoesquema->save();
            return 0;
        }

        return sizeof($matrizdetalle);
    }

    public function editarCursoEsquema(Request $request)
    {
        $id = $request->id;
        $cursogrupo = CursoEsquema::findOrFail($id);
        $cursogrupo->detalle = $request->detalle;
        $cursogrupo->codigo = $request->codigo;
        $cursogrupo->save();
        return 1;
    }
}
