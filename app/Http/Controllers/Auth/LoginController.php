<?php 
namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Auth;
use Exception;
use App\Models\Docente;
use App\Http\Controllers\AdminController;

class LoginController extends Controller { 
 /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;
    /*** Where to redirect users after login. 
 ** @var string 
 */
    protected $redirectTo = '/home';
    /*** Create a new controller instance. 
 * * @return void 
 */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }
    public function redirectToGoogle() {
        return Socialite::driver('google')->redirect();
    }
    public function handleGoogleCallback(Request $request) {
        try
        {
            $user = Socialite::driver('google')->user();
            $finduser = Docente::findByEmail($user->email);
            return redirect($this->initSession($finduser));
        }
        catch(Exception $e) {
            error_log($e->getMessage());
            return redirect('/');
        }
    }
    public function initSession($finduser)
    {
        error_log("mi usuario");
        error_log($finduser);
        if ($finduser->superadmin>0) {
            session()->put('id', $finduser->id);
            session()->put('userType','superadmin');
            session()->put('name', $finduser->nombre);
            session()->put('email', $finduser->email);
            session()->put('superadmin', $finduser->superadmin);
            session()->put('admin', $finduser->admin);
            session()->put('docente', $finduser->docente);
            error_log(session()->get('userType'));
            error_log(session()->get('email'));
            return '/facultades';
        } 
        if ($finduser->admin == '1') {
            session()->put('id', $finduser->id);
            session()->put('userType', 'admin');
            session()->put('name', $finduser->nombre);
            session()->put('email', $finduser->email);
            session()->put('superadmin', $finduser->superadmin);
            session()->put('admin', $finduser->admin);
            session()->put('docente', $finduser->docente);
            error_log(session()->get('userType'));
            error_log(session()->get('name'));
            error_log(session()->get('email'));
            return '/admin';
        } 
        if ($finduser->docente == '1') {
            session()->put('id', $finduser->id);
            session()->put('userType', 'docente');
            session()->put('name', $finduser->nombre);
            session()->put('email', $finduser->email);
            session()->put('superadmin', $finduser->superadmin);
            session()->put('admin', $finduser->admin);
            session()->put('docente', $finduser->docente);
            session()->put('mensajeEsquema', 'VACIO');
            session()->put('tipoMensajeEsquema', 'success');
            session()->put('mensajeAlumno', 'VACIO');
            session()->put('tipoMensajeAlumno', 'success');
            error_log(session()->get('userType'));
            error_log(session()->get('name'));
            error_log(session()->get('email'));
            return '/docente';
        }
        return '/';
    }

    public static function updateSession()
    {
        $finduser = Docente::findByEmail(session()->get('email'));
        session()->put('superadmin', $finduser->superadmin);
        session()->put('admin', $finduser->admin);
        session()->put('docente', $finduser->docente);;
    }

    public function verifySession(Request $request)
    {
        if (session()->get('superadmin')) {
            return redirect('/facultades');
        } 
        if (session()->get('admin')) {
            return redirect('/admin');
        } 
        if (session()->get('docente')) {
            return redirect('/docente');
            }
        return redirect('/');
    }

    public function logout(Request $request)
    {
        /*
        if(Auth::check())
        {
            Auth::logout();
        }*/
        session()->flush();
        return redirect('/');
    }

}
?>