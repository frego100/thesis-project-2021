<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ResultadosEstudiante;
use App\Models\Plan;
use App\Models\Criterio;
use App\Models\CompetenciasRE;

class ResultadosEstudianteController extends Controller
{
    //

    public function index()
    {
        $resultadosestudiantes=ResultadosEstudiante::with(['plan'])->where('estado','A')->get();
        //return $escuelas;
        return view('resultadosestudiante.index',compact('resultadosestudiantes'));
    }

    public function create()
    {
        $planes=Plan::all();
        return view('resultadosestudiante.create',compact('planes'));
    }
  
    public function store(Request $request)
    {
        $input = $request->all();
        $input['plan_id'] = $request['plan'];
        $resultadosestudiante = ResultadosEstudiante::create($input);
        return redirect('resultadosestudiantes');
       // return \response()->json(['res' => true, 'message' =>'Guardado correctamente'], 200);
    }


    public function show($id)
    {
        $resultadosestudiante=ResultadosEstudiante::find($id);
        return view('resultadosestudiante.show',compact('resultadosestudiante'));
    }

    
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $input['plan_id'] = $request['plan'];
        $resultadosestudiante = ResultadosEstudiante::find($id);
        $resultadosestudiante->update($input);

        //return \response()->json(['res' => true, 'message' =>'actualizado correctamente'], 200);
        return redirect('resultadosestudiantes')->with('message', 'Actualizado!');
    }

    public function edit($id)
    {
        $planes=Plan::all();
        $resultadosestudiante = ResultadosEstudiante::findOrFail($id);
        return view('resultadosestudiante.edit', compact('resultadosestudiante','planes'));
    }

    public function delete($id)
    {
        $resultadosestudiante = ResultadosEstudiante::find($id);

        $criterio = Criterio::where('resultadoestudiante_id',$id)->where('estado','A')->get();

        $competenciares = CompetenciasRE::where('resultadoestudiante_id',$id)->where('estado','A')->get();

        $contador = sizeof($criterio) + sizeof($competenciares);

        if($contador == 0){
            $resultadosestudiante->estado = 'D';
            $resultadosestudiante->save();
            return redirect('resultadosestudiantes')->with(['success' => 'Se elimino exitosamente']);
        }

        return redirect('resultadosestudiantes')->with(['error' => 'No se puede eliminar resultado del estudiante porque tiene relacion con '. $contador.' registros']);
    }

    //ajax
    public function insertarResultado(Request $request)
    {
        $input = $request->all();
        $input['plan_id'] = $request->plan_id;
        $input['codigore'] = $request->codigore;
        $input['re'] = $request->re;
        $resultadoestudiante = ResultadosEstudiante::create($input);
        return 1;
    }
    public function eliminarResultado(Request $request)
    {
        $id = $request->id;
        $resultadoestudiante = ResultadosEstudiante::findOrFail($id);

        $criterio = Criterio::where('resultadoestudiante_id',$id)->where('estado','A')->get();
        
        $competenciasre = CompetenciasRE::where('resultadoestudiante_id',$id)->where('estado','A')->get();

        $contador = sizeof($criterio) + sizeof($competenciasre);

        if($contador == 0){
            $resultadoestudiante->estado = 'D';
            $resultadoestudiante->save();
            return 0;
        }

        return $contador;
    }

    public function editarResultado(Request $request)
    {
        $id = $request->id;
        $resultadoestudiante = ResultadosEstudiante::findOrFail($id);
        $resultadoestudiante->codigore = $request->codigore;
        $resultadoestudiante->re = $request->re;
        $resultadoestudiante->save();
        return 1;
    }
}
