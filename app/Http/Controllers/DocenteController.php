<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Docente;
use App\Models\CursoGrupo;
use App\Models\User;

class DocenteController extends Controller
{
    public function index()
    {
        $docentes=Docente::where('estado','A')->get();
        return view('docente.index',compact('docentes'));
    }

    public function create()
    {
        return view('docente.create');
    }
  
    public function store(Request $request)
    {
        $input = $request->all();
        $docente = Docente::create($input);
        return redirect('docentes');
    }

    public function show($id)
    {
        $docente=Docente::find($id);
        return view('docente.show',compact('docente'));
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $midocente = Docente::find($id);
        if(!isset($input['superadmin']))
        {
            error_log("No hay superadmin");
            $input['superadmin']= false;
        }
        if(!isset($input['admin']))
        {
            error_log("No hay admin");
            $input['admin']= false;
        }
        if(!isset($input['docente']))
        {
            error_log("No hay docente");
            $input['docente']= false;
        }
        $midocente->update($input);
        return redirect('docentes')->with('message', 'Actualizado!');
}

    public function edit($id)
    {
        $docentes = Docente::findOrFail($id);
        return view('docente.edit', compact('docentes'));
    }

    public function delete($id)
    {
        $docentes = Docente::find($id);

        $cursogrupo = CursoGrupo::where('docente_id',$id)->where('estado','A')->get();

        if(sizeof($cursogrupo) == 0){
            $docentes->estado = 'D';
            $docentes->save();
            return redirect('docentes')->with(['success' => 'Se elimino exitosamente']);
        }

        return redirect('docentes')->with(['error' => 'No se puede eliminar docente porque tiene relacion con '. sizeof($cursogrupo).' registros']);
    }
}
