<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Criterio;
use App\Models\Curso;
use App\Models\CursoMatriz;
use App\Models\CursoEsquema;
use App\Models\MatrizDetalle;
use App\Models\EstudianteMatriz;

class MatrizDetalleController extends Controller
{
     //
     public function index()
     {
         $matrizdetalles = MatrizDetalle::with(['criterio','cursomatriz','cursoesquema'])->where('estado','A')->get();
         return view('matrizdetalle.index',compact('matrizdetalles'));
     }
 
     public function create()
     {
         $criterios=Criterio::all();
         $cursomatrizs=CursoMatriz::all();
         $cursos = Curso::all();
         $cursoesquemas=CursoEsquema::all();
        return view('matrizdetalle.create',compact('criterios','cursomatrizs','cursos','cursoesquemas'));
     }
   
     public function show($id)
     {
         $matrizdetalle = MatrizDetalle::find($id);
         return view('matrizdetalle.show',compact('matrizdetalle'));
     }
 
     public function store(Request $request)
     {
         $input = $request->all();
         $input['criterio_id']= $request['criterio'];
         $input['cursomatriz_id']= $request['cursomatriz'];
         $input['cursoesquema_id']= $request['cursoesquema'];
         $matrizdetalle = MatrizDetalle::create($input);
 
         return view('matrizdetalle.index')->with('message','insertado correctamente');
     }
 
     public function update(Request $request, $id)
     {
         $input = $request->all();
         $matrizdetalle = MatrizDetalle::find($id);
         $matrizdetalle->update($input);
 
         return view('matrizdetalle.index')->with('message','insertado correctamente');
     }
     public function edit($id)
     {
         //aumentando
         $matrizdetalle = MatrizDetalle::find($id);
         $criterios=Criterio::all();
         $cursomatrizs = CursoMatriz::all();
         $cursoesquemas = CursoEsquema::findOrFail($id);
         return view('matrizdetalle.edit', compact('matrizdetalle','criterios','cursomatrizs','cursoesquemas'));
     }
     public function delete($id)
     {
         $matrizdetalle = MatrizDetalle::find($id);

         $estudiantematriz = EstudianteMatriz::where('matriz_detalle_id',$id)->where('estado','A')->get();

        if(sizeof($estudiantematriz) == 0){
            $matrizdetalle->estado = 'D';
            $matrizdetalle->save();
            return redirect('matrizdetalles')->with(['success' => 'Se elimino exitosamente']);
        }

        return redirect('matrizdetalles')->with(['error' => 'No se puede eliminar matridetalle porque tiene relacion con '. sizeof($estudiantematriz).' registros']);
     }
}
