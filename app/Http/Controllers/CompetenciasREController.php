<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CompetenciasRE;
use App\Models\Competencia;
use App\Models\ResultadosEstudiante;

class CompetenciasREController extends Controller
{
    //
    public function index()
    {
        $competenciasre = CompetenciasRE::with(['competencia','resultadoestudiante'])->where('estado','A')->get();
        return view('competenciasre.index',compact('competenciasre'));
    }
    public function create()
    {
        $competencias = Competencia::all()->where('estado','A');
        $resultadosestudiantes = ResultadosEstudiante::all();
        return view('competenciasre.create',compact('competencias','resultadosestudiantes'));
    }
    public function show($id)
    {
        $competenciasre = CompetenciasRE::find($id);
        return view('competenciasre.show',compact('competenciasre'));
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['competencia_id']= $request['competencias_res'];
        $input['resultadoestudiante_id']= $request['competencias_res'];
        $competenciasre = CompetenciasRE::create($input);

        return view("competenciasre.index")->with('message','insertado correctamente');
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $competenciasre = CompetenciasRE::find($id);
        $competenciasre->update($input);

        return view('competenciasre.index')->with('message','insertado correctamente');
    }
    public function edit($id)
    {
        $competencias = Competencia::all();
        $resultadosestudiantes = ResultadosEstudiante::all();
        $competenciasre = CompetenciasRE::findOrFail($id);
        return view('competenciasre.edit', compact('competenciasre','competencias','resultadosestudiantes'));
    }
    public function delete($id)
    {
        $competenciasre = CompetenciasRE::find($id);
        $competenciasre->estado = 'D';
        $competenciasre->save();
        return view('competenciasre.index')->with('message','eliminado correctamente');
    }

    //ajax
    public function insertarCompetenciaRE(Request $request)
    {
        $input = $request->all();
        $input['resultadoestudiante_id'] = $request->resultadoestudiante_id;
        $input['competencia_id'] = $request->competencia_id;
        $competenciasre = CompetenciasRE::create($input);
        return response()->json
        ([
            'competenciasre' => $competenciasre
        ]);
    }
    public function eliminarCompetenciaRE(Request $request)
    {
        $id = $request->id;
        $competenciasre = CompetenciasRE::findOrFail($id);
        $competenciasre->estado = 'D';
        $competenciasre->save();
        return 1;
    }
}
