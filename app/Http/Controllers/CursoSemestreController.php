<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CursoSemestre;
use App\Models\Curso;
use App\Models\EscuelaSemestre;
use App\Models\CursoGrupo;
use App\Models\CursoMatriz;
use App\Models\CursoEsquema;

class CursoSemestreController extends Controller
{
    public function index()
    {
        $cursosemestre = CursoSemestre::with(['curso','escuelasemestre'])->where('estado','A')->get();
        return view('cursosemestre.index',compact('cursosemestre'));
    }
    public function create()
    {
        $cursos = Curso::all();
        $escuelasemestres = EscuelaSemestre::all();
        return view('cursosemestre.create',compact('escuelasemestres','cursos'));
    }
    public function show($id)
    {
        $cursosemestre = CursoSemestre::find($id);
        return view('cursosemestre.show',compact('cursosemestre'));
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['curso_id']= $request['curso']; 
        $input['escuelasemestre_id']= $request['escuelasemestre']; 
        $cursosemestre = CursoSemestre::create($input);

        return view('cursosemestre.index')->with('message','insertado correctamente');
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $cursosemestre = CursoSemestre::find($id);
        $cursosemestre->update($input);
        return view('cursosemestre.index')->with('message','insertado correctamente');
    }
    public function edit($id)
    {
        $escuelasemestres = EscuelaSemestre::all();
        $cursos = Curso::all();
        $cursosemestre = CursoSemestre::findOrFail($id);
        return view('cursosemestre.edit', compact('cursosemestre','cursos','escuelasemestres'));
    }
    public function delete($id)
    {
        $cursosemestre = CursoSemestre::find($id);

        $cursogrupo = CursoGrupo::where('cursosemestre_id',$id)->where('estado','A')->get();

        $cursomatriz = CursoMatriz::where('cursosemestre_id',$id)->where('estado','A')->get();

        $cursoesquema = CursoEsquema::where('cursosemestre_id',$id)->where('estado','A')->get();

        $contador = sizeof($cursogrupo) + sizeof($cursomatriz) + sizeof($cursoesquema);

        if($contador == 0){
            $cursosemestre->estado = 'D';
            $cursosemestre->save();
            return redirect('cursosemestres')->with(['success' => 'Se elimino exitosamente']);
        }

        return redirect('cursosemestres')->with(['error' => 'No se puede eliminar cursosemestre porque tiene relacion con '.$contador.' registros']);
    }
    
    //ajax
    public function insertarCursoSemestre(Request $request)
    {
        $input = $request->all();
        $input['curso_id'] = $request->curso_id;
        $input['escuelasemestre_id'] = $request->escuelasemestre_id;
        $cursosemestre = CursoSemestre::create($input);
        return 1;
    }
    public function eliminarCursoSemestre(Request $request)
    {
        $id = $request->id;
        $cursosemestre = CursoSemestre::findOrFail($id);

        $cursogrupo = CursoGrupo::where('cursosemestre_id',$id)->where('estado','A')->get();

        $cursomatriz = CursoMatriz::where('cursosemestre_id',$id)->where('estado','A')->get();
        
        $cursoesquema = CursoEsquema::where('cursosemestre_id',$id)->where('estado','A')->get();

        $contador = sizeof($cursogrupo) + sizeof($cursomatriz) + sizeof($cursoesquema);

        if($contador == 0){
            $cursosemestre->estado = 'D';
            $cursosemestre->save();
            return 0;
        }

        return $contador;
    }

    public function editarCursoSemestre(Request $request)
    {
        $id = $request->id;
        $cursosemestre = CursoSemestre::findOrFail($id);
        $cursosemestre->curso_id = $request->curso_id;
        $cursosemestre->save();
        return 1;
    }
}
