<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Estudiante;
use App\Models\CursoEstudiante;

class EstudianteController extends Controller
{
   
    public function index()
    {   
        $estudiantes=Estudiante::where('estado','A')->get();

        return view("estudiante.index",compact('estudiantes'));
    }

    public function create()
    {
        return view('estudiante.create');
    }
  
    public function store(Request $request)
    {
        $input = $request->all();
        $estudiante = Estudiante::create($input);
        return redirect('estudiantes')->with('message', 'Guardado Exitosamente');
    }

    public function show($id)
    {
        $estudiante=Estudiante::find($id);
        return view('estudiante.show', compact('estudiante'));
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $estudiante = Estudiante::find($id);
        $estudiante->update($input);
        return redirect('estudiantes')->with('message', 'Actualizado!');
    }


    public function edit($id)
    {
        $estudiante = Estudiante::findOrFail($id);
        return view('estudiante.edit', compact('estudiante'));
    }

    public function delete($id)
    {
        $estudiante = Estudiante::find($id);

        $cursoestudiante = CursoEstudiante::where('estudiante_id',$id)->where('estado','A')->get();

        if(sizeof($cursoestudiante) == 0){
            $estudiante->estado = 'D';
            $estudiante->save();
            return redirect('estudiantes')->with(['success' => 'Se elimino exitosamente']);
        }

        return redirect('estudiantes')->with(['error' => 'No se puede eliminar estudiante porque tiene relacion con '. sizeof($cursoestudiante).' registros']);
    }
    
}

