<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;

use App\Models\Curso;
use App\Models\CursoEstudiante;

use Illuminate\Support\Facades\DB;

use App\Models\Docente;
use App\Models\CursoGrupo;
use App\Models\EscuelaSemestre;
use App\Models\CursoSemestre;
use App\Models\CursoEsquema;
use App\Models\CursoMatriz;
use App\Models\MatrizDetalle;
use App\Models\Semestre;
use App\Models\Escuela;
use App\Models\Estudiante;
use App\Models\EstudianteMatriz;
use App\Models\Criterio;
use App\Models\DatoMatriz;

use App\Exports\MatrizGrupoExport;
use Maatwebsite\Excel\Facades\Excel;

use PDF;

class VistasDocenteControllerLuis extends Controller
{
    public function cargaIndexDocentes(Request $request)
    {
        error_log("INICIO DE INDEX DOCENTE");
        $semestres_n=[];
        $escuelasemestres_n=[];
        $cursosemestres_n=[];
        $cursogrupos_n=[];

        $semestres=[];
        $escuelasemestres=[];
        $cursosemestres=[];
        $cursogrupos=[];
        $escuelas=[];
        $cursos = [];
        $grupos = [];

        $cargo;

        $idDocente = $request->session()->get('id');
        error_log($idDocente);
        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('estado','A')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        error_log($cursogrupos_general);

        //Busca los semestres
        foreach($cursogrupos_general as $item)
        {
            array_push($semestres_n, $item->cursosemestre->escuelasemestre->semestre->id);
        }

        array_unique($semestres_n);
        $semestres = Semestre::whereIn('id',$semestres_n)->where('estado','A')->get();
        error_log("SEMESTRES");
        error_log($semestres);

        //Busca las escuela-semestres
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre_id == $semestres[0]->id)
            {
                array_push($escuelasemestres_n, $item->cursosemestre->escuelasemestre->id);
                error_log("Nuevo dato en EscuelaSemestre_n");
                error_log($item->cursosemestre->escuelasemestre->id);
            }
        }

        array_unique($escuelasemestres_n);
        $escuelasemestres = EscuelaSemestre::whereIn('id',$escuelasemestres_n)->where('estado','A')->get();
        error_log("ESCUELA SEMESTRES");
        error_log($escuelasemestres);

        //Busca los curso-semestres
        foreach($cursogrupos_general as $item)
        {
            foreach($escuelasemestres as $es)
            {
                if($es->id == $item->cursosemestre->escuelasemestre_id)
                {
                    array_push($cursosemestres_n, $item->cursosemestre->id);
                    array_push($cursos, $item->cursosemestre->curso);
                }
            }
        }

        array_unique($cursosemestres_n);
        $cursosemestres = CursoSemestre::whereIn('id',$cursosemestres_n)
            ->where('estado','A')
            ->with(['curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();
        error_log("CURSO SEMESTRES");
        error_log($cursosemestres);

        //Busca los curso-grupos
        foreach($cursogrupos_general as $item)
        {
            if($cursosemestres[0]->id == $item->cursosemestre_id)
                array_push($cursogrupos_n, $item->id);
        }

        array_unique($cursogrupos_n);

        //Busca coordinador
        $micurso = CursoGrupo::where('docente_id',$idDocente)
            ->where('cursosemestre_id',$cursosemestres[0]->id)
            ->get();

        error_log("MI CURSO");
        error_log($micurso);

        //Busca grupos y docente
        if($micurso[0]->coordinador == 1)
        {
            error_log("ES COODINADOR");
            $cursogrupos = CursoGrupo::where('cursosemestre_id',$cursosemestres[0]->id)
                ->where('estado','A')->get();
            $docente = Docente::where('id',$cursogrupos[0]->docente_id)->get();
            $cargo = "Coordinador";
        }
        else
        {
            error_log("NO ES COODINADOR");
            $cursogrupos = $micurso;
            $docente = Docente::where('id',$idDocente)->get();
            $cargo = "Docente";
        }

        error_log("CURSO GRUPOS");
        error_log($cursogrupos);

        error_log("DOCENTE");
        error_log($docente);

        return view('user.docente.index',compact('semestres','cursos','cursogrupos','cargo'));
    }

    public function cargaVistaMatriz(Request $request)
    {
        //PRIMERA SOLUCION
        error_log("INICIO DE MATRIZ");
        $semestres_n=[];
        $escuelasemestres_n=[];
        $cursosemestres_n=[];
        $cursogrupos_n=[];
        //$matrizdetalles_n=[];

        $semestres=[];
        $escuelasemestres=[];
        $cursosemestres=[];
        $cursogrupos=[];
        $escuelas=[];
        $cursos = [];
        $grupos = [];
        $matrizdetalles = [];
        $cursoestudiantes = [];

        $cargo;

        $selected_semestre = $request->semestre;
        $selected_curso = $request->curso;
        $selected_grupo = $request->grupo;

        error_log("VALORES SELECCIONADOS");
        error_log($selected_semestre);
        error_log($selected_curso);
        error_log($selected_grupo);
        error_log("FIN VALORES SELECCIONADOS");

        $micursosemestre;

        $idDocente = $request->session()->get('id');
        //error_log($idDocente);
        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('estado','A')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        error_log($cursogrupos_general);

        //Busca los semestres
        foreach($cursogrupos_general as $item)
        {
            array_push($semestres_n, $item->cursosemestre->escuelasemestre->semestre->id);
        }

        array_unique($semestres_n);
        $semestres = Semestre::whereIn('id',$semestres_n)->where('estado','A')->get();
        error_log("SEMESTRES");
        error_log($semestres);
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_semestre))
        {
            $selected_semestre = $semestres[0]->id;
        }
        //Busca las escuela-semestres
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre_id == $selected_semestre)
            {
                array_push($escuelasemestres_n, $item->cursosemestre->escuelasemestre->id);
                error_log("Nuevo dato en EscuelaSemestre_n");
                error_log($item->cursosemestre->escuelasemestre->id);
            }
        }

        array_unique($escuelasemestres_n);
        $escuelasemestres = EscuelaSemestre::whereIn('id',$escuelasemestres_n)->where('estado','A')->get();
        error_log("ESCUELA SEMESTRES");
        error_log($escuelasemestres);

        //Busca los curso-semestres
        foreach($cursogrupos_general as $item)
        {
            foreach($escuelasemestres as $es)
            {
                if($es->id == $item->cursosemestre->escuelasemestre_id)
                {
                    array_push($cursosemestres_n, $item->cursosemestre->id);
                    array_push($cursos, $item->cursosemestre->curso);
                }
            }
        }

        array_unique($cursosemestres_n);
        $cursosemestres = CursoSemestre::whereIn('id',$cursosemestres_n)
            ->where('estado','A')
            ->with(['curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();
        error_log("CURSO SEMESTRES");
        error_log($cursosemestres);

        //IF para evitar fallos con la entrada rapida
        if(empty($selected_curso))
        {
            $selected_curso = $cursosemestres[0]->curso->id;
            error_log("valor por defecto en selected_curso");
        }
        //Busca el curso semestre selecionado
        foreach($cursosemestres as $item)
        {
            if($item->curso_id == $selected_curso)
            {
                error_log("Selecciono curso semestre");
                $micursosemestre = $item;
                break;
            }
        }

        error_log("MI CURSO SEMESTRES");
        error_log($micursosemestre);

        //Busca los curso-grupos
        foreach($cursogrupos_general as $item)
        {
            if($micursosemestre->id == $item->cursosemestre_id)
                array_push($cursogrupos_n, $item->id);
        }

        array_unique($cursogrupos_n);

        //Busca coordinador
        $micurso = CursoGrupo::where('docente_id',$idDocente)
            ->where('cursosemestre_id',$micursosemestre->id)
            ->get();

        error_log("MI CURSO");
        error_log($micurso);
        //IF para evitar error con entrada GET
        if(empty($selected_grupo))
        {
            $selected_grupo = $cursogrupos[0]->id;
        }
        //Busca grupos y docente
        if($micurso[0]->coordinador == 1)
        {
            error_log("ES COODINADOR");
            $cursogrupos = CursoGrupo::where('cursosemestre_id',$micursosemestre->id)
                ->where('estado','A')
                ->get();
            $micursogrupo = CursoGrupo::where('id',$selected_grupo)
                ->where('estado','A')
                ->get();
            error_log("MI CURSO GRUPO SELECCIONADO");
            error_log($micursogrupo);
            $docente = Docente::where('id',$micursogrupo[0]->docente_id)->get();
            $cargo = "Coordinador";
        }
        else
        {
            error_log("NO ES COODINADOR");
            $cursogrupos = $micurso;
            $docente = Docente::where('id',$idDocente)->get();
            $cargo = "Docente";
        }

        error_log("CURSO GRUPOS");
        error_log($cursogrupos);

        error_log("DOCENTE");
        error_log($docente);

        error_log($micursosemestre);

        //Busca las matrices relacionadas al curso
        $cursomatrizs = CursoMatriz::where('cursosemestre_id',$micursosemestre->id)
            ->where('estado','A')
            /*
            ->with(['matrizdetalles' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['matrizdetalles.criterio' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['matrizdetalles.cursoesquema' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            */
            ->get();

        error_log("MATRICES");
        error_log($cursomatrizs);
        /*
        error_log("Carga de promedios de matrices");
        $cursogrupo_id = $request->cursogrupo_id;

        $cursogrupo = CursoGrupo::where('id',$cursogrupo_id)
            ->where('estado','A')
            ->first();
        error_log("cursogrupo");
        error_log($cursogrupo);
        */

        $cursoestudiantes = CursoEstudiante::where('cursogrupo_id',$selected_grupo)
            ->where('estado','A')
            ->with(['estudiante' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        foreach ($cursoestudiantes as $ce)
        {
            $promedios = [];
            $promedio_final = 0;
            foreach ($cursomatrizs as $cm)
            {
                $promedio_actual = $this->promedioMatrizEstudiante($cm->id, $ce->id);
                array_push($promedios, $promedio_actual);
                $promedio_final = $promedio_final + ($promedio_actual*$cm->ponderacion/100);
            }
            array_push($promedios, $promedio_final);
            $ce->promedios = $promedios;
        }

        /*
        if(sizeof($cursomatrizs)>0)
        {
            $matrizdetalles = MatrizDetalle::where('cursomatriz_id',$cursomatrizs[0]->id)
                ->where('estado','A')
                ->with(['criterio' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursoesquema' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->get();

            error_log("MATRIZ DETALLE ID");

            foreach($matrizdetalles as $item)
            {
                array_push($matrizdetalles_n,$item->id);
                error_log($item->id);
            }

            error_log("MATRICES DETALLE");
            error_log($matrizdetalles);
        }

        //Buscar alumnos del curso seleccionado previamente
        $cursoestudiantes = CursoEstudiante::where('cursogrupo_id',$selected_grupo)
            ->where('estado','A')
            ->with(['estudiante' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['estudiantematrizs' => function ($query) use ($matrizdetalles_n)
            {
                $query->where('estado','A')
                    ->whereIn('matriz_detalle_id',$matrizdetalles_n)
                    ->orderBy('matriz_detalle_id');
            }
            ])
            ->get();

        error_log("CURSO ESTUDIANTES");
        error_log($cursoestudiantes);

        */

        $escuelasemestres = EscuelaSemestre::where('semestre_id',$selected_semestre)->get();
        $cursosemestres = CursoSemestre::where('curso_id',$selected_curso)->get();

        foreach($escuelasemestres as $es){
            foreach($cursosemestres as $cs){
                if($es->id == $cs->escuelasemestre_id)
                    $escuelasemestre = $es;
            }
        }

        return view('user.docente.matriz',compact('semestres','cursos','cursogrupos','docente','selected_semestre','selected_curso','selected_grupo','cargo','cursomatrizs','cursoestudiantes','escuelasemestre'));
    }

    public function cargaEsquemaTrabajo(Request $request)
    {
        //PRIMERA SOLUCION
        error_log("INICIO DE ESQUEMA");
        $semestres_n=[];
        $escuelasemestres_n=[];
        $cursosemestres_n=[];
        $cursogrupos_n=[];

        $semestres=[];
        $escuelasemestres=[];
        $cursosemestres=[];
        $cursogrupos=[];
        $escuelas=[];
        $cursos = [];
        $esquemas = [];

        $selected_semestre = $request->semestre;
        $selected_curso = $request->curso;

        $idDocente = $request->session()->get('id');
        error_log($idDocente);
        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('coordinador','1')
            ->where('estado','A')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        error_log($cursogrupos_general);

        //Busca los semestres
        foreach($cursogrupos_general as $item)
        {
            array_push($semestres_n, $item->cursosemestre->escuelasemestre->semestre->id);
        }

        array_unique($semestres_n);
        $semestres = Semestre::whereIn('id',$semestres_n)->where('estado','A')->get();
        error_log("SEMESTRES");
        error_log($semestres);
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_semestre))
        {
            $selected_semestre = $semestres[0]->id;
        }
        //Busca las escuela-semestres
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre_id == $selected_semestre)
            {
                array_push($escuelasemestres_n, $item->cursosemestre->escuelasemestre->id);
                error_log("Nuevo dato en EscuelaSemestre_n");
                error_log($item->cursosemestre->escuelasemestre->id);
            }
        }

        array_unique($escuelasemestres_n);
        $escuelasemestres = EscuelaSemestre::whereIn('id',$escuelasemestres_n)->where('estado','A')->get();
        error_log("ESCUELA SEMESTRES");
        error_log($escuelasemestres);

        //Busca los curso-semestres
        foreach($cursogrupos_general as $item)
        {
            foreach($escuelasemestres as $es)
            {
                if($es->id == $item->cursosemestre->escuelasemestre_id)
                {
                    array_push($cursosemestres_n, $item->cursosemestre->id);
                    array_push($cursos, $item->cursosemestre->curso);
                    if($item->cursosemestre->curso->id == $selected_curso)
                    {
                        $cursosemestres = $item->cursosemestre;
                    }
                }
            }
        }

        array_unique($cursosemestres_n);
        /*
        $cursosemestres = CursoSemestre::where('id',$cursosemestres_n[0])
            ->where('estado','A')
            ->get();
        error_log("CURSO SEMESTRES");
        error_log($cursosemestres);
        */

        //Busca los curso-esquemas

        $esquemas = CursoEsquema::where('cursosemestre_id',$cursosemestres->id)
            ->where('estado','A')
            ->orderBy('codigo')
            ->get();

        return view('user.docente.esquema',compact('semestres','cursos','esquemas','selected_semestre','selected_curso','cursosemestres'));

    }

    public function cargaMatrizCabecera(Request $request)
    {
        //PRIMERA SOLUCION
        error_log("INICIO DE ESQUEMA");
        $semestres_n=[];
        $escuelasemestres_n=[];
        $cursosemestres_n=[];
        $cursogrupos_n=[];

        $semestres=[];
        $escuelasemestres=[];
        $cursosemestres=[];
        $cursogrupos=[];
        $escuelas=[];
        $cursos = [];
        $esquemas = [];

        $selected_semestre = $request->semestre;
        $selected_curso = $request->curso;

        $idDocente = $request->session()->get('id');
        error_log($idDocente);
        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('coordinador','1')
            ->where('estado','A')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        error_log($cursogrupos_general);

        //Busca los semestres
        foreach($cursogrupos_general as $item)
        {
            array_push($semestres_n, $item->cursosemestre->escuelasemestre->semestre->id);
        }

        array_unique($semestres_n);
        $semestres = Semestre::whereIn('id',$semestres_n)->where('estado','A')->get();
        error_log("SEMESTRES");
        error_log($semestres);
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_semestre))
        {
            $selected_semestre = $semestres[0]->id;
        }
        //Busca las escuela-semestres
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre_id == $selected_semestre)
            {
                array_push($escuelasemestres_n, $item->cursosemestre->escuelasemestre->id);
                error_log("Nuevo dato en EscuelaSemestre_n");
                error_log($item->cursosemestre->escuelasemestre->id);
            }
        }

        array_unique($escuelasemestres_n);
        $escuelasemestres = EscuelaSemestre::whereIn('id',$escuelasemestres_n)->where('estado','A')->get();
        error_log("ESCUELA SEMESTRES");
        error_log($escuelasemestres);

        //Busca los curso-semestres
        foreach($cursogrupos_general as $item)
        {
            foreach($escuelasemestres as $es)
            {
                if($es->id == $item->cursosemestre->escuelasemestre_id)
                {
                    array_push($cursosemestres_n, $item->cursosemestre->id);
                    array_push($cursos, $item->cursosemestre->curso);
                    if($item->cursosemestre->curso->id == $selected_curso)
                    {
                        $cursosemestres = $item->cursosemestre;
                    }
                }
            }
        }

        array_unique($cursosemestres_n);
        /*
        $cursosemestres = CursoSemestre::where('id',$cursosemestres_n[0])
            ->where('estado','A')
            ->get();
        error_log("CURSO SEMESTRES");
        error_log($cursosemestres);
        */

        //Busca los curso-esquemas

        $cursomatrizs = CursoMatriz::where('cursosemestre_id',$cursosemestres->id)
            ->where('estado','A')
            ->get();

        $contador = 1;
        foreach($cursomatrizs as $item)
        {
            $item->minumero = $contador;
            $contador = $contador + 1;
        }

        error_log($cursomatrizs);

        return view('user.docente.matrizCabecera',compact('semestres','cursos','cursomatrizs','selected_semestre','selected_curso','cursosemestres'));

    }

    public function cargaNotasAlumno(Request $request)
    {
        //PRIMERA SOLUCION
        error_log("INICIO DE CARGA NOTAS");
        $semestres_n=[];
        $escuelasemestres_n=[];
        $cursosemestres_n=[];
        $cursogrupos_n=[];
        $cursomatrizs_n=[];
        $cursoesquemas_n=[];

        $semestres=[];
        $escuelasemestres=[];
        $cursosemestres=[];
        $cursogrupos=[];
        $escuelas=[];
        $cursos = [];
        $grupos = [];
        $matrizdetalles = [];
        $cursoestudiantes = [];

        $cargo;

        $promedio_final=0;
        $promedios = [];

        $selected_semestre = $request->semestre;
        $selected_curso = $request->curso;
        $selected_grupo = $request->grupo;
        $selected_alumno = $request->AlumnoID;
        $selected_matriz = $request->matriz;

        error_log("VALORES SELECCIONADOS");
        error_log($selected_semestre);
        error_log($selected_curso);
        error_log($selected_grupo);
        error_log($selected_alumno);
        error_log($selected_matriz);
        error_log("FIN VALORES SELECCIONADOS");

        $micursosemestre;

        $idDocente = $request->session()->get('id');
        //error_log($idDocente);
        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('estado','A')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        error_log($cursogrupos_general);

        //Busca los semestres
        foreach($cursogrupos_general as $item)
        {
            array_push($semestres_n, $item->cursosemestre->escuelasemestre->semestre->id);
        }

        array_unique($semestres_n);
        $semestres = Semestre::whereIn('id',$semestres_n)->where('estado','A')->get();
        error_log("SEMESTRES");
        error_log($semestres);
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_semestre))
        {
            $selected_semestre = $semestres[0]->id;
        }
        //Busca las escuela-semestres
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre_id == $selected_semestre)
            {
                array_push($escuelasemestres_n, $item->cursosemestre->escuelasemestre->id);
                error_log("Nuevo dato en EscuelaSemestre_n");
                error_log($item->cursosemestre->escuelasemestre->id);
            }
        }

        array_unique($escuelasemestres_n);
        $escuelasemestres = EscuelaSemestre::whereIn('id',$escuelasemestres_n)->where('estado','A')->get();
        error_log("ESCUELA SEMESTRES");
        error_log($escuelasemestres);

        //Busca los curso-semestres
        foreach($cursogrupos_general as $item)
        {
            foreach($escuelasemestres as $es)
            {
                if($es->id == $item->cursosemestre->escuelasemestre_id)
                {
                    array_push($cursosemestres_n, $item->cursosemestre->id);
                    array_push($cursos, $item->cursosemestre->curso);
                }
            }
        }

        array_unique($cursosemestres_n);
        $cursosemestres = CursoSemestre::whereIn('id',$cursosemestres_n)
            ->where('estado','A')
            ->with(['curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();
        error_log("CURSO SEMESTRES");
        error_log($cursosemestres);

        //IF para evitar fallos con la entrada rapida
        if(empty($selected_curso))
        {
            $selected_curso = $cursosemestres[0]->curso->id;
            error_log("valor por defecto en selected_curso");
        }
        //Busca el curso semestre selecionado
        foreach($cursosemestres as $item)
        {
            if($item->curso_id == $selected_curso)
            {
                error_log("Selecciono curso semestre");
                $micursosemestre = $item;
                break;
            }
        }

        error_log("MI CURSO SEMESTRES");
        error_log($micursosemestre);

        //Busca los curso-grupos
        foreach($cursogrupos_general as $item)
        {
            if($micursosemestre->id == $item->cursosemestre_id)
                array_push($cursogrupos_n, $item->id);
        }

        array_unique($cursogrupos_n);

        //Busca coordinador
        $micurso = CursoGrupo::where('docente_id',$idDocente)
            ->where('cursosemestre_id',$micursosemestre->id)
            ->get();

        error_log("MI CURSO");
        error_log($micurso);

        //Busca grupos y docente
        if($micurso[0]->coordinador == 1)
        {
            error_log("ES COODINADOR");
            $cursogrupos = CursoGrupo::where('cursosemestre_id',$micursosemestre->id)
                ->where('estado','A')
                ->get();
            //IF para evitar error con entrada GET
            if(empty($selected_grupo))
            {
                $selected_grupo = $cursogrupos[0]->id;
            }

            $micursogrupo = CursoGrupo::where('id',$selected_grupo)
                ->where('estado','A')
                ->get();
            error_log("MI CURSO GRUPO SELECCIONADO");
            error_log($micursogrupo);
            $docente = Docente::where('id',$micursogrupo[0]->docente_id)->get();
            $cargo = "Coordinador";
        }
        else
        {
            error_log("NO ES COODINADOR");
            $cursogrupos = $micurso;
            $docente = Docente::where('id',$idDocente)->get();
            $cargo = "Docente";
        }

        error_log("CURSO GRUPOS");
        error_log($cursogrupos);

        error_log("DOCENTE");
        error_log($docente);

        error_log($micursosemestre);

        //IF para evitar error con entrada GET
        if(empty($selected_grupo))
        {
            $selected_grupo = $cursogrupos[0]->id;
        }
        //Buscar alumnos del curso seleccionado previamente
        $cursoestudiantes = CursoEstudiante::where('cursogrupo_id',$selected_grupo)
            ->where('estado','A')
            ->with(['estudiante' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        error_log("CURSO ESTUDIANTES");
        error_log($cursoestudiantes);

        //Busca las matrices relacionadas al curso
        $cursomatrizs = CursoMatriz::where('cursosemestre_id',$micursosemestre->id)
            ->where('estado','A')
            ->get();

        error_log("MATRICES");
        error_log($cursomatrizs);

        //Busca los esquemas relacionadas al curso
        $cursoesquemas = CursoEsquema::where('cursosemestre_id',$micursosemestre->id)
            ->where('estado','A')
            ->orderBy('codigo')
            ->get();

        //IF para evitar error con entrada GET
        if(empty($selected_alumno))
        {
            $selected_alumno = $cursoestudiantes[0]->id;
        }
        error_log("VALOR DE SELECTED ALUMNO DESPUES DE IF");
        error_log($selected_alumno);

        //carga id's de las matrices
        foreach ($cursomatrizs as $item) 
        {
            array_push($cursomatrizs_n, $item->id);
            $promedios[$item->id] = $this->promedioMatrizEstudiante($item->id, $selected_alumno);
            $promedio_final = $promedio_final + $promedios[$item->id]*$item->ponderacion/100;
        }

        //$promedio_final = round($promedio_final, 2);

        //carga id's de los esquemas
        foreach ($cursoesquemas as $item) 
        {
            $mis_notas = [];
            array_push($cursoesquemas_n, $item->id);
            $matrizdetalles = MatrizDetalle::where('cursoesquema_id',$item->id)
                /*
                ->with(['estudiantematrizs',function ($query) use($selected_alumno)
                {
                    $query->where('estado','A')
                        ->where('curso_estudiante_id',$selected_alumno);
                }
                ])
                */
                ->where('estado','A')
                ->orderBy('cursomatriz_id')
                ->get();
            foreach ($matrizdetalles as $md)
            {
                $nota = EstudianteMatriz::where('matriz_detalle_id',$md->id)
                    ->where('estado','A')
                    ->where('curso_estudiante_id',$selected_alumno)
                    ->get();
                $mis_notas[$md->cursomatriz_id] = $nota;

            }
            $item->notas = $mis_notas;
        }

        error_log("MATRICES ESQUEMA");
        error_log($cursoesquemas);
/*
        //POSIBLE COMENTARIO
        if(sizeof($cursomatrizs)>0)
        {
            //IF para evitar error con entrada GET
            if(empty($selected_matriz))
            {
                $selected_matriz = $cursomatrizs[0]->id;
            }
            $matrizdetalles = MatrizDetalle::where('cursomatriz_id',$selected_matriz)
                ->where('estado','A')
                ->orderBy('cursoesquema_id')
                ->with(['cursoesquema' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['estudiantematrizs' => function ($query) use($selected_alumno)
                {
                    $query->where('estado','A')
                        ->where('curso_estudiante_id',$selected_alumno);
                }
                ])

                ->get();

            error_log("MATRICES DETALLE");
            error_log($matrizdetalles);
        }
*/
        $datomatrizs = DatoMatriz::where('curso_estudiante_id', $selected_alumno)
            ->where('curso_matriz_id', $cursomatrizs[0]->id)
            ->where('estado','A')
            ->get();

        return view('user.docente.docente_notas',compact('semestres','cursos','cursogrupos','docente','selected_semestre','selected_curso','selected_grupo','selected_alumno','selected_matriz','cargo','cursoesquemas','cursomatrizs','cursoestudiantes','matrizdetalles','cursomatrizs_n','cursoesquemas_n','promedios','promedio_final','datomatrizs'));
    }

    //Funciones para carga AJAX

    public static function cargaMatrizAlumno(Request $request)
    {
        $cursomatriz_id = $request->cursomatriz_id;
        $cursoestudiante_id = $request->cursoestudiante_id;
        $matrizdetalles;

        //$micursosemestre_id;

        $cursoesquemas_n = [];

        $uncursomatriz = CursoMatriz::where('id',$cursomatriz_id)
            ->where('estado','A')
            ->get();

        error_log("CARGAMATRIZALUMNO AJAX");
        error_log($uncursomatriz);

        $micursosemestre_id = $uncursomatriz[0]->cursosemestre_id;

        //Busca las matrices relacionadas al curso
        $cursomatrizs = CursoMatriz::where('cursosemestre_id',$micursosemestre_id)
            ->where('estado','A')
            ->get();

        //Busca los esquemas relacionadas al curso
        $cursoesquemas = CursoEsquema::where('cursosemestre_id',$micursosemestre_id)
            ->where('estado','A')
            ->orderBy('codigo')
            ->get();

        //carga id's de los esquemas
        foreach ($cursoesquemas as $item) 
        {
            $mis_notas = [];
            array_push($cursoesquemas_n, $item->id);
            $matrizdetalles = MatrizDetalle::where('cursoesquema_id',$item->id)
                /*
                ->with(['estudiantematrizs',function ($query) use($selected_alumno)
                {
                    $query->where('estado','A')
                        ->where('curso_estudiante_id',$selected_alumno);
                }
                ])
                */
                ->where('estado','A')
                ->orderBy('cursomatriz_id')
                ->get();
            foreach ($matrizdetalles as $md)
            {
                $nota = EstudianteMatriz::where('matriz_detalle_id',$md->id)
                    ->where('estado','A')
                    ->where('curso_estudiante_id',$cursoestudiante_id)
                    ->get();
                $mis_notas[$md->cursomatriz_id] = $nota;

            }
            $item->notas = $mis_notas;
        }

        /*
        
        $matrizdetalles = MatrizDetalle::where('cursomatriz_id',$cursomatriz_id)
            ->where('estado','A')
            ->orderBy('cursoesquema_id')
            ->with(['cursoesquema' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['estudiantematrizs' => function ($query) use($cursoestudiante_id)
            {
                $query->where('estado','A')
                    ->where('curso_estudiante_id',$cursoestudiante_id);
            }
            ])
            ->get();
        */
        return response()->json
        ([
            'cursoesquemas_n' => $cursoesquemas_n,
            'cursoesquemas' => $cursoesquemas
        ]);
    }

    public static function cargaCursosDocenteEspecifico(Request $request)
    {
        $semestre_id = $request->semestre_id;
        $verificar_coordinador = $request->coordinador;
        $escuela_id = $request->escuela_id;
        $cursos=[];
        $idDocente = $request->session()->get('id');

        if(empty($verificar_coordinador))
        {   
            $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
                ->where('estado','A')
                ->with(['cursosemestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.curso' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->get();
        }
        else
        {
            $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
                ->where('coordinador','1')
                ->where('estado','A')
                ->with(['cursosemestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.curso' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->get();
        }
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre->id == $semestre_id)
            {
                array_push($cursos, $item->cursosemestre->curso);
            }
        }

        $escuelasemestre = EscuelaSemestre::where('semestre_id',$semestre_id)->where('escuela_id',$escuela_id)->first();

        return response()->json
        ([
            'cursos' => $cursos,
            'escuelasemestre' => $escuelasemestre
        ]);
    }

    public static function cargaGruposDocenteEspecifico(Request $request)
    {
        error_log("AJAX DE CURSOS");
        $curso_id = $request->curso_id;
        $semestre_id = $request->semestre_id;
        $grupos=[];
        $idDocente = $request->session()->get('id');
        $micurso_n;
        $micursosemestre_n;
        $docente;
        $cargo;

        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('estado','A')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();


        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->curso->id == $curso_id && $item->cursosemestre->escuelasemestre->semestre->id == $semestre_id)
            {
                $micurso_n = $item->id;
                $micursosemestre_n = $item->cursosemestre->id;
                error_log("CURSO ID");
                error_log($micurso_n);
            }
        }

        $micurso = CursoGrupo::where('id',$micurso_n)
            ->get();

        if($micurso[0]->coordinador == 1)
        {
            error_log("ES COODINADOR");
            $grupos = CursoGrupo::where('cursosemestre_id',$micursosemestre_n)
                ->where('estado','A')->get();
            $docente = Docente::where('id',$grupos[0]->docente_id)->get();
            $cargo = "Coordinador";
        }
        else
        {
            error_log("NO ES COODINADOR");
            $grupos = $micurso;
            $docente = Docente::where('id',$idDocente)->get();
            $cargo = "Docente";
        }
        error_log("MI DOCENTE");
        error_log($docente);
        return response()->json
        ([
            'grupos' => $grupos,
            'docente' => $docente,
            'cargo' => $cargo
        ]);
    }

    public static function cargaDocenteEspecifico(Request $request)
    {
        error_log("CARGA DE DOCENTE");
        $grupo_id = $request->grupo_id;

        $micurso = CursoGrupo::where('id',$grupo_id)
            ->with(['docente' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();
        error_log("MI CURSO");
        error_log($micurso);

        $docente = $micurso[0]->docente;

        error_log("MI DOCENTE");
        error_log($docente);

        return response()->json
        ([
            'docente' => $docente
        ]);
    }

    public static function cargaEsquemasCurso(Request $request)
    {
        error_log("AJAX DE ESQUEMAS");
        $curso_id = $request->curso_id;
        $semestre_id = $request->semestre_id;
        $grupos=[];
        $idDocente = $request->session()->get('id');
        $micurso_n;
        $micursosemestre_n;
        $docente;

        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('coordinador','1')
            ->where('estado','A')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();


        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->curso->id == $curso_id && $item->cursosemestre->escuelasemestre->semestre->id == $semestre_id)
            {
                $micurso_n = $item->id;
                $micursosemestre_n = $item->cursosemestre->id;
                error_log("CURSO GRUPO ID");
                error_log($micurso_n);
            }
        }

        error_log("CURSO SEMESTRES AJAX");
        error_log($micursosemestre_n);

        $micurso = CursoGrupo::where('id',$micurso_n)
            ->get();

        if($micurso[0]->coordinador == 1)
        {
            error_log("ES COODINADOR");
            $grupos = CursoGrupo::where('cursosemestre_id',$micursosemestre_n)
                ->where('estado','A')->get();
            $docente = Docente::where('id',$grupos[0]->docente_id)->get();
        }

        $cursoesquemas = CursoEsquema::where('cursosemestre_id',$micursosemestre_n)
            ->where('estado','A')
            ->orderBy('codigo')
            ->get();

        return response()->json
        ([
            'cursoesquemas' => $cursoesquemas,
            'cursosemestre' => $micursosemestre_n
        ]);
    }

    public static function cargaCursoMatriz(Request $request)
    {
        $grupo_id = $request->grupo_id;

        $cursomatrizs_n = [];

        $cursogrupo = CursoGrupo::where('id',$grupo_id)
            ->where('estado','A')
            ->first();

        $cursomatrizs = CursoMatriz::where('cursosemestre_id',$cursogrupo->cursosemestre_id)
            ->where('estado','A')
            ->get();

        foreach ($cursomatrizs as $cm)
        {
            array_push($cursomatrizs_n, $cm->id);
        }

        return response()->json
        ([
            'cursomatrizs' => $cursomatrizs,
            'cursomatrizs_n' => $cursomatrizs_n
        ]);
    }

    public static function cargaMatrizDetalle(Request $request)
    {
        error_log("AJAX CARGA MATRIZ DETALLE");
        $cursomatriz_id = $request->cursomatriz_id;

        $matrizdetalles = MatrizDetalle::where('cursomatriz_id',$cursomatriz_id)
            ->where('estado','A')
            //->orderBy('cursoesquema_id')
            ->with(['criterio' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursoesquema' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        $matrizdetalles_sort = $matrizdetalles->sortBy(function ($item) 
        {
            return $item->cursoesquema->codigo;
        });
        $matrizdetalles_sort->values()->all();

        $new = $matrizdetalles_sort->flatten();

        error_log($new);
        return response()->json
        ([
            'matrizdetalles' => $new
        ]);
    }

    public static function cargaMatrizsCurso(Request $request)
    {
        error_log("AJAX DE ESQUEMAS");
        $curso_id = $request->curso_id;
        $semestre_id = $request->semestre_id;
        $grupos=[];
        $idDocente = $request->session()->get('id');
        $micurso_n;
        $micursosemestre_n;
        $docente;

        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('coordinador','1')
            ->where('estado','A')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();


        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->curso->id == $curso_id && $item->cursosemestre->escuelasemestre->semestre->id == $semestre_id)
            {
                $micurso_n = $item->id;
                $micursosemestre_n = $item->cursosemestre->id;
                error_log("CURSO GRUPO ID");
                error_log($micurso_n);
            }
        }

        error_log("CURSO SEMESTRES AJAX");
        error_log($micursosemestre_n);

        $micurso = CursoGrupo::where('id',$micurso_n)
            ->get();

        if($micurso[0]->coordinador == 1)
        {
            error_log("ES COODINADOR");
            $grupos = CursoGrupo::where('cursosemestre_id',$micursosemestre_n)
                ->where('estado','A')->get();
            $docente = Docente::where('id',$grupos[0]->docente_id)->get();
        }

        $cursomatrizs = CursoMatriz::where('cursosemestre_id',$micursosemestre_n)
            ->where('estado','A')
            ->get();

        return response()->json
        ([
            'cursomatrizs' => $cursomatrizs,
            'cursosemestre' => $micursosemestre_n
        ]);
    }

    public static function cargaNotasDetalle(Request $request)
    {
        error_log("CARGA NOTAS DETALLE");
        $cursomatriz_id = $request->cursomatriz_id;
        $cursogrupo_id = $request->cursogrupo_id;

        $matrizdetalles_n = [];

        $matrizdetalles = MatrizDetalle::where('cursomatriz_id',$cursomatriz_id)
            ->where('estado','A')
            //->orderBy('cursoesquema_id')
            ->get();

        $matrizdetalles = $matrizdetalles->sortBy(function ($item) 
        {
            return $item->cursoesquema->codigo;
        });

        $matrizdetalles->values()->all();

        $matrizdetalles_sort = $matrizdetalles->flatten();

        foreach($matrizdetalles_sort as $item)
        {
            array_push($matrizdetalles_n,$item->id);
            error_log($item->id);
        }

        error_log("MATRICES DETALLE");
        error_log($matrizdetalles);
    

        //Buscar alumnos del curso seleccionado previamente
        $cursoestudiantes = CursoEstudiante::where('cursogrupo_id',$cursogrupo_id)
            ->where('estado','A')
            ->with(['estudiante' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['estudiantematrizs' => function ($query) use ($matrizdetalles_n)
            {
                $query->where('estado','A')
                    ->whereIn('matriz_detalle_id',$matrizdetalles_n);
            }
            ])
            ->get();

        foreach($cursoestudiantes as $ce)
        {
            $misNotas = [];
            foreach($ce->estudiantematrizs as $em)
            {
                $misNotas[$em->matriz_detalle_id] = $em; 
            }
            $ce->notas = $misNotas;
            $ce->promedio_matriz = VistasDocenteControllerLuis::promedioMatrizEstudiante($cursomatriz_id,$ce->id);
        }

        error_log("CURSO ESTUDIANTE");
        error_log($cursoestudiantes);

        return response()->json
        ([
            'cursoestudiantes' => $cursoestudiantes,
            'matrizdetalles_n' => $matrizdetalles_n
        ]);
    }

    public function cargaMatrizPromedio(Request $request)
    {
        error_log("CARGA DE PROMEDIOS CABECERA");
        $grupo_id = $request->grupo_id;
        $curso_estudiante_id = $request->curso_estudiante_id;

        $promedios = [];
        $promedio_final = 0;

        $cursogrupo = CursoGrupo::where('id',$grupo_id)
            ->where('estado','A')
            ->first();

        $cursomatrizs = CursoMatriz::where('cursosemestre_id',$cursogrupo->cursosemestre_id)
            ->where('estado','A')
            ->get();

        foreach ($cursomatrizs as $item)
        {
            $promedios[$item->id] = $this->promedioMatrizEstudiante($item->id, $curso_estudiante_id);
            $promedio_final = $promedio_final + $item->ponderacion*$promedios[$item->id]/100;
        }

        error_log("nota promedio");
        error_log($promedio_final);

        return response()->json
        ([
            'promedios' => $promedios,
            'promedio_final' => $promedio_final
        ]);
    }

    public function cargaMatrizPromedios(Request $request)
    {
        error_log("Carga de promedios de matrices");
        $cursogrupo_id = $request->cursogrupo_id;

        $cursogrupo = CursoGrupo::where('id',$cursogrupo_id)
            ->where('estado','A')
            ->first();
        error_log("cursogrupo");
        error_log($cursogrupo);
        $cursomatrizs = CursoMatriz::where('cursosemestre_id',$cursogrupo->cursosemestre_id)
            ->where('estado','A')
            ->get();

        $cursoestudiantes = CursoEstudiante::where('cursogrupo_id',$cursogrupo_id)
            ->where('estado','A')
            ->with(['estudiante' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        foreach ($cursoestudiantes as $ce)
        {
            $promedios = [];
            $promedio_final = 0;
            foreach ($cursomatrizs as $cm)
            {
                $promedio_actual = $this->promedioMatrizEstudiante($cm->id, $ce->id);
                array_push($promedios, $promedio_actual);
                $promedio_final = $promedio_final + ($promedio_actual*$cm->ponderacion/100);
            }
            array_push($promedios, $promedio_final);
            $ce->promedios = $promedios;
        }

        return response()->json
        ([
            'cursomatrizs' => $cursomatrizs,
            'cursoestudiantes' => $cursoestudiantes
        ]);
    }

    public function cargaDatoMatriz(Request $request)
    {
        error_log("Carga los datos de la matriz especificada");
        $curso_matriz_id = $request->curso_matriz_id;
        $curso_estudiante_id = $request->curso_estudiante_id;

        $datomatriz = DatoMatriz::where('estado','A')
            ->where('curso_matriz_id',$curso_matriz_id)
            ->where('curso_estudiante_id',$curso_estudiante_id)
            ->get();

        return response()->json
        ([
            'datomatriz' => $datomatriz
        ]);
    }

    //funciones auxiliares

    public static function promedioMatrizEstudiante($curso_matriz_id, $curso_estudiante_id)
    {
        $promedio = 0.00;
        $notas = MatrizDetalle::where('estado','A')
            ->where('cursomatriz_id',$curso_matriz_id)
            ->with(['estudiantematrizs' => function ($query) use ($curso_estudiante_id)
            {
                $query->where('estado','A')
                ->where('curso_estudiante_id',$curso_estudiante_id);
            }
            ])
            ->get();

        foreach ($notas as $nota) 
        {
            if(isset($nota->estudiantematrizs[0]->nota))
            {
                error_log("Mi nota: ".$nota->estudiantematrizs[0]->nota);
                error_log("Mi ponderacion: ".$nota->ponderacion);
                $promedio = $promedio + ($nota->estudiantematrizs[0]->nota*$nota->ponderacion/100)
                ;
                error_log("Mi promedio actual: ".$nota->estudiantematrizs[0]->nota);
            }
        }

        return $promedio;
    }

    //funciones de exportacion de datos

    public static function exportarMatrizGrupo(Request $request)
    {
        $curso_id = $request->curso;
        $semestre_id = $request->semestre;
        $cursogrupo_id = $request->grupo;

        error_log("exportarCursoMatrizAlumno");
        error_log($cursogrupo_id);

        $curso = Curso::where('id',$curso_id)
            ->where('estado','A')
            ->with('plan.escuela')
            ->firstOrFail();

        //$escuela = Escuela::where('id',$cursosemestre[0]->escuelasemestre->escuela_id)->where('estado','A')->firstOrFail();
        $escuela = $curso->plan->escuela;

        $semestre = Semestre::where('id',$semestre_id)->where('estado','A')->firstOrFail();

        $cursogrupo = CursoGrupo::where('id',$cursogrupo_id)
            ->where('estado','A')
            ->firstOrFail();

        $docente = Docente::where('id',$cursogrupo->docente_id)->where('estado','A')->firstOrFail();

        $cursomatrizs = CursoMatriz::where('cursosemestre_id',$cursogrupo->cursosemestre_id)
            ->where('estado','A')
            ->firstOrFail();

        $cursoestudiantes = CursoEstudiante::where('cursogrupo_id',$cursogrupo_id)
            ->where('estado','A')
            ->with('estudiante')
            ->firstOrFail();

        $matrizdetalles = MatrizDetalle::where('cursomatriz_id', $cursomatrizs->id)
            ->with(['cursoesquema' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['estudiantematrizs' => function ($query) use($cursoestudiantes)
            {
                $query->where('estado','A')
                    ->where('curso_estudiante_id',$cursoestudiantes->id);
            }
            ])
            ->where('estado','A')
            ->orderBy('cursoesquema_id')
            ->get();

        /*
        return Excel::download(new AlumnoExport($escuela,$semestre,$curso,$cursogrupo,$docente), $semestre->anio.'-'.$semestre->semestre.'_'.$curso->codigo.'_'.$cursogrupo->grupo.'.xlsx');
        */

        return (new MatrizGrupoExport($escuela,$semestre,$curso,$cursogrupo,$docente,$cursomatrizs,$cursoestudiantes,$matrizdetalles))->download('CONT1 '.$semestre->anio.$semestre->semestre.' '.$curso->curso.' Grupo-'.$cursogrupo->grupo.'.pdf', \Maatwebsite\Excel\Excel::DOMPDF);
    }

    //reporte de pdf por alumno
    public function matrizAlumnoPDF(Request $request)
    {
        error_log("PDF PRUEBA MATRIZ ALUMNO");

        $curso_id = $request->curso;
        $semestre_id = $request->semestre;
        $cursogrupo_id = $request->grupo;
        $curso_estudiante_id = $request->alumno;
        $matriz_id = $request->matrizid;

        $curso = Curso::where('id',$curso_id)
            ->where('estado','A')
            ->with('plan.escuela')
            ->firstOrFail();

        $escuela = $curso->plan->escuela;

        $semestre = Semestre::where('id',$semestre_id)->where('estado','A')->firstOrFail();

        $cursogrupo = CursoGrupo::where('id',$cursogrupo_id)
            ->where('estado','A')
            ->firstOrFail();

        $docente = Docente::where('id',$cursogrupo->docente_id)->where('estado','A')->firstOrFail();

        $cursomatrizs = CursoMatriz::where('id',$matriz_id)
            ->where('estado','A')
            ->firstOrFail();

        $cursoestudiantes = CursoEstudiante::where('id',$curso_estudiante_id)
            ->where('estado','A')
            ->get();

        error_log("curso estudiante");
        error_log($cursoestudiantes);

        $matrizdetalles = MatrizDetalle::where('cursomatriz_id', $cursomatrizs->id)
            ->with(['cursoesquema' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['estudiantematrizs' => function ($query) use($cursoestudiantes)
            {
                $query->where('estado','A')
                    ->where('curso_estudiante_id',$cursoestudiantes[0]->id);
            }
            ])
            ->where('estado','A')
            ->orderBy('cursoesquema_id')
            ->get();

        $matrizdetalles = $matrizdetalles->sortBy(function ($item) 
        {
            return $item->cursoesquema->codigo;
        });

        $matrizdetalles->values()->all();

        $matrizdetalles_sort = $matrizdetalles->flatten();

        $cursoestudiantes[0]->matrizdetalles_sort = $matrizdetalles_sort;

        $cursoestudiantes[0]->promedio = $this->promedioMatrizEstudiante($matriz_id, $curso_estudiante_id);

        $datomatriz = DatoMatriz::where('estado','A')
            ->where('curso_matriz_id',$matriz_id)
            ->where('curso_estudiante_id',$curso_estudiante_id)
            ->get();

        $cursoestudiantes[0]->datomatriz = $datomatriz;
        //devolver vista
        //return view('export.exportMatrizGrupo',compact('escuela','semestre','curso','cursogrupo','docente','cursomatrizs','cursoestudiantes'));

        //generar  DOMPDF
        //$pdf = PDF::loadView('export.exportMatrizGrupo',compact('escuela','semestre','curso','cursogrupo','docente','cursomatrizs','cursoestudiantes'));
        //$pdf->set_paper (‘a4′,’landscape’);

        //dercargar  DOMPDF
        //return $pdf->download($semestre->anio.$semestre->semestre.' '.$curso->curso.' Grupo-'.$cursogrupo->grupo.' '.$cursomatrizs->matriz.' '.$cursoestudiantes[0]->estudiante->estudiante.'.pdf');

        $vista = view('export.exportMatrizGrupo',compact('escuela','semestre','curso','cursogrupo','docente','cursomatrizs','cursoestudiantes'))->render();

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont = true;

        $mpdf->WriteHTML($vista);
        $mpdf->Output();
        
    }


    //reporte de pdf por grupo
    public function matrizGrupoPDF(Request $request)
    {
        error_log("PDF PRUEBA MATRIZ GRUPO");

        $curso_id = $request->curso;
        $semestre_id = $request->semestre;
        $cursogrupo_id = $request->grupo;
        $matriz_id = $request->matrizid;

        $curso = Curso::where('id',$curso_id)
            ->where('estado','A')
            ->with('plan.escuela')
            ->firstOrFail();

        $escuela = $curso->plan->escuela;

        $semestre = Semestre::where('id',$semestre_id)->where('estado','A')->firstOrFail();

        $cursogrupo = CursoGrupo::where('id',$cursogrupo_id)
            ->where('estado','A')
            ->firstOrFail();

        $docente = Docente::where('id',$cursogrupo->docente_id)->where('estado','A')->firstOrFail();

        $cursomatrizs = CursoMatriz::where('id',$matriz_id)
            ->where('estado','A')
            ->firstOrFail();

        $cursoestudiantes = CursoEstudiante::where('cursogrupo_id',$cursogrupo_id)
            ->where('estado','A')
            ->get();

        foreach ($cursoestudiantes as $ce)
        {
            $matrizdetalles = MatrizDetalle::where('cursomatriz_id', $cursomatrizs->id)
                ->with(['cursoesquema' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['estudiantematrizs' => function ($query) use($ce)
                {
                    $query->where('estado','A')
                        ->where('curso_estudiante_id',$ce->id);
                }
                ])
                ->where('estado','A')
                ->orderBy('cursoesquema_id')
                ->get();

            $matrizdetalles = $matrizdetalles->sortBy(function ($item) 
            {
                return $item->cursoesquema->codigo;
            });

            $matrizdetalles->values()->all();

            $matrizdetalles_sort = $matrizdetalles->flatten();

            $ce->matrizdetalles_sort = $matrizdetalles_sort;

            $ce->promedio = $this->promedioMatrizEstudiante($matriz_id, $ce->id);

            $datomatriz = DatoMatriz::where('estado','A')
            ->where('curso_matriz_id',$matriz_id)
            ->where('curso_estudiante_id',$ce->id)
            ->get();

            $ce->datomatriz = $datomatriz;
        }

        error_log("CURSOESTUDIANTES");
        error_log($cursoestudiantes);



        //return view('export.exportMatrizGrupo',compact('escuela','semestre','curso','cursogrupo','docente','cursomatrizs','cursoestudiantes'));


        //$pdf = PDF::loadView('export.exportMatrizGrupo',compact('escuela','semestre','curso','cursogrupo','docente','cursomatrizs','cursoestudiantes'));
        //$pdf->set_paper (‘a4′,’landscape’);

        //return $pdf->download($semestre->anio.$semestre->semestre.' '.$curso->curso.' Grupo-'.$cursogrupo->grupo.' '.$cursomatrizs->matriz.'.pdf');


//MPDF
        $vista = view('export.exportMatrizGrupo',compact('escuela','semestre','curso','cursogrupo','docente','cursomatrizs','cursoestudiantes'))->render();
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont = true;
        $mpdf->use_kwt = true;

        $mpdf->WriteHTML($vista);
        $mpdf->Output();
        
    }

}

