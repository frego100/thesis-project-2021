<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DatoMatriz;

class DatoMatrizController extends Controller
{
    //determinar que hacer al insertar nota
    public function modificarDatoMatriz(Request $request)
    {
        $dato_matriz_id = $request->dato_matriz_id;
        if($dato_matriz_id == 0)
        {
            $respuesta = DatoMatrizController::insertarDatoMatriz($request);
        }
        else
        {
            $respuesta = DatoMatrizController::editarDatoMatriz($request);
        }
        return $respuesta;
    }

    //ajax
    public function insertarDatoMatriz(Request $request)
    {
        $resultado = 'insertar';
        //$input = $request->all();
        $input = [];
        $input['titulo'] = '';
        $input['resumen'] = '';
        $input['curso_matriz_id'] = $request->curso_matriz_id;
        $input['curso_estudiante_id'] = $request->curso_estudiante_id;

        $input[$request->ndato] = $request->dato;

        $datomatriz = DatoMatriz::create($input);
        
        return response()->json
        ([
            'resultado' => $resultado,
            'nid' => $datomatriz->id
        ]);
    }

    public function eliminarDatoMatriz(Request $request)
    {
        $id = $request->dato_matriz_id;
        $datomatriz = DatoMatriz::findOrFail($id);
        $datomatriz->estado = 'D';
        $datomatriz->save();
        return 1;
    }

    public function editarDatoMatriz(Request $request)
    {
        error_log("Se edita el DATO MATRIZ");
        $resultado = 'editar';
        $id = $request->dato_matriz_id;
        $datomatriz = DatoMatriz::where('estado','A')->findOrFail($id);
        /*
        if(isset($request->titulo))
        {
            $datomatriz->titulo = $request->titulo;
        }
        if(isset($request->resumen))
        {
            $datomatriz->resumen = $request->resumen;
        }
        */
        $datomatriz[$request->ndato] = $request->dato;
        $datomatriz->save();

        error_log("DATO MATRIZ guardado");
        error_log($datomatriz);

        return response()->json
        ([
            'resultado' => $resultado,
        ]);
    }
}
