<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Models\Facultad;
use App\Models\Escuela;
use App\Models\Plan;
use App\Models\Curso;
use App\Models\CursoTipo;
use App\Models\Competencia;
use App\Models\ResultadosEstudiante;
use App\Models\Criterio;
use App\Models\EscuelaSemestre;
use App\Models\CursoSemestre;
use App\Models\CursoGrupo;
use App\Models\CompetenciasRE;
use App\Models\Docente;

class AdminController extends Controller
{
    public function index()
    {
    	$facultades=Facultad::where('estado','A')->get();
    	$escuelas=Escuela::where('facultad_id',$facultades[0]->id)->where('estado','A')->get();
        return view('user.admin.index',compact('facultades','escuelas'));
    }

    public function competencia(Request $request)
    {
        $selected_facultad = $request->facultad;
        $selected_escuela = $request->escuela;
        $selected_plan = $request->plan;
        error_log($selected_facultad);
        error_log($selected_escuela);
        error_log($selected_plan);

        $facultades=[];
        $escuelas=[];
        $planes=[];
        $competencias=[];

    	$facultades=Facultad::where('estado','A')->get();
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_facultad))
        {
            $selected_facultad = $facultades[0]->id; 
        }
        //
        if(sizeof($facultades)>0)
        {
    	   $escuelas=Escuela::where('facultad_id',$selected_facultad)->where('estado','A')->get();
        }
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_escuela) && sizeof($escuelas)>0)
        {
            $selected_escuela = $escuelas[0]->id; 
        }
        //
        if(sizeof($escuelas)>0)
        {
    	   $planes=Plan::where('escuela_id',$selected_escuela)->where('estado','A')->get();
        }
                //IF para evitar fallos con la entrada rapida
        if(empty($selected_escuela) && sizeof($escuelas)>0)
        {
            $selected_plan = $planes[0]->id; 
        }
        //
        if(sizeof($planes)>0)
        {
    	   $competencias=Competencia::where('plan_id',$selected_plan)->where('estado','A')->get();
        }
        return view('user.admin.competencia',compact('facultades','escuelas','planes','competencias','selected_facultad','selected_escuela','selected_plan'));
    }

	public function curso(Request $request)
    {
    	    	// $facultades=Facultad::where('estado','A')->get();
    	// $escuelas=Escuela::where('facultad_id',$facultades[0]->id)->where('estado','A')->get();
    	// $planes=Plan::where('escuela_id',$escuelas[0]->id)->where('estado','A')->get();
    	// $cursos=Curso::where('plan_id',$planes[1]->id)->where('estado','A')->get();
        // return view('user.admin.curso',compact('facultades','escuelas','planes','cursos'));
        //IF para evitar fallos con la entrada rapida

        $selected_facultad = $request->facultad;
        $selected_escuela = $request->escuela;
		$selected_plan = $request->plan;
        $escuelas=[];
        $planes=[];
		$cursos=[];
        error_log($selected_facultad);
        error_log($selected_escuela);
		error_log($selected_plan);

        $facultades=Facultad::where('estado','A')->get();

        if(empty($selected_facultad))
        {
            $selected_facultad = $facultades[0]->id; 
        }
        //
        if(sizeof($facultades)>0)
        {
    	   $escuelas=Escuela::where('facultad_id',$selected_facultad)->where('estado','A')->get();
        }
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_escuela) && sizeof($escuelas)>0)
        {
            $selected_escuela = $escuelas[0]->id; 
        }
        //
		if(sizeof($escuelas)>0)
        {
    	   $planes=Plan::where('escuela_id',$selected_escuela)
    		  ->where('estado','A')
			  ->get();
        }
        if(sizeof($planes)>0)
        {
    	   $cursos=Curso::where('plan_id',$selected_plan)
    		  ->where('estado','A')
              ->with(['cursotipo' => function ($query)
    		{
    			$query->where('estado','A');
    		}
    		])
    		  ->get();
        }
        return view('user.admin.curso',compact('facultades','escuelas','planes','cursos','selected_facultad','selected_escuela','selected_plan'));
    }

	public function resultadoEstudiante(Request $request)
    {
		$selected_facultad = $request->facultad;
        $selected_escuela = $request->escuela;
		$selected_plan = $request->plan;
		error_log($selected_facultad);
        error_log($selected_escuela);
		error_log($selected_plan);
        $escuelas=[];
        $planes=[];
		$resultadosestudiantes=[];
    	$facultades=Facultad::where('estado','A')->get();
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_facultad))
        {
            $selected_facultad = $facultades[0]->id; 
        }
        //
        if(sizeof($facultades)>0)
        {
    	   $escuelas=Escuela::where('facultad_id',$selected_facultad)->where('estado','A')->get();
        }
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_escuela) && sizeof($escuelas)>0)
        {
            $selected_escuela = $escuelas[0]->id; 
        }
        //
		if(sizeof($escuelas)>0)
        {
    	   $planes=Plan::where('escuela_id',$selected_escuela)
    		  ->where('estado','A')
			  ->get();
        }
        if(sizeof($planes)>0)
        {
    	   $resultadosestudiantes=ResultadosEstudiante::where('plan_id',$selected_plan)
    		  ->where('estado','A')
    		  ->get();
        }

        return view('user.admin.resultadoestudiantes',compact('resultadosestudiantes','facultades','escuelas','planes','selected_escuela','selected_facultad','selected_plan'));
    	// $facultades=Facultad::where('estado','A')->get();
    	// $escuelas=Escuela::where('facultad_id',$facultades[0]->id)->where('estado','A')->get();
    	// $planes=Plan::where('escuela_id',$escuelas[0]->id)->where('estado','A')->get();
	    // $resultadosestudiantes=ResultadosEstudiante::where('plan_id',$planes[0]->id)->where('estado','A')->get();
        // return view('user.admin.resultadoestudiantes',compact('facultades','escuelas','planes','resultadosestudiantes'));
    }

	public function cursoEsp(Request $request)
    {
        $selected_facultad = $request->facultad;
        $selected_escuela = $request->escuela;
        $selected_escuelasemestre = $request->escuelasemestre;
        $selected_cursosemestre = $request->cursoID;

        $facultades=[];
        $escuelas=[];
        $escuelasemestres=[];
        $cursosemestres=[];
        $cursogrupos=[];

        error_log($selected_facultad);
        error_log($selected_escuela);
        error_log($selected_cursosemestre);


    	$facultades=Facultad::where('estado','A')->get();
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_facultad))
        {
            $selected_facultad = $facultades[0]->id; 
        }
        //
        if(sizeof($facultades)>0)
        {
    	   $escuelas=Escuela::where('facultad_id',$selected_facultad)->where('estado','A')->get();
        }
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_escuela) && sizeof($escuelas)>0)
        {
            $selected_escuela = $escuelas[0]->id; 
        }
        //
        if(sizeof($escuelas)>0)
        {
    	   $escuelasemestres=EscuelaSemestre::where('escuela_id',$selected_escuela)
    		->where('estado','A')
    		->with(['semestre' => function ($query)
    		{
    			$query->where('estado','A');
    		}
    		])->get();
        }
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_escuelasemestre) && sizeof($escuelasemestres)>0)
        {
            $selected_escuelasemestre = $escuelasemestres[0]->id;
        }
        //
        if(sizeof($escuelasemestres)>0)
        {
		  $cursosemestres=CursoSemestre::where('escuelasemestre_id',$selected_escuelasemestre)
			->where('estado','A')
			->with(['curso' => function ($query)
    		{
    			$query->where('estado','A');
    		}
    		])->get();
        }
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_cursosemestre) && sizeof($cursosemestres)>0)
        {
            $selected_cursosemestre = $cursosemestres[0]->id; 
        }
        //
        if(sizeof($cursosemestres)>0)
        {
		  $cursogrupos=CursoGrupo::where('cursosemestre_id',$selected_cursosemestre)
			->where('estado','A')
			->with(['docente' => function ($query)
    		{
    			$query->where('estado','A');
    		}
    		])->get();
        }

        $escuelasemestre = EscuelaSemestre::where('id',$selected_escuelasemestre)->first();

        return view('user.admin.curso_especifico',compact('facultades','escuelas','escuelasemestres','cursosemestres','cursogrupos','selected_facultad','selected_escuela','selected_escuelasemestre','selected_cursosemestre','escuelasemestre'));
    }
	public function semestreCurso(Request $request)
    {
        $selected_facultad = $request->facultad;
        $selected_escuela = $request->escuela;
        $facultades=[];
        $escuelas=[];
        $escuelasemestres=[];
        $cursosemestres=[];
        error_log($selected_facultad);
        error_log($selected_escuela);
    	$facultades=Facultad::where('estado','A')->get();
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_facultad))
        {
            $selected_facultad = $facultades[0]->id; 
        }
        //
        if(sizeof($facultades)>0)
        {
    	   $escuelas=Escuela::where('facultad_id',$selected_facultad)->where('estado','A')->get();
        }
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_escuela) && sizeof($escuelas)>0)
        {
            $selected_escuela = $escuelas[0]->id; 
        }
        //
        if(sizeof($escuelas)>0)
        {
    	   $escuelasemestres=EscuelaSemestre::where('escuela_id',$selected_escuela)
    		  ->where('estado','A')
    		  ->with(['semestre' => function ($query)
    		  {
    			 $query->where('estado','A');
    		  }
    		  ])->get();
        }
        if(sizeof($escuelasemestres)>0)
        {
    		$cursosemestres=CursoSemestre::where('escuelasemestre_id',$escuelasemestres[0]->id)
    			->where('estado','A')
    			->with(['curso' => function ($query)
        		{
        			$query->where('estado','A')
                    ->with(['plan' => function ($query)
                    {
                        $query->where('estado','A');
                    }
                    ]);
        		}
        		])->get();
        }
        return view('user.admin.semestres',compact('facultades','escuelas','escuelasemestres','cursosemestres','selected_facultad','selected_escuela'));
    }

        public function reEsp(Request $request)
    {
        $selected_facultad = $request->facultad;
        $selected_escuela = $request->escuela;
        $selected_plan = $request->plan;
        $selected_re = $request->reID;

        $facultades=[];
        $escuelas=[];
        $planes=[];
        $resultadosestudiantes=[];
        $criterios=[];

        $facultades=Facultad::where('estado','A')->get();
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_facultad))
        {
            $selected_facultad = $facultades[0]->id; 
        }
        //
        if(sizeof($facultades)>0)
        {
            $escuelas=Escuela::where('facultad_id',$selected_facultad)->where('estado','A')->get();
        }
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_escuela) && sizeof($escuelas)>0)
        {
            $selected_escuela = $escuelas[0]->id; 
        }
        //
        if(sizeof($escuelas)>0)
        {
            $planes=Plan::where('escuela_id',$selected_escuela)->where('estado','A')->get();
        }
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_plan) && sizeof($planes)>0)
        {
            $selected_plan = $planes[0]->id;
        }
        //
        if(sizeof($planes)>0)
        {
            $resultadosestudiantes=ResultadosEstudiante::where('plan_id',$selected_plan)->where('estado','A')->get();
        }
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_re) && sizeof($resultadosestudiantes)>0)
        {
            $selected_re = $resultadosestudiantes[0]->id;
        }
        //
        if(sizeof($resultadosestudiantes)>0)
        {
            $criterios=Criterio::where('resultadoestudiante_id',$selected_re)->where('estado','A')->get();
        }
        return view('user.admin.resultado_especifico_subRE',compact('facultades','escuelas','planes','resultadosestudiantes','criterios','selected_facultad','selected_escuela','selected_plan','selected_re'));
    }

    public function competenciaEsp(Request $request)
    {   
        $selected_facultad = $request->facultad;
        $selected_escuela = $request->escuela;
        $selected_plan = $request->plan;
        $selected_competencia = $request->competenciaID;

        $facultades=[];
        $escuelas=[];
        $planes=[];
        $competencias=[];
        $competenciaRes=[];

        $facultades=Facultad::where('estado','A')->get();
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_facultad))
        {
            $selected_facultad = $facultades[0]->id; 
        }
        //
        if(sizeof($facultades)>0)
        {
            $escuelas=Escuela::where('facultad_id',$selected_facultad)->where('estado','A')->get();
        }

        //IF para evitar fallos con la entrada rapida
        if(empty($selected_escuela) && sizeof($escuelas)>0)
        {
            $selected_escuela = $escuelas[0]->id; 
        }
        //
        if(sizeof($escuelas)>0)
        {
            $planes=Plan::where('escuela_id',$selected_escuela)->where('estado','A')->get();
        }

        //IF para evitar fallos con la entrada rapida
        if(empty($selected_plan) && sizeof($planes)>0)
        {
            $selected_plan = $planes[0]->id;
        }
        //
        if(sizeof($planes)>0)
        {
            $competencias=Competencia::where('plan_id',$selected_plan)->where('estado','A')->get();
        }

        //IF para evitar fallos con la entrada rapida
        if(empty($selected_competencia) && sizeof($competencias)>0)
        {
            $selected_competencia = $competencias[0]->id; 
        }
        //
        if(sizeof($competencias)>0)
        {
            $competenciaRes=CompetenciasRE::where('competencia_id',$selected_competencia)
            ->where('estado','A')
            ->with(['resultadoestudiante' => function ($query)
            {
                $query->where('estado','A');
            }
            ])->get();
        }
        $resultadosestudiantes = ResultadosEstudiante::where('plan_id',$selected_plan)->where('estado','A')->get();
        return view('user.admin.competencia_especifica',compact('resultadosestudiantes','facultades','escuelas','planes','competencias','competenciaRes','selected_facultad','selected_escuela','selected_plan','selected_competencia'));
    
        // $facultades=Facultad::where('estado','A')->get();
        // $escuelas=Escuela::where('facultad_id',$facultades[0]->id)->where('estado','A')->get();
        // $planes=Plan::where('escuela_id',$escuelas[0]->id)->where('estado','A')->get();
        // $competencias=Competencia::where('plan_id',$planes[1]->id)->where('estado','A')->get();
        // $competenciaRes=CompetenciasRE::where('competencia_id',$competencias[0]->id)
        //  ->where('estado','A')
        //  ->with(['resultadoestudiante' => function ($query)
        //  {
        //      $query->where('estado','A');
        //  }
        //  ])->get();
        // return view('user.admin.competencia_especifica',compact('facultades','escuelas','planes','competencias','competenciaRes'));
    }

	public function planEstudio(Request $request)
    {
		$selected_facultad = $request->facultad;
        $selected_escuela = $request->escuela;
        $facultades=[];
        $escuelas=[];
        $planes=[];
        // $cursosemestres=[];
        error_log($selected_facultad);
        error_log($selected_escuela);
    	$facultades=Facultad::where('estado','A')->get();
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_facultad))
        {
            $selected_facultad = $facultades[0]->id; 
        }
        //
        if(sizeof($facultades)>0)
        {
    	   $escuelas=Escuela::where('facultad_id',$selected_facultad)->where('estado','A')->get();
        }
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_escuela) && sizeof($escuelas)>0)
        {
            $selected_escuela = $escuelas[0]->facultad_id; 
        }
        //
        if(sizeof($escuelas)>0)
        {
    	   $planes=Plan::where('escuela_id',$selected_escuela)
    		  ->where('estado','A')
			  ->get();
        }
        return view('user.admin.planestudios',compact('facultades','escuelas','planes','selected_facultad','selected_escuela'));
    }
    public function resultadoECompetencia(Request $request)
	{
		$selected_facultad = $request->facultad;
        $selected_escuela = $request->escuela;
		$selected_plan = $request->plan;
        $selected_resultadoestudiante = $request->reID;
    	$facultades=Facultad::where('estado','A')->get();
        $escuelas=[];
        $planes=[];
		$resultadosestudiantes=[];
        $resultadosECompetencia=[];

        $competencias=[];
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_facultad))
        {
            $selected_facultad = $facultades[0]->id; 
        }
        //
        if(sizeof($facultades)>0)
        {
    	   $escuelas=Escuela::where('facultad_id',$selected_facultad)->where('estado','A')->get();
        }
        //IF para evitar fallos con la entrada rapida
        if(empty($selected_escuela) && sizeof($escuelas)>0)
        {
            $selected_escuela = $escuelas[0]->id; 
        }
        //
		if(sizeof($escuelas)>0)
        {
    	   $planes=Plan::where('escuela_id',$selected_escuela)->where('estado','A')->get();
        }
        if(empty($selected_plan) && sizeof($planes)>0)
        {
            $selected_plan = $planes[0]->id; 
        }
        if(sizeof($planes)>0)
        {
    	   $resultadosestudiantes=ResultadosEstudiante::where('plan_id',$selected_plan)->where('estado','A')->get();
        }
        if(empty($selected_resultadoestudiante) && sizeof($resultadosestudiantes)>0)
        {
    	   $selected_resultadoestudiante= $resultadosestudiantes[0]->id;
        }
        if(sizeof($resultadosestudiantes)>0){
           $resultadosECompetencia=CompetenciasRE::where('resultadoestudiante_id',$selected_resultadoestudiante)
            ->where('estado','A')->with(['competencia' => function ($query)
            {
                $query->where('estado','A');
            }])->get();
        }
        $competencias = Competencia::where('plan_id',$selected_plan)->where('estado','A')->get();
        return view('user.admin.resultado_especifico_competencia',compact('competencias','resultadosECompetencia','resultadosestudiantes','facultades','escuelas','planes','selected_escuela','selected_facultad','selected_plan','selected_resultadoestudiante'));
	}
    //Funciones para carga AJAX

    public static function cargaEscuelas(Request $request)
    {
    	$facultad_id = $request->facultad_id;
    	$escuelas=Facultad::where('id',$facultad_id)
    		->with(['escuelas' => function ($query)
    		{
    			$query->where('estado','A');
    		}
    	])->get();
        return response()->json
        ([
        	'escuelas' => $escuelas
        ]);
    }

    public static function cargaPlanes(Request $request)
    {
    	$escuela_id = $request->escuela_id;
    	$planes=Escuela::where('id',$escuela_id)
    		->with(['plans' => function ($query)
    		{
    			$query->where('estado','A');
    		}
    	])->get();
        return response()->json
        ([
        	'planes' => $planes
        ]);
    }

    public static function cargaCompetencias(Request $request)
    {
    	$plan_id = $request->plan_id;
    	$competencias=Plan::where('id',$plan_id)
    		->with(['competencias' => function ($query)
    		{
    			$query->where('estado','A');
    		}
    	])->get();
        return response()->json
        ([
        	'competencias' => $competencias
        ]);
    }

	public static function cargaCursos(Request $request)
    {
    	$plan_id = $request->plan_id;
    	$cursos=Plan::where('id',$plan_id)
    		->with(['cursos' => function ($query)
    		{
    			$query->where('estado','A')->with(['cursotipo' => function ($query)
                {
                    $query->where('estado','A');
                }
            ]);
    		}
    	])->get();
        return response()->json
        ([
        	'cursos' => $cursos
        ]);
    }


    public static function cargaRE(Request $request)
    {
    	$plan_id = $request->plan_id;
    	$resultadosestudiantes=Plan::where('id',$plan_id)
    		->with(['resultadosestudiantes' => function ($query)
    		{
    			$query->where('estado','A');
    		}
    	])->get();
        return response()->json
        ([
        	'resultadosestudiantes' => $resultadosestudiantes
        ]);
    }

	public static function cargaCriterios(Request $request)
    {
    	$resultadoestudiante_id = $request->resultadoestudiante_id;
    	$criterios=Criterio::where('resultadoestudiante_id',$resultadoestudiante_id)
            ->where('estado','A')->get();
        return response()->json
        ([
        	'criterios' => $criterios
        ]);
    }

	public static function cargaResultadosEstudiantes(Request $request)
    {
    	$plan_id = $request->plan_id;
    	$resultadosestudiantes=Plan::where('id',$plan_id)
    		->with(['resultadosestudiantes' => function ($query)
    		{
    			$query->where('estado','A');
    		}
    	])->get();
        return response()->json
        ([
        	'resultadosestudiantes' => $resultadosestudiantes
        ]);
    }
	//Diego
	public static function cargaResultadosEsp(Request $request)
    {
    	$competencia_id = $request->competencia_id;
    	$resultadosestudiantes=CompetenciasRE::where('competencia_id',$competencia_id)
			->where('estado','A')
    		->with(['resultadoestudiante' => function ($query)
    		{
    			$query->where('estado','A');
    		}
    	])->get();
        return response()->json
        ([
        	'resultadosestudiantes' => $resultadosestudiantes
        ]);
    }

	public static function cargaEscuelasemestres(Request $request)
    {
    	$escuela_id = $request->escuela_id;
    	$escuelasemestres=EscuelaSemestre::where('escuela_id',$escuela_id)
    		->where('estado','A')
    		->with(['semestre' => function ($query)
    		{
    			$query->where('estado','A');
    		}
    		])->get();
        return response()->json
        ([
        	'escuelasemestres' => $escuelasemestres
        ]);
    }

    public static function cargaCursosemestre(Request $request)
    {
        $escuelasemestre_id = $request->escuelasemestre_id;
        $escuelasemestre = EscuelaSemestre::where('id',$escuelasemestre_id)->where('estado','A')->first();
        $cursosemestres=CursoSemestre::where('escuelasemestre_id',$escuelasemestre_id)
                ->where('estado','A')
                ->with(['curso' => function ($query)
                {
                    $query->where('estado','A')
                    ->with(['plan' => function ($query)
                    {
                        $query->where('estado','A');
                    }
                    ]);
                }
                ])->get();
        return response()->json
        ([
            'cursosemestres' => $cursosemestres,
            'escuelasemestre' => $escuelasemestre
        ]);
    }

    public static function cargaCursodocente(Request $request)
    {
        $cursosemestre_id = $request->cursosemestre_id;
        $escuelasemestre_id = $request->escuelasemestre_id;
        $cursogrupos=CursoGrupo::where('cursosemestre_id',$cursosemestre_id)
            ->where('estado','A')
            ->with(['docente' => function ($query)
            {
                $query->where('estado','A');
            }
            ])->get();
        
        $escuelasemestre = EscuelaSemestre::where('id',$escuelasemestre_id)->first();
        return response()->json
        ([
            'cursogrupos' => $cursogrupos,
            'escuelasemestre' => $escuelasemestre
        ]);
    }

    public static function cargaDocentes(Request $request)
    {
        $docentes=Docente::where('estado','A')->get();
        return response()->json
        ([
            'docentes' => $docentes
        ]);
    }
    public static function cargaResultadoECompetencia(Request $request)
    {
    	$resultadoestudiante_id = $request->resultadoestudiante_id;
    	$competenciaRE=CompetenciasRE::where('resultadoestudiante_id',$resultadoestudiante_id)
			->where('estado','A')
    		->with(['competencia' => function ($query)
    		{
    			$query->where('estado','A');
    		}
    	])->get();
        return response()->json
        ([
        	'competenciaRE' => $competenciaRE
        ]);
    }

    public static function cargaCursoTipo(Request $request)
    {
        $cursoTipo=CursoTipo::where('estado','A')->get();
        return response()->json
        ([
            'cursoTipo' => $cursoTipo
        ]);
    }

    public static function editarEstadoSemestre(Request $request)
    {
        $escuelasemestre_id = $request->escuelasemestre_id;
        $estado_valor = $request->estado_valor;

        $escsemact = EscuelaSemestre::findOrFail($escuelasemestre_id);

        if(($escsemact->estadosemestre == "Abierto" && $estado_valor == 1) || ($escsemact->estadosemestre == "Cerrado" && $estado_valor == 0)){
            if($estado_valor == 0){
                $escsemact->estadosemestre = "Abierto";
                $escsemact->save();
                return 1;
            }else{
                $escsemact->estadosemestre = "Cerrado";
                $escsemact->save();
                return 1;
            }
        }

        if($estado_valor==0){
            $id = $escuelasemestre_id;
            $escuelasemestreactualizar = EscuelaSemestre::findOrFail($id);
            $escuelasemestreactualizar->estadosemestre = '';
            $escuelasemestreactualizar->save();
            return 1;
        }
    }
}

