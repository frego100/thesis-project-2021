<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\ResultadosEstudiante;
use App\Models\CursoMatriz;
use App\Models\Curso;
use App\Models\CursoSemestre;
use App\Models\CursoGrupo;
use App\Models\Semestre;
use App\Models\EscuelaSemestre;
use App\Models\Docente;
use App\Models\Escuela;
use App\Models\MatrizDetalle;
use App\Models\Criterio;
use App\Models\CursoEsquema;
use App\Models\CursoEstudiante;
use App\Models\EstudianteMatriz;
use App\Models\Estudiante;

use App\Exports\ObjetoReporte;
use App\Exports\EsquemaExport;
use App\Exports\AlumnoExport;
use App\Exports\ListaAlumnosPorMatrizExport;
use App\Exports\ListaAlumnosExport;
use App\Imports\EsquemaImport;
use App\Imports\AlumnoImport;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class VistasDocenteControllerWilliam extends Controller
{
    //

    public function crearMatriz(Request $request)
    {

        $selected_semestre=$request->semestre;
        $selected_curso=$request->curso;
        $selected_matriz=$request->matriz;
        error_log("CARGAMATRIZ");
        error_log($selected_semestre);
        error_log($selected_curso);
        error_log($selected_matriz);
        $semestres_n=[];
        $escuelasemestres_n=[];
        $cursosemestres_n=[];
        $cursogrupos_n=[];

        $semestres=[];
        $escuelasemestres=[];
        $cursosemestres=[];
        $cursogrupos=[];
        $escuelas=[];
        $cursos = [];
        $grupos = [];
        $matrizs=[];
        $matrizdetalle_general=[];
        $criterios=[];
        $cursoesquemas_n=[];
        $cursoesquemas=[];
        $cursoesquemas_seleccionados=[];
        $cursoesquemas_seleccionados_n=[];

        $suma_ponderacion = 0;

        $cargo;

        $idDocente = $request->session()->get('id');
        error_log($idDocente);
        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('estado','A')->where('coordinador','1')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        error_log($cursogrupos_general);

        //Busca los semestres
        foreach($cursogrupos_general as $item)
        {
            array_push($semestres_n, $item->cursosemestre->escuelasemestre->semestre->id);
        }

        array_unique($semestres_n);
        $semestres = Semestre::whereIn('id',$semestres_n)->where('estado','A')->get();
        error_log("SEMESTRES");
        error_log($semestres);

        if(empty($selected_semestre))
        {
            $selected_semestre = $semestres[0]->id;
        }

        //Busca las escuela-semestres
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre_id == $selected_semestre)
            {
                array_push($escuelasemestres_n, $item->cursosemestre->escuelasemestre->id);
                error_log("Nuevo dato en EscuelaSemestre_n");
                error_log($item->cursosemestre->escuelasemestre->id);
            }
        }

        array_unique($escuelasemestres_n);
        $escuelasemestres = EscuelaSemestre::whereIn('id',$escuelasemestres_n)->where('estado','A')->get();
        error_log("ESCUELA SEMESTRES");
        error_log($escuelasemestres);

        //Busca los curso-semestres
        foreach($cursogrupos_general as $item)
        {
            foreach($escuelasemestres as $es)
            {
                if($es->id == $item->cursosemestre->escuelasemestre_id)
                {
                    array_push($cursosemestres_n, $item->cursosemestre->id);
                    array_push($cursos, $item->cursosemestre->curso);
                }
            }
        }

        array_unique($cursosemestres_n);
        $cursosemestres = CursoSemestre::whereIn('id',$cursosemestres_n)
            ->where('estado','A')
            ->with(['curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();
        error_log("CURSO SEMESTRES");
        error_log($cursosemestres);

        if(empty($selected_curso))
        {
            $selected_curso = $cursosemestres[0]->curso->id;
            error_log("valor por defecto en selected_curso");
        }
        //Busca el curso semestre selecionado
        foreach($cursosemestres as $item)
        {
            if($item->curso_id == $selected_curso)
            {
                error_log("Selecciono curso semestre");
                $micursosemestre = $item;
                break;
            }
        }

        // foreach($cursosemestres as $cursosemestre_item)
        // {
        //     $matrizs=CursoMatriz::Where('estado','A').where('cursosemestre_id',$cursosemestre_item->id);
        //     foreach($matrizs_bd as $matriz_item)
        //     {
        //         if($cursosemestre_item->id == $matriz_item->cursosemestre_id)
        //         {
        //             array_push($matrizs,$matriz_item);
        //         }
        //     }
        // }

        error_log("CURSOWILLIAN");
        error_log($micursosemestre);

        $matrizs=CursoMatriz::where('estado','A')->where('cursosemestre_id',$micursosemestre->id)->get();
        error_log("matrizs");
        error_log($matrizs);

        if(empty($selected_matriz) && sizeof($matrizs)>0)
        {
            $selected_matriz = $matrizs[0]->id;
            error_log("valor por defecto en selected_curso");
        }

        $matrizdetalle_general=MatrizDetalle::where('estado','A')->where('cursomatriz_id',$selected_matriz)
            ->with(['cursoesquema' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();
        error_log("matrizdetalleWILLIAN");
        error_log($matrizdetalle_general);

        //Busca los criterios
        $curso = Curso::where('id',$selected_curso)->firstOrFail();
        $resultados = ResultadosEstudiante::where('plan_id',$curso->plan_id)->get();
        error_log("RESULTADOS WILLIANNNNNNNNNNNNN");
        error_log($resultados);

        foreach($resultados as $res){
            $tempcrit = Criterio::where('resultadoestudiante_id',$res->id)->get();
            foreach($tempcrit as $crit){
                array_push($criterios,$crit);
            }
        }

        $cursoesquemas=CursoEsquema::where('cursosemestre_id',$micursosemestre->id)
            ->orderBy('codigo')
            ->get();

        foreach($cursoesquemas as $esquema){
            $temp=$matrizdetalle_general->where('cursoesquema.id',$esquema->id)->first();
            if(!empty($temp)){
                $esquema->ponderacion=$temp->ponderacion;
                $esquema->matrizdetalle_id=$temp->id;
                $suma_ponderacion = $suma_ponderacion + $temp->ponderacion;
            }else{
                $esquema->ponderacion='SIN PONDERACION';
                $esquema->matrizdetalle_id=0;
            }
        }

        error_log("CURSOESQUEMAWILL");
        error_log($cursoesquemas);

        // error_log("CRITERIOS");
        // error_log($criterios);

        //Busca los esquemas seleccionados
        // foreach($matrizdetalle_general as $item)
        // {
        //     array_push($cursoesquemas_seleccionados_n, $item->cursoesquema->id);
        // }

        // foreach($cursoesquemas_seleccionados_n as $item)
        // {
        //     $temp=CursoEsquema::where('id',$item)->first();
        //     array_push($cursoesquemas_seleccionados, $temp);
        // }

        // // $cursoesquemas = CursoEsquema::whereIn('id',$cursoesquemas_n)->where('estado','A')->get();
        // error_log("CURSOESQUEMAS");
        // error_log($cursoesquemas_seleccionados);
        // error_log(sizeof($cursoesquemas_seleccionados_n));

        return view('user.docente.crearMatriz',compact('semestres','cursos','criterios','matrizs','matrizdetalle_general','cursoesquemas','selected_semestre','selected_curso','selected_matriz','suma_ponderacion'));
    }

    public static function cargaCursosDocenteEspecifico(Request $request)
    {
        $semestre_id = $request->semestre_id;
        error_log($semestre_id);
        $verificar_coordinador = $request->coordinador;
        $cursos=[];
        $idDocente = $request->session()->get('id');

        if(empty($verificar_coordinador))
        {   
            $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
                ->where('estado','A')->where('coordinador','1')
                ->with(['cursosemestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.curso' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->get();
        }
        else
        {
            $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
                ->where('coordinador','1')
                ->where('estado','A')
                ->with(['cursosemestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.curso' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
                ->get();
        }
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre->id == $semestre_id)
            {
                array_push($cursos, $item->cursosemestre->curso);
            }
        }

        return response()->json
        ([
            'cursos' => $cursos
        ]);
    }

    //ajax
    public function insertarMatriz(Request $request)
    {
        $semestre_id=$request->semestre_id;
        $curso_id=$request->curso_id;

        $escuelasemestre=EscuelaSemestre::where('semestre_id',$semestre_id)->get();

        error_log("escuelasemestrwillian");
        error_log($escuelasemestre);

        foreach($escuelasemestre as $item)
        {
            $cursosemestre=CursoSemestre::where('curso_id',$curso_id)->where('escuelasemestre_id',$item->id)->get();
            error_log("cursosemestrewillianfor");
            error_log($cursosemestre);
            if(!$cursosemestre->isEmpty())
                break;
        }

        $input = $request->all();
        $input['matriz'] = $request->matriz;
        $input['ponderacion'] = $request->ponderacion;
        $input['cursosemestre_id'] = $cursosemestre[0]->id;
        $input['fechacalificacioninicio'] = $request->fechacalificacioninicio;
        $input['fechacalificacionfin'] = $request->fechacalificacionfin;

        error_log("inputwillian");

        $curso = CursoMatriz::create($input);
        return 1;
    }

    // public function insertarMatriz(Request $request)
    // {
    //     $input = $request->all();
    //     $input['matriz'] = $request->matriz;
    //     $input['ponderacion'] = $request->ponderacion;
    //     $input['cursosemestre_id'] = $request->cursosemestre_id;
    //     $input['fechacalificacioninicio'] = $request->fechacalificacioninicio;
    //     $input['fechacalificacionfin'] = $request->fechacalificacionfin;
    //     $curso = CursoMatriz::create($input);
    //     return 1;
    // }

    public static function cargaCursoSemestre(Request $request)
    {
        $cursoSemestre=CursoSemestre::where('Estado','A')->get();
        return response()->json
        ([
            'cursoSemestre' => $cursoSemestre
        ]);
    }

    public static function cargaMatriz(Request $request)
    {
        $curso_id = $request->curso_id;
        $semestre_id = $request->semestre_id;

        $matrizs=[];

        $escuelasemestre=EscuelaSemestre::where('semestre_id',$semestre_id)->get();

        error_log("escuelasemestrwillian");
        error_log($escuelasemestre);

        foreach($escuelasemestre as $item)
        {
            $cursosemestre=CursoSemestre::where('curso_id',$curso_id)->where('escuelasemestre_id',$item->id)->get();
            error_log("cursosemestrewillianfor");
            error_log($cursosemestre);
            if(!$cursosemestre->isEmpty())
                break;
        }

        error_log("cursosemestrewillian");

        if(sizeof($cursosemestre)>0){
            $matrizs=CursoMatriz::where('estado','A')->where('cursosemestre_id',$cursosemestre[0]->id)->get();
        }else{
            error_log("entro al else");
        }

        error_log("matrizswillian");
        error_log($matrizs);

        return response()->json
        ([
            'matrizs' => $matrizs
        ]);
        
    }

    public static function cargaSemestre(Request $request)
    {
        $semestres_n=[];
        $escuelasemestres_n=[];
        $escuelasemestres_n2=[];
        $cursosemestres_n=[];
        $cursogrupos_n=[];

        $semestres=[];
        $escuelasemestres=[];
        $cursosemestres=[];
        $cursogrupos=[];
        $escuelas=[];
        $cursos = [];
        $grupos = [];
        $matrizs=[];

        $cargo;
        
        $idDocente = $request->session()->get('id');
        error_log($idDocente);
        $cursogrupos_general = CursoGrupo::where('docente_id',$idDocente)
            ->where('estado','A')->where('coordinador','1')
            ->with(['cursosemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['cursosemestre.escuelasemestre.semestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();

        error_log($cursogrupos_general);

        //Busca los semestres
        foreach($cursogrupos_general as $item)
        {
            array_push($semestres_n, $item->cursosemestre->escuelasemestre->semestre->id);
        }

        array_unique($semestres_n);
        $semestres = Semestre::whereIn('id',$semestres_n)->where('estado','A')->get();
        error_log("semestreswillian");
        error_log($semestres);
        return response()->json
        ([
            'semestres' => $semestres
        ]);
    }

    public static function cargaCurso(Request $request)
    {
        $semestre_id=$request->semestre_id;
        foreach($cursogrupos_general as $item)
        {
            if($item->cursosemestre->escuelasemestre->semestre_id == $semestre_id)
            {
                array_push($escuelasemestres_n, $item->cursosemestre->escuelasemestre->id);
                error_log("Nuevo dato en EscuelaSemestre_n");
                error_log($item->cursosemestre->escuelasemestre->id);
            }
        }

        array_unique($escuelasemestres_n);
        $escuelasemestres = EscuelaSemestre::whereIn('id',$escuelasemestres_n)->where('estado','A')->get();
        error_log("ESCUELA SEMESTRESLAPTM");
        error_log($escuelasemestres);

        //Busca los curso-semestres
        foreach($cursogrupos_general as $item)
        {
            foreach($escuelasemestres as $es)
            {
                if($es->id == $item->cursosemestre->escuelasemestre_id)
                {
                    array_push($cursosemestres_n, $item->cursosemestre->id);
                    array_push($cursos, $item->cursosemestre->curso);
                }
            }
        }
        return response()->json
        ([
            'cursos' => $cursos
        ]);
    }

    public static function cargaResultado(Request $request)
    {

        // $criterios = Criterio::where('estado','A')->get();
        $matriz_id = $request->matriz_id;
        $curso_id = $request->curso_id;
        $criterios = [];

        $curso = Curso::where('id',$curso_id)->where('estado','A')->firstOrFail();
        error_log("RESULTADOS WILLIANNNNNNNNNNNNNCURSOERROR");
        error_log($curso_id);
        $resultados = ResultadosEstudiante::where('plan_id',$curso->plan_id)->where('estado','A')->get();
        error_log("RESULTADOS WILLIANNNNNNNNNNNNN");
        error_log($resultados);

        foreach($resultados as $res){
            $tempcrit = Criterio::where('resultadoestudiante_id',$res->id)->where('estado','A')->get();
            foreach($tempcrit as $crit){
                array_push($criterios,$crit);
            }
        }
        // error_log("CRITERIOS WILLIANNNNNNNNNNNNN");
        // error_log($criterios);


        $criterios_n=[];

        $matrizdetalle_general=MatrizDetalle::where('estado','A')->where('cursomatriz_id',$matriz_id)
            ->with(['criterio' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->with(['criterio.resultadoestudiante' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();
        error_log("matrizdetalleWILLIAN");
        error_log($matrizdetalle_general);

        // //Busca los semestres
        // foreach($matrizdetalle_general as $item)
        // {
        //     array_push($criterios_n, $item->criterio->id);
        // }

        // array_unique($criterios_n);
        // $criterios = Criterio::whereIn('id',$criterios_n)->where('estado','A')->get();
        // error_log("CRITERIOS");
        // error_log($criterios);

        return response()->json
        ([
            'criterios' => $criterios,
            'matrizdetalle_general' => $matrizdetalle_general
        ]);
        
    }

    public static function cargaEsquema(Request $request)
    {
        $matriz_id = $request->matriz_id;

        $semestre_id=$request->semestre_id;
        $curso_id=$request->curso_id;

        $escuelasemestre=EscuelaSemestre::where('semestre_id',$semestre_id)->where('estado','A')->get();

        error_log("escuelasemestrwillian");
        error_log($escuelasemestre);

        foreach($escuelasemestre as $item)
        {
            $cursosemestre=CursoSemestre::where('curso_id',$curso_id)->where('escuelasemestre_id',$item->id)->where('estado','A')->get();
            error_log("cursosemestrewillianfor");
            error_log($cursosemestre);
            if(!$cursosemestre->isEmpty())
                break;
        }

        $cursosemestres = CursoSemestre::where('id',$cursosemestre[0]->id)
            ->where('estado','A')
            ->with(['curso' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();
        error_log("CURSO SEMESTRES");
        error_log($cursosemestres);

        $esquemas_n=[];

        $matrizdetalle_general=MatrizDetalle::where('estado','A')->where('cursomatriz_id',$matriz_id)
            ->with(['cursoesquema' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
            ->get();
        error_log("matrizdetalleWILLIANESQUEMA");
        error_log($matrizdetalle_general);

        $criterios = [];

        $curso = Curso::where('id',$curso_id)->where('estado','A')->firstOrFail();
        $resultados = ResultadosEstudiante::where('plan_id',$curso->plan_id)->where('estado','A')->get();
        error_log("RESULTADOS WILLIANNNNNNNNNNNNN");
        error_log($resultados);

        foreach($resultados as $res){
            $tempcrit = Criterio::where('resultadoestudiante_id',$res->id)->where('estado','A')->get();
            foreach($tempcrit as $crit){
                array_push($criterios,$crit);
            }
        }

        $esquemas=CursoEsquema::where('cursosemestre_id',$cursosemestres[0]->id)->where('estado','A')->orderBy('codigo')->get();

        error_log("esquemaWILL");
        error_log($esquemas);

        foreach($esquemas as $esquema){
            $temp=$matrizdetalle_general->where('cursoesquema.id',$esquema->id)->where('estado','A')->first();
            if(!empty($temp)){
                $esquema->ponderacion=$temp->ponderacion;
                $esquema->matrizdetalle_id=$temp->id;
            }else{
                $esquema->ponderacion='SIN PONDERACION';
                $esquema->matrizdetalle_id=0;
            }
        }

        error_log("ESQUEMAWILL");
        error_log($esquemas);

        //Busca los semestres
        // foreach($matrizdetalle_general as $item)
        // {
        //     array_push($esquemas_n, $item->cursoesquema->id);
        // }

        // array_unique($esquemas_n);
        // $esquemas = CursoEsquema::whereIn('id',$esquemas_n)->where('estado','A')->get();
        // error_log("ESQUEMAS");
        // error_log($esquemas);

        return response()->json
        ([
            'esquemas' => $esquemas,
            'matrizdetalle_general' => $matrizdetalle_general,
            'criterios' => $criterios
        ]);
        
    }

    public function verificarEstadoSemestre(Request $request){
        $semestre_id = $request->semestre_id;
        $curso_id = $request->curso_id;

        $escuelasemestres = EscuelaSemestre::where('semestre_id',$semestre_id)->get();
        $cursosemestres = CursoSemestre::where('curso_id',$curso_id)->get();

        foreach($escuelasemestres as $es){
            foreach($cursosemestres as $cs){
                if($es->id == $cs->escuelasemestre_id)
                    $escuelasemestre = $es;
            }
        }
        
        error_log("VERIFICAR ESTADOSEMESTREEEEEEEEEEEEE");
        error_log($escuelasemestres);
        error_log($cursosemestres);

        return response()->json
        ([
            'escuelasemestre' => $escuelasemestre
        ]);
    }

    public function editarEsquema(Request $request)
    {
        $matrizdetalle_id=$request->matrizdet_id;
        $cursoesquema_id=$request->cursoesquema;
        $matriz_id=$request->matriz_id;
        $criterio_valor=$request->criterio_valor;
        $pond=$request->pond;

        error_log("escuelasemestrwillian");
        error_log($matrizdetalle_id);
        error_log($cursoesquema_id);
        error_log($matriz_id);
        error_log($criterio_valor);
        error_log($pond);

        if($criterio_valor==0){
            $id = $matrizdetalle_id;
            $mdetalleeliminar = MatrizDetalle::findOrFail($id);
            $mdetalleeliminar->estado = 'D';
            $mdetalleeliminar->save();
            return 1;
        }

        if($matrizdetalle_id == 0){
            if($pond=="SIN PONDERACION"){
                $pond1 = 0;
            }else{
                $pond1 = $pond;
            }
            $input = $request->all();
            $input['criterio_id'] = $criterio_valor;
            $input['cursomatriz_id'] = $matriz_id;
            $input['cursoesquema_id'] = $cursoesquema_id;
            $input['ponderacion'] = $pond1;
            $matriz_detalle = MatrizDetalle::create($input);
            return 1;
        }else{
            $id = $matrizdetalle_id;
            $mdetalle = MatrizDetalle::findOrFail($id);
            $mdetalle->criterio_id = $criterio_valor;
            $mdetalle->cursomatriz_id = $matriz_id;
            $mdetalle->cursoesquema_id = $cursoesquema_id;
            $mdetalle->ponderacion = $pond;
            $mdetalle->save();
            return 1;
        }

        // $escuelasemestre=EscuelaSemestre::where('semestre_id',$semestre_id)->get();

        // error_log("escuelasemestrwillian");
        // error_log($escuelasemestre);

        // foreach($escuelasemestre as $item)
        // {
        //     $cursosemestre=CursoSemestre::where('curso_id',$curso_id)->where('escuelasemestre_id',$item->id)->get();
        //     error_log("cursosemestrewillianfor");
        //     error_log($cursosemestre);
        //     if(!$cursosemestre->isEmpty())
        //         break;
        // }

        // $input = $request->all();
        // $input['matriz'] = $request->matriz;
        // $input['ponderacion'] = $request->ponderacion;
        // $input['cursosemestre_id'] = $cursosemestre[0]->id;
        // $input['fechacalificacioninicio'] = $request->fechacalificacioninicio;
        // $input['fechacalificacionfin'] = $request->fechacalificacionfin;

        // error_log("inputwillian");

        // $curso = CursoMatriz::create($input);
        // return 1;
    }

    public static function exportarEsquema(Request $request){
        $curso_id = $request->curso;
        $semestre_id = $request->semestre;
        $cursosemestre_id = $request->cursoSemestre;

        $curso = Curso::where('id',$curso_id)->where('estado','A')->firstOrFail();
        $semestre = Semestre::where('id',$semestre_id)->where('estado','A')->firstOrFail();
        $cursosemestre = CursoSemestre::where('id',$cursosemestre_id)
        ->with(['escuelasemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
        ->get();

        $escuela = Escuela::where('id',$cursosemestre[0]->escuelasemestre->escuela_id)->where('estado','A')->firstOrFail();

        $cursogrupo = CursoGrupo::where('cursosemestre_id',$cursosemestre_id)->where('coordinador','1')->get();

        $docente = Docente::where('id',$cursogrupo[0]->docente_id)->where('estado','A')->firstOrFail();

        error_log("EXPORTAR SEMESTRE: ");
        error_log($escuela);

        return Excel::download($exportar = new EsquemaExport($escuela,$semestre,$curso,$docente,$cursosemestre[0]), $semestre->anio.'-'.$semestre->semestre.'_'.$curso->codigo.'.xlsx');
    }

    public function importarEsquema(Request $request){

        if($request->file('esquemasexcel')->isValid()){
            $fileEsquemas = $request->file('esquemasexcel');

            $cursosemestre = $request->cursoSemestre;
            $curso = $request->cursoE;
            $semestre = $request->semestreE;
            $docente = $request->session()->get('id');

            $cursosemestreO = CursoSemestre::where('id',$cursosemestre)
            ->with(['escuelasemestre' => function ($query)
                {
                    $query->where('estado','A');
                }
                ])
            ->get();

            $escuela = Escuela::where('id',$cursosemestreO[0]->escuelasemestre->escuela_id)->where('estado','A')->firstOrFail();
            error_log("ESCUELA IMPORT");
            error_log($escuela);

            error_log("IMPORTAR SEMESTRE: ");
            // error_log($fileEsquemas);
            
            Excel::import(new EsquemaImport($escuela->id,$semestre,$curso,$docente,$cursosemestre,$request), $fileEsquemas);
            
            $url = redirect()->getUrlGenerator()->previous();
            error_log($url);

            // $modificar = $request->session()->put('mensaje','MENSAJEMODIFICADO');
            $mensaje = $request->session()->get('mensajeEsquema');
            $tipoMensaje = $request->session()->get('tipoMensajeEsquema');

            error_log($mensaje);
        
            return redirect('docente/esquemas?semestre='.$semestre.'+&curso='.$curso.'&grupo=1')->with([$tipoMensaje => $mensaje]);
        
        } else {
            dd("UPS LO SENTIMOS ERROR");
        }
    }

    public static function exportarAlumno(Request $request){
        $curso_id = $request->curso;
        $semestre_id = $request->semestre;
        $cursogrupo_id = $request->grupo;

        error_log("ALUMNO EXPORT");
        error_log($cursogrupo_id);

        $curso = Curso::where('id',$curso_id)->where('estado','A')->firstOrFail();
        $semestre = Semestre::where('id',$semestre_id)->where('estado','A')->firstOrFail();

        $cursogrupo = CursoGrupo::where('id',$cursogrupo_id)->where('estado','A')->firstOrFail();
        $cursosemestre = CursoSemestre::where('id',$cursogrupo->cursosemestre_id)
        ->with(['escuelasemestre' => function ($query)
            {
                $query->where('estado','A');
            }
            ])
        ->get();

        $escuela = Escuela::where('id',$cursosemestre[0]->escuelasemestre->escuela_id)->where('estado','A')->firstOrFail();

        $docente = Docente::where('id',$cursogrupo->docente_id)->where('estado','A')->firstOrFail();

        error_log("EXPORTAR ALUMNO: ");
        error_log($escuela);

        return Excel::download(new AlumnoExport($escuela,$semestre,$curso,$cursogrupo,$docente), $semestre->anio.'-'.$semestre->semestre.'_'.$curso->codigo.'_'.$cursogrupo->grupo.'.xlsx');
    }

    public function importarAlumno(Request $request){

        if($request->file('alumnosexcel')->isValid()){
            $fileAlumnos = $request->file('alumnosexcel');

            $curso = $request->cursoI;
            $semestre = $request->semestreI;
            $grupo = $request->grupoI;
            $docente = $request->docenteI;
            
            Excel::import(new AlumnoImport($semestre,$curso,$grupo,$docente,$request), $fileAlumnos);

            $mensaje = $request->session()->get('mensajeAlumno');
            $tipoMensaje = $request->session()->get('tipoMensajeAlumno');
        
            // return redirect($url)->with(['success' => 'La lista de alumnos ha sido ingresada']);
            return redirect('docente/alumnos?semestre='.$semestre.'+&curso='.$curso.'&grupo='.$grupo)->with([$tipoMensaje => $mensaje]);
        
        } else {
            dd("UPS LO SENTIMOS ERROR");
        }
    }

    // public function importarAlumno(Request $request){

    //     if($request->file('alumnosexcel')->isValid()){
    //         $fileAlumnos = $request->file('alumnosexcel');

    //         $curso = $request->cursoI;
    //         $semestre = $request->semestreI;
    //         $grupo = $request->grupoI;
    //         $docente = $request->docenteI;

    //         error_log("CURSO IMPORT");
    //         error_log($docente);

    //         $cursogrupo = CursoGrupo::where('id',$grupo)->where('estado','A')->firstOrFail();

    //         $cursosemestreO = CursoSemestre::where('id',$cursogrupo->cursosemestre_id)
    //         ->with(['escuelasemestre' => function ($query)
    //             {
    //                 $query->where('estado','A');
    //             }
    //             ])
    //         ->get();

    //         $escuela = Escuela::where('id',$cursosemestreO[0]->escuelasemestre->escuela_id)->where('estado','A')->firstOrFail();
    //         // error_log("ESCUELA IMPORT");
    //         // error_log($escuela);

    //         // error_log("IMPORTAR SEMESTRE: ");
    //         // // error_log($fileAlumnos);
            
    //         Excel::import(new AlumnoImport($escuela->id,$semestre,$curso,$grupo,$docente,$request), $fileAlumnos);
            
    //         $url = redirect()->getUrlGenerator()->previous();
    //         error_log($url);

    //         $mensaje = $request->session()->get('mensajeAlumno');
    //         $tipoMensaje = $request->session()->get('tipoMensajeAlumno');

        
    //         // return redirect($url)->with(['success' => 'La lista de alumnos ha sido ingresada']);
    //         return redirect('docente/alumnos?semestre='.$semestre.'+&curso='.$curso.'&grupo='.$grupo)->with([$tipoMensaje => $mensaje]);
        
    //     } else {
    //         dd("UPS LO SENTIMOS ERROR");
    //     }
    // }

    public static function exportarLista(Request $request){
        $semestre_id = $request->semestreR;
        $curso_id = $request->cursoR;
        $cursogrupo_id = $request->grupoR;
        $docente_id = $request->docenteR;

        $matrizdetalles = [];
        $matrizsheet = [];
        $notas = [];
        $arreglonotas = [];
        $criterios = [];
        $esquemas = [];
        $criteriosreporte = [];
        $esquemasreporte = [];
        $cursomatrizreporte = [];

        error_log("CURSOGRUPOLISTA: ");
        error_log($cursogrupo_id);

        $docente = Docente::where('id',$docente_id)->where('estado','A')->first();

        $semestre = Semestre::where('id',$semestre_id)->where('estado','A')->first();
        
        $curso = Curso::where('id',$curso_id)->where('estado','A')->first();

        $estudiantes = CursoEstudiante::where('cursogrupo_id',$cursogrupo_id)->where('estado','A')->get();

        $cursogrupo = CursoGrupo::where('id',$cursogrupo_id)->where('estado','A')->firstOrFail();

        $cursosemestre = CursoSemestre::where('id',$cursogrupo->cursosemestre_id)->where('estado','A')->first();

        $escuelasemestre = EscuelaSemestre::where('id',$cursosemestre->escuelasemestre_id)->where('estado','A')->first();

        $escuela = Escuela::where('id',$escuelasemestre->escuela_id)->where('estado','A')->first();

        // $esquemas = CursoEsquema::where('cursosemestre_id',$cursogrupo->cursosemestre_id)->where('estado','A')->get();

        $cursomatriz = CursoMatriz::where('cursosemestre_id',$cursogrupo->cursosemestre_id)->get();

        foreach($cursomatriz as $cm){
            $matrizdetalle = MatrizDetalle::where('cursomatriz_id',$cm->id)->where('estado','A')->orderBy('cursoesquema_id')
            ->with(['cursoesquema' => function ($query)
                {
                    $query->where('estado','A');
                }
            ])->get();
            $matrizdetalle = $matrizdetalle->sortBy('cursoesquema.codigo');
            error_log("MATRIZDETALLESSSSSSSSSSSSSSS: ");
            error_log($matrizdetalle);
            array_push($matrizsheet,$matrizdetalle);
            if(sizeof($matrizdetalle)>0)
                array_push($cursomatrizreporte,$cm);
            foreach($matrizdetalle as $md){
                array_push($matrizdetalles,$md);
            }
        }
        
        // foreach($cursomatriz as $cm){
        //     $matrizdetalle = MatrizDetalle::where('cursomatriz_id',$cm->id)->where('estado','A')->orderBy('cursoesquema_id')->get();
        //     error_log("MATRIZDETALLESSSSSSSSSSSSSSS: ");
        //     error_log($matrizdetalle);
        //     array_push($matrizsheet,$matrizdetalle);
        //     if(sizeof($matrizdetalle)>0)
        //         array_push($cursomatrizreporte,$cm);
        //     foreach($matrizdetalle as $md){
        //         array_push($matrizdetalles,$md);
        //     }
        // }

        foreach($estudiantes as $est){
            $Estudiante = Estudiante::where('id',$est->estudiante_id)->where('estado','A')->first();
            foreach($matrizsheet as $ms){
                foreach($ms as $mds){
                    error_log("MATRIZDETALLEPORSHEETTTTTTTTTTTTTTTTT: ");
                    error_log($mds);
                    $nota = EstudianteMatriz::where('curso_estudiante_id',$est->id)->where('matriz_detalle_id',$mds->id)->where('estado','A')->first();
                    error_log($nota);
                    array_push($notas,$nota);
                }
                if(sizeof($notas)>0){
                    array_push($arreglonotas,$notas);
                }
                $notas = [];
            }
            // $est->notas = $notas;
            $est->datos = $Estudiante;
            // $notas =[];
            $est->notas = $arreglonotas;
            $arreglonotas = [];
        }

        foreach($matrizsheet as $sheet){
            foreach($sheet as $md1){
                $criterio = Criterio::where('id',$md1->criterio_id)->where('estado','A')->first();
                $esquema = CursoEsquema::where('id',$md1->cursoesquema_id)->where('estado','A')->first();
                array_push($criterios,$criterio);
                array_push($esquemas,$esquema);
            }

            if(sizeof($criterios)>0)
                array_push($criteriosreporte,$criterios);

            if(sizeof($esquemas)>0)
                array_push($esquemasreporte,$esquemas);
            $criterios = [];
            $esquemas = [];
        }

        $reporteporMatriz = [];
        $reporte = [];

        error_log("MATRIZSHEEETTTTTTT: ");
        error_log(sizeof($matrizsheet));

        if(sizeof($estudiantes)==0)
        return redirect('docente/matriz?semestre='.$semestre->id.'+&curso='.$curso->id.'&grupo='.$cursogrupo->id)->with(['error' => 'Debe haber alumnos matriculados para generar el reporte']);

        for($i=0; $i<sizeof($estudiantes[0]->notas); $i++){
            foreach($estudiantes as $est){
                $re = new ObjetoReporte($est,$est->notas[$i]);
                array_push($reporteporMatriz,$re);
            }
            array_push($reporte,$reporteporMatriz);
            $reporteporMatriz = [];
        }

        // foreach($estudiantes as $est){
        //     foreach($est->notas as $not){
        //         $re = new ObjetoReporte($est,$not);
        //         array_push($reporteporMatriz,$re);
        //         error_log("OBJETOREPORTEEEEEEEEEEEEEE: ");
        //         error_log($not[0]);
        //     }
        //     array_push($reporte,$reporteporMatriz);
        //     $reporteporMatriz = [];
        // }

        // foreach($reporteporMatriz as $rep){
        //     error_log("REPORTEEEEEEEEEEEEEEEEEEEEEE: ");
        //     error_log($rep->notas);
        // }

        // foreach($estudiantes as $est){
        //     $Estudiante = Estudiante::where('id',$est->estudiante_id)->where('estado','A')->first();
        //     foreach($matrizdetalles as $matdet){
        //         $nota = EstudianteMatriz::where('curso_estudiante_id',$est->id)->where('matriz_detalle_id',$matdet->id)->where('estado','A')->first();
        //         // error_log("NOTASSSSSSSSSSSSSSSSSSSSSS: ");
        //         // error_log($nota);
        //         array_push($notas,$nota);
        //     }
        //     $est->notas = $notas;
        //     $est->datos = $Estudiante;
        //     $notas =[];
        // }

        // foreach($matrizdetalles as $md1){
        //     $criterio = Criterio::where('id',$md1->criterio_id)->where('estado','A')->first();
        //     $esquema = CursoEsquema::where('id',$md1->cursoesquema_id)->where('estado','A')->first();
        //     $md1->criterio = $criterio;
        //     $md1->esquema = $esquema;
        // }

        error_log("CURSOGRUPOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO: ");
        error_log(sizeof($cursomatrizreporte));

        return Excel::download(new ListaAlumnosExport($escuela,$semestre,$curso,$docente,$cursogrupo,$reporte,$esquemasreporte,$criteriosreporte,$cursomatrizreporte,$matrizsheet), 'Registros de notas '.$semestre->anio.'-'.$semestre->semestre.'_'.$curso->curso.'_GRUPO_'.$cursogrupo->grupo.'_'.str_replace('/',' ',$docente->nombre).' .xlsx');
    }
}



    // //controla las salidas de form
    // function controladorForm(e)
    // {
    //     //var form = $("form");
    //     var form = document.getElementById("myForm");
    //     if(e.target.id === "reporte")
    //     {
    //         form.method = "POST";
    //         form.action = ac2;
    //     }
    //     else
    //     {
    //         $('#AlumnoID').attr('value', e.target.id);
    //         form.method = "GET";
    //         form.action = ac1;
    //     }

    //     $( "form" ).submit();
    // }


    // $('body').on('click','.mialumno',controladorForm);
    // $('body').on('click','#reporte',controladorForm);
?>