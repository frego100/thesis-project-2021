<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Semestre;
use App\Models\EscuelaSemestre;

class SemestreController extends Controller
{
    //

    public function index()

    {   
        $semestres=Semestre::where('estado','A')->get();
        return view("semestre.index",compact('semestres'));
    }

    public function create()
    {
        return view('semestre.create');
    }
  
    public function store(Request $request)
    {
        $input = $request->all();
        $semestre = Semestre::create($input);
        return redirect('semestres')->with('message', 'Guardado Exitosamente');
    }

    public function show($id)
    {
        $semestre=Semestre::find($id);
        return view('semestre.show', compact('semestre'));
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $semestre = Semestre::find($id);
        $semestre->update($input);
        return redirect('semestres')->with('message', 'Actualizado!');
    }


    public function edit($id)
    {
        $semestre = Semestre::findOrFail($id);
        return view('semestre.edit', compact('semestre'));
    }

    public function delete($id)
    {
        $semestre = Semestre::find($id);

        $escuelasemestre = EscuelaSemestre::where('semestre_id',$id)->where('estado','A')->get();

        if(sizeof($escuelasemestre) == 0){
            $semestre->estado = 'D';
            $semestre->save();
            return redirect('semestres')->with(['success' => 'Se elimino exitosamente']);
        }

        return redirect('semestres')->with(['error' => 'No se puede eliminar semestre porque tiene relacion con '. sizeof($escuelasemestre).' registros']);
    }
}
