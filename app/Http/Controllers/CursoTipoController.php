<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CursoTipo;
use App\Models\Curso;

class CursoTipoController extends Controller
{
    
    public function index()
    {
        $cursoTipos= CursoTipo::paginate(10);

        $cursoTipos=CursoTipo::where('estado','A')->get();

        return view("cursoTipo.index",compact('cursoTipos'));
    }
  
    public function create()
    {
        return view('cursoTipo.create');
    }

    public function store(Request $request)
    {
    
        $input = $request->all();
        $cursoTipo = CursoTipo::create($input);
        return redirect('cursoTipos')->with('message', 'Guardado Exitosamente');
    }

    public function show($id)
    {
        $cursoTipo=CursoTipo::find($id);
        return view('cursoTipo.show', compact('cursoTipo'));
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $cursoTipo = CursoTipo::find($id);
        $cursoTipo->update($input);
        return redirect('cursoTipos')->with('message', 'Actualizado!');
    }

    public function edit($id)
    {
        $cursoTipo = CursoTipo::findOrFail($id);
        return view('cursoTipo.edit', compact('cursoTipo'));
    }

    public function delete($id)
    {
        $cursoTipo = CursoTipo::find($id);

        $curso = Curso::where('cursotipo_id',$id)->where('estado','A')->get();

        if(sizeof($curso) == 0){
            $cursoTipo->estado = 'D';
            $cursoTipo->save();
            return redirect('cursoTipos')->with(['success' => 'Se elimino exitosamente']);
        }

        return redirect('cursoTipos')->with(['error' => 'No se puede eliminar cursotipo porque tiene relacion con '. sizeof($curso).' registros']);
    }
}
