<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UsuarioController extends Controller
{

    public function index()
    {   
        $usuarios=User::get();
        return view("usuarios.index",compact('usuarios'));
    }
    
    public function create()
    {
        return view('usuarios.create');
    }
    public function show($id)
    {
        $usuario = User::find($id);
        return view('usuarios.show',compact('usuario'));

    }

    public function store(Request $request)
    {

        $input = $request->all();
        $usuario = User::create($input);
        //return \response()->json(['res' => true, 'message' =>'insertado correctamente'], 200);
        return redirect('usuarios')->with('message', 'Guardado Exitosamente');
        //return view('facultad.index');
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $usuario = User::find($id);
        $usuario->update($input);
        //return \response()->json(['res' => true, 'message' =>'actualizado correctamente'], 200);
        return redirect('usuarios')->with('message', 'Actualizado!');
    }


    public function edit($id)
    {
        $usuario = User::findOrFail($id);
        return view('usuarios.edit', compact('usuario'));
    }

    public function delete($id)
    {
        $usuario = User::find($id);
        $usuario->estado = 'D';
        $usuario->save();
        return redirect('usuarios')->with('message', 'Eliminado!');
    }
}
