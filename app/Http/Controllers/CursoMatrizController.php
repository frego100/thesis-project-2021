<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CursoMatriz;
use App\Models\MatrizDetalle;
use App\Models\Curso;

class CursoMatrizController extends Controller
{
    public function index()
    {   
        $cursomatrizs=CursoMatriz::with(['cursosemestre'])->where('estado','A')->get();

        return view('cursomatriz.index',compact('cursomatrizs'));
    }

    public function create()
    {
        $cursos = Curso::all()->where('estado','A');
        return view('cursomatriz.create', compact('cursos'));
    }
  
    public function store(Request $request)
    {
        $input = $request->all();
        $input['curso_id'] = $request['curso'];
        $cursomatriz = CursoMatriz::create($input);
        return redirect('cursomatrizs')->with('message', 'Guardado Exitosamente');
    }

    public function show($id)
    {
        $cursomatriz=CursoMatriz::find($id);
        return view('cursomatriz.show', compact('cursomatriz'));
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $input['curso_id'] = $request['curso'];
        $cursomatriz = CursoMatriz::find($id);
        $cursomatriz->update($input);
        return redirect('cursomatrizs')->with('message', 'Actualizado!');
    }


    public function edit($id)
    {
        $cursos = Curso::all();
        $cursomatriz = CursoMatriz::findOrFail($id);
        return view('cursomatriz.edit', compact('cursomatriz','cursos'));
    }

    public function delete($id)
    {
        $cursomatriz = CursoMatriz::find($id);

        $matrizdetalle = MatrizDetalle::where('cursomatriz_id',$id)->where('estado','A')->get();

        if(sizeof($matrizdetalle) == 0){
            $cursomatriz->estado = 'D';
            $cursomatriz->save();
            return redirect('cursomatrizs')->with(['success' => 'Se elimino exitosamente']);
        }

        return redirect('cursomatrizs')->with(['error' => 'No se puede eliminar matriz porque tiene relacion con '. sizeof($matrizdetalle).' registros']);
    }

    //ajax
    public function insertarCursoMatriz(Request $request)
    {
        $input = $request->all();
        $input['matriz'] = $request->matriz;
        $input['ponderacion'] = $request->ponderacion;
        $input['fechacalificacioninicio'] = $request->fechainicio;
        $input['fechacalificacionfin'] = $request->fechafin;
        $input['cursosemestre_id'] = $request->cursosemestre_id;
        $cursomatriz = CursoMatriz::create($input);
        return 1;
    }

    public function eliminarCursoMatriz(Request $request)
    {
        $id = $request->id;
        $cursomatriz = CursoMatriz::findOrFail($id);

        $matrizdetalle = MatrizDetalle::where('cursomatriz_id',$id)->where('estado','A')->get();

        if(sizeof($matrizdetalle) == 0){
            $cursomatriz->estado = 'D';
            $cursomatriz->save();
            return 0;
        }
        return sizeof($matrizdetalle);
    }

    public function editarCursoMatriz(Request $request)
    {
        $id = $request->id;
        $cursomatriz = CursoMatriz::findOrFail($id);
        $cursomatriz->matriz = $request->matriz;
        $cursomatriz->ponderacion = $request->ponderacion;
        $cursomatriz->fechacalificacioninicio = $request->fechainicio;
        $cursomatriz->fechacalificacionfin = $request->fechafin;
        $cursomatriz->save();
        return 1;
    }
}
