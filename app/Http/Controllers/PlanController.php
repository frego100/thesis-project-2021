<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Plan;
use App\Models\Escuela;
use App\Models\ResultadosEstudiante;
use App\Models\Competencia;
use App\Models\Curso;

class PlanController extends Controller
{
    public function index()
    {
        $planes=Plan::with(['escuela'])->where('estado','A')->get();
        return view('plan.index',compact('planes'));
    }

    public function create()
    {
        $escuelas=Escuela::all();
        return view('plan.create',compact('escuelas'));
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['escuela_id']= $request['escuela'];
        $plan = Plan::create($input);
        return redirect('planes');
    }
 
    public function show($id)
    {
        $plan=Plan::find($id);
        return view('plan.show', compact('plan'));
    }
    
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $plan = Plan::find($id);
        $plan->update($input);
        return redirect('planes')->with('message', 'Actualizado!');
    }

    public function edit($id)
    {
        $escuelas=Escuela::all();
        $plan = Plan::findOrFail($id);
        return view('plan.edit', compact('plan','escuelas'));
    }

    public function delete($id)
    {
        $plan = Plan::find($id);

        $resultadoestudiante = ResultadosEstudiante::where('plan_id',$id)->where('estado','A')->get();

        $competencia = Competencia::where('plan_id',$id)->where('estado','A')->get();

        $curso = Curso::where('plan_id',$id)->where('estado','A')->get();

        $contador = sizeof($resultadoestudiante) + sizeof($competencia) + sizeof($curso);

        if($contador == 0){
            $plan->estado = 'D';
            $plan->save();
            return redirect('planes')->with(['success' => 'Se elimino exitosamente']);
        }

        return redirect('planes')->with(['error' => 'No se puede eliminar plan porque tiene relacion con '. $contador.' registros']);
    }
}