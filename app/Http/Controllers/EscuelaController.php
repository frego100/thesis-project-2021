<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Escuela;
use App\Models\Facultad;
use App\Models\EscuelaSemestre;
use App\Models\Plan;

class EscuelaController extends Controller
{
    public function index()
    {
        $escuelas=Escuela::with(['facultad'])->where('estado','A')->get();
        return view('escuela.index',compact('escuelas'));
    }

    public function create()
    {
        $facultades=Facultad::all();
        return view('escuela.create',compact('facultades'));
    }
  
    public function store(Request $request)
    {
        $input = $request->all();
        $input['facultad_id'] = $request['facultad'];
        $escuela = Escuela::create($input);
        return redirect('escuelas');
    }

    public function show($id)
    {
        $escuela=Escuela::find($id);
        return view('escuela.show',compact('escuela'));
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $input['facultad_id'] = $request['facultad'];
        $escuela = Escuela::find($id);
        $escuela->update($input);
        return redirect('escuelas')->with('message', 'Actualizado!');
    }

    public function edit($id)
    {
        $facultades=Facultad::all();
        $escuela = Escuela::findOrFail($id);
        return view('escuela.edit', compact('escuela','facultades'));
    }

    public function delete($id)
    {
        $escuela = Escuela::find($id);

        $escuelasemestre = EscuelaSemestre::where('escuela_id',$id)->where('estado','A')->get();

        $planes = Plan::where('escuela_id',$id)->where('estado','A')->get();

        $contador = sizeof($escuelasemestre) + sizeof($planes);

        if($contador == 0){
            $escuela->estado = 'D';
            $escuela->save();
            return redirect('escuelas')->with(['success' => 'Se elimino exitosamente']);
        }

        return redirect('escuelas')->with(['error' => 'No se puede eliminar escuela porque tiene relacion con '. $contador.' registros']);
    }
}
