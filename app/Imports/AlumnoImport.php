<?php

namespace App\Imports;

use App\Models\CursoEsquema;
use App\Models\Curso;
use App\Models\Escuela;
use App\Models\Semestre;
use App\Models\CursoGrupo;
use App\Models\CursoSemestre;
use App\Models\Docente;
use App\Models\Estudiante;
use App\Models\CursoEstudiante;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class AlumnoImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    protected $semestreForm;
    protected $cursoForm;
    protected $grupoForm;
    protected $docenteForm;
    protected $request;

    function __construct($semestre,$curso,$grupo,$docente,$request) {
        $this->semestreForm = $semestre;
        $this->cursoForm = $curso;
        $this->grupoForm = $grupo;
        $this->docenteForm = $docente;
        $this->request = $request;
    }

    public function collection(Collection $collection)
    {
        $validador = 1;
        $contadorgrupo = 0;
        $contadorgrupomensaje = 0;
        $contadorMatriculados = 0;
        $mensaje = "";
        $array_Excel = [];

        error_log("IMPORTAR COLLECTION: ");
        error_log($collection);
        
        error_log($collection[10][1]);

        for($i=10; $i<sizeof($collection); $i++){
            $cui = $collection[$i][1];
            $nombres = $collection[$i][2];

            $estudiantes = Estudiante::where('cui',$cui)->where('estado','A')->get();

            if(sizeof($estudiantes) == 0){
                $input['cui'] = $cui;
                $input['estudiante'] = $nombres;
                $estudiante = Estudiante::create($input);
            }

            $matricula_estudiante = Estudiante::where('cui',$cui)->where('estado','A')->first();

            $curso_estudiante = CursoEstudiante::where('estudiante_id',$matricula_estudiante->id)->where('cursogrupo_id',$this->grupoForm)->where('estado','A')->get();

            if(sizeof($curso_estudiante) == 0){
                $input['estudiante_id'] = $matricula_estudiante->id;
                $input['cursogrupo_id'] = $this->grupoForm;
                $estudiante = CursoEstudiante::create($input);
                $contadorMatriculados+=1;
            }
        }

        $mensaje = $this->request->session()->put('mensajeAlumno','Se matriculo '.$contadorMatriculados.' estudiantes excepto los que ya estan matriculados en el grupo');
        $tipoMensaje = $this->request->session()->put('tipoMensajeAlumno','success');

        // $escuelaExcel = $collection[2][3];
        // $semestreExcel = $collection[3][3];
        // $cursoExcel = $collection[4][3];
        // $grupoExcel = $collection[5][3];
        // $docenteExcel = $collection[6][3];
        // $nombreEscuelaExcel = $collection[2][1];
        // $nombreSemestreExcel = $collection[3][1];
        // $nombreCursoExcel = $collection[4][1];
        // $nombreGrupoExcel = $collection[5][1];
        // $nombreDocenteExcel = $collection[6][1];

        // $nombreSemestreForm = $semestre->anio . "-" . $semestre->semestre;
    }


}
