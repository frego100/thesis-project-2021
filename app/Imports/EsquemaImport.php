<?php

namespace App\Imports;

use App\Models\CursoEsquema;
use App\Models\Curso;
use App\Models\Escuela;
use App\Models\Semestre;
use App\Models\CursoSemestre;
use App\Models\Docente;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class EsquemaImport implements ToCollection
{
    /**
    * @param Collection $collection
    */

    protected $escuelaForm;
    protected $semestreForm;
    protected $cursoForm;
    protected $docenteForm;
    protected $cursosemestreForm;
    protected $request;

    function __construct($escuela,$semestre,$curso,$docente,$cursosemestre,$request) {
        $this->escuelaForm = $escuela;
        $this->semestreForm = $semestre;
        $this->cursoForm = $curso;
        $this->docenteForm = $docente;
        $this->cursosemestreForm = $cursosemestre;
        $this->request = $request;
    }

    public function collection(Collection $collection)
    {
        
        // foreach ($collection as $row) 
        // {
        //     $codigo = $row['codigo'];
        //     $detalle = $row['detalle'];

        //     error_log("EXPORTAR DETALLE: ");
        //     error_log($detalle);
        // }

        // for($i=0; $i<sizeof($collection); $i++)
        // {
        //     $codigo = $collection[$i][1];
        //     $detalle = $collection[$i][0];
        //     if(empty($codigo) && empty($detalle))
        //         $collection->forget($i);
        // }

        $validador = 1;
        $contadorCreados = 0;
        $validadorMensaje = 1;
        $mensaje = "";
        $array_Excel = [];

        error_log("IMPORTAR COLLECTION: ");
        error_log($collection);

        $escuelaExcel = $collection[2][3];
        $semestreExcel = $collection[3][3];
        $cursoExcel = $collection[4][3];
        $docenteExcel = $collection[5][3];
        $nombreEscuelaExcel = $collection[2][1];
        $nombreSemestreExcel = $collection[3][1];
        $nombreCursoExcel = $collection[4][1];
        $nombreDocenteExcel = $collection[5][1];

        // $cursosemestre = CursoSemestre::findOrFail($cursosemestreExcel);
        
        //CONSISTENCIA ENTRE SISTEMA Y EXCEL
        if($escuelaExcel != $this->escuelaForm){
            $validador = 0;
            $modificarerr1 = $this->request->session()->put('mensajeEsquema','La escuela en el archivo no corresponde');
            $modificarerrt1 = $this->request->session()->put('tipoMensajeEsquema','error');
            return;
        }

        if($semestreExcel != $this->semestreForm){
            $modificarerr1 = $this->request->session()->put('mensajeEsquema','El semestre en el archivo no corresponde');
            $modificarerrt1 = $this->request->session()->put('tipoMensajeEsquema','error');
            return;
        }

        if($cursoExcel != $this->cursoForm){
            $modificarerr1 = $this->request->session()->put('mensajeEsquema','El curso en el archivo no corresponde');
            $modificarerrt1 = $this->request->session()->put('tipoMensajeEsquema','error');
            return;
        }

        if($docenteExcel != $this->docenteForm){
            $modificarerr1 = $this->request->session()->put('mensajeEsquema','El docente en el archivo no corresponde');
            $modificarerrt1 = $this->request->session()->put('tipoMensajeEsquema','error');
            return;
        }

        $escuela = Escuela::where('id',$this->escuelaForm)->where('estado','A')->firstOrFail();
        $semestre = Semestre::where('id',$this->semestreForm)->where('estado','A')->firstOrFail();
        $curso = Curso::where('id',$this->cursoForm)->where('estado','A')->firstOrFail();
        $docente = Docente::where('id',$this->docenteForm)->where('estado','A')->firstOrFail();

        $nombreSemestreForm = $semestre->anio . "-" . $semestre->semestre;

        //CONSISTENCIA EXCEL
        if($nombreEscuelaExcel != $escuela->escuela){
            $validador = 0;
            $modificarerr2 = $this->request->session()->put('mensajeEsquema','El nombre de la escuela en el archivo fue modificado');
            $modificarerrt2 = $this->request->session()->put('tipoMensajeEsquema','error');
            return;
        }
        
        if($nombreSemestreExcel != $nombreSemestreForm){
            $modificarerr2 = $this->request->session()->put('mensajeEsquema','El nombre del semestre en el archivo fue modificado');
            $modificarerrt2 = $this->request->session()->put('tipoMensajeEsquema','error');
            return;
        }

        if($nombreCursoExcel != $curso->curso){
            $modificarerr2 = $this->request->session()->put('mensajeEsquema','El nombre del curso en el archivo fue modificado');
            $modificarerrt2 = $this->request->session()->put('tipoMensajeEsquema','error');
            return;
        }

        if($nombreDocenteExcel != $docente->nombre){
            $modificarerr2 = $this->request->session()->put('mensajeEsquema','El nombre del docente en el archivo fue modificado');
            $modificarerrt2 = $this->request->session()->put('tipoMensajeEsquema','error');
            return;
        }

        if(sizeof($collection) == 10){
            $modificarerr2 = $this->request->session()->put('mensajeEsquema','No se encuentran esquemas en el archivo');
            $modificarerrt2 = $this->request->session()->put('tipoMensajeEsquema','error');
            return;
        }
        
        //CONSISTENCIA ESQUEMAS

        for( $k = 10; $k<sizeof($collection); $k++){
            $codigo = $collection[$k][1];
            $detalle = $collection[$k][2];
            $temp = CursoEsquema::where('codigo',$codigo)->where('estado','A')->get();
                if(sizeof($temp) == 0){
                    $contadorCreados+=1;
                }
        }

        for( $j = 10; $j<sizeof($collection); $j++){
            $codigo = $collection[$j][1];
            $temp = CursoEsquema::where('codigo',$codigo)->where('estado','A')->get();
            if(sizeof($temp) > 0){
                $validador = 0;
                break;
            }
        }

        error_log("VALIDADOR!!!!!!!!!!!!");
        error_log($validador);

        if($validador == 1){
            for( $i=10; $i<sizeof($collection); $i++){
                $codigo = $collection[$i][1];
                $detalle = $collection[$i][2];
    
                error_log("EXPORTAR DETALLE: ");
                error_log($codigo);
                error_log($detalle);

                $temp = CursoEsquema::where('codigo',$codigo)->where('estado','A')->get();
                if(sizeof($temp) == 0){
                    $input['codigo'] = $codigo;
                    $input['detalle'] = $detalle;
                    $input['cursosemestre_id'] = $this->cursosemestreForm;
                    $esquema = CursoEsquema::create($input);
                }
    
                // $input['codigo'] = $codigo;
                // $input['detalle'] = $detalle;
                // $input['cursosemestre_id'] = $this->cursosemestreForm;
                // $esquema = CursoEsquema::create($input);
    
            }

            $modificar = $this->request->session()->put('mensajeEsquema','Se ingresaron '.$contadorCreados.' esquemas.');
            $modificart = $this->request->session()->put('tipoMensajeEsquema','success');

        }else{
            $modificarerr3 = $this->request->session()->put('mensajeEsquema','Se ingreso '.$contadorCreados.' esquemas excepto los que ya existen en el sistema');
            $modificarerrt3 = $this->request->session()->put('tipoMensajeEsquema','error');
        }

        // return $mensaje;
    }
}
