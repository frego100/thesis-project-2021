<?php

namespace App\Imports;

use App\Models\CursoEsquema;
use App\Models\Curso;
use App\Models\Escuela;
use App\Models\Semestre;
use App\Models\CursoGrupo;
use App\Models\CursoSemestre;
use App\Models\Docente;
use App\Models\Estudiante;
use App\Models\CursoEstudiante;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class AlumnoImport implements ToCollection
{
    /**
    * @param Collection $collection
    */

    protected $escuelaForm;
    protected $semestreForm;
    protected $cursoForm;
    protected $grupoForm;
    protected $docenteForm;
    protected $request;

    function __construct($escuela,$semestre,$curso,$grupo,$docente,$request) {
        $this->escuelaForm = $escuela;
        $this->semestreForm = $semestre;
        $this->cursoForm = $curso;
        $this->grupoForm = $grupo;
        $this->docenteForm = $docente;
        $this->request = $request;
    }

    public function collection(Collection $collection)
    {
        $validador = 1;
        $contadorgrupo = 0;
        $contadorgrupomensaje = 0;
        $contadorCreados = 0;
        $contadornoCreados = 0;
        $mensaje = "";
        $array_Excel = [];

        error_log("IMPORTAR COLLECTION: ");
        error_log($collection);

        $escuelaExcel = $collection[2][3];
        $semestreExcel = $collection[3][3];
        $cursoExcel = $collection[4][3];
        $grupoExcel = $collection[5][3];
        $docenteExcel = $collection[6][3];
        $nombreEscuelaExcel = $collection[2][1];
        $nombreSemestreExcel = $collection[3][1];
        $nombreCursoExcel = $collection[4][1];
        $nombreGrupoExcel = $collection[5][1];
        $nombreDocenteExcel = $collection[6][1];

        //CONSISTENCIA ENTRE SISTEMA Y EXCEL
        if($escuelaExcel != $this->escuelaForm){
            $validador = 0;
            $modificarerr1 = $this->request->session()->put('mensajeAlumno','La escuela en el archivo no corresponde');
            $modificarerrt1 = $this->request->session()->put('tipoMensajeAlumno','error');
            return;
        }

        if($semestreExcel != $this->semestreForm){
            $modificarerr1 = $this->request->session()->put('mensajeAlumno','El semestre en el archivo no corresponde');
            $modificarerrt1 = $this->request->session()->put('tipoMensajeAlumno','error');
            return;
        }

        if($cursoExcel != $this->cursoForm){
            $modificarerr1 = $this->request->session()->put('mensajeAlumno','El curso en el archivo no corresponde');
            $modificarerrt1 = $this->request->session()->put('tipoMensajeAlumno','error');
            return;
        }

        if($grupoExcel != $this->grupoForm){
            $modificarerr1 = $this->request->session()->put('mensajeAlumno','El grupo en el archivo no corresponde');
            $modificarerrt1 = $this->request->session()->put('tipoMensajeAlumno','error');
            return;
        }

        if($docenteExcel != $this->docenteForm){
            $modificarerr1 = $this->request->session()->put('mensajeAlumno','El docente en el archivo no corresponde');
            $modificarerrt1 = $this->request->session()->put('tipoMensajeAlumno','error');
            return;
        }

        $escuela = Escuela::where('id',$this->escuelaForm)->where('estado','A')->firstOrFail();
        $semestre = Semestre::where('id',$this->semestreForm)->where('estado','A')->firstOrFail();
        $curso = Curso::where('id',$this->cursoForm)->where('estado','A')->firstOrFail();
        $cursogrupo = CursoGrupo::where('id',$this->grupoForm)->where('estado','A')->firstOrFail();
        $docente = Docente::where('id',$this->docenteForm)->where('estado','A')->firstOrFail();

        $nombreSemestreForm = $semestre->anio . "-" . $semestre->semestre;

        //CONSISTENCIA EXCEL
        if($nombreEscuelaExcel != $escuela->escuela){
            $validador = 0;
            $modificarerr2 = $this->request->session()->put('mensajeAlumno','El nombre de la escuela en el archivo fue modificado');
            $modificarerrt2 = $this->request->session()->put('tipoMensajeAlumno','error');
            return;
        }

        if($nombreSemestreExcel != $nombreSemestreForm){
            $modificarerr2 = $this->request->session()->put('mensajeAlumno','El nombre del semestre en el archivo fue modificado');
            $modificarerrt2 = $this->request->session()->put('tipoMensajeAlumno','error');
            return;
        }

        if($nombreCursoExcel != $curso->curso){
            $modificarerr2 = $this->request->session()->put('mensajeAlumno','El nombre del curso en el archivo fue modificado');
            $modificarerrt2 = $this->request->session()->put('tipoMensajeAlumno','error');
            return;
        }

        if($nombreGrupoExcel != $cursogrupo->grupo){
            $modificarerr2 = $this->request->session()->put('mensajeAlumno','El nombre del grupo en el archivo fue modificado');
            $modificarerrt2 = $this->request->session()->put('tipoMensajeAlumno','error');
            return;
        }

        if($nombreDocenteExcel != $docente->nombre){
            $modificarerr2 = $this->request->session()->put('mensajeAlumno','El nombre del docente en el archivo fue modificado');
            $modificarerrt2 = $this->request->session()->put('tipoMensajeAlumno','error');
            return;
        }

        if(sizeof($collection) == 11){
            $modificarerr2 = $this->request->session()->put('mensajeAlumno','No se encuentran alumnos en el archivo');
            $modificarerrt2 = $this->request->session()->put('tipoMensajeAlumno','error');
            $validador = 0;
        }

        error_log("IMPORTAR VALIDADOR!!!!!!!!!!!!!!!: ");
        error_log($validador);

        if($validador == 1){
            for( $i=11; $i<sizeof($collection); $i++){
                $cui = $collection[$i][1];
                $alumno = Estudiante::where('cui',$cui)->where('estado','A')->get();

                if(sizeof($alumno) > 0){
                    $temp = CursoEstudiante::where('estudiante_id',$alumno[0]->id)->where('cursogrupo_id',$this->grupoForm)->where('estado','A')->get();
                    if(sizeof($temp) == 0){
                        $temp2 = CursoSemestre::where('id',$cursogrupo->cursosemestre_id)->where('estado','A')->firstOrFail();
                        $temp3 = CursoGrupo::where('cursosemestre_id',$temp2->id)->where('estado','A')->get();
                        foreach ($temp3 as $grupo){
                            $temp4 = CursoEstudiante::where('cursogrupo_id',$grupo->id)->where('estudiante_id',$alumno[0]->id)->where('estado','A')->get();
                            if(sizeof($temp4) > 0){
                                $contadorgrupo+=1;
                            }
                            error_log("CURSOSEMSTRES POR GRUPO");
                            error_log($temp3);
                        }
                        if($contadorgrupo == 0){
                            $input['estudiante_id'] = $alumno[0]->id;
                            $input['cursogrupo_id'] = $this->grupoForm;
                            $estudiante = CursoEstudiante::create($input);
                            $contadorCreados+=1;
                        }else{
                            $contadornoCreados+=1;
                        }
                    }
                }else{
                    error_log("ALUMNO NO REGISTRADO EN EL SISTEMA");
                }
    
                error_log("IMPORTAR ALUMNO: ");
                error_log($cui);
    
                // $input['estudiante_id'] = $alumno[0]->id;
                // $input['cursogrupo_id'] = $this->grupoForm;
                // $estudiante = CursoEstudiante::create($input);
    
            }
        }

        for( $j = 11; $j<sizeof($collection); $j++){
            $cui = $collection[$j][1];
            $alumno = Estudiante::where('cui',$cui)->where('estado','A')->get();
            if(sizeof($alumno) > 0){
                error_log("CUIIIIIIIIIIIII!!!!!!!!!!!!!!!: ");
                error_log($alumno);
                $temp = CursoEstudiante::where('estudiante_id',$alumno[0]->id)->where('cursogrupo_id',$this->grupoForm)->where('estado','A')->get();
                if(sizeof($temp) == 0){
                    $temp2 = CursoSemestre::where('id',$cursogrupo->cursosemestre_id)->where('estado','A')->firstOrFail();
                    $temp3 = CursoGrupo::where('cursosemestre_id',$temp2->id)->where('estado','A')->get();
                    foreach ($temp3 as $grupo){
                        $temp4 = CursoEstudiante::where('cursogrupo_id',$grupo->id)->where('estudiante_id',$alumno[0]->id)->where('estado','A')->get();
                        if(sizeof($temp4) > 0){
                            $contadorgrupomensaje +=1;
                        }
                        error_log("CURSOSEMSTRES POR GRUPO");
                        error_log($temp4);
                    }
                    if($contadorgrupomensaje == 0){
                    }else{
                        $modificarerr3 = $this->request->session()->put('mensajeAlumno','Se ingreso '.$contadornoCreados.' alumnos excepto los que ya existen en el curso, porque ya estan matriculados en el curso');
                        $modificarerrt3 = $this->request->session()->put('tipoMensajeAlumno','error');
                        break;
                    }
                }else{
                    $modificarerr3 = $this->request->session()->put('mensajeAlumno','Se ingreso '.$contadorCreados.' alumnos excepto los que ya existen en el grupo, porque ya estan matriculados en el grupo');
                    $modificarerrt3 = $this->request->session()->put('tipoMensajeAlumno','error');
                    break;
                }
            }else{
                error_log("ALUMNO NO REGISTRADO EN EL SISTEMA");
                $modificarerr3 = $this->request->session()->put('mensajeAlumno','Se ingreso '.$contadorCreados.' de alumnos excepto los que no estan registrados en el sistema');
                $modificarerrt3 = $this->request->session()->put('tipoMensajeAlumno','error');
                break;
            }
            $modificar = $this->request->session()->put('mensajeAlumno','Se ingresaron '.$contadorCreados.' alumnos.');
            $modificart = $this->request->session()->put('tipoMensajeAlumno','success');
        }
    }
}
