<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Semestre extends Model
{
    use HasFactory;

    protected $table = "semestres";
    protected $fillable = [
        "id",
        "semestre",
        "anio",
        "inicio",
        "fin",
        "estado"
    ];

    //relaciones
    public function escuelasemestres(){
        return $this->hasMany(EscuelaSemestre::class);
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }
}
