<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CursoSemestre extends Model
{
    use HasFactory;
    protected $table = "curso_semestres";
    protected $fillable=[
        "curso_id",
        "escuelasemestre_id",
        "estado"
    ];
    //relaciones
    public function curso()
    {
    	return $this->belongsTo(Curso::class);
    }
    public function escuelasemestre()
    {
    	return $this->belongsTo(EscuelaSemestre::class);
    }
    public function cursoesquemas()
    {
    	return $this->hasMany(CursoEsquema::class);
    }
    public function cursomatrizs()
    {
    	return $this->hasMany(CursoMatriz::class);
    }
    public function cursogrupos()
    {
    	return $this->hasMany(CursoGrupo::class);
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }
}
