<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    use HasFactory;

    protected $table = "cursos";
    protected $fillable = [
        "id",
        "plan_id",
        "cursotipo_id",
        "codigo",
        "curso",
        "descripcion",
        "semestre",
        "horasteoria",
        "horaspractica",
        "creditos",
        "estado"
    ];

    public function cursotipo()
    {
        return $this->belongsTo(CursoTipo::class);
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function cursosemestres()
    {
        return $this->hasMany(CursoSemestre::class);
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }
}
