<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Facultad extends Model
{
    use HasFactory;
    protected $table = "facultads";

    protected $fillable = [
        "facultad",
        "estado"
    ];

    
    //relaciones
    public function escuelas(){
        return $this->hasMany(Escuela::class);
    }


    public function getName()
    {
        return 'facultad';
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }
}
