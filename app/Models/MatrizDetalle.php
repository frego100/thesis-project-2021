<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MatrizDetalle extends Model
{
    use HasFactory;

    protected $table = "matriz_detalles";
    protected $fillable = [
        "id",
        "criterio_id",
        "cursomatriz_id",
        "cursoesquema_id",
        "ponderacion",
        "estado"
    ];

    public function cursomatriz()
    {
        return $this->belongsTo(CursoMatriz::class);
    }

    public function criterio()
    {
        return $this->belongsTo(Criterio::class);
    }

    public function cursoesquema()
    {
        return $this->belongsTo(CursoEsquema::class);
    }

    public function estudiantematrizs()
    {
        return $this->hasMany(EstudianteMatriz::class);
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }
}
