<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CursoMatriz extends Model
{
    use HasFactory;

    protected $table = "curso_matrizs";

    protected $fillable =
    [
    	"cursosemestre_id",
    	"matriz",
    	"ponderacion",
    	"estado",
    	"fechacalificacioninicio",
    	"fechacalificacionfin"
    ];

    //relaciones
    public function cursosemestre()
    {
    	return $this->belongsTo(CursoSemestre::class);
    }

    public function matrizdetalles()
    {
    	return $this->hasMany(MatrizDetalle::class);
    }

    public function datomatrizs()
    {
        return $this->hasMany(DatoMatriz::class);
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }
}
