<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Docente extends Model
{
    use HasFactory;

	protected $table = "docentes";

    protected $fillable =
    [
        "nombre",
        "email",
        "superadmin",
        "admin",
        "docente",
        "estado"
    ];

    //relaciones

    public function cursogrupos()
    {
    	return $this->hasMany(Cursogrupo::class);
    }

    public static function findByEmail($email)
    {
        return static::where('email',$email)->where('estado','A')->first();
    }
    
    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }
}
