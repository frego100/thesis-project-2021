<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CursoGrupo extends Model
{
    use HasFactory;

    protected $table = "curso_grupos";

    protected $fillable =
    [
    	"cursosemestre_id",
    	"docente_id",
    	"grupo",
    	"coordinador",
    	"estado"
    ];

    //relaciones
    public function cursosemestre()
    {
    	return $this->belongsTo(CursoSemestre::class);
    }

    public function docente()
    {
    	return $this->belongsTo(Docente::class);
    }

    public function cursoestudiantes()
    {
    	return $this->hasMany(CursoEstudiante::class);
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }
}
