<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use HasFactory;

    protected $table = "plans";

    protected $fillable = [
        "anio",
        "estado",
        "escuela_id"
    ];

    protected $hidden=[
        "created_at",
        "updated_at"
    ];
    
    //relaciones 
    public function escuela(){
        return $this->belongsTo(Escuela::class);
    }
   
    public function competencias(){
        return $this->hasMany(Competencia::class);
    }
    public function resultadosestudiantes(){
        return $this->hasMany(ResultadosEstudiante::class);
    }
    public function cursos(){
        return $this->hasMany(Curso::class);
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }
}
