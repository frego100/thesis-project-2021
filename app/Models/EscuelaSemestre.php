<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EscuelaSemestre extends Model
{
    use HasFactory;
    protected $table = "escuela_semestres";
    protected $fillable=[
        "escuela_id",
        "semestre_id",
        "estadosemestre",
        "estado"
    ];
    //relaciones
    public function escuela()
    {
    	return $this->belongsTo(Escuela::class);
    }
    public function semestre()
    {
    	return $this->belongsTo(Semestre::class);
    }
    public function cursosemestres()
    {
    	return $this->hasManys(CursoSemestre::class);
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }
}
