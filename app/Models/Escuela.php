<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Escuela extends Model
{
    use HasFactory;
    protected $table = "escuelas";

    protected $fillable = [
        "escuela",
        "estado",
        "facultad_id"
    ];
    
    protected $hidden=[
        "created_at",
        "updated_at"
    ];

    //relaciones 
      public function facultad(){
        return $this->belongsTo(Facultad::class);
    }

     public function plans(){
        return $this->hasMany(Plan::class);
    }

    public function escuelasemestres(){
        return $this->hasMany(EscuelaSemestre::class);
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }
}
