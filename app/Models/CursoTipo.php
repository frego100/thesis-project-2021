<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CursoTipo extends Model
{
    use HasFactory;
    protected $table = "curso_tipos";

    protected $fillable = [
        "cursotipo",
        "estado",
    ];

    protected $hidden=[
        "created_at",
        "updated_at"
    ];


   //relaciones
   public function cursos(){
    return $this->hasMany(Curso::class);
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }
}
