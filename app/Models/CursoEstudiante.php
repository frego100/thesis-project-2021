<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CursoEstudiante extends Model
{
    use HasFactory;

    protected $table = "curso_estudiantes";

    protected $fillable = 
    [
        "estado",
        "cursogrupo_id",
        "estudiante_id"
    ];

    //relaciones 
    public function cursogrupo(){
        return $this->belongsTo(CursoGrupo::class);
    }
    public function estudiante(){
        return $this->belongsTo(Estudiante::class);
    }
    public function estudiantematrizs(){
        return $this->hasMany(EstudianteMatriz::class);
    }

    public function datomatrizs()
    {
        return $this->hasMany(DatoMatriz::class);
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }

}
