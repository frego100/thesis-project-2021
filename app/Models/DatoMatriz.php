<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatoMatriz extends Model
{
    use HasFactory;

    protected $table = "dato_matrizs";

    protected $fillable =
    [
    	"curso_matriz_id",
    	"curso_estudiante_id",
    	"titulo",
    	"resumen",
        "trivial",
        "pertinencia",
    	"estado"
    ];

    //relaciones
    public function cursoestudiante()
    {
    	return $this->belongsTo(CursoEstudiante::class);
    }

    public function cursomatriz()
    {
    	return $this->belongsTo(CursoMatriz::class);
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }
}
