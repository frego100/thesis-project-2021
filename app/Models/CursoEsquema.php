<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CursoEsquema extends Model
{
    use HasFactory;

    protected $table = "curso_esquemas";

    protected $fillable =
    [
    	"cursosemestre_id",
    	"detalle",
    	"codigo",
    	"estado"
    ];

    //relaciones
    public function cursosemestre()
    {
    	return $this->belongsTo(CursoSemestre::class);
    }

    public function matrizdetalles()
    {
    	return $this->hasMany(MatrizDetalle::class);
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }
}
