<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompetenciasRE extends Model
{
    use HasFactory;
    protected $table="competencias_res";
    protected $fillable=[
        "resultadoestudiante_id",
        "competencia_id",
        "estado"
    ];
    protected $hidden=[
        "created_at",
        "updated_at"
    ];
    //relaciones
    public function resultadoestudiante(){
        return $this->belongsTo(ResultadosEstudiante::class);
    }
    public function competencia(){
        return $this->belongsTo(Competencia::class);
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }
}
