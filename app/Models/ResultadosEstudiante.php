<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResultadosEstudiante extends Model
{
    use HasFactory;

    protected $table = "resultados_estudiantes";
    protected $fillable = [
        "id",
        "plan_id",
        "codigore",
        "re",
        "estado"
    ];

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function competenciasres()
    {
        return $this->hasMany(CompetenciasRE::class);
    }

    public function criterios()
    {
        return $this->hasMany(Criterio::class);
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }
}
