<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstudianteMatriz extends Model
{
    use HasFactory;

    protected $table = "estudiante_matrizs";

    protected $fillable =
    [
    	"matriz_detalle_id",
    	"curso_estudiante_id",
    	"nota",
    	"estado",
    ];

    //relaciones
    public function matrizdetalle()
    {
    	return $this->belongsTo(MatrizDetalle::class);
    }

    public function cursoestudiante()
    {
    	return $this->belongsTo(CursoEstudiante::class);
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }
}
