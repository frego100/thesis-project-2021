<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Criterio extends Model
{
    use HasFactory;
    protected $table="criterios";
    protected $fillable=[
        "codigo",
        "criterio",
        "insatisfactorio",
        "enproceso",
        "satisfactorio",
        "sobresaliente",
        "estado",
        "resultadoestudiante_id"
    ];
    public function resultadoestudiante()
    {
    	return $this->belongsTo(ResultadosEstudiante::class);
    }
    public function matrizdetalles()
    {
    	return $this->hasMany(MatrizDetalle::class);
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }

}
