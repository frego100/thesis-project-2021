<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    use HasFactory;
    protected $table = "estudiantes" ;
    protected $fillable = [
        "estudiante",
        "cui",
        "dni",
        "email",
        "estado"
    ];
    public function cursoestudiantes()
    {
    	return $this->hasMany(CursoEstudiante::class);
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }
    
}
