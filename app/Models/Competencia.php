<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Competencia extends Model
{
    use HasFactory;

    protected $table = "competencias";
    protected $fillable = [
        "id",
        "plan_id",
        "codigo",
        "competencia",
        "estado"
    ];

    public function plan(){
        return $this->belongsTo(Plan::class);
    }

    public function competenciasres()
    {
        return $this->hasMany(CompetenciasRE::class);
    }

    //formato de datetime
    public function getDateFormat()
    {
        return 'Y-m-d\TH:i:s.v';
    }
}
