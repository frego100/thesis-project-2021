$(function() {
    $(".menu").click(function() {
        $(".page-wrapper").toggleClass("toggled");
        $(".keep").toggleClass("width");
        $(".navbar").toggleClass("toggled");
    });
});

$('ul li').on('click', function() {
    $('li').removeClass('active');
    $(this).addClass('active');
});