<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- bootstrap css-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Styles -->

    <link rel="stylesheet" href="https://use.fontawesome.com/c11bd11b0a.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">

    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styleadmin.css') }}" rel="stylesheet">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="https://kit.fontawesome.com/50fc801e50.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">



</head>

<body>
    @include('includes.adminavbar')
    <div class="page-wrapper chiller-theme">
        @include('includes.lateralAdmin')
        <main class="page-content">
            @yield('content')
        </main>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
    crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('js/admin.js') }}"></script>


    <script>
        $(document).ready( function () {
            $('#cursosemestre').DataTable({
                
                "language":{
                    "lengthMenu": "Mostrar _MENU_ por pagina",
                    "zeroRecords": "Ningun resultado para esta tabla",
                    "info": "Mostrando _PAGE_ de _PAGES_",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "Buscar",
                    "paginate": {
                        "first": "Primer",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                autoFill: true,
                colReorder: true,
             
                scrollCollapse: true
                
                
            });
            $('#cursogrupo').DataTable({
                
                "language":{
                    "lengthMenu": "Mostrar _MENU_ por pagina",
                    "zeroRecords": "Ningun resultado para esta tabla",
                    "info": "Mostrando _PAGE_ de _PAGES_",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "Buscar",
                    "paginate": {
                        "first": "Primer",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                autoFill: true,
                colReorder: true,
             
                scrollCollapse: true
                
                
            });

            $('#competencia').DataTable({
                
                "language":{
                    "lengthMenu": "Mostrar _MENU_ por pagina",
                    "zeroRecords": "Ningun resultado para esta tabla",
                    "info": "Mostrando _PAGE_ de _PAGES_",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "Buscar",
                    "paginate": {
                        "first": "Primer",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                autoFill: true,
                colReorder: true,
             
                scrollCollapse: true
                
                
            });

            $('#curso').DataTable({
                
                "language":{
                    "lengthMenu": "Mostrar _MENU_ por pagina",
                    "zeroRecords": "Ningun resultado para esta tabla",
                    "info": "Mostrando _PAGE_ de _PAGES_",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "Buscar",
                    "paginate": {
                        "first": "Primer",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                autoFill: true,
                colReorder: true,
             
                scrollCollapse: true
                
                
            });

            
            $('#resultadosestudiante').DataTable({
                
                "language":{
                    "lengthMenu": "Mostrar _MENU_ por pagina",
                    "zeroRecords": "Ningun resultado para esta tabla",
                    "info": "Mostrando _PAGE_ de _PAGES_",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "Buscar",
                    "paginate": {
                        "first": "Primer",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                autoFill: true,
                colReorder: true,
             
                scrollCollapse: true
                
                
            });

            $('#criterio').DataTable({
                
                "language":{
                    "lengthMenu": "Mostrar _MENU_ por pagina",
                    "zeroRecords": "Ningun resultado para esta tabla",
                    "info": "Mostrando _PAGE_ de _PAGES_",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "Buscar",
                    "paginate": {
                        "first": "Primer",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                autoFill: true,
                colReorder: true,
             
                scrollCollapse: true
                
                
            });

            
            $('#resultadosECompetencia').DataTable({
                
                "language":{
                    "lengthMenu": "Mostrar _MENU_ por pagina",
                    "zeroRecords": "Ningun resultado para esta tabla",
                    "info": "Mostrando _PAGE_ de _PAGES_",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "Buscar",
                    "paginate": {
                        "first": "Primer",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                autoFill: true,
                colReorder: true,
             
                scrollCollapse: true
                
                
            });


            $('#myTable2').DataTable();
    } );
    </script>

</body>

</html>
