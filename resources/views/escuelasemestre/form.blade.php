<div class="form-group {{ $errors->has('escuela') ? 'has-error' : ''}}">
    <label for="escuela" class="control-label">{{ 'Escuela' }}</label>
    <select name="escuela" class="form-control" id="escuela" required>
        @foreach($escuelas as $item)              
        <option value="{{ $item->id }}" {{ isset($escuelasemestre->escuela_id) ?  ($item->id == $escuelasemestre->escuela_id ? 'selected' : '') : '' }}>
            {{ $item->escuela }}
        </option>                      
    @endforeach  
</select>
    {!! $errors->first('plan', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('semestre') ? 'has-error' : ''}}">
    <label for="semestre" class="control-label">{{ 'Semestre' }}</label>
    <select name="semestre" class="form-control" id="semestre" required>
        @foreach($semestres as $item)              
        <option value="{{ $item->id }}" {{ isset($escuelasemestre->semestre_id) ?  ($item->id == $escuelasemestre->semestre_id ? 'selected' : '') : '' }}>
            {{ $item->semestre }}
        </option>                      
    @endforeach  
</select>
    {!! $errors->first('plan', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn boton-color btn-sm" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
</div>

