@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="row justify-content-around">
                            <div class="col">Ver</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/resultadosEstudiante.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">

                        <a href="{{ url('resultadosestudiantes') }}" title="Cancelar"><button class="btn boton-color btn-sm"><i
                                    class="fa fa-arrow-left" aria-hidden="true"></i> Cancelar</button></a>
                        <form method="POST" action="{{ url('resultadosestudiantes/' . $resultadosestudiante->id) }}"
                            accept-charset="UTF-8" style="display:inline">
                            @method('put')
                            {{ csrf_field() }}
                            <button type="submit" class="btn boton-color2 btn-sm" title="Editar"><i
                                    class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                        </form>
                        <form method="POST" action="{{ url('resultadosestudiantes' . $resultadosestudiante->id) }}"
                            accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn boton-color2 btn-sm" title="Eliminar"
                                onclick="return confirm(&quot;¿Seguro que quiere eliminar?&quot;)"><i class="fa fa-trash-o"
                                    aria-hidden="true"></i></button>
                        </form>
                        <br />
                        <br />

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $resultadosestudiante->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Codigo </th>
                                        <td> {{ $resultadosestudiante->codigore }} </td>
                                    </tr>
                                    <tr>
                                        <th> Resultado de Estudiante </th>
                                        <td> {{ $resultadosestudiante->re }} </td>
                                    </tr>
                                    <tr>
                                        <th> Estado </th>
                                        <td> {{ $resultadosestudiante->estado }} </td>
                                    </tr>
                                    <tr>
                                        <th> Plan </th>
                                        <td> {{ $resultadosestudiante->plan->anio }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
