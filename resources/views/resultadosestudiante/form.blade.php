<div class="form-group {{ $errors->has('codigore') ? 'has-error' : '' }}">
    <label for="codigore" class="control-label">{{ 'Codigo' }}</label>
    <input class="form-control" name="codigore" type="text" id="codigore" placeholder="R.#"
        value="{{ isset($resultadosestudiante->codigore) ? $resultadosestudiante->codigore : '' }}" required>
    {!! $errors->first('codigore', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('re') ? 'has-error' : '' }}">
    <label for="re" class="control-label">{{ 'Resultado Estudiante' }}</label>
    <textarea class="form-control" name="re" type="text" id="re"
        value="{{ isset($resultadosestudiante->re) ? $resultadosestudiante->re : '' }}"required>{{isset($resultadosestudiante->re)?$resultadosestudiante->re:'' }}</textarea>
    {!! $errors->first('re', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('plan') ? 'has-error' : '' }}">
    <label for="plan" class="control-label">{{ 'plan' }}</label>
    <select name="plan" class="form-control" id="plan">
        @foreach ($planes as $plan)
            <option value="{{ $plan->id }}"
                {{ isset($resultadosestudiante->plan_id) ? ($plan->id == $resultadosestudiante->plan_id ? 'selected' : '') : '' }}>
                {{ $plan->anio }}
            </option>
        @endforeach
    </select>
    {!! $errors->first('plan', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn boton-color btn-sm" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
</div>
