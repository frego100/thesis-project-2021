@extends('layouts.adminlayout')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Resultado Especifico') }}</div>

                <div class="card-body">

                            <div class="form-group {{ $errors->has('facultad') ? 'has-error' : ''}}">
                                <label for="facultad" class="control-label">{{ 'Facultad' }}</label>
                                <select name="facultad" class="form-control" id="facultad" >
                                @foreach($facultades as $item)
                                <option value="{{ $item->id }}">{{ $item->facultad }}</option>
                                @endforeach   

                            </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>


                           <div class="form-group {{ $errors->has('escuela') ? 'has-error' : ''}}">
                                <label for="escuela" class="control-label">{{ 'Escuela' }}</label>
                                <select name="escuela" class="form-control" id="escuela" >
                                @foreach($escuelas as $item)
                                <option value="{{ $item->id }}">{{ $item->escuela }}</option>
                                @endforeach   
                                                               

                            </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div> 
                            
                            
                           <div class="form-group {{ $errors->has('plan') ? 'has-error' : ''}}">
                                <label for="plan" class="control-label">{{ 'Plan de Estudio' }}</label>
                                <select name="plan" class="form-control" id="plan" >
                                @foreach($planes as $item)
                                <option value="{{ $item->id }}">{{ $item->anio }}</option>
                                @endforeach
                                                               

                            </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>   

                            <div class="form-group {{ $errors->has('re') ? 'has-error' : ''}}">
                                <label for="re" class="control-label">{{ 'Resultado del Estudiante' }}</label>
                                <select name="re" class="form-control" id="re" >
                                @foreach($resultadosestudiantes as $item)
                                <option value="{{ $item->id }}">{{ $item->re }}</option>
                                @endforeach
                                                               

                            </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div> 

                            <table class="table table-bordered" name="criterio" id="criterio">
                            <thead class="thead-light">
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Codigo</th>
                                <th scope="col">Criterio</th>  
                                <th scope="col">Insatisfactorio</th>  
                                <th scope="col">En Proceso</th>  
                                <th scope="col">Satisfactorio</th>  
                                <th scope="col">Sobresaliente</th>                                                               
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($criterios as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->codigo }}</td>
                                        <td>{{ $item->criterio }}</td>
                                        <td>{{ $item->insatistactorio }}</td>
                                        <td>{{ $item->enproceso }}</td>
                                        <td>{{ $item->satisfactorio }}</td>
                                        <td>{{ $item->sobresaliente }}</td> 
                                    </tr>
                                @endforeach
                            </tbody>
                            </table>                                                                                        
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function ()
    {
        function cargaescuelas() 
        {
            var facultad_id = $('#facultad').val();
            if(facultad_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.escuelas') }}",
                    data:
                    {
                        facultad_id: facultad_id
                    },  
                    success:function (data) 
                    {
                        $('#escuela').empty();
                        $.each(data.escuelas[0].escuelas,function(index,escuela)
                        {
                                    
                            $('#escuela').append('<option value="'+escuela.id+'">'+escuela.escuela+'</option>');
                        });
                        cargaplanes();
                    }
                });
            }
            else
            {
                $('#escuela').empty();
                cargaplanes();
            }

        };

        function cargaplanes() 
        {
            var escuela_id = $('#escuela').val();
            if(escuela_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.planes') }}",
                    data: 
                    {
                        escuela_id: escuela_id
                    },
                    success:function (data) 
                    {
                        $('#plan').empty();
                        $.each(data.planes[0].plans,function(index,plan)
                        {    
                            $('#plan').append('<option value="'+plan.id+'">'+plan.anio+'</option>');
                        });
                        cargaResultadoEstudiante();
                    }
                });
            }
            else
            {
                $('#plan').empty();
                cargaResultadoEstudiante();
            }
                    
        };

        function cargaResultadoEstudiante(){
            var plan_id = $('#plan').val();
            if(plan_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.re') }}",
                    data: 
                    {
                        plan_id: plan_id
                    },
                    success:function (data) 
                    {
                        $('#re').empty();
                        $.each(data.resultadosestudiantes[0].resultadosestudiantes,function(index,re)
                        {    
                            $('#re').append('<option value="'+re.id+'">'+re.re+'</option>');
                        });
                        cargaCriterio();
                    }
                });
            }
            else
            {
                $('#re').empty();
                cargaCriterio();
            }
        };  

        
        function cargaCriterio()
        { 
            var re_id = $('#re').val();
            if(re_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.criterio') }}", 
                    data:
                    {
                        re_id: re_id
                    },
                    success:function (data)
                    {
                        $('#criterio').empty();
                        $('#criterio').append('<thead class="thead-light"><tr><th scope="col">#</th><th scope="col">Codigo</th><th scope="col">Criterio</th></tr></thead><tbody>');

                        $.each(data.criterios[0].criterios,function(index,criterio)
                        {    
                            $('#criterio').append('<tr><td>'+index+'</td><td>'+criterios.codigo+'</td><td>'+criterio.criterio+'</td></tr>');
                        });
                        $('#criterio').append('</tbody>');
                    }
                });
            }
            else
            {
                $('#criterio').empty();
                $('#criterio').append('<thead class="thead-light"><tr><th scope="col">#</th><th scope="col">Codigo</th><th scope="col">Criterio</th></tr></thead><tbody>');
            }
        }

        $('#facultad').on('change', cargaescuelas);
        $('#escuela').on('change', cargaplanes);
        $('#plan').on('change', cargaResultadoEstudiante);
        $('#re').on('change', cargaCriterio);
       

    });


</script>

@endsection