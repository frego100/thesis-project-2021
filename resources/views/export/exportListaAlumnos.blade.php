
<table>
    <thead>
        <tr><th>Universidad Nacional de San Agustin de Arequipa</th></tr>
        <tr><th>{{$escuela->escuela}}</th></tr>
        <tr><th>Semestre:</th><th> {{ $semestre->anio }}-{{ $semestre->semestre }}</th></tr>
        <tr></tr>
        <tr><th>Curso:</th><th>{{ $curso->curso }}</th></tr>
        <tr><th>Docente:</th><th>{{ $docente->nombre }}</th></tr>
    </thead>
</table>

<br><br>
<table>
<thead>
<tr>
    <th style="width: 20px"></th>
    <th style="width: 40px"></th>
    <th></th>
    @php
    $sumaponderacion = "";
    $columnapond = 68;
    $k = 0;
    @endphp
    @foreach($matrizdetalle as $md)
    <th style="border:thick; width: 10px; text-align: center">{{$md->ponderacion}}</th>
    @php
    $sumaponderacion = $sumaponderacion.(chr($columnapond+$k)).'10';
    if($k!=sizeof($matrizdetalle)-1)
        $sumaponderacion = $sumaponderacion.'+';
    $k+=1;
    @endphp
    @endforeach
    <th>={{$sumaponderacion}}</th>
</tr>
<tr>
    <th style="width: 20px"></th>
    <th style="width: 40px"></th>
    <th></th>
    @foreach($esquemas as $esquema)
    <th style="border:thick; width: 10px">{{$esquema->detalle}}</th>
    @endforeach
    <th style="border:thick; width:15px; font-weight: bold; vertical-align:center; text-align:center" rowspan="2">Promedio</th>
    <th></th>
    <th style="border:thick; width: 10px; font-weight: bold; vertical-align:center; text-align:center" colspan="{{sizeof($criteriost)}}">RESUMEN - RESULTADOS ESTUDIANTES</th>
</tr>
<tr>
    <th style="border:thick; width: 20px; text-align: center">N°</th>
    <th style="border:thick; width: 40px">Apellidos y Nombres</th>
    <th style="border:thick; text-align: center">Grupo</th>
    @foreach($criterios as $criterio)
    <th style="border:thick; width: 10px; font-weight: bold; text-align: center">{{$criterio->codigo}}</th>
    @endforeach
    <th ></th>
    @foreach($criteriost as $cri)
        <th style="border:thick; width: 10px; font-weight: bold; text-align: center">{{$cri->codigo}}</th>
    @endforeach
</tr>
</thead>
@php
$fila = 13;
$filacriterios = 13;
@endphp
<tbody>
    @foreach($reporte as $rep)
        @php
        $cont++;
        $columna = 68;
        $promedio = "";
        $cantidad = 0;
        $i = 0;
        @endphp
        <tr>
            <td style="border:medium; width: 20px">{{$cont}}</td>
            <td style="border:medium">{{ $rep->estudiante->datos->estudiante }}</td>
            <td style="border:medium; text-align: center">{{ $grupo->grupo }}</td>
            @foreach($rep->notas as $nota)
                @if($nota!=null)
                <td style="border:medium; text-align: center">{{$nota->nota}}</td>
                @else
                <td style="border:medium; text-align: center">0</td>
                @endif

                @php
                if($columna>90){
                    $formula = '(A'.(chr($columna-26).$fila).'*A'.(chr($columna-26)).'10/100'.')';
                }else{
                    $formula = '('.(chr($columna).$fila).'*'.(chr($columna)).'10/100'.')';
                }
                $promedio = $promedio.$formula;
                $columna+=1;
                if($i!=sizeof($rep->notas)-1)
                    $promedio = $promedio.'+';
                $i+=1;
                @endphp
            @endforeach
        <td style="border:medium; text-align: center">={{$promedio}}</td>
        @php
        $fila+=1;
        @endphp
        <td></td>
        @php
        $columnacriterios = 68;
        @endphp
        @foreach($criteriost as $cri)
        @php
            $promediocriterios = "(";
            for($i=0;$i<sizeof($criterios);$i++){
                if($cri->codigo == $criterios[$i]->codigo){
                    $promediocriterios = $promediocriterios.(chr($columnacriterios+$i)).$filacriterios;
                    $cantidad+=1;
                    if($i!=sizeof($criterios)-1){
                        $promediocriterios = $promediocriterios.'+';
                    }
                }
            }
            if(substr($promediocriterios,-1) == '+')
                $promediocriterios = substr($promediocriterios,0,strlen($promediocriterios)-1);
            $promediocriterios = $promediocriterios.')';
            if($cantidad>1)
                $promediocriterios = $promediocriterios.'/'.$cantidad;
            $cantidad = 0;
        @endphp
        <td style="border:medium; width: 20px; text-align: center">={{$promediocriterios}}</td>
        @endforeach
        @php
        $filacriterios+=1;
        @endphp
        </tr>
    @endforeach
    </tbody>
</table>