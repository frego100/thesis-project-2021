
<table>
    <thead>
        <tr><th>Universidad Nacional de San Agustin de Arequipa</th></tr>
        <tr><th>{{$escuela->escuela}}</th></tr>
        <tr><th>Semestre:</th><th> {{ $semestre->anio }}-{{ $semestre->semestre }}</th></tr>
        <tr></tr>
        <tr><th>Curso:</th><th>{{ $curso->curso }}</th></tr>
        <tr><th>Docente:</th><th>{{ $docente->nombre }}</th></tr>
    </thead>
</table>

<br><br>
<table>
<thead>
<tr>
    <th style="width: 20px"></th>
    <th style="width: 40px"></th>
    <th></th>
    @foreach($cursomatriz as $cm)
    <th style="border:thick; width: 10px; font-weight: bold; text-align: center">{{$cm->matriz}}</th>
    @endforeach
    <th style="border:thick; width:15px; font-weight: bold; vertical-align:center; text-align:center"rowspan="2">Promedio</th>
</tr>
<tr>
    <th style="border:thick; width: 20px; text-align: center">N°</th>
    <th style="border:thick; width: 40px">Apellidos y Nombres</th>
    <th style="border:thick; text-align: center">Grupo</th>
    @foreach($cursomatriz as $cm1)
    <th style="border:thick; width: 10px; font-weight: bold; text-align: center">{{$cm1->ponderacion}}</th>
    @endforeach
</tr>
</thead>
@php
$filanota = 13;
$fila = 12;
@endphp
<tbody>
    @foreach($reporte as $rep)
        @php
        $cont++;
        $columnanota = 68;
        $columna = 68;
        $promedio = "";
        $promediofinal = "";
        $i = 0;
        $j=0;
        $contador = 0;
        @endphp
        <tr>
            <td style="border:medium; width: 20px">{{$cont}}</td>
            <td style="border:medium">{{ $rep->estudiante->datos->estudiante }}</td>
            <td style="border:medium; text-align: center">{{ $grupo->grupo }}</td>
            @foreach($cursomatriz as $cm2)
                @php
                $inc = sizeof($esquemas[$contador]);
                $columnanota = $columnanota + $inc;
                if($columnanota>90){
                    $promedio = '\''.$cm2->matriz.'\''.'!A'.(chr($columnanota-26)).$filanota;
                }else{
                    $promedio = '\''.$cm2->matriz.'\''.'!'.(chr($columnanota)).$filanota;
                }
                $columnanota = 68;
                @endphp
                <td style="border:medium; text-align: center">={{$promedio}}</td>

                @php
                if($columna>90){
                    $formula = '(A'.(chr($columna-26).$fila).'*A'.(chr($columna-26)).'11/100'.')';
                }else{
                    $formula = '('.(chr($columna).$fila).'*'.(chr($columna)).'11/100'.')';
                }
                $promediofinal = $promediofinal.$formula;
                $columna+=1;
                if($j!=sizeof($cursomatriz)-1)
                    $promediofinal = $promediofinal.'+';
                $j+=1;
                $contador+=1;
                @endphp
            @endforeach
            <td style="border:medium; text-align: center">={{$promediofinal}}</td>
        @php
        $filanota+=1;
        $fila+=1;
        @endphp
        </tr>
    @endforeach
    </tbody>
</table>