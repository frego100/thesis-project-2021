@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Vista {{ $cursoesquema->id }}</div>
                <div class="card-body">

                    <a href="{{ url('cursoesquemas') }}" title="Back"><button class="btn boton-color2 btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i>Regresar</button></a>
                    <form method="POST" action="{{ url('cursoesquemas/' . $cursoesquema->id) }}" accept-charset="UTF-8" style="display:inline">
                        @method('put')
                        {{ csrf_field() }}
                        <button type="submit" class="btn boton-color btn-sm" title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                    </form>
                    <form method="POST" action="{{ url('cursoesquemas' . $cursoesquema->id) }}" accept-charset="UTF-8" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn boton-color btn-sm" title="Eliminar" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    </form>
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $cursoesquema->id }}</td>
                                </tr>
                                <tr>
                                    <th> Estado </th>
                                    <td> {{ $cursoesquema->codigo }} </td>
                                </tr>
                                <tr>
                                    <th> Escuela </th>
                                    <td> {{ $cursoesquema->detalle }} </td>
                                </tr>
                                <tr>
                                    <th> Semestre </th>
                                    <td> {{ $cursoesquema->estado }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection