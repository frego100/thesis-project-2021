<div class="form-group {{ $errors->has('cursoesquema') ? 'has-error' : ''}}">
    <label for="cursoesquema" class="control-label">{{ 'Curso' }}</label>
    <select name="cursoesquema" class="form-control" id="cursoesquema" required>
        @foreach($cursoesquema as $item)              
        <option value="{{ $item->id }}" {{ isset($cursoesquema->cursosemestre_id) ?  ($item->id == $cursoesquema->cursosemestre_id ? 'selected' : '') : '' }}>
            {{ $item->cursoesquema }}
        </option>                      
    @endforeach  
</select>
    {!! $errors->first('plan', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
</div>

