@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="row justify-content-around">
                        <div class="col">Curso - Esquema</div>
                        
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/competencias.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        </div>
                    <div class="card-body">
                            
                        <div class="table-responsive">
                            <table id="myTable" class="display">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Codigo</th>
                                        <th>Detalle</th>
                                        <th>Estado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($cursoesquemas as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->codigo }}</td>
                                        <td>{{ $item->detalle }}</td>
                                        <td>{{ $item->estado }}</td>
                                        <td>
                                            <a href="{{ url('cursoesquemas/' . $item->id) }}" title="Ver"><button class="btn btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <form method="POST" action="{{ url('cursoesquemas/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                @method('put')
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-sm" title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                            </form>
                                            <form method="POST" action="{{ url('cursoesquemas/delete/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-sm" title="Eliminar" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                            <a href="{{ url('/cursoesquemas/create') }}" class="btn boton-color btn-sm" title="Agregar">
                                <i class="fa fa-plus" aria-hidden="true"></i> Agregar
                            </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


