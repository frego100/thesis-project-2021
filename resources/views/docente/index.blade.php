@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center" >
            <div class="col-md-10">
                <div class="card">
                    @if(session()->has('success'))
                        <div class="alert alert-success col-md-4">{{ session()->get('success') }}</div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger col-md-4">{{ session()->get('error') }}</div>
                    @endif
                    <div class="card-header">
                        <div class="row justify-content-around">
                            <div class="col">Docente</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/competencias.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        
                        <div class="table-responsive">
                            <table id="myTable" class="display">
                                <thead>
                                    <tr>
                                        <th>#</th>                                
                                        <th>Nombre</th>
                                        <th>Email</th>
                                        <th>Roles</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($docentes as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>                                        
                                        <td>{{ $item->nombre }}</td>
                                        <td>{{ $item->email }}</td>
                                        @php
                                            $roles = '';
                                            $contador = 0;
                                            if($item->superadmin)
                                            {
                                                $roles = $roles.'Super Administrador';
                                                $contador++;
                                            }
                                            if($item->admin)
                                            {
                                                if($contador>0)
                                                    $roles = $roles.', Administrador';
                                                else
                                                    $roles = $roles.'Administrador';
                                                $contador++;
                                            }
                                            if($item->docente)
                                            {
                                                if($contador>0)
                                                    $roles = $roles.', Docente';
                                                else
                                                    $roles = $roles.'Docente';
                                            }
                                        @endphp
                                        <td>{{ $roles }}</td>
                                        <td>
                                            <a href="{{ url('docentes/' . $item->id) }}" title="Ver Docente"><button class="btn btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <form method="POST" action="{{ url('docentes/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                @method('put')
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-sm" title="Editar Docente"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                            </form>
                                            <form method="POST" action="{{ url('docentes/delete/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-sm" title="Eliminar Docente" onclick="return confirm(&quot;¿Desea eliminar?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ url('/docentes/create') }}" class="btn boton-color btn-sm" title="Agregar Nuevo Docente">
                            <i class="fa fa-plus" aria-hidden="true"></i> Agregar
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.alert').fadeOut(7000);

    </script>
    
@endsection


