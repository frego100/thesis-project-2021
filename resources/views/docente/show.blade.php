@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">

                <div class="card-header">
                        <div class="row justify-content-around">
                            <div class="col">Docente {{ $docente->nombre }}</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/competencias.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                </div>

                <div class="card-body">

                    <a href="{{ url('docentes') }}" title="Cancelar"><button class="btn boton-color2 btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i>Cancelar</button></a>
                    <form method="POST" action="{{ url('docentes/' . $docente->id) }}" accept-charset="UTF-8" style="display:inline">
                        @method('put')
                        {{ csrf_field() }}
                        <button type="submit" class="btn boton-color btn-sm" title="Editar Docente"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                    </form>
                    <form method="POST" action="{{ url('docentes' . $docente->id) }}" accept-charset="UTF-8" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn boton-color btn-sm" title="Eliminar Docente" onclick="return confirm(&quot;¿Desesa eliminar docente?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    </form>
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $docente->id }}</td>
                                </tr>
                                
                                    <th> Nombre</th>
                                    <td> {{ $docente->nombre }} </td>
                                </tr>
                                @php
                                    $roles = '';
                                    $contador = 0;
                                    if($docente->superadmin)
                                    {
                                        $roles = $roles.'Super Administrador';
                                        $contador++;
                                    }
                                    if($docente->admin)
                                    {
                                        if($contador>0)
                                            $roles = $roles.', Administrador';
                                        else
                                            $roles = $roles.'Administrador';
                                        $contador++;
                                    }
                                    if($docente->docente)
                                    {
                                        if($contador>0)
                                            $roles = $roles.', Docente';
                                        else
                                            $roles = $roles.'Docente';
                                    }
                                @endphp
                                <tr>
                                    <th> Roles </th>
                                    <td> {{ $roles }} </td>
                                </tr>
                                <tr>
                                    <th> estado </th>
                                    <td> {{ $docente->estado }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection