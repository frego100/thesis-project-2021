<div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
    <label for="nombre" class="control-label">{{ 'Nombre' }}</label>
    <input class="form-control" name="nombre" type="text" id="nombre" value="{{ isset($docentes->nombre) ? $docentes->nombre : ''}}" required>
    {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="email" id="email" value="{{ isset($docentes->email) ? $docentes->email : ''}}" required>
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('rol') ? 'has-error' : ''}}">
    <label for="rol" class="control-label">{{ 'Roles' }}</label>
        <br>
        <input type="checkbox" id="superadmin" name="superadmin" value="1" {{ isset($docentes) && $docentes->superadmin ? 'checked' : '' }}>
        <label for="superadmin"> Super Administrador</label>
        <br>
        <input type="checkbox" id="admin" name="admin" value="1" {{ isset($docentes) && $docentes->admin ? 'checked' : '' }}>
        <label for="admin"> Administrador</label>
        <br>
        <input type="checkbox" id="docente" name="docente" value="1" {{ isset($docentes) && $docentes->docente ? 'checked' : '' }}>
        <label for="docente"> Docente</label>
</div>
<br>

<div class="form-group">
    <input class="btn boton-color btn-sm" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
</div>

