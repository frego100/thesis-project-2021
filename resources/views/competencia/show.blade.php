@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="row justify-content-around">
                            <div class="col">Ver</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/competencias.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">

                        <a href="{{ url('competencias') }}" title="Back"><button class="btn boton-color2 btn-sm"><i
                                    class="fa fa-arrow-left" aria-hidden="true"></i> Cancelar</button></a>
                        <form method="POST" action="{{ url('competencias/' . $competencia->id) }}" accept-charset="UTF-8"
                            style="display:inline">
                            @method('put')
                            {{ csrf_field() }}
                            <button type="submit" class="btn boton-color btn-sm" title="Edit Post"><i
                                    class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                        </form>
                        <form method="POST" action="{{ url('competencias' . $competencia->id) }}" accept-charset="UTF-8"
                            style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn boton-color btn-sm" title="Delete Post"
                                onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                    aria-hidden="true"></i></button>
                        </form>
                        <br />
                        <br />

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $competencia->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Codigo </th>
                                        <td> {{ $competencia->codigo }} </td>
                                    </tr>
                                    <tr>
                                        <th> Competencia </th>
                                        <td> {{ $competencia->competencia }} </td>
                                    </tr>
                                    <tr>
                                        <th> Estado </th>
                                        <td> {{ $competencia->estado }} </td>
                                    </tr>
                                    <tr>
                                        <th> Plan </th>
                                        <td> {{ $competencia->plan->plan }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
