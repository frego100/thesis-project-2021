@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <div class="row justify-content-around">
                        <div class="col">Editar</div>
                        <div class="col-1">
                            <a target="_blank" href="{{ asset('files/competencias.pdf') }}"
                                class="btn boton-color btn-sm" title="Info">
                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <a href="{{ url('competencias') }}" title="Back"><button class="btn boton-color2 btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Cancelar</button></a>
 

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <form method="POST" action="{{ url('competencias/' . $competencia->id . '/edit') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}

                        @include ('competencia.form', ['formMode' => 'edit'])

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection