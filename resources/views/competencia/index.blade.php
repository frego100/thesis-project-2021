@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    @if(session()->has('success'))
                        <div class="alert alert-success col-md-4">{{ session()->get('success') }}</div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger col-md-4">{{ session()->get('error') }}</div>
                    @endif
                    <div class="card-header">
                        <div class="row justify-content-around">
                            <div class="col">Competencia</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/competencias.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        
                        <div class="table-responsive">
                            <table id="myTable" class="display">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Codigo</th>
                                        <th>Competencia</th>
                                        <th>Estado</th>
                                        <th>Plan</th>
                                        <th>Acciones</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($competencias as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->codigo }}</td>
                                            <td class="text-competencia">{{ $item->competencia }}</td>
                                            <td>{{ $item->estado }}</td>
                                            <td>{{ $item->plan->anio }}</td>
                                            <td><div class="col-12">
                                                <a href="{{ url('competencias/' . $item->id) }}" title="View Post"><button
                                                        class="btn btn-sm"><i class="fa fa-eye"
                                                            aria-hidden="true"></i></button></a>
                                                <form method="POST" action="{{ url('competencias/' . $item->id) }}"
                                                    accept-charset="UTF-8" style="display:inline">
                                                    @method('put')
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-sm" title="Edit Post"><i
                                                            class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                                </form>
                                                <form method="POST"
                                                    action="{{ url('competencias/delete/' . $item->id) }}"
                                                    accept-charset="UTF-8" style="display:inline">
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-sm" title="Delete Post"
                                                        onclick="return confirm(&quot;¿Realmente desea eliminar?&quot;)"><i
                                                            class="fa fa-trash-o" aria-hidden="true" id="delete"></i></button>
                                                </form>
                                            </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            <a href="{{ url('/competencias/create') }}" class="btn boton-color btn-sm"
                            title="Agregar nueva competencia">
                            <i class="fa fa-plus" aria-hidden="true"></i> Agregar
                        </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.alert').fadeOut(7000);

    </script>
    
@endsection
