<div class="form-group {{ $errors->has('codigo') ? 'has-error' : ''}}">
    <label for="codigo" class="control-label">{{ 'Codigo' }}</label>
    <input class="form-control" name="codigo" type="text" id="codigo" value="{{ isset($competencia->codigo) ? $competencia->codigo : ''}}" required>
    {!! $errors->first('codigo', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('competencia') ? 'has-error' : ''}}">
    <label for="competencia" class="control-label">{{ 'Competencia' }}</label>
    <textarea class="form-control" name="competencia"  id="competencia" value="{{isset($competencia->competencia)?$competencia->competencia:''}}" required>{{isset($competencia->competencia)?$competencia->competencia:''}}</textarea>
        {!! $errors->first('competencia', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('plan') ? 'has-error' : ''}}">
    <label for="plan" class="control-label">{{ 'plan' }}</label>
    <select name="plan" class="form-control" id="plan" >
        @foreach($planes as $plan)              
        <option value="{{ $plan->id }}" {{ isset($competencia->plan_id) ?  ($plan->id == $competencia->plan_id ? 'selected' : '') : '' }}>
            {{ $plan->anio }}
        </option>                      
    @endforeach  
</select>
    {!! $errors->first('plan', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn boton-color btn-sm" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
</div>

