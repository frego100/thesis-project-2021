<div class="form-group {{ $errors->has('grupo') ? 'has-error' : ''}}">
    <label for="grupo" class="control-label">{{ 'Grupo' }}</label>
    <input class="form-control" name="grupo" type="text" id="grupo" value="{{ isset($cursogrupo->grupo) ? $cursogrupo->grupo : ''}}" required>
    {!! $errors->first('grupo', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('coordinador') ? 'has-error' : ''}}">
    <label for="coordinador" class="control-label">{{ 'Coordinador' }}</label>
    <input class="form-control" name="coordinador" type="text" id="coordinador" value="{{ isset($cursogrupo->coordinador) ? $cursogrupo->coordinador : ''}}" required>
    {!! $errors->first('coordinador', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('estado') ? 'has-error' : ''}}">
    <label for="estado" class="control-label">{{ 'Estado' }}</label>
    <input class="form-control" name="estado" type="text" id="estado" value="{{ isset($cursogrupo->estado) ? $cursogrupo->estado : ''}}" required>
    {!! $errors->first('cui', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
    <input class="btn boton-color" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
</div>

