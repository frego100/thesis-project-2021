@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Vista {{ $cursogrupo->id }}</div>
                <div class="card-body">

                    <a href="{{ url('cursogrupos') }}" title="Back"><button class="btn boton-color2 btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i>Regresar</button></a>
                    <form method="POST" action="{{ url('cursogrupos/' . $cursogrupo->id) }}" accept-charset="UTF-8" style="display:inline">
                        @method('put')
                        {{ csrf_field() }}
                        <button type="submit" class="btn boton-color btn-sm" title="Edit Post"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                    </form>
                    <form method="POST" action="{{ url('cursogrupos' . $cursogrupo->id) }}" accept-charset="UTF-8" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn boton-color btn-sm" title="Delete Post" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    </form>
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $cursogrupo->id }}</td>
                                </tr>
                                <tr>
                                    <th> Estado </th>
                                    <td> {{ $cursogrupo->grupo }} </td>
                                </tr>
                                <tr>
                                    <th> Escuela </th>
                                    <td> {{ $cursogrupo->coordinador }} </td>
                                </tr>
                                <tr>
                                    <th> Semestre </th>
                                    <td> {{ $cursogrupo->estado }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection