@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="row justify-content-around">
                            <div class="col">Ver</div>
                            <div class="col-1">
                                <a target="_blank" href="{{asset('files/escuelas.pdf')}}" class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">

                        <a href="{{ url('escuelas') }}" title="Regresar"><button class="btn boton-color2 btn-sm"><i
                                    class="fa fa-arrow-left" aria-hidden="true"></i> Cancelar</button></a>
                        <form method="POST" action="{{ url('escuelas/' . $escuela->id) }}" accept-charset="UTF-8"
                            style="display:inline">
                            @method('put')
                            {{ csrf_field() }}
                            <button type="submit" class="btn boton-color btn-sm" title="Editar"><i class="fa fa-pencil-square-o"
                                    aria-hidden="true"></i></button>
                        </form>
                        <form method="POST" action="{{ url('escuelas' . $escuela->id) }}" accept-charset="UTF-8"
                            style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn boton-color btn-sm" title="Eliminar"
                                onclick="return confirm(&quot;¿Realmente desea Eliminar?&quot;)"><i class="fa fa-trash-o"
                                    aria-hidden="true"></i></button>
                        </form>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $escuela->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Escuela </th>
                                        <td> {{ $escuela->escuela }} </td>
                                    </tr>
                                    <tr>
                                        <th> Facultad </th>
                                        <td> {{ $escuela->facultad->facultad }} </td>
                                    </tr>
                                    <tr>
                                        <th> estado </th>
                                        <td> {{ $escuela->estado }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
