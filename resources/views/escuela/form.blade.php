<div class="form-group {{ $errors->has('escuela') ? 'has-error' : ''}}">
    <label for="escuela" class="control-label">{{ 'Nombre de Escuela' }}</label>
    <input class="form-control" name="escuela" type="text" id="escuela" value="{{ isset($escuela->escuela) ? $escuela->escuela : ''}}" required>
    {!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('facultad') ? 'has-error' : ''}}">
    <label for="facultad" class="control-label">{{ 'Facultad' }}</label>
    <select name="facultad" class="form-control" id="facultad" required>
        @foreach($facultades as $facultad)              
        <option value="{{ $facultad->id }}" {{ isset($escuela->facultad_id) ?  ($facultad->id == $escuela->facultad_id ? 'selected' : '') : '' }}>
            {{ $facultad->facultad }}
        </option>                      
    @endforeach  
</select>
    {!! $errors->first('facultad', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn boton-color btn-sm" type="submit" value="{{ $formMode === 'edit' ? 'Editar' : 'Crear' }}">
</div>