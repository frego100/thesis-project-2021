<div class="form-group {{ $errors->has('cursoTipo') ? 'has-error' : ''}}">
    <label for="cursoTipo" class="control-label">{{ 'Nombre del Tipo de Curso' }}</label>
    <input class="form-control" name="curso_tipo" type="text" id="cursoTipo" value="{{ isset($curso_tipo->curso_tipo) ? $curso_tipo->curso_tipo : ''}}" required >
    {!! $errors->first('cursoTipo', '<p class="help-block">:message</p>') !!}
</div>

<br>
<div class="form-group">
    <input class="btn boton-color btn-sm" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
</div>
