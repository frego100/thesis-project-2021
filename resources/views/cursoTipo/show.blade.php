@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <!--<div class="card-header">Curso Tipos{{ $cursoTipo->curso_tipo }}</div>-->
                    <div class="card-header">
                        <div class="row justify-content-around">
                            <div class="col">CursoTipo</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/competencias.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                <div class="card-body">                  

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>ID</th><td>{{ $cursoTipo->id }}</td>
                                </tr>
                                <tr>
                                    <th> Curso Tipo </th>
                                    <td> {{ $cursoTipo->curso_tipo }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <a href="{{ url('cursoTipos') }}" title="Cancelar"><button class="btn boton-color2 btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i>Cancelar</button></a>
                    <form method="POST" action="{{ url('cursoTipos/' . $cursoTipo->id) }}" accept-charset="UTF-8" style="display:inline">
                        @method('put')
                        {{ csrf_field() }}
                        <button type="submit" class="btn boton-color btn-sm" title="Editar Curso"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                    </form>
                    <form method="POST" action="{{ url('cursoTipos/delete/' . $cursoTipo->id) }}" accept-charset="UTF-8" style="display:inline">
                        {{ csrf_field() }}
                        <button type="submit" class="btn boton-color btn-sm" title="Eliminar Curso" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    </form>
                    <br/>
                    <br/>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection