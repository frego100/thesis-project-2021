<div class="form-group {{ $errors->has('codigo') ? 'has-error' : ''}}">
    <label for="codigo" class="control-label">{{ 'Codigo' }}</label>
    <input class="form-control" name="codigo" type="text" id="codigo" value="{{ isset($criterio->codigo) ? $criterio->codigo : ''}}" required>
    {!! $errors->first('codigo', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('criterio') ? 'has-error' : ''}}">
    <label for="criterio" class="control-label">{{ 'Criterio' }}</label>
    <input class="form-control" name="criterio" type="text" id="criterio" value="{{ isset($criterio->criterio) ? $criterio->criterio : ''}}" required>
    {!! $errors->first('criterio', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('resultadosestudiante') ? 'has-error' : ''}}">
    <label for="resultadosestudiante" class="control-label">{{ 'Resultado Estudiante' }}</label>
    <select name="resultadosestudiante" class="form-control" id="resultadosestudiante" required>
        @foreach($resultadosestudiantes as $resultadosestudiante)              
        <option value="{{ $resultadosestudiante->id }}" {{ isset($criterio->resultadosestudiante_id) ?  ($resultadosestudiante->id == $criterio->resultadosestudiante_id ? 'selected' : '') : '' }}>
            {{ $resultadosestudiante->codigore }}
        </option>                      
    @endforeach  
</select>
    {!! $errors->first('resultadosestudiante', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('insatisfactorio') ? 'has-error' : ''}}">
    <label for="insatisfactorio" class="control-label">{{ 'Insatisfactorio' }}</label>
    <input class="form-control" name="insatisfactorio" type="text" id="insatisfactorio" value="{{ isset($criterio->insatisfactorio) ? $criterio->insatisfactorio : ''}}" required>
    {!! $errors->first('insatisfactorio', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('enproceso') ? 'has-error' : ''}}">
    <label for="enproceso" class="control-label">{{ 'En proceso' }}</label>
    <input class="form-control" name="enproceso" type="text" id="enproceso" value="{{ isset($criterio->enproceso) ? $criterio->enproceso : ''}}" required>
    {!! $errors->first('enproceso', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('satisfactorio') ? 'has-error' : ''}}">
    <label for="satisfactorio" class="control-label">{{ 'Satisfactorio' }}</label>
    <input class="form-control" name="satisfactorio" type="text" id="satisfactorio" value="{{ isset($criterio->satisfactorio) ? $criterio->satisfactorio : ''}}" required>
    {!! $errors->first('satisfactorio', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('sobresaliente') ? 'has-error' : ''}}">
    <label for="sobresaliente" class="control-label">{{ 'Sobresaliente' }}</label>
    <input class="form-control" name="sobresaliente" type="text" id="sobresaliente" value="{{ isset($criterio->sobresaliente) ? $criterio->sobresaliente : ''}}" required>
    {!! $errors->first('sobresaliente', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn boton-color btn-sm" type="submit" value="{{ $formMode === 'edit' ? 'Editar' : 'Crear' }}">
</div>