@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <!--<div class="card-header">Criterio {{ $criterio->criterio }}</div>-->
                <div class="card-header">
                    <div class="row justify-content-around">
                        <div class="col">Criterios</div>
                        <div class="col-1">
                            <a target="_blank" href="{{ asset('files/competencias.pdf') }}"
                                class="btn boton-color btn-sm" title="Info">
                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <a href="{{ url('criterios') }}" title="Cancelar"><button class="btn boton-color2 btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i>Cancelar</button></a>
                    <form method="POST" action="{{ url('criterios/' . $criterio->id) }}" accept-charset="UTF-8" style="display:inline">
                        @method('put')
                        {{ csrf_field() }}
                        <button type="submit" class="btn boton-color btn-sm" title="Editar Criterio"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                    </form>
                    <form method="POST" action="{{ url('criterios' . $criterio->id) }}" accept-charset="UTF-8" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn boton-color btn-sm" title="Eliminar Criterio" onclick="return confirm(&quot;¿Eliminar Criterio?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    </form>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $criterio->id }}</td>
                                </tr>
                                <tr>
                                    <th>Codigo</th>
                                    <td>{{ $criterio->codigo }}</td>
                                </tr>
                                <tr>
                                    <th> Criterio </th>
                                    <td> {{ $criterio->criterio }} </td>
                                </tr>
                                <tr>
                                    <th> Resultado Estudiante </th>
                                    <td> {{ $criterio->resultadoestudiante->resultadoestudiante }} </td>
                                </tr>
                                <tr>
                                    <th> Insatisfactorio </th>
                                    <td> {{ $criterio->insatisfactorio }} </td>
                                </tr>
                                <tr>
                                    <th> En proceso </th>
                                    <td> {{ $criterio->enproceso }} </td>
                                </tr>
                                <tr>
                                    <th> Satisfactorio </th>
                                    <td> {{ $criterio->satisfactorio }} </td>
                                </tr>
                                <tr>
                                    <th> Sobresaliente </th>
                                    <td> {{ $criterio->sobresaliente }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection