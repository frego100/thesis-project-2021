@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center" >
            <div class="col-md-11">
                <div class="card">
                    @if(session()->has('success'))
                        <div class="alert alert-success col-md-4">{{ session()->get('success') }}</div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger col-md-4">{{ session()->get('error') }}</div>
                    @endif
                    <div class="card-header">
                        <div class="row justify-content-around">
                            <div class="col">Criterios</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/competencias.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                                            
                        <div class="table-responsive">
                            <table id="myTable" class="display">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Codigos</th>
                                        <th>Criterio</th>
                                        <th>Resultados Estudiante</th>                                        
                                        <th>Insatisfactorio</th>
                                        <th>En proceso</th>
                                        <th>Satisfactorio</th>
                                        <th>Sobresaliente</th>
                                        <th>Estado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($criterios as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->codigo }}</td>
                                        <td>{{ $item->criterio }}</td>
                                        <td>{{ $item->resultadoestudiante->re }}</td>                                        
                                        <td>{{ $item->insatisfactorio }}</td>
                                        <td>{{ $item->enproceso }}</td>
                                        <td>{{ $item->satisfactorio }}</td>
                                        <td>{{ $item->sobresaliente }}</td>
                                        <td>{{ $item->estado }}</td>
                                        <td>
                                            <a href="{{ url('criterios/' . $item->id) }}" title="Ver Criterio"><button class="btn btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                       
                                            <form method="POST" action="{{ url('criterios/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                @method('put')
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-sm" title="Editar Criterio"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                            </form>
                                        
                                            <form method="POST" action="{{ url('criterios/delete/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-sm" title="Eliminar Criterio" onclick="return confirm(&quot;¿Esta seguro que desea eliminar el criterio?&quot;)"><i class="fa fa-trash-o" aria-hidden="true" id="delete"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ url('/criterios/create') }}" class="btn boton-color btn-sm" title="Agregar Criterio">
                            <i class="fa fa-plus" aria-hidden="true"></i> Agregar
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.alert').fadeOut(7000);

    </script>
@endsection


