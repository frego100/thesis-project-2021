<div class="form-group {{ $errors->has('criterio') ? 'has-error' : ''}}">
    <label for="criterio" class="control-label">{{ 'Criterio' }}</label>
    <select name="criterio" class="form-control" id="criterio" required>
        @foreach($criterios as $item)              
        <option value="{{ $item->id }}" {{ isset($matrizdetalle->criterio_id) ?  ($item->id == $matrizdetalle->criterio_id ? 'selected' : '') : '' }} >
            {{ $item->criterio }}
        </option>                      
    @endforeach  
</select>
    {!! $errors->first('plan', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('curso') ? 'has-error' : ''}}">
    <label for="curso" class="control-label">{{ 'Curso' }}</label>
    <select name="curso" class="form-control" id="curso" required>
        @foreach($cursos as $item)              
        <option value="{{ $item->id }}" {{ isset($matrizdetalle->cursomatriz_id) ?  ($item->id == $matrizdetalle->cursomatriz_id ? 'selected' : '') : '' }}>
            {{ $item->curso }}
        </option>                      
    @endforeach  
</select>
    {!! $errors->first('plan', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('esquema') ? 'has-error' : ''}}">
    <label for="esquema" class="control-label">{{ 'Esquema' }}</label>
    <select name="esquema" class="form-control" id="esquema" required>
        @foreach($cursoesquemas as $item)              
        <option value="{{ $item->id }}" {{ isset($matrizdetalle->cursoesquema_id) ?  ($item->id == $matrizdetalle->cursoesquema_id ? 'selected' : '') : '' }}>
            {{ $item->esquema }}
        </option>                      
    @endforeach  
</select>
    {!! $errors->first('plan', '<p class="help-block">:message</p>') !!}
</div>
<br>
<div class="form-group">
    <input class="btn boton-color" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
</div>

