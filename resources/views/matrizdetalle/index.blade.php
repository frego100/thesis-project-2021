@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-7">
                <div class="card">
                    @if(session()->has('success'))
                        <div class="alert alert-success col-md-4">{{ session()->get('success') }}</div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger col-md-4">{{ session()->get('error') }}</div>
                    @endif
                    <div class="card-header">
                    <div class="row justify-content-around">
                            <div class="col">Matriz Detalle</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/competencias.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                       
                       <!--
                        <form method="GET" action="{{ url('/matrizdetalles') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                        -->
                        <br>
                        <div class="table-responsive">
                            <table id="myTable" class="display">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Ponderacion</th>
                                        <th>Estado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($matrizdetalles as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->ponderacion }}</td>
                                        <td>{{ $item->estado }}</td>
                                        <td>
                                            <div>
                                            <a href="{{ url('matrizdetalles/' . $item->id) }}" title="Ver"><button class="btn btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <form method="POST" action="{{ url('matrizdetalles/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                @method('put')
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-sm" title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                            </form>
                                            <form method="POST" action="{{ url('matrizdetalles/delete/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-sm" title="Eliminar" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                            </form>
                                        </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                            <a href="{{ url('/matrizdetalles/create') }}" class="btn boton-color btn-sm" title="Agregar">
                                <i class="fa fa-plus" aria-hidden="true"></i> Agregar
                            </a>
                       </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.alert').fadeOut(7000);

    </script>

@endsection


