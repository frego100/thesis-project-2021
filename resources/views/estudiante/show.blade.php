@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Vista {{ $estudiante->estudiante}}</div>
                <div class="card-body">

                    <a href="{{ url('estudiantes') }}" title="Back"><button class="btn boton-color2 btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</button></a>
                    
                    <form method="POST" action="{{ url('estudiantes/' . $estudiante->id) }}" accept-charset="UTF-8"
                            style="display:inline">
                            @method('put')
                            {{ csrf_field() }}
                            <button type="submit" class="btn boton-color btn-sm" title="Editar"><i class="fa fa-pencil-square-o"
                                    aria-hidden="true"></i></button>
                    </form>
                    <form method="POST" action="{{ url('estudiantes' . '/' . $estudiante->id) }}" accept-charset="UTF-8" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn boton-color btn-sm" title="Eliminar Estudiante" onclick="return confirm(&quot;Confirmar eliminacion?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    </form>
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>CUI</th><td>{{ $estudiante->cui }}</td>
                                </tr>
                                <tr>
                                    <th> Estudiante </th>
                                    <td> {{ $estudiante->estudiante }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection