@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Crear</div>

                <div class="card-body">

                    <a href="{{ url('/estudiantes') }}" title="Regresar"><button class="btn boton-color2 btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</button></a>
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <form method="POST" action="{{ url('/estudiantes') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        @include ('estudiante.form', ['formMode' => 'create'])
                    </form>    


                    <!-- <form method="POST" action="{{url('estudiantes')}}" >
                        @csrf
                        <div class="form-group">
                            <label for="estudiante">Nombre del Estudiante</label>
                            <input id="estudiante" type="text" class="form-control @error('estudiante') is-invalid @enderror"
                                name="estudiante" value="{{ old('estudiante') }}" required autocomplete="estudiante" autofocus>
                            @error('estudiante')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror   
                        </div>
                        <div class="form-group">
                            <label for="cui">CUI</label>
                            <input id="cui" type="text" class="form-control @error('cui') is-invalid @enderror"
                                name="cui" value="{{ old('cui') }}" required autocomplete="cui" autofocus>
                            @error('cui')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror   
                        </div>
                        <div class="form-group">
                            <label for="dni">DNI</label>
                            <input id="dni" type="text" class="form-control @error('dni') is-invalid @enderror"
                                name="dni" value="{{ old('dni') }}" required autocomplete="dni" autofocus>
                            @error('dni')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror   
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror   
                        </div>
                        <div class="form-group">
                            <label for="estado">Estado</label>
                            <input id="estado" type="text" class="form-control @error('estado') is-invalid @enderror"
                                name="estado" value="{{ old('estado') }}" required autocomplete="estado" autofocus>
                            @error('estado')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror   
                        </div>
                        <button type="submit" class="btn boton-color btn-sm">Guardar</button>
                    </form>
                    -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection