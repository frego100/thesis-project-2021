<div class="form-group {{ $errors->has('estudiante') ? 'has-error' : ''}}">
    <label for="estudiante" class="control-label">{{ 'Nombre del Estudiante' }}</label>
    <input class="form-control" name="estudiante" type="text" id="estudiante" value="{{ isset($estudiante->estudiante) ? $estudiante->estudiante : ''}}" required>
    {!! $errors->first('estudiante', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('cui') ? 'has-error' : ''}}">
    <label for="cui" class="control-label">{{ 'CUI' }}</label>
    <input class="form-control" name="cui" type="number" id="cui" maxlength="8" minlength="8" value="{{ isset($estudiante->cui) ? $estudiante->cui : ''}}" required>
    {!! $errors->first('cui', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('dni') ? 'has-error' : ''}}">
    <label for="dni" class="control-label">{{ 'DNI' }}</label>
    <input class="form-control" name="dni" type="number" id="dni" maxlength="8" minlength="8" value="{{ isset($estudiante->dni) ? $estudiante->dni : ''}}" required>
    {!! $errors->first('dni', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="email" id="email" value="{{ isset($estudiante->email) ? $estudiante->email : ''}}" required>
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn boton-color" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
</div>
