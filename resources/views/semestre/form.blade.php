<div class="form-group {{ $errors->has('semestre') ? 'has-error' : '' }}">
    <label for="semestre" class="control-label">{{ 'Semestre' }}</label>
    <input class="form-control" name="semestre" type="text" id="semestre"
        value="{{ isset($semestre->semestre) ? $semestre->semestre : '' }}" required>

    {!! $errors->first('semestre', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('anio') ? 'has-error' : '' }}">
    <label for="anio" class="control-label">{{ 'Año' }}</label>
    <input class="form-control" name="anio" type="number" id="anio"
        value="{{ isset($semestre->anio) ? $semestre->anio : '' }}" required>

    {!! $errors->first('inicio', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('inicio') ? 'has-error' : '' }}">
    <label for="inicio" class="control-label">{{ 'Inicio' }}</label>
    <input class="form-control" name="inicio" type="date" id="inicio"
        value="{{ isset($semestre->inicio) ? $semestre->inicio : '' }}">

    {!! $errors->first('inicio', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('fin') ? 'has-error' : '' }}">
    <label for="fin" class="control-label">{{ 'Fin' }}</label>
    <input class="form-control" name="fin" type="date" id="fin" 
    value="{{ isset($semestre->fin) ? $semestre->fin : '' }}">

    {!! $errors->first('fin', '<p class="help-block">:message</p>') !!}
</div>
<br>


<div class="form-group">
    <input class="btn boton-color btn-sm" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
</div>
