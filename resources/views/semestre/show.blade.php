@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <div class="row justify-content-around">
                        <div class="col">Semestre</div>
                        <div class="col-1">
                            <a target="_blank" href="{{ asset('files/competencias.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="card-body">

                    <a href="{{ url('semestres') }}" title="Regresar"><button class="btn boton-color2 btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i>Cancelar</button></a>
                    <form method="POST" action="{{ url('semestres/' . $semestre->id) }}" accept-charset="UTF-8" style="display:inline">
                        @method('put')
                        {{ csrf_field() }}
                        <button type="submit" class="btn boton-color btn-sm" title="Editar Semestre"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                    </form>
                    <form method="POST" action="{{ url('semestres/delete/' . $semestre->id) }}" accept-charset="UTF-8" style="display:inline">
                        {{ csrf_field() }}
                        <button type="submit" class="btn boton-color btn-sm" title="Eliminar Semestre" onclick="return confirm(&quot;¿Desea eliminar?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    </form>
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $semestre->id }}</td>
                                </tr>
                                <tr>
                                    <th> Semestre </th>
                                    <td> {{ $semestre->semestre }} </td>
                                </tr>
                                <tr>
                                    <th> Inicio </th>
                                    <td> {{ $semestre->inicio }} </td>
                                </tr>
                                <tr>
                                    <th> Fin </th>
                                    <td> {{ $semestre->fin }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection