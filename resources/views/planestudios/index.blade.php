@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Plan de Estudios</div>
                        <div class="card-body">
                            <a href="{{ url('/cursos') }}" class="btn btn-success btn-sm" title="Go to curso">
                                <i class="fa fa-plus" aria-hidden="true"></i> Curso
                            </a>
                            <a href="{{ url('/competencias') }}" class="btn btn-success btn-sm" title="Go to competencia">
                                <i class="fa fa-plus" aria-hidden="true"></i> Competencia
                            </a>
                            <a href="{{ url('/resultadosestudiantes') }}" class="btn btn-success btn-sm" title="Go to RE">
                                <i class="fa fa-plus" aria-hidden="true"></i> Resultado del Estudiante
                            </a>
                        </div>
                </div>
            </div>
        </div>
</div>

@endsection