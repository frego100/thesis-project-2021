<nav id="sidebar" class="sidebar-wrapper ">
<aside class="keep">
    <ul>
        <li>
            <span class="menu"><i class="fa fa-bars"></i></span>
            
        </li>
        <li class="{{ Request::is('admin*') ? 'active' : '' }}">
            <a href="/docente">
                <i class="fa fa-home fa-2x"></i>
                Inicio
            </a>
        </li>
        @php
            if(session()->get('superadmin')){
        @endphp
        <li>
            <a href="/facultades">
                <i class="fa fa-chevron-right fa-2x" aria-hidden="true"></i>
                Ir a Super Administrador
            </a>
        </li>  
        @php
            }
            if(session()->get('admin')){
        @endphp
        <li>
            <a href="/admin">
                <i class="fa fa-chevron-right fa-2x" aria-hidden="true"></i>
                Ir a Administrador
            </a>
        </li>  
        @php
            }
        @endphp
    </ul>
</aside>
</nav>