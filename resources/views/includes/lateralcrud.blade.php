<nav id="sidebar" class="sidebar-wrapper ">
    <aside class="keep">
        <ul>
            <li >
                <span class="menu"><i class="fa fa-bars"></i></span>
            </li>
            @php
                if(session()->get('admin')){
            @endphp
            <li>
                <a href="/admin">
                    <i class="fa fa-chevron-right fa-2x" aria-hidden="true"></i>
                    Ir a Administrador
                </a>
            </li>  
            @php
                }
                if(session()->get('docente')){
            @endphp
            <li>
                <a href="/docente">
                    <i class="fa fa-chevron-right fa-2x" aria-hidden="true"></i>
                    Ir a Docente
                </a>
            </li>  
            @php
                }
                if(session()->get('admin') || session()->get('docente')){
            @endphp
            <hr style="text-align: left;border-top: 2px solid #fff;">
            @php
                }
            @endphp
            <li class="{{ Request::is('competencias*') ? 'active' : '' }}">
                <a href="/competencias">
                    <i class="fa fa-medal fa-2x"></i>
                    Competencia
                </a>
            </li>
            <!-- 
            <li class="{{ Request::is('competenciasres*') ? 'active' : '' }}">
                <a href="/competenciasres">
                    <div class="fa"><i class="fa-medal"></i><i class="fa-poll "></i> </div>
                    Competencia - R.E.
                </a>
            </li>
            -->
            <li class="{{ Request::is('criterios*') ? 'active' : '' }}">
                <a href="/criterios">
                    <i class="fa fa-ruler-vertical fa-2x"></i>
                    Criterio
                </a>
            </li>
            <li class="{{ Request::is('cursos') ? 'active' : '' }}">
                <a href="/cursos">
                    <i class="fa fa-book fa-2x"></i>
                    Curso
                </a>
            </li>
            <!--
            <li class="{{ Request::is('cursoesquemas*') ? 'active' : '' }}">
                <a href="/cursoesquemas">
                    <div class="fa"><i class="fa-book"></i><i class="fa-clipboard-list "></i> </div>
                    Curso - Esquema
                </a>
            </li>
             -->
            <li class="{{ Request::is('cursoestudiantes*') ? 'active' : '' }}">
                <a href="/cursoestudiantes">
                    <div class="fa"><i class="fa-book"></i><i class="fa-user-graduate "></i> </div>
                    Curso - Estudiante
                </a>
            </li>
            <li class="{{ Request::is('cursogrupos*') ? 'active' : '' }}">
                <a href="/cursogrupos">
                    <div class="fa"><i class="fa-book"></i><i class="fa-users "></i> </div>
                    Curso - Grupo
                </a>
            </li>
            <li class="{{ Request::is('cursomatrizs*') ? 'active' : '' }}">
                <a href="/cursomatrizs">
                    <div class="fa"><i class="fa-book"></i><i class="fa-file-word "></i> </div>
                    Curso - Matriz
                </a>
            </li>
            <li class="{{ Request::is('cursosemestres*') ? 'active' : '' }}">
                <a href="/cursosemestres">
                    <div class="fa"><i class="fa-book"></i><i class="fa-calendar-alt "></i> </div>
                    Curso - Semestre
                </a>
            </li>
            <li class="{{ Request::is('cursoTipos*') ? 'active' : '' }}">
                <a href="/cursoTipos">
                    <i class="fa fa-bookmark fa-2x"></i>
                    Curso - Tipo
                </a>
            </li>
            <li class="{{ Request::is('docentes*') ? 'active' : '' }}">
                <a href="/docentes">
                    <i class="fa fa-chalkboard-teacher fa-2x"></i>
                    Docente
                </a>
            </li>
            <li class="{{ Request::is('escuelas') ? 'active' : '' }}">
                <a href="/escuelas">
                    <i class="fa fa-school fa-2x"></i>
                    Escuela
                </a>
            </li>
            <li class="{{ Request::is('escuelasemestres*') ? 'active' : '' }}">
                <a href="/escuelasemestres">
                    <div class="fa"><i class="fa-school"></i><i class="fa-calendar-alt "></i> </div>
                    Escuela - Semestre
                </a>
            </li>
            <li class="{{ Request::is('estudiantes*') ? 'active' : '' }}">
                <a href="/estudiantes">
                    <i class="fa fa-graduation-cap fa-2x"></i>
                    Estudiante
                </a>
            </li>
            <li class="{{ Request::is('estudiantematrizs*') ? 'active' : '' }}">
                <a href="/estudiantematrizs">
                    <div class="fa"><i class="fa-graduation-cap"></i><i class="fa-file-word"></i> </div>
                    Estudiante - Matriz
                </a>
            </li>
            <li class="{{ Request::is('facultades*') ? 'active' : '' }}">
                <a href="/facultades">
                    <i class="fa fa-building fa-2x"></i>
                    Facultad
                </a>
            </li>
            <li class="{{ Request::is('matrizdetalles*') ? 'active' : '' }}">
                <a href="/matrizdetalles">
                    <i class="fa fa-file-word fa-2x"></i>
                    Matriz Detalle
                </a>
            </li>
            <li class="{{ Request::is('planes*') ? 'active' : '' }}">
                <a href="/planes">
                    <i class="fa fa-chess-knight fa-2x"></i>
                    Plan
                </a>
            </li>
            <li class="{{ Request::is('resultadosestudiantes*') ? 'active' : '' }}">
                <a href="/resultadosestudiantes">
                    <i class="fa fa-poll fa-2x"></i>
                    Resultados Estudiante
                </a>
            </li>
            <li class="{{ Request::is('semestres*') ? 'active' : '' }}">
                <a href="/semestres">
                    <i class="fa fa-calendar-alt fa-2x"></i>
                    Semestre
                </a>
            </li>
        </ul>
    </aside>
</nav>
