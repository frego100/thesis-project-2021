@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <div class="row justify-content-around">
                            <div class="col">Cursos</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/competencias.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>    

                <div class="card-body">

                    <a href="{{ url('cursos') }}" title="Regresar"><button class="btn boton-color2 btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i>Cancelar</button></a>
                    <form method="POST" action="{{ url('cursos/' . $curso->id) }}" accept-charset="UTF-8" style="display:inline">
                        @method('put')
                        {{ csrf_field() }}
                        <button type="submit" class="btn boton-color btn-sm" title="Editar Curso"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                    </form>
                    <form method="POST" action="{{ url('cursos' . $curso->id) }}" accept-charset="UTF-8" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn boton-color btn-sm" title="Eliminar Curso" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    </form>
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $curso->id }}</td>
                                </tr>
                                <tr>
                                    <th> Codigo </th>
                                    <td> {{ $curso->codigo }} </td>
                                </tr>
                                <tr>
                                    <th> Curso </th>
                                    <td> {{ $curso->curso }} </td>
                                </tr>
                                <tr>
                                    <th> Descripcion </th>
                                    <td> {{ $curso->descripcion }} </td>
                                </tr>
                                <tr>
                                    <th> Semestre </th>
                                    <td> {{ $curso->semestre }} </td>
                                </tr>
                                <tr>
                                    <th> Horas teoria </th>
                                    <td> {{ $curso->horasteoria }} </td>
                                </tr>
                                <tr>
                                    <th> Horas Practica </th>
                                    <td> {{ $curso->horaspractica }} </td>
                                </tr>
                                <tr>
                                    <th> Creditos </th>
                                    <td> {{ $curso->creditos }} </td>
                                </tr>
                                <tr>
                                    <th> estado </th>
                                    <td> {{ $curso->estado }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection