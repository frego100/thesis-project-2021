<div class="form-group {{ $errors->has('codigo') ? 'has-error' : ''}}">
    <label for="codigo" class="control-label">{{ 'Codigo' }}</label>
    <input class="form-control" name="codigo" type="text" id="codigo" value="{{ isset($curso->codigo) ? $curso->codigo : ''}}" required>
    {!! $errors->first('codigo', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('curso') ? 'has-error' : ''}}">
    <label for="curso" class="control-label">{{ 'Curso' }}</label>
    <input class="form-control" name="curso" type="text" id="curso" value="{{ isset($curso->curso) ? $curso->curso : ''}}" required>
    {!! $errors->first('curso', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('descripcion') ? 'has-error' : ''}}">
    <label for="descripcion" class="control-label">{{ 'Descripcion' }}</label>
    <input class="form-control" name="descripcion" type="text" id="codigo" value="{{ isset($curso->descripcion) ? $curso->descripcion : ''}}" required>
    {!! $errors->first('descripcion', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('semestre') ? 'has-error' : ''}}">
    <label for="semestre" class="control-label">{{ 'Semestre' }}</label>
    <input class="form-control" name="semestre" type="text" id="semestre" value="{{ isset($curso->semestre) ? $curso->semestre : ''}}" required>
    {!! $errors->first('semestre', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('horasteoria') ? 'has-error' : ''}}">
    <label for="horasteoria" class="control-label">{{ 'Horas Teoria' }}</label>
    <input class="form-control" name="horasteoria" type="number" id="horasteoria" value="{{ isset($curso->horasteoria) ? $curso->horasteoria : ''}}" required>
    {!! $errors->first('horasteoria', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('horaspractica') ? 'has-error' : ''}}">
    <label for="horaspractica" class="control-label">{{ 'Horas Practica' }}</label>
    <input class="form-control" name="horaspractica" type="number" id="horaspractica" value="{{ isset($curso->horaspractica) ? $curso->horaspractica : ''}}" required>
    {!! $errors->first('horaspractica', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('creditos') ? 'has-error' : ''}}">
    <label for="creditos" class="control-label">{{ 'Creditos' }}</label>
    <input class="form-control" name="creditos" type="number" id="creditos" value="{{ isset($curso->creditos) ? $curso->creditos : ''}}" required>
    {!! $errors->first('creditos', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('estado') ? 'has-error' : ''}}">
    <label for="estado" class="control-label">{{ 'Estado' }}</label>
    <input class="form-control" name="estado" type="text" id="estado" value="{{ isset($curso->estado) ? $curso->estado : ''}}" required>
    {!! $errors->first('estado', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('plan_id') ? 'has-error' : ''}}">
    <label for="plan_id" class="control-label">{{ 'Plan ID' }}</label>
    <select name="plan_id" class="form-control" id="plan_id" required>
        @foreach($planes as $item)              
            <option value="{{ $item->id }}" {{ isset($curso->plan_id) ?  ($item->id == $curso->plan_id ? 'selected' : '') : '' }}>
                {{ $item->anio }}
            </option>                      
        @endforeach  
    </select>
    {!! $errors->first('planes', '<p class="help-block">:message</p>') !!}
</div>



<div class="form-group {{ $errors->has('cursotipo_id') ? 'has-error' : ''}}">
    <label for="cursotipo_id" class="control-label">{{ 'Curso Tipo' }}</label>
    <select name="cursotipo_id" class="form-control" id="cursotipo_id" required>
        @foreach($cursotipos as $item)              
            <option value="{{ $item->id }}" {{ isset($curso->cursotipo_id) ?  ($item->id == $curso->cursotipo_id ? 'selected' : '') : '' }}>
                {{ $item->cursotipo }}
            </option>                      
        @endforeach  
    </select>
</div>

<br>

<div class="form-group">
    <input class="btn boton-color btn-sm" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
</div>