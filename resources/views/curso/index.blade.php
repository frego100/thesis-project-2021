@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-11">
                <div class="card">
                    @if(session()->has('success'))
                        <div class="alert alert-success col-md-4">{{ session()->get('success') }}</div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger col-md-4">{{ session()->get('error') }}</div>
                    @endif
                    <div class="card-header">
                        <div class="row justify-content-around">
                            <div class="col">Cursos</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/competencias.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="table-responsive">
                            <table id="myTable" class="display">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Codigo</th>
                                        <th>Curso</th>
                                        <th>Descripcion</th>
                                        <th>Semestre</th>
                                        <th>Horas Teoria</th>
                                        <th>Horas Practica</th>
                                        <th>Creditos</th>
                                        <th>CursoTipo</th>
                                        <th>Plan</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($cursos as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->codigo }}</td>
                                            <td>{{ $item->curso }}</td>
                                            <td>{{ $item->descripcion }}</td>
                                            <td>{{ $item->semestre }}</td>
                                            <td>{{ $item->horasteoria }}</td>
                                            <td>{{ $item->horaspractica }}</td>
                                            <td>{{ $item->creditos }}</td>
                                            <td>{{ $item->cursotipo->curso_tipo }}</td>
                                            <td>{{ $item->plan->anio }}</td>
                                            <td>
                                                <a href="{{ url('cursos/' . $item->id) }}" title="Ver Curso">
                                                    <button class="btn btn-sm"><i class="fa fa-eye"
                                                            aria-hidden="true"></i></button></a>
                                                
                                                <form method="POST" action="{{ url('cursos/' . $item->id) }}"
                                                    accept-charset="UTF-8" style="display:inline">
                                                    @method('put')
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-sm" title="Editar Curso"><i
                                                            class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                                </form>
                                                <form method="POST" action="{{ url('cursos/delete/' . $item->id) }}"
                                                    accept-charset="UTF-8" style="display:inline">
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-sm" title="Eliminar Curso"
                                                        onclick="return confirm(&quot;¿Esta seguro que desea eliminar el curso?&quot;)"><i
                                                            class="fa fa-trash-o" aria-hidden="true" id="delete"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ url('/cursos/create') }}" class="btn boton-color btn-sm"
                        title="Agregar un Nuevo Curso">
                        <i class="fa fa-plus" aria-hidden="true"></i>Agregar
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        console.log('INICIO JQUERY');
        $('.alert').fadeOut(7000);

    </script>
@endsection
