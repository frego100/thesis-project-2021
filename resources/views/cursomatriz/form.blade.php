<div class="form-group {{ $errors->has('codigo') ? 'has-error' : ''}}">
    <label for="codigo" class="control-label">{{ 'Codigo' }}</label>
    <!--<select name="codigo" class="form-control" id="codigo" >-->
    <input class="form-control" name="codigo" type="text" id="codigo"
        value="{{ isset($cursomatriz->matriz) ? $cursomatriz->matriz : '' }}" required>
    {!! $errors->first('codigo', '<p class="help-block">:message</p>') !!}
    
</div>
<div class="form-group {{ $errors->has('ponderacion') ? 'has-error' : '' }}">
    <label for="ponderacion" class="control-label">{{ 'Ponderación' }}</label>
    <input class="form-control" name="ponderacion" type="text" id="ponderacion"
        value="{{ isset($cursomatriz->ponderacion) ? $cursomatriz->ponderacion : '' }}" required>

    {!! $errors->first('ponderacion', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('inicio') ? 'has-error' : '' }}">
    <label for="inicio" class="control-label">{{ 'Fecha Inicio' }}</label>
    <input class="form-control" name="inicio" type="text" id="inicio"
        value="{{ isset($cursomatriz->fechacalificacioninicio) ? $cursomatriz->fechacalificacionfin : '' }}" required>

    {!! $errors->first('inicio', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fechafin') ? 'has-error' : '' }}">
    <label for="fechafin" class="control-label">{{ 'Fecha Fin' }}</label>
    <input class="form-control" name="fechafin" type="text" id="fechafin"
        value="{{ isset($cursomatriz->fechacalificacionfin) ? $cursomatriz->fechacalificacionfin : '' }}" required>

    {!! $errors->first('fechafin', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
    <input class="btn boton-color" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
</div>
