@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Resultado: {{ $cursomatriz->cursomatrizs }}</div>
                <div class="card-body">

                    <a href="{{ url('cursomatrizs') }}" title="Back"><button class="btn boton-color2 btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</button></a>
                    <form method="POST" action="{{ url('cursomatrizs/' . $cursomatriz->id) }}" accept-charset="UTF-8" style="display:inline">
                        @method('put')
                        {{ csrf_field() }}
                        <button type="submit" class="btn boton-color btn-sm" title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                    </form>
                    <form method="POST" action="{{ url('cursomatrizs' . $cursomatriz->id) }}" accept-charset="UTF-8" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn boton-color btn-sm" title="Eliminar" onclick="return confirm(&quot;Confirmae eliminiación?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    </form>
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $cursomatriz->id }}</td>
                                </tr>
                                <tr>
                                    <th> Codigo </th>
                                    <td> {{ $cursomatriz->matriz }} </td>
                                </tr>
                                <tr>
                                    <th> Poderacion </th>
                                    <td> {{ $cursomatriz->ponderacion }} </td>
                                </tr>
                                <tr>
                                    <th> Fecha-Inicio </th>
                                    <td> {{ $cursomatriz->fechacalificacioninicio }} </td>
                                </tr>
                                <tr>
                                    <th> Fecha-Fin </th>
                                    <td> {{ $cursomatriz->fechacalificacionfin }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection