@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-9">
                <div class="card">
                    @if(session()->has('success'))
                        <div class="alert alert-success col-md-4">{{ session()->get('success') }}</div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger col-md-4">{{ session()->get('error') }}</div>
                    @endif
                    <div class="card-header">
                        <div class="row justify-content-around">
                            <div class="col">Facultad</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/facultades.pdf') }}" class="btn boton-color btn-sm"
                                    title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="card-body">
                        
                        <div class="table-responsive">
                            <table  id="myTable" class="display">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Facultad</th>
                                        <th>Estado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($facultades as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->facultad }}</td>
                                            <td>{{ $item->estado }}</td>
                                            <td>
                                                <a href="{{ url('facultades/' . $item->id) }}"
                                                    title="Ver Facultad"><button class="btn btn-sm"><i
                                                            class="fa fa-eye" aria-hidden="true"></i></button></a>
                                                <form method="POST" action="{{ url('facultades/' . $item->id) }}"
                                                    accept-charset="UTF-8" style="display:inline">
                                                    @method('put')
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-sm" title="Editar Facultad"><i
                                                            class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                                </form>
                                                <form method="POST" action="{{ url('facultades/delete/' . $item->id) }}"
                                                    accept-charset="UTF-8" style="display:inline">
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-sm" title="Eliminar Facultad"
                                                        onclick="return confirm(&quot;¿Realmente desea eliminar?&quot;)"><i
                                                            class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div>
                            <a href="{{ url('/facultades/create') }}" class="btn boton-color btn-sm" title="Agregar Facultad">
                                <i class="fa fa-plus" aria-hidden="true"></i> Agregar
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.alert').fadeOut(7000);

    </script>
    
@endsection
