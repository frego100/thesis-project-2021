@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="row justify-content-around">
                            <div class="col">Crear Facultad</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/facultades.pdf') }}" class="btn boton-color btn-sm"
                                    title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif


                        <form method="POST" action="{{ url('/facultades') }}" accept-charset="UTF-8"
                            class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            @include ('facultad.form', ['formMode' => 'create'])
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
