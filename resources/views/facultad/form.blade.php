<div class="form-group  {{ $errors->has('facultad') ? 'has-error' : ''}}">
    <label for="facultad" class="control-label">{{ 'Nombre de la Facultad' }}</label>
    <input class="form-control" name="facultad" type="text" id="facultad" value="{{ isset($facultad->facultad) ? $facultad->facultad : ''}}" required>
    {!! $errors->first('facultad', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn boton-color btn-sm" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
    
    <a href="{{ url('facultades') }}" title=""><button class="btn boton-color2 btn-sm">Cancelar</button></a>
</div>

