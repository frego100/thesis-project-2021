<div class="form-group {{ $errors->has('cursogrupo') ? 'has-error' : '' }}">
    <label for="cursogrupo" class="control-label">{{ 'Grupo' }}</label>
    <select name="cursogrupo" class="form-control" id="cursogrupo" required>
        @foreach ($cursogrupos as $item)
            <option value="{{ $item->id }}"
                {{ isset($cursoestudiante->curso_grupo_id) ? ($item->id == $cursoestudiante->curso_grupo_id ? 'selected' : '') : '' }}>
                {{ $item->grupo }}
            </option>
        @endforeach
    </select>
    {!! $errors->first('cursogrupo', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('estudiante') ? 'has-error' : '' }}">
    <label for="estudiante" class="control-label">{{ 'Estudiante' }}</label>
    <select name="estudiante" class="form-control" id="estudiante" required>
        @foreach ($estudiantes as $item)
            <option value="{{ $item->id }}"
                {{ isset($cursoestudiante->estudiante_id) ? ($item->id == $cursoestudiante->estudiante_id ? 'selected' : '') : '' }}>
                {{ $item->estudiante }}
            </option>
        @endforeach
    </select>
    {!! $errors->first('estudiante', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn boton-color btn-sm" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
</div>
