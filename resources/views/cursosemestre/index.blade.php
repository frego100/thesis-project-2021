@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    @if(session()->has('success'))
                        <div class="alert alert-success col-md-4">{{ session()->get('success') }}</div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger col-md-4">{{ session()->get('error') }}</div>
                    @endif
                    <div class="card-header">
                        <div class="row justify-content-around">
                            <div class="col">Curso Semestre</div>
                            <div class="col-1">
                                <a target="_blank" href="{{asset('files/escuelas.pdf')}}" class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                        <div class="card-body">
                        
                        <div class="table-responsive">
                        <br>
                            <table id="myTable" class="display">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Escuela Semestre ID</th>
                                        <th>Estado</th>                                    
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($cursosemestre as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->escuelasemestre_id }}</td>
                                            <td>{{ $item->estado }}</td>                                                                                        
                                            <td>
                                                <a href="{{ url('cursosemestres/' . $item->id) }}"
                                                    title="Ver Curso-Estudiante"><button class="btn btn-sm"><i
                                                            class="fa fa-eye" aria-hidden="true"></i></button></a>
                                                <form method="POST" action="{{ url('cursosemestres/' . $item->id) }}"
                                                    accept-charset="UTF-8" style="display:inline">
                                                    @method('put')
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-sm"
                                                        title="Editar Curso-Estudiante"><i class="fa fa-pencil-square-o"
                                                            aria-hidden="true"></i></button>
                                                </form>
                                                <form method="POST"
                                                    action="{{ url('cursosemestres/delete/' . $item->id) }}"
                                                    accept-charset="UTF-8" style="display:inline">
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-sm" title="Eliminar Curso-Semestre"
                                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                            class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                            <a href="{{ url('/cursosemestres/create') }}" class="btn boton-color btn-sm"
                                title="Agregar Curso-Estudiante">
                                <i class="fa fa-plus" aria-hidden="true"></i> Agregar
                            </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.alert').fadeOut(7000);

    </script>

@endsection
