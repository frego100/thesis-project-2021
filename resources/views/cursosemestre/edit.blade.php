@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Editar Curso - Semestre</div>
                    <div class="card-body">
                        <a href="{{ url('cursosemestres') }}" title="Regresar"><button class="btn boton-color2 btn-sm"><i
                                    class="fa fa-arrow-left" aria-hidden="true"></i>Cancelar</button></a>

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('cursosemestres/' . $cursosemestre->id . '/edit') }}"
                            accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}

                            @include ('cursosemestre.form', ['formMode' => 'edit'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
