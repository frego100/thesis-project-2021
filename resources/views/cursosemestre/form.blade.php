<div class="form-group {{ $errors->has('cursosemestre') ? 'has-error' : '' }}">
    <label for="cursosemestre" class="control-label">{{ 'Grupo' }}</label>
    <select name="cursosemestre" class="form-control" id="cursosemestre">
        @foreach ($escuelasemestres as $item)
            <option value="{{ $item->id }}"
                {{ isset($cursosemestre->escuelasemestre_id) ? ($item->id == $cursosemestre->escuelasemestre_id? 'selected' : '') : '' }}>
                {{ $item->escuela_id }}
            </option>
        @endforeach
    </select>
    {!! $errors->first('cursogrupo', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn boton-color btn-sm" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
</div>
