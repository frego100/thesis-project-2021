@extends('layouts.adminlayout')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="row justify-content-around">
                            <div class="col">Curso Específico</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/files_Admin/curso-especifico.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>

                    </div>

                    <div class="card-body">
                        <div class="form-group {{ $errors->has('facultad') ? 'has-error' : '' }}">
                            <label for="facultad" class="control-label">{{ 'Facultad' }}</label>
                            <select name="facultad" class="form-control" id="facultad">
                                @foreach ($facultades as $item)
                                    <option value="{{ $item->id }}" {{ isset($item->id) ?  ($item->id == $selected_facultad ? 'selected' : '') : '' }}>{{ $item->facultad }}</option>
                                @endforeach

                            </select>
                            <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                        </div>

                        <div class="form-group {{ $errors->has('escuela') ? 'has-error' : '' }}">
                            <label for="escuela" class="control-label">{{ 'Escuela' }}</label>
                            <select name="escuela" class="form-control" id="escuela">
                                @foreach ($escuelas as $item)
                                    <option value="{{ $item->id }}" {{ isset($item->id) ?  ($item->id == $selected_escuela ? 'selected' : '') : '' }}>{{ $item->escuela }}</option>
                                @endforeach

                            </select>
                            <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                        </div>

                        <div class="form-group {{ $errors->has('escuelasemestre') ? 'has-error' : '' }}">
                            <label for="escuelasemestre" class="control-label">{{ 'Semestres' }}</label>
                            <select name="escuelasemestre" class="form-control" id="escuelasemestre">
                                @foreach ($escuelasemestres as $item)
                                    <option value="{{ $item->id }}" {{ isset($item->id) ?  ($item->id == $selected_escuelasemestre ? 'selected' : '') : '' }}>{{ $item->semestre->anio }}-{{ $item->semestre->semestre }}</option>
                                @endforeach

                            </select>
                            <!--{!! $errors->first('plan', '<p class="help-block">:message</p>') !!}-->
                        </div>

                        <div class="form-group {{ $errors->has('estado') ? 'has-error' : '' }}" id="estadosemestre">
                            <label for="estado" class="control-label" >{{ 'Estado Semestre: ' }} {{$escuelasemestre->estadosemestre}}</label>
                            <!--{!! $errors->first('plan', '<p class="help-block">:message</p>') !!}-->
                        </div>

                        <div class="form-group {{ $errors->has('cursosemestre') ? 'has-error' : '' }}">
                            <label for="cursosemestre" class="control-label">{{ 'Curso' }}</label>
                            <select name="cursosemestre" class="form-control" id="cursosemestre">
                                @foreach ($cursosemestres as $item)
                                    <option value="{{ $item->id }}" {{ isset($item->id) ?  ($item->id == $selected_cursosemestre ? 'selected' : '') : '' }}>{{ $item->curso->curso }}</option>
                                @endforeach

                            </select>
                            <!--{!! $errors->first('plan', '<p class="help-block">:message</p>') !!}-->
                        </div>
                        <div class="content-tabla">
                            <table class="display" style="width:100%" name="cursogrupo" id="cursogrupo">
                                <thead>
                                    <tr>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Docente</th>
                                            <th scope="col">Grupo</th>
                                            <th scope="col">Responsable</th>
                                            <th scope="col">Acciones</th>
                                        </tr>
        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($cursogrupos as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td id="docente{{ $item->id }}">{{ $item->docente->nombre }}</td>
                                            <td>{{ $item->grupo }}</td>
                                            <td>
                                            @if($escuelasemestre->estadosemestre == 'Abierto' || $item->coordinador == '1')
                                                <input type="radio" class="responsable" id="{{$item->id}}" name="coordinador" {{($item->coordinador == '1' ? 'checked="true"' : '') }} >
                                            @endif
                                            </td>                                            
                                            <td>
                                            @if($escuelasemestre->estadosemestre == 'Abierto')
                                                <button type="submit" id="{{ $item->id }}" class="btn btn-sm editar" value="Editar" title="Editar Docente"><i class="fa fa-pencil-square-o" id="{{ $item->id }}" aria-hidden="true"></i></button>
                                                <button type="submit" id="{{ $item->id }}" class="btn btn-sm eliminar" value="Eliminar" title="Eliminar Docente"><i class="fa fa-trash-o" id="{{ $item->id }}"aria-hidden="true"></i></button>
                                            @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <div class="form-group">
                            <input class="btn boton-color btn-sm" type="button" value="Agregar" id="agregarDocente" style="{{ $escuelasemestre->estadosemestre == "Cerrado" ? 'display: none;' : ''}}">
                        </div>
                            <input type="hidden" id="cursosemestreID" name="cursosemestreID" value="0">

                    </div>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var numerador = {{ sizeof($cursogrupos) }} + 1;
    var editarId = 0;

    $(document).ready(function ()
    {
        var datosPrevios;

        function cargaescuelas() 
        {
            var facultad_id = $('#facultad').val();
            if(facultad_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.escuelas') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        facultad_id: facultad_id
                    },  
                    success:function (data) 
                    {
                        $('#escuela').empty();
                        $.each(data.escuelas[0].escuelas,function(index,escuela)
                        {
                                    
                            $('#escuela').append('<option value="'+escuela.id+'">'+escuela.escuela+'</option>');
                        });
                        cargasemestres();
                    }
                });
            }
            else
            {
                $('#escuela').empty();
                cargasemestres();
            }

        };

        function cargasemestres() 
        {
            var escuela_id = $('#escuela').val();
            if(escuela_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.escuelasemestre') }}",
                    data: 
                    {
                        "_token": "{{ csrf_token() }}",
                        escuela_id: escuela_id
                    },
                    success:function (data) 
                    {
                        $('#escuelasemestre').empty();
                        $.each(data.escuelasemestres,function(index,escuelasemestre)
                        {    
                            $('#escuelasemestre').append('<option value="'+escuelasemestre.id+'">'+escuelasemestre.semestre.anio+'-'+escuelasemestre.semestre.semestre+'</option>');
                        });
                        cargacursos();
                    }

                });
            }
            else
            {
                $('#escuelasemestre').empty();
                cargacursos();
            }
                    
        };

        function cargacursos() 
        {
            var escuelasemestre_id = $('#escuelasemestre').val();
            if(escuelasemestre_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cursosemestre') }}",
                    data: 
                    {
                        "_token": "{{ csrf_token() }}",
                        escuelasemestre_id: escuelasemestre_id
                    },
                    success:function (data) 
                    {
                        $('#cursosemestre').empty();
                        $.each(data.cursosemestres,function(index,cursosemestre)
                        {    
                            $('#cursosemestre').append('<option value="'+cursosemestre.id+'">'+cursosemestre.curso.curso+'</option>');
                        });
                        $('#estadosemestre').empty();
                        $('#estadosemestre').append('<label for="estado" class="control-label">Estado Semestre: ' + data.escuelasemestre.estadosemestre  + '</label>');
                        cargadocentes();   
                    }
                    
                });
            }
            else
            {
                $('#cursosemestre').empty();
                cargadocentes();
            }
                    
        };


        function cargadocentes()
        {
            numerador = 1;
            var cursosemestre_id = $('#cursosemestre').val();
            var escuelasemestre_id = $('#escuelasemestre').val();
            console.log(cursosemestre_id);
            if(cursosemestre_id != 'null')
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cursodocente') }}",
                    data: 
                    {
                        "_token": "{{ csrf_token() }}",
                        cursosemestre_id: cursosemestre_id,
                        escuelasemestre_id:escuelasemestre_id
                    },
                    success:function (data) 
                    {
                        $('#cursogrupo').empty();
                        $('#cursogrupo').append('<thead class="thead-light">'+
                            '<tr><th scope="col">#</th>'+
                                '<th scope="col">Docente</th>'+
                                '<th scope="col">Grupo</th>'+
                                '<th scope="col">Responsable</th>'+'<th scope="col">Acciones</th></tr></thead><tbody>');
                        $.each(data.cursogrupos,function(index,cursogrupo)
                        {    
                            if(cursogrupo.coordinador == 1)
                            {
                                $('#cursogrupo').append('<tr><td>'+numerador+'</td><td>'+cursogrupo.docente.nombre+'</td><td>'+cursogrupo.grupo+'</td><td><input type="radio" class="responsable" id="'+cursogrupo.id+'" name="coordinador" checked="true"></td><td><button type="submit" id="'+cursogrupo.id+'" class="btn btn-sm editar" value="Editar" title="Editar Docente"><i class="fa fa-pencil-square-o" id="'+cursogrupo.id+'" aria-hidden="true"></i></button> <button type="submit" id="'+cursogrupo.id+'" class="btn btn-sm eliminar" value="Eliminar" title="Eliminar Docente"><i class="fa fa-trash-o" id="'+cursogrupo.id+'"aria-hidden="true"></i></button></td></tr>');

                                
                            }
                            else
                            {
                                //$('#cursogrupo').append('<tr><td>'+numerador+'</td><td>'+cursogrupo.docente.nombre+'</td><td>'+cursogrupo.grupo+'</td><td><input type="radio" class="responsable" id="'+cursogrupo.id+'" name="coordinador"></td><td><input type="button" id="'+cursogrupo.id+'" class="editar" value="Editar"><input type="button" id="'+cursogrupo.id+'" class="eliminar" value="Eliminar"></td></tr>');
                                //$('#cursogrupo').append('<tr><td>'+numerador+'</td><td>'+cursogrupo.docente.nombre+'</td><td>'+cursogrupo.grupo+'</td><td><input type="radio" class="responsable" id="'+cursogrupo.id+'" name="coordinador"></td><td><button type="submit" id="'+cursogrupo.id+'" class="btn btn-sm editar" value="Editar" title="Editar Docente"><i class="fa fa-pencil-square-o" id="'+cursogrupo.id+'" aria-hidden="true"></i></button> <button type="submit" id="'+cursogrupo.id+'" class="btn btn-sm eliminar" value="Eliminar" title="Eliminar Docente"><i class="fa fa-trash-o" id="'+cursogrupo.id+'"aria-hidden="true"></i></button></td></tr>');
                                
                                if(data.escuelasemestre.estadosemestre == "Abierto"){
                                    $('#cursogrupo').append('<tr><td>'+numerador+'</td><td>'+cursogrupo.docente.nombre+'</td><td>'+cursogrupo.grupo+'</td><td><input type="radio" class="responsable" id="'+cursogrupo.id+'" name="coordinador"></td><td><button type="submit" id="'+cursogrupo.id+'" class="btn btn-sm editar" value="Editar" title="Editar Docente"><i class="fa fa-pencil-square-o" id="'+cursogrupo.id+'" aria-hidden="true"></i></button> <button type="submit" id="'+cursogrupo.id+'" class="btn btn-sm eliminar" value="Eliminar" title="Eliminar Docente"><i class="fa fa-trash-o" id="'+cursogrupo.id+'"aria-hidden="true"></i></button></td></tr>');
                                }else{
                                    $('#cursogrupo').append('<tr><td>'+numerador+'</td><td>'+cursogrupo.docente.nombre+'</td><td>'+cursogrupo.grupo+'</td><td></td><td><button type="submit" id="'+cursogrupo.id+'" class="btn btn-sm editar" value="Editar" title="Editar Docente"><i class="fa fa-pencil-square-o" id="'+cursogrupo.id+'" aria-hidden="true"></i></button> <button type="submit" id="'+cursogrupo.id+'" class="btn btn-sm eliminar" value="Eliminar" title="Eliminar Docente"><i class="fa fa-trash-o" id="'+cursogrupo.id+'"aria-hidden="true"></i></button></td></tr>');
                                }

                            }

                            numerador += 1;


                        });
                        
                        $('#estadosemestre').empty();
                        $('#estadosemestre').append('<label for="estado" class="control-label">Estado Semestre: ' + data.escuelasemestre.estadosemestre  + '</label>');

                        $('#cursogrupo').append('</tbody>');
                        if(data.escuelasemestre.estadosemestre == "Abierto"){
                            $('#agregarDocente').show();
                        }else{
                            $('#agregarDocente').hide();
                            $('.eliminar').hide();
                            $('.editar').hide();
                        }
                        //if(numerador == 1){$('#agregarDocente').hide();}
                    }
                    
                });
            }
            else
            {
                console.log("entro else carga docente");
                $('#cursosemestre').empty();
                $('#agregarDocente').hide();
            }
                    
        };

        function agregarDocentesVista()
        {
            $('.editar').hide();
            $('.eliminar').hide();
            $('#agregarDocente').hide();
            $('#cursosemestreID').attr('value', $('#cursosemestre').val());
            if(numerador==1)
            {
                $('#cursogrupo').append('<tr><td>'+numerador+'</td><td><select name="nuevo_docente" class="form-control" id="nuevoDocente"></select></td><td><input type="text" value="A" id="nuevogrupo"></td><td><input type="radio" id="nuevoresponsable" checked="true" name="coordinador"></td><td><input type="button" id="guardarDocente" value="Guardar"><input type="button" id="noGuardarDocente" value="Cancelar"></td></tr>');
            }
            else
            {
                //$('#cursogrupo').append('<tr><td>'+numerador+'</td><td><select name="nuevo_docente" class="form-control" id="nuevoDocente"></select></td><td><input type="text" value="A" id="nuevogrupo"></td><td></td><td><input type="button" id="guardarDocente" value="Guardar"><input type="button" id="noGuardarDocente" value="Cancelar"></td></tr>');
                $('#cursogrupo').append('<tr><td>'+numerador+'</td><td><select name="nuevo_docente" class="form-control" id="nuevoDocente"></select></td><td><input type="text" value="A" id="nuevogrupo"></td><td></td><td><button type="submit" id="guardarDocente" value="Guardar" class="btn boton-color btn-sm" title="Guardar Docente"><i class="fa fa-floppy-o" aria-hidden="true"></i></button><button type="submit" id="noGuardarDocente" value="Cancelar" class="btn boton-color btn-sm" title="Cancelar"><i class="fa fa-window-close-o" aria-hidden="true"></i></button></td></tr>');
            }
            var cursosemestre_id = $('#cursosemestre').val();
            numerador +=1;

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.docentes') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                    },
                    success:function (data) 
                    {
                        $.each(data.docentes,function(index,docente)
                        {    
                            $('#nuevoDocente').append('<option value="'+docente.id+'">'+docente.nombre+'</option>');
                        });
                    }
                    
                });
                    
        };

        function guardarDocenteVista()
        {
            var grupo = $('#nuevogrupo').val();
            var coordinador = '0';
            if(numerador==2)
                coordinador = '1';
            var cursosemestre_id = $('#cursosemestreID').val();
            var docente_id = $('#nuevoDocente').val();

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.insertarcursogrupo') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        grupo:grupo,
                        coordinador:coordinador,
                        cursosemestre_id:cursosemestre_id,
                        docente_id:docente_id
                    },
                    success:function() 
                    {
                        cargadocentes();                        
                    }
                    
                });

            $('#agregarDocente').show();
        };

        function cancelarGuardarDocenteVista(e)
        {
            $(e.target ).closest( "tr" ).remove();
            $('#agregarDocente').show();
            $('.editar').show();
            $('.eliminar').show();
            numerador -=1;
        };

        //mensaje de eliminacion, no incluido
        function confirmarEliminacion(e) 
        {
            $('#spanMessage').html('¿Está seguro(a) de guardar los cambios?');
            $("#dialogConfirm").dialog(
            {
                resizable: false,
                height: 140,
                modal: true,
                title: 'Mensaje Modal',
                buttons:
                {
                    'Aceptar': function ()
                    {
                        eliminarDocenteVista(e);
                        $(this).dialog("close");
                    },
                        'Cancelar': function () {
                        $(this).dialog("close");
                    }
                }
            });
        }        

        function eliminarDocenteVista(e)
        {
            var myId = e.target.id;
            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.eliminarcursogrupo') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        id:myId
                    },
                    success:function(resp) 
                    {

                        $('.alert').remove();
                        if(resp != 0){
                            $('.card').prepend('<div class="alert alert-danger col-md-4">No se puede eliminar esquema porque tiene relacion con '+ resp +' registros</div>');
                            $('.alert').fadeOut(7000);
                        }else{
                            $('.card').prepend('<div class="alert alert-success col-md-4">Se elimino exitosamente</div>');
                            $('.alert').fadeOut(7000);
                        }
                        cargadocentes();                        
                    }
                    
                });
        };

        function editarDocenteVista(e)
        {
                $('.editar').hide();
                $('.eliminar').hide();
                $('#agregarDocente').hide();

                datosPrevios = $(e.target ).closest( "tr" ).html();

                editarId = e.target.id;
                var myTr = $(e.target ).closest( "tr" );

                var profesor = myTr.find('td:eq(1)').text();
                var grupo = myTr.find('td:eq(2)').text();
                var responsable = myTr.find('td:eq(3)').text();

                $(e.target ).closest( "td" ).append('<input type="button" id="guardarDocenteEditado" value="Guardar"><input type="button" id="noGuardarDocenteEditado" class="noEditar" value="Cancelar">');


                myTr.find('td:eq(1)').empty();
                myTr.find('td:eq(1)').append('<select id="editarDocente" class="form-control"></select>');

                myTr.find('td:eq(2)').empty();
                myTr.find('td:eq(2)').append('<input type="text" id="editarGrupo" value="'+grupo+'">');


            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.docentes') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                    },
                    success:function (data) 
                    {
                        $.each(data.docentes,function(index,docente)
                        {
                            if(profesor == docente.nombre)
                            {
                                $('#editarDocente').append('<option value="'+docente.id+'" selected>'+docente.nombre+'</option>')
                            }
                            else
                            {
                                $('#editarDocente').append('<option value="'+docente.id+'">'+docente.nombre+'</option>');
                            }
                        });
                    }
                    
                });
            
        };

        function guardarEditarDocenteVista(e)
        {
            var grupo = $('#editarGrupo').val();
            var docente_id = $('#editarDocente').val();
            var id = editarId;

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.editarcursogrupo') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        id:id,
                        grupo:grupo,
                        docente_id:docente_id
                    },
                    success:function() 
                    {
                        cargadocentes();
                        //poner notificacion de editado exitoso
                    }
                    
                });

            $('#agregarDocente').show();
        };

        function cancelarEditarDocenteVista(e)
        {
            console.log("entro cancelar edicion");
            var myTr = $(e.target ).closest( "tr" );
            myTr.empty();
            myTr.append(datosPrevios);
            $('.editar').show();
            $('.eliminar').show();
            $('#agregarDocente').show();
        };

        function cambiarCoordinador(e)
        {
            console.log("cambio de coordinador");
            var id = e.target.id;

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.editarcursogrupocoordinador') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        id:id,
                    }
                    
                });
        }


        $('#facultad').on('change', cargaescuelas);
        $('#escuela').on('change', cargasemestres);
        $('#escuelasemestre').on('change', cargacursos);
        $('#cursosemestre').on('change', cargadocentes);

        $('#agregarDocente').on('click', agregarDocentesVista);
        $('body').on('click','#guardarDocente',guardarDocenteVista);
        $('body').on('click','#noGuardarDocente',cancelarGuardarDocenteVista);

        $('body').on('click','#guardarDocenteEditado',guardarEditarDocenteVista);
        $('body').on('click','.editar',editarDocenteVista);
        $('body').on('click','.noEditar',cancelarEditarDocenteVista);

        $('body').on('click','.eliminar',eliminarDocenteVista);

        $('body').on('change','input[type="radio"]', cambiarCoordinador);
    });

</script>

@endsection
