@extends('layouts.adminlayout')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Resultado Especifico') }}</div>

                <div class="card-body">

                            <div class="form-group {{ $errors->has('facultad') ? 'has-error' : ''}}">
                                <label for="facultad" class="control-label">{{ 'Facultad' }}</label>
                                <select name="facultad" class="form-control" id="facultad" >
                                @foreach($facultades as $item)
                                <option value="{{ $item->id }}"{{ isset($item->id) ?  ($item->id == $selected_facultad ? 'selected' : '') : '' }}>{{ $item->facultad }}</option>
                                @endforeach   

                            </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>


                           <div class="form-group {{ $errors->has('escuela') ? 'has-error' : ''}}">
                                <label for="escuela" class="control-label">{{ 'Escuela' }}</label>
                                <select name="escuela" class="form-control" id="escuela" >
                                @foreach($escuelas as $item)
                                <option value="{{ $item->id }}"{{ isset($item->id) ?  ($item->id == $selected_escuela ? 'selected' : '') : '' }}>{{ $item->escuela }}</option>
                                @endforeach   
                                                               

                            </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div> 
                            
                            
                           <div class="form-group {{ $errors->has('plan') ? 'has-error' : ''}}">
                                <label for="plan" class="control-label">{{ 'Plan de Estudio' }}</label>
                                <select name="plan" class="form-control" id="plan" >
                                @foreach($planes as $item)
                                <option value="{{ $item->id }}"{{ isset($item->id) ?  ($item->id == $selected_plan ? 'selected' : '') : '' }}>{{ $item->anio }}</option>
                                @endforeach
                                                               

                            </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>   

                            <div class="form-group {{ $errors->has('re') ? 'has-error' : ''}}">
                                <label for="re" class="control-label">{{ 'Resultado del Estudiante' }}</label>
                                <select name="re" class="form-control" id="re" >
                                @forelse ($resultadosestudiantes as $item)
                                    <option value="{{ $item->id }}"{{ isset($item->id) ?  ($item->id == $selected_resultadoestudiante ? 'selected' : '') : '' }}>{{ $item->re }}</option>
                                @empty
                                <option></option>
                                @endforelse
                                                               

                            </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div> 
                            
                        <div class="content-tabla">
                            <table class="display" name="resultadosECompetencia" id="resultadosECompetencia">
                            <thead class="thead-light">
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Relacion</th>
                                <th scope="col">Codigo - Competencia</th>                                                           
                                </tr>
                            </thead>
                            <tbody id="comp">
                                @foreach($competencias as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td><input type="checkbox" id="{{ $item->id }}" class="competencia" 
                                            @foreach($resultadosECompetencia as $item2)
                                                {{ ($item->id == $item2->competencia->id ? 'name='.$item2->id.' checked=true' : '') }}
                                            @endforeach
                                            ></td>
                                        <td><label for="{{ $item->id }}">{{ $item->codigo }}</label></td>
                                    </tr>
                                @endforeach
                            </tbody>
                            </table>   
                        </div>                                                                                     
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function ()
    {
        var numerador = 1;
        var myCheck;

        function cargaescuelas() 
        {
            var facultad_id = $('#facultad').val();
            if(facultad_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.escuelas') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        facultad_id: facultad_id
                    },  
                    success:function (data) 
                    {
                        $('#escuela').empty();
                        $.each(data.escuelas[0].escuelas,function(index,escuela)
                        {
                                    
                            $('#escuela').append('<option value="'+escuela.id+'">'+escuela.escuela+'</option>');
                        });
                        cargaplanes();
                    }
                });
            }
            else
            {
                $('#escuela').empty();
                cargaplanes();
            }

        };

        function cargaplanes() 
        {
            var escuela_id = $('#escuela').val();
            if(escuela_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.planes') }}",
                    data: 
                    {
                        "_token": "{{ csrf_token() }}",
                        escuela_id: escuela_id
                    },
                    success:function (data) 
                    {
                        $('#plan').empty();
                        $.each(data.planes[0].plans,function(index,plan)
                        {    
                            $('#plan').append('<option value="'+plan.id+'">'+plan.anio+'</option>');
                        });
                        cargaResultadoEstudiante();
                    }
                });
            }
            else
            {
                $('#plan').empty();
                cargaResultadoEstudiante();
            }
                    
        };

        function cargaResultadoEstudiante(){
            var plan_id = $('#plan').val();
            if(plan_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.re') }}",
                    data: 
                    {
                        "_token": "{{ csrf_token() }}",
                        plan_id: plan_id
                    },
                    success:function (data) 
                    {
                        $('#re').empty();
                        $.each(data.resultadosestudiantes[0].resultadosestudiantes,function(index,re)
                        {    
                            $('#re').append('<option value="'+re.id+'">'+re.re+'</option>');
                        });
                        cargaCompetencia();
                    }
                });
            }
            else
            {
                $('#re').empty();
                cargaCompetencia();
            }
        }; 

        function cargaCompetencia()
        {
            numerador = 1;
            var plan_id = $('#plan').val();
            if(plan_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.competencias') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        plan_id: plan_id
                    },
                    success:function (data)
                    {
                        $('#comp').empty();
                        $.each(data.competencias[0].competencias,function(index,competencia)
                        {    
                            $('#comp').append('<tr><td>'+numerador+'</td><td><input type="checkbox" id="'+competencia.id+'" class="competencia"></td><td><label for="'+competencia.id+'">'+competencia.codigo+'</label></td></tr>');
                            numerador = numerador +1;
                        });
                        cargaresultadoECompetencia();
                    }
                });
            }
            else
            {
                $('#comp').empty();
                cargaresultadoECompetencia();
            }
        }

        function cargaresultadoECompetencia()
        { 
            var resultadoestudiante_id = $('#re').val();
            if(resultadoestudiante_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.resultadoECompetencia') }}", 
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        resultadoestudiante_id: resultadoestudiante_id
                    },
                    success:function (data)
                    {
                        quitarCheck();
                        $.each(data.competenciaRE, function(index,competenciaRE)
                        {
                            if($('#'+competenciaRE.competencia.id).attr("class") == 'competencia')
                            {
                                console.log("entro ");
                                console.log(competenciaRE.competencia.id);
                                $('#'+competenciaRE.competencia.id).prop("checked",true);
                                $('#'+competenciaRE.competencia.id).attr("name",competenciaRE.id);
                            }
                        });
                    }
                });
            }
            else
            {
                $('#comp').empty();
            }
        }

        function quitarCheck()
        {
            $('.competencia').prop("checked",false);
            $('.competencia').attr("name",'');
        }

        function guardarCheck()
        {
            var competencia_id = myCheck.attr("id");
            var resultadoestudiante_id = $('#re').val();

            console.log("id guardar" + competencia_id);

            $.ajax(
            {
                type:'POST',
                url:"{{ route('ajax.insertarcompetenciare') }}", 
                data:
                {
                    "_token": "{{ csrf_token() }}",
                    competencia_id: competencia_id,
                    resultadoestudiante_id: resultadoestudiante_id
                },
                success:function (data)
                {
                    console.log(data.competenciasre.id);
                    myCheck.prop("checked", true);
                    myCheck.attr("name", data.competenciasre.id);
                }
            });
        }

        function eliminarCheck()
        {
            var id = myCheck.attr("name");

            console.log("id borrar" + id);

            $.ajax(
            {
                type:'POST',
                url:"{{ route('ajax.eliminarcompetenciare') }}",
                data:
                {
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                success:function (data)
                {
                    myCheck.attr("name","");
                    myCheck.prop("checked", false);
                }
            });
        }

        function verificarCheck(e)
        {
            myCheck = $(e.target);
            if( myCheck.prop("checked"))
            {
                console.log("se guardo check");
                guardarCheck();
            }
            else
            {
                console.log("se borro check");
                eliminarCheck();
            }
        }

        $('#facultad').on('change', cargaescuelas);
        $('#escuela').on('change', cargaplanes);
        $('#plan').on('change', cargaResultadoEstudiante);
        $('#re').on('change', cargaresultadoECompetencia);
        $('body').on('change','.competencia', verificarCheck);
    });


</script>

@endsection