@extends('layouts.adminlayout')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Competencias') }}</div>

                <div class="card-body">

                            <div class="form-group {{ $errors->has('facultad') ? 'has-error' : ''}}">
                                <label for="facultad" class="control-label">{{ 'Facultad' }}</label>
                                <select name="facultad" class="form-control" id="facultad" >
                                    @foreach ($facultades as $item)
                                        <option value="{{ $item->id }}"{{ isset($item->id) ?  ($item->id == $selected_facultad ? 'selected' : '') : '' }}>{{ $item->facultad }}</option>
                                    @endforeach

                            </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>


                           <div class="form-group {{ $errors->has('escuela') ? 'has-error' : ''}}">
                                <label for="escuela" class="control-label">{{ 'Escuela' }}</label>
                                <select name="escuela" class="form-control" id="escuela" >
                                    @foreach ($escuelas as $item)
                                        <option value="{{ $item->id }}"{{ isset($item->id) ?  ($item->id == $selected_escuela ? 'selected' : '') : '' }}>{{ $item->escuela }}</option>
                                    @endforeach                          

                            </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div> 
                            
                            
                           <div class="form-group {{ $errors->has('plan') ? 'has-error' : ''}}">
                                <label for="plan" class="control-label">{{ 'Plan de Estudio' }}</label>
                                <select name="plan" class="form-control" id="plan" >
                                    @foreach ($planes as $item)
                                        <option value="{{ $item->id }}"{{ isset($item->id) ?  ($item->id == $selected_plan ? 'selected' : '') : '' }}>{{ $item->anio }}</option>
                                    @endforeach                            

                            </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            

                            <div class="form-group {{ $errors->has('competencia') ? 'has-error' : ''}}">
                                <label for="competencia" class="control-label">{{ 'Competencia' }}</label>
                                <select name="competencia" class="form-control" id="competencia" >
                                    @foreach ($competencias as $item)
                                        <option value="{{ $item->id }}"{{ isset($item->id) ?  ($item->id == $selected_competencia ? 'selected' : '') : '' }}>{{ $item->competencia }}</option>
                                    @endforeach                               
                            </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div> 
                                   
                            <br>


                            <table class="table table-bordered" name="resultadosestudiante" id="resultadosestudiante">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Relacion</th>
                                    <th scope="col">Codigo - Resultado Estudiante</th>
                                </tr>
                            </thead>
                            <tbody id="re">
                                @foreach ($resultadosestudiantes as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td><input type="checkbox" id="{{ $item->id }}" class="resultadoE" 
                                            @foreach($competenciaRes as $item2)
                                                {{ ($item->id == $item2->resultadoestudiante->id ? 'name='.$item2->id.' checked=true' : '') }}
                                            @endforeach
                                            ></td>
                                        <td><label for="{{ $item->id }}">{{ $item->codigore }}</label></td>
                                    </tr>
                                @endforeach
                            </tbody>
                            </table>
                                                                                                                
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function ()
    {
        var myCheck;
        var numerador = 1;

        function cargaescuelas() 
        {
            var facultad_id = $('#facultad').val();
            if(facultad_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.escuelas') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        facultad_id: facultad_id
                    },  
                    success:function (data) 
                    {
                        $('#escuela').empty();
                        $.each(data.escuelas[0].escuelas,function(index,escuela)
                        {
                                    
                            $('#escuela').append('<option value="'+escuela.id+'">'+escuela.escuela+'</option>');
                        });
                        cargaplanes();
                    }
                });
            }
            else
            {
                $('#escuela').empty();
                cargaplanes();
            }

        };

        function cargaplanes() 
        {
            var escuela_id = $('#escuela').val();
            if(escuela_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.planes') }}",
                    data: 
                    {
                        "_token": "{{ csrf_token() }}",
                        escuela_id: escuela_id
                    },
                    success:function (data) 
                    {
                        $('#plan').empty();
                        $.each(data.planes[0].plans,function(index,plan)
                        {    
                            $('#plan').append('<option value="'+plan.id+'">'+plan.anio+'</option>');
                        });
                        cargacompetencias();
                    }
                });
            }
            else
            {
                $('#plan').empty();
                cargacompetencias();
            }
                    
        };

        function cargacompetencias()
        { 
            var plan_id = $('#plan').val();
            if(plan_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.competencias') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        plan_id: plan_id
                    },
                    success:function (data)
                    {
                        $('#competencia').empty();
                        $.each(data.competencias[0].competencias,function(index,competencia)
                        {    
                            $('#competencia').append('<option value="'+competencia.id+'">'+competencia.competencia+'</option>');
                            var maxlength=100;
                            $('#competencia>option').text(function(i,text){
                                if(text.length > maxlength){
                                    return text.substr(0, maxlength)+ '...';
                                }
                            });
                        });
                        cargaResultadoEstudiante();
                    }
                });
            }
            else
            {
                $('#competencia').empty();
                cargaResultadoEstudiante();
            }
        }

        function cargaResultadoEstudiante()
        {
            numerador = 1;
            var plan_id = $('#plan').val();
            if(plan_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.re') }}",
                    data: 
                    {
                        "_token": "{{ csrf_token() }}",
                        plan_id: plan_id
                    },
                    success:function (data) 
                    {
                        $('#re').empty();
                        $.each(data.resultadosestudiantes[0].resultadosestudiantes,function(index,re)
                        {    
                            $('#re').append('<tr><td>'+numerador+'</td><td><input type="checkbox" id="'+re.id+'" class="resultadoE"></td><td><label for="'+re.id+'">'+re.codigore+'</label></td></tr>');
                            numerador = numerador +1;
                        });
                        cargaResultadosRelacion();
                    }
                });
            }
            else
            {
                $('#re').empty();
            }
        }

        function cargaResultadosRelacion()
        { 
            var competencia_id = $('#competencia').val();
            if(competencia_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.competenciasEspecifico') }}", 
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        competencia_id: competencia_id
                    },
                    success:function (data)
                    {
                        quitarCheck();
                        $.each(data.resultadosestudiantes, function(index,resultadosestudiante)
                        {
                            if($('#'+resultadosestudiante.resultadoestudiante.id).attr("class") == 'resultadoE')
                            {
                                console.log("entro ");
                                console.log(resultadosestudiante.resultadoestudiante.id);
                                $('#'+resultadosestudiante.resultadoestudiante.id).prop("checked",true);
                                $('#'+resultadosestudiante.resultadoestudiante.id).attr("name",resultadosestudiante.id);
                            }
                        });
                    }
                });
            }
            else
            {
                $('#re').empty();
            }
        }

        function quitarCheck()
        {
            $('.resultadoE').prop("checked",false);
            $('.resultadoE').attr("name",'');
        }

        function guardarCheck()
        {
            var competencia_id = $('#competencia').val();
            var resultadoestudiante_id = myCheck.attr("id");

            console.log("id guardar" + competencia_id);

            $.ajax(
            {
                type:'POST',
                url:"{{ route('ajax.insertarcompetenciare') }}", 
                data:
                {
                    "_token": "{{ csrf_token() }}",
                    competencia_id: competencia_id,
                    resultadoestudiante_id: resultadoestudiante_id
                },
                success:function (data)
                {
                    console.log(data.competenciasre.id);
                    myCheck.prop("checked", true);
                    myCheck.attr("name", data.competenciasre.id);
                }
            });
        }

        function eliminarCheck()
        {
            var id = myCheck.attr("name");

            console.log("id borrar" + id);

            $.ajax(
            {
                type:'POST',
                url:"{{ route('ajax.eliminarcompetenciare') }}",
                data:
                {
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                success:function (data)
                {
                    myCheck.attr("name","");
                    myCheck.prop("checked", false);
                }
            });
        }

        function verificarCheck(e)
        {
            console.log("inicia verificarCheck");
            myCheck = $(e.target);
            if( myCheck.prop("checked"))
            {
                console.log("se guardo check");
                guardarCheck();
            }
            else
            {
                console.log("se borro check");
                eliminarCheck();
            }
        }

        $('#facultad').on('change', cargaescuelas);
        $('#escuela').on('change', cargaplanes);
        $('#plan').on('change',cargacompetencias);
        $('#competencia').on('change',cargaResultadosRelacion);
        $('body').on('change','.resultadoE', verificarCheck);

    });


</script>
@endsection