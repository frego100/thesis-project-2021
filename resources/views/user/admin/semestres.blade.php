@extends('layouts.adminlayout')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        <div class="row justify-content-around">
                            <div class="col">Semestre</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/files_Admin/semestre.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>

                    </div>
                    <div class="card-body">
                    <form method="GET" action="{{ url('/admin/cursoEsp') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        <div class="form-group {{ $errors->has('facultad') ? 'has-error' : '' }}">
                            <label for="facultad" class="control-label">{{ 'Facultad' }}</label>
                            <select name="facultad" class="form-control" id="facultad">
                                @foreach ($facultades as $item)
                                    <option value="{{ $item->id }}" {{ isset($item->id) ?  ($item->id == $selected_facultad ? 'selected' : '') : '' }}>{{ $item->facultad }}</option>
                                @endforeach
                            </select>
                            <!--{!! $errors->first('facultad', '<p class="help-block">:message</p>') !!}-->
                        </div>

                        <div class="form-group {{ $errors->has('escuela') ? 'has-error' : '' }}">
                            <label for="escuela" class="control-label">{{ 'Escuela' }}</label>
                            <select name="escuela" class="form-control" id="escuela">
                                @foreach ($escuelas as $item)
                                    <option value="{{ $item->id }} " {{ isset($item->id) ?  ($item->id == $selected_escuela ? 'selected' : '') : '' }}>{{ $item->escuela }}</option>
                                @endforeach
                            </select>
                            <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                        </div>

                        <div class="form-group {{ $errors->has('escuelasemestre') ? 'has-error' : '' }}">
                            <label for="escuelasemestre" class="control-label">{{ 'Semestres' }}</label>
                            <select name="escuelasemestre" class="form-control" id="escuelasemestre">
                                @foreach ($escuelasemestres as $item)
                                    <option value="{{ $item->id }}">{{ $item->semestre->anio }}-{{ $item->semestre->semestre }}</option>
                                @endforeach

                            </select>
                            <!--{!! $errors->first('plan', '<p class="help-block">:message</p>') !!}-->
                        </div>

                        <div class="form-group {{ $errors->has('estado') ? 'has-error' : '' }}">
                            <label for="estadosemestre" class="control-label">{{ 'Estado Semestre' }}</label>
                            <select name="estadosemestre" class="form-control" id="estadosemestre">
                                @if(isset($escuelasemestres[0]))
                                <option value=0 {{ $escuelasemestres[0]->estadosemestre == 'Abierto' ? 'selected' : '' }}>Abierto</option>
                                <option value=1 {{ $escuelasemestres[0]->estadosemestre == 'Cerrado' ? 'selected' : '' }} >Cerrado</option>
                                @endif
                            </select>
                            <!--{!! $errors->first('estado', '<p class="help-block">:message</p>') !!}-->
                        </div>
                    
                        <div class="content-tabla">
                            <table class="display" style="width:100%" name="cursosemestre" id="cursosemestre">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Plan</th>
                                        <th scope="col">Codigo</th>
                                        <th scope="col">Curso</th>
                                        <th scope="col">Semestre</th>
                                        <th scope="col">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($cursosemestres as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td id="{{ $item->curso->plan->id }}">{{ $item->curso->plan->anio }}</td>
                                            <td>{{ $item->curso->codigo }}</td>
                                            <td id="{{ $item->curso->id }}"><p id="{{ $item->id }}">{{ $item->curso->curso }}</p></td>
                                            <td>{{ $item->curso->semestre }}</td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <button type="button" id="{{ $item->id }}" class="btn btn-sm editar" >Editar</button>
                                                    </div>
                                                    <div class="col-4">
                                                    <button type="button" id="{{ $item->id }}" class="btn btn-sm eliminar" >Eliminar</button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <div class="form-group">
                            <input class="btn btn-primary" type="button" value="Agregar" id="agregarCurso">
                        </div>
                        <input type="hidden" id="cursoID" name="cursoID" value="0">
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var numerador = {{ sizeof($cursosemestres) }} + 1;
    var datos_nuevo_curso;

    $(document).ready(function ()
    {
        
        var datosPrevios;
        var editarId = 0;
        var editar_plan = 0;
        var editar_curso = 0;

        function cargaescuelas() 
        {
            var facultad_id = $('#facultad').val();
            if(facultad_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.escuelas') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        facultad_id: facultad_id
                    },  
                    success:function (data) 
                    {
                        $('#escuela').empty();
                        $.each(data.escuelas[0].escuelas,function(index,escuela)
                        {
                                    
                            $('#escuela').append('<option value="'+escuela.id+'">'+escuela.escuela+'</option>');
                        });
                        cargasemestres();
                    }
                });
            }
            else
            {
                $('#escuela').empty();
                cargasemestres();
            }

        };

        function cargasemestres() 
        {
            var escuela_id = $('#escuela').val();
            if(escuela_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.escuelasemestre') }}",
                    data: 
                    {
                        "_token": "{{ csrf_token() }}",
                        escuela_id: escuela_id
                    },
                    success:function (data) 
                    {
                        $('#escuelasemestre').empty();
                        $.each(data.escuelasemestres,function(index,escuelasemestre)
                        {    
                            $('#escuelasemestre').append('<option value="'+escuelasemestre.id+'">'+escuelasemestre.semestre.anio+'-'+escuelasemestre.semestre.semestre+'</option>');
                        });  
                        cargasemestrecurso();
                    }
                });
            }
            else
            {
                $('#escuelasemestre').empty();
                cargasemestrecurso();
            }        
        };

        function cargasemestrecurso() 
        {
            numerador = 1;
            var escuelasemestre_id = $('#escuelasemestre').val();
            if(escuelasemestre_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cursosemestre') }}",
                    data: 
                    {
                        "_token": "{{ csrf_token() }}",
                        escuelasemestre_id: escuelasemestre_id
                    },
                    success:function (data)
                    {
                        $('#cursosemestre').empty();
                        $('#cursosemestre').append('<thead class="thead-light"><tr><th scope="col">#</th><th scope="col">Plan</th><th scope="col">Codigo</th><th scope="col">Curso</th><th scope="col">Semestre</th><th scope="col">Acciones</th></tr></thead><tbody>');

                        $.each(data.cursosemestres,function(index,cursosemestre)
                        {    
                            $('#cursosemestre').append('<tr><td>'+numerador+'</td><td sid="'+cursosemestre.curso.plan.id+'">'+cursosemestre.curso.plan.anio+'</td><td>'+cursosemestre.curso.codigo+'</td><td id="'+cursosemestre.curso.id+'"><p id="'+cursosemestre.id+'">'+cursosemestre.curso.curso+'</p></td><td>'+cursosemestre.curso.semestre+'</td>'+
                            '<td>'+
                            '<div class="row">'+
                            '<div class="col-4"><input type="button" id="'+cursosemestre.id+'" class="btn btn-primary btn-sm editar" value="Editar"></div>'+
                            '<div class="col-4"><input class="btn btn-danger btn-sm eliminar" type="button" id="'+cursosemestre.id+'" value="Eliminar"></div>'+
                            '</div>'+
                            '</td></tr>');

                       


                            numerador += 1;
                        });
                        $('#cursosemestre').append('</tbody>');

                        $('#estadosemestre').empty();
                        if(data.escuelasemestre.estadosemestre == "Abierto"){
                            $('#estadosemestre').append('<option value="0" selected>Abierto</option>');
                            $('#estadosemestre').append('<option value="1">Cerrado</option>');
                        }else{
                            $('#estadosemestre').append('<option value="0">Abierto</option>');
                            $('#estadosemestre').append('<option value="1" selected>Cerrado</option>');
                        }
                    }
                });
            } 
            else
            {
                $('#cursosemestre').empty();
                $('#cursosemestre').append('<thead class="thead-light"><tr><th scope="col">#</th><th scope="col">Curso</th><th scope="col">Semestre</th><th scope="col">Estado</th></tr></thead><tbody>');
            }
        }

        function editarEstadoSemestre(e)
        {

            var escuelasemestre_id = $('#escuelasemestre').val();
            var select = (e.target);
            var estado_valor=$('option:selected',select).attr('value');

            console.log('ESCUELASEMESTRE');
            console.log(estado_valor);

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.editarestadosemestre') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        escuelasemestre_id:escuelasemestre_id,
                        estado_valor:estado_valor
                    },
                    success:function() 
                    {                       
                    }
                    
                });

        };

        function IrVistaCursoespecifico(e)
        {
            console.log(e.target.nodeName);
            $('#cursoID').attr('value', e.target.id);
            console.log(e.target.id);
            $( "form" ).submit();
        };

        function agregarCursoVista()
        {
            $('.editar').hide();
            $('.eliminar').hide();
            $('#agregarCurso').hide();

            editar_plan = 0;
            editar_curso = 0;

            
            $('#cursosemestre').append('<tr><td>'+numerador+'</td><td><select name="nuevo_plan" class="form-control" id="nuevoPlan"></select></td><td>codigo</td><td><select name="nuevo_curso" class="form-control" id="nuevoCurso"></select></td><td>semestre</td>'+
            '<td>'+
                '<div class="row">'+
                    '<div class="col-5"><input class="btn btn-info btn-sm" type="button" id="guardarCurso" value="Guardar"></div>'+
                    '<div class="col-5"><input class="btn btn-danger btn-sm" type="button" id="noGuardarCurso" value="Cancelar"></div>'+
                '</div>'+
            '</td></tr>');


            cargaPlanes();
            //cargaCursos();

            numerador +=1;
                    
        };

        function guardarCursoVista()
        {
            var curso_id = $('#nuevoCurso').val();
            var escuelasemestre_id = $('#escuelasemestre').val();

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.insertarcursosemestre') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        curso_id:curso_id,
                        escuelasemestre_id:escuelasemestre_id
                    },
                    success:function() 
                    {
                        cargasemestrecurso();                        
                    }
                    
                });

            $('#agregarCurso').show();
        };

        function cancelarGuardarCursoVista(e)
        {
            $(e.target ).closest( "tr" ).remove();
            $('#agregarCurso').show();
            $('.editar').show();
            $('.eliminar').show();
            numerador -=1;
        };

        function cargaPlanes() 
        {
            var escuela_id = $('#escuela').val();
            console.log("carga planes: escuela_id "+escuela_id);
            if(escuela_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.planes') }}",
                    data: 
                    {
                        "_token": "{{ csrf_token() }}",
                        escuela_id: escuela_id
                    },
                    success:function (data) 
                    {
                        if(editar_plan==0)
                        {
                            $.each(data.planes[0].plans,function(index,plan)
                            {   
                                $('#nuevoPlan').append('<option value="'+plan.id+'">'+plan.anio+'</option>');
                            });
                        }
                        else
                        {
                            $.each(data.planes[0].plans,function(index,plan)
                            {   
                                if(editar_plan != plan.id)
                                {
                                    $('#nuevoPlan').append('<option value="'+plan.id+'">'+plan.anio+'</option>');
                                }
                                else
                                {
                                    $('#nuevoPlan').append('<option value="'+plan.id+'" selected>'+plan.anio+'</option>');;
                                }
                            });   
                        }
                        cargaCursos();
                    }
                });
            }
        };

        function cargaCursos()
        { 
            console.log("entro a cargar curso");
            var plan_id = $('#nuevoPlan').val();
            console.log(plan_id);
            if(plan_id)
            {
                console.log("paso a if");
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cursos') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        plan_id: plan_id
                    },     
                    success:function (data)
                    {
                        $('#nuevoCurso').empty();
                        datos_nuevo_curso = data.cursos[0].cursos;
                        if(editar_curso == 0)
                        {
                            $.each(data.cursos[0].cursos,function(index,curso)
                            {   
                                $('#nuevoCurso').append('<option value="'+curso.id+'">'+curso.curso+'</option>');
                            });
                            var myTr = $('#nuevoCurso').closest( "tr" );
                            myTr.find('td:eq(2)').empty();
                            myTr.find('td:eq(2)').append(datos_nuevo_curso[0].codigo);

                            myTr.find('td:eq(4)').empty();
                            myTr.find('td:eq(4)').append(datos_nuevo_curso[0].semestre);
                        }
                        else
                        {
                            $.each(data.cursos[0].cursos,function(index,curso)
                            {   
                                if(editar_curso == curso.id)
                                {
                                    $('#nuevoCurso').append('<option value="'+curso.id+'" selected>'+curso.curso+'</option>');
                                    var myTr = $('#nuevoCurso').closest( "tr" );
                                }
                                else
                                {
                                    $('#nuevoCurso').append('<option value="'+curso.id+'">'+curso.curso+'</option>');
                                }
                            });
                            editar_plan = 0;
                            editar_curso = 0;
                        }

                    }
                });
            }
        }

        function cambiarDatosTabla(e)
        {
            var myTr = $(e.target ).closest( "tr" );
            var myCursoId = $('#nuevoCurso').val();

            $.each(datos_nuevo_curso,function(index,curso)
            {   
                if(curso.id == myCursoId)
                {
                    myTr.find('td:eq(2)').empty();
                    myTr.find('td:eq(2)').append(curso.codigo);

                    myTr.find('td:eq(4)').empty();
                    myTr.find('td:eq(4)').append(curso.semestre);
                }
            });
        }

        function editarCursoVista(e)
        {
            $('.editar').hide();
            $('.eliminar').hide();
            $('#agregarCurso').hide();

            datosPrevios = $(e.target ).closest( "tr" ).html();

            editarId = e.target.id;
            var myTr = $(e.target ).closest( "tr" );
            
            editar_plan = myTr.find('td:eq(1)').attr('id');
            editar_curso = myTr.find('td:eq(3)').attr('id');

            console.log(editar_plan);
            console.log(editar_curso);

            $(e.target ).closest( "td" ).append('<div class="row">'+
                    '<div class="col-4"><input type="button" class="btn btn-info btn-sm" id="guardarCursoEditado" value="Guardar"></div>'+
                    '<div class="col-4"><input type="button"  id="noGuardarCursoEditado" class="noEditar btn btn-danger btn-sm" value="Cancelar"></div>'+
                '</div>');s

            myTr.find('td:eq(1)').empty();
            myTr.find('td:eq(1)').append('<select id="nuevoPlan" class="form-control"></select>');

            myTr.find('td:eq(3)').empty();
            myTr.find('td:eq(3)').append('<select id="nuevoCurso" class="form-control"></select>');

            cargaPlanes();
        };
        
        function guardarEditarCursoVista(e)
        {
            var id = editarId;
            var curso_id = $('#nuevoCurso').val();

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.editarcursosemestre') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        id:id,
                        curso_id:curso_id
                    },
                    success:function() 
                    {
                        cargasemestrecurso();
                    }
                    
                });

            $('#agregarDocente').show();
        };

        function cancelarEditarCursoVista(e)
        {
            console.log("entro cancelar edicion");
            var myTr = $(e.target ).closest( "tr" );
            myTr.empty();
            myTr.append(datosPrevios);
            $('.editar').show();
            $('.eliminar').show();
            $('#agregarCurso').show();
        };

        function eliminarCursoVista(e)
        {
            var myId = e.target.id;
            $.ajax(
            {
                type:'POST',
                url:"{{ route('ajax.eliminarcursosemestre') }}",
                data:
                {
                    "_token": "{{ csrf_token() }}",
                    id:myId
                },
                success:function(resp) 
                {

                    $('.alert').remove();
                    if(resp != 0){
                        $('.card').prepend('<div class="alert alert-danger col-md-4">No se puede eliminar esquema porque tiene relacion con '+ resp +' registros</div>');
                        $('.alert').fadeOut(7000);
                    }else{
                        $('.card').prepend('<div class="alert alert-success col-md-4">Se elimino exitosamente</div>');
                        $('.alert').fadeOut(7000);
                    }
                    cargasemestrecurso();                     
                }    
            });
        };


        $('#facultad').on('change', cargaescuelas);
        $('#escuela').on('change', cargasemestres);
        $('#escuelasemestre').on('change',cargasemestrecurso);
        $('body').on('click','p',IrVistaCursoespecifico);
        //$(document).on("click","input[type='text' disabled='on']",IrVistaCursoespecifico);

        $('#agregarCurso').on('click', agregarCursoVista);
        $('body').on('click','#guardarCurso',guardarCursoVista);
        $('body').on('click','#noGuardarCurso',cancelarGuardarCursoVista);
        $('body').on('change','#nuevoPlan',cargaCursos);
        $('body').on('change','#nuevoCurso',cambiarDatosTabla);

        $('body').on('click','#guardarCursoEditado',guardarEditarCursoVista);
        $('body').on('click','.editar',editarCursoVista);
        $('body').on('click','.noEditar',cancelarEditarCursoVista);

        $('body').on('click','.eliminar',eliminarCursoVista);
        $('body').on('change','#estadosemestre',editarEstadoSemestre);

    });
</script>

@endsection
