@extends('layouts.adminlayout')
@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="row justify-content-around">
                            <div class="col">Admin</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/files_Admin/administrador.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>

                    </div>

                    <div class="card-body">

                        <form method="GET" accept-charset="UTF-8" enctype="multipart/form-data">
                        
                        <div class="form-group {{ $errors->has('facultad') ? 'has-error' : '' }}">
                            <label for="facultad" class="control-label">{{ 'Facultad' }}</label>
                            <select name="facultad" class="form-control" id="facultad">
                                @foreach ($facultades as $item)
                                    <option value="{{ $item->id }}">{{ $item->facultad }}</option>
                                @endforeach

                            </select>
                            <!--{!! $errors->first('facultad', '<p class="help-block">:message</p>') !!}-->
                        </div>


                        <div class="form-group {{ $errors->has('escuela') ? 'has-error' : '' }}">
                            <label for="escuela" class="control-label">{{ 'Escuela' }}</label>
                            <select name="escuela" class="form-control" id="escuela">
                                @foreach ($escuelas as $item)
                                    <option value="{{ $item->id }}">{{ $item->escuela }}</option>
                                @endforeach

                            </select>
                            <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                        </div>

                        <br>

                        <div class="form-group">
                            <input class="btn boton-color" type="submit" value="Plan de Estudios" id="plan" formaction="{{ url('/admin/planEstudio') }}">

                            <input class="btn boton-color" type="submit" value="Semestres" id="semestre" formaction="{{ url('/admin/semestreCurso') }}">
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function() {

            $('#facultad').on('change', function(e) {

                var facultad_id = e.target.value;
                $.ajax({
                    type: 'POST',
                    url: "{{ route('ajax.escuelas') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        facultad_id: facultad_id
                    },

                    success: function(data) {
                        $('#escuela').empty();
                        console.log('prueba0');
                        console.log(data.escuelas);
                        console.log('prueba1');
                        console.log(data.escuelas[0]);
                        $.each(data.escuelas[0].escuelas, function(index, escuela) {

                            $('#escuela').append('<option value="' + escuela.id + '">' +
                                escuela.escuela + '</option>');
                        })
                    }
                })
            });

        });
    </script>


@endsection
