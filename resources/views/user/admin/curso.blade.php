@extends('layouts.adminlayout')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">{{ __('Cursos') }}</div>
                    <div class="card-body">
                        <form>
                        <div class="form-group {{ $errors->has('facultad') ? 'has-error' : '' }}">
                            <label for="facultad" class="control-label">{{ 'Facultad' }}</label>
                            <select name="facultad" class="form-control" id="facultad">
                                @foreach ($facultades as $item)
                                    <option value="{{ $item->id }}"
                                        {{ isset($item->id) ? ($item->id == $selected_facultad ? 'selected' : '') : '' }}>
                                        {{ $item->facultad }}</option>
                                @endforeach
                            </select>
                            <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                        </div>
                        <br>
                        <div class="form-group {{ $errors->has('escuela') ? 'has-error' : '' }}">
                            <label for="escuela" class="control-label">{{ 'Escuela' }}</label>
                            <select name="escuela" class="form-control" id="escuela">
                                @foreach ($escuelas as $item)
                                    <option value="{{ $item->id }}"
                                        {{ isset($item->id) ? ($item->id == $selected_escuela ? 'selected' : '') : '' }}>
                                        {{ $item->escuela }}</option>
                                @endforeach
                            </select>
                            <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                        </div>
                        <br>
                        <div class="form-group {{ $errors->has('plan') ? 'has-error' : '' }}">
                            <label for="plan" class="control-label">{{ 'Plan de Estudio' }}</label>
                            <select name="plan" class="form-control" id="plan">
                                @foreach ($planes as $item)
                                    <option value="{{ $item->id }}"
                                        {{ isset($item->id) ? ($item->id == $selected_plan ? 'selected' : '') : '' }}>
                                        {{ $item->anio }}</option>
                                @endforeach
                            </select>
                            <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                        </div>
                        <br>
                        <div class="form-group">
                            <input class="btn boton-color" type="button" value="Agregar" id="agregarCurso">
                        </div>
                        
                        <div class="content-tabla">
                        <table class="display table-re" style="width:100%" name="curso" id="curso">
                            <thead class="thead-light">
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">CursoTipo</th>
                                <th scope="col">Codigo</th>
                                <th scope="col">Curso</th>
                                <th scope="col">Descripcion</th>
                                <th scope="col">Semestre</th>
                                <th scope="col">HorasTeoria</th>
                                <th scope="col">HorasPractica</th>
                                <th scope="col">Creditos</th>
                                <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>

                                <!-- <tr>
                                    <th scope="row">1</th>
                                    <td>1</td>
                                    <td>2017</td>
                                    <td>Regular</td>
                                    <td>1701102</td>
                                    <td>RAZONAMIENTO MATEMATICO</td>  
                                    <td>1</td>
                                    <td>2</td>
                                    <td>2</td>
                                    <td>3</td>                              
                                    </tr> -->

                                @foreach ($cursos as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td id="{{ $item->cursotipo->id }}">{{ $item->cursotipo->cursotipo }}</td>
                                        <td>{{ $item->codigo }}</td>
                                        <td>{{ $item->curso }}</td>
                                        <td>{{ $item->descripcion }}</td>
                                        <td>{{ $item->semestre }}</td>
                                        <td>{{ $item->horasteoria }}</td>
                                        <td>{{ $item->horaspractica }}</td>
                                        <td>{{ $item->creditos }}</td>
                                        <td><button type="button" id="{{ $item->id }}" class="btn btn-sm editar" >Editar</button>
                                            <button type="button" id="{{ $item->id }}" class="btn btn-sm eliminar" >Eliminar</button></td>
                                        <!-- <td><input type="button" id="{{ $item->id }}" class="editar" value="Editar"><input type="button" id="{{ $item->id }}" class="eliminar" value="Eliminar"></td> -->
                                    </tr>
                                @endforeach
                            </tbody>
                         </table>
                        </div>
                           
                        <div class="form-group">
                                    <input class="btn boton-color2" type="submit" value="Atrás"
                                    formaction="{{ url('/admin/planEstudio') }}">
                            <!-- <a href="/admin/planEstudio" class="btn boton-color2">Atrás</a> -->
                        </div> 
                    </form>                                                                                           
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var datosPrevios;
    var editarId;

    var numerador = {{ sizeof($cursos) }} + 1;

    $(document).ready(function ()
    {
        var editar_cursoTipo = 0;

        function cargaescuelas() 
        {
            var facultad_id = $('#facultad').val();
            if(facultad_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.escuelas') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        facultad_id: facultad_id
                    },  
                    success:function (data) 
                    {
                        $('#escuela').empty();
                        $.each(data.escuelas[0].escuelas,function(index,escuela)
                        {
                                    
                            $('#escuela').append('<option value="'+escuela.id+'">'+escuela.escuela+'</option>');
                        });
                        cargaplanes();
                    }
                });
            }
            else
            {
                $('#escuela').empty();
                cargaplanes();
            }

        };

        function cargaplanes() 
        {
            var escuela_id = $('#escuela').val();
            if(escuela_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.planes') }}",
                    data: 
                    {
                        "_token": "{{ csrf_token() }}",
                        escuela_id: escuela_id
                    },
                    success:function (data) 
                    {
                        $('#plan').empty();
                        $.each(data.planes[0].plans,function(index,plan)
                        {    
                            $('#plan').append('<option value="'+plan.id+'">'+plan.anio+'</option>');
                        });
                        cargacursos();
                    }
                });
            }
            else
            {
                $('#plan').empty();
                cargacursos();
            }
                    
        };

        function cargacursos()
        { 
            numerador = 1;
            var plan_id = $('#plan').val();
            if(plan_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cursos') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        plan_id: plan_id
                    },

                    
                    success:function (data)
                    {

                        $('#curso').empty();
                        $('#curso').append('<thead class="thead-light">'+
                                                '<tr>'+
                                                '<th scope="col">#</th>'+
                                                '<th scope="col">CursoTipo</th>'+
                                                '<th scope="col">Codigo</th>'+
                                                '<th scope="col">Curso</th>'+
                                                '<th scope="col">Descripcion</th>'+
                                                '<th scope="col">Semestre</th>'+
                                                '<th scope="col">HorasTeoria</th>'+
                                                '<th scope="col">HorasPractica</th>'+
                                                '<th scope="col">Creditos</th>'+
                                                '<th scope="col">Acciones</th>'+
                                                '</tr>'+
                                            '</thead>'+
                                            '<tbody>');

                        $.each(data.cursos[0].cursos,function(index,curso)
                        {    
                            $('#curso').append('<tr><td>'+numerador+'</td><td id="'+curso.cursotipo.id+'">'+curso.cursotipo.cursotipo+'</td><td>'+curso.codigo+'</td><td>'+curso.curso+'</td><td>'+curso.descripcion+'</td><td>'+curso.semestre+'</td><td>'+curso.horasteoria+'</td><td>'+curso.horaspractica+'</td><td>'+curso.creditos+'</td><td><button type="button" id="'+ curso.id+'" class="btn btn-sm editar" ><i  aria-hidden="true"></i></button><button type="button" id="'+curso.id+'" class="btn btn-sm eliminar" ><i aria-hidden="true"></i></button></td></tr>');
                                                                                                                                                                                                                                                                                                                                                 // <input type="button" id="'+curso.id+'" class="editar" value="Editar"><input type="button" id="'+curso.id+'" class="eliminar" value="Eliminar">
                            numerador+=1;
                        });
                        $('#curso').append('</tbody>');
                    }
                });
            }
            else
            {
                $('#curso').empty();
                $('#curso').append('<thead class="thead-light"><tr><th scope="col">#</th><th scope="col">IDCurso</th><th scope="col">IDPlan</th><th scope="col">IDCursoTipo</th><th scope="col">Codigo</th><th scope="col">Curso</th><th scope="col">Semestre/th><th scope="col">HorasTeoria</th><th scope="col">HorasPractica</th><th scope="col">Creditos</th></tr></thead><tbody>');
            }

            $('#agregarCurso').show();
        }

        //comienza

        function cargaCursoTipo()
        {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cursotipo') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                    },

                    success:function (data)
                    {
                        console.log("mi curso tipo es "+editar_cursoTipo);

                        //$('#nuevpCursoTipo').empty();
                        if(editar_cursoTipo == 0)
                        {
                            console.log("entro a if");
                            $.each(data.cursoTipo,function(index,cursotipo)
                            {    
                                $('#nuevoCursoTipo').append('<option value="'+cursotipo.id+'">'+cursotipo.cursotipo+'</option>');
                            });
                        }
                        else
                        {
                            console.log("no entro a if");
                            $.each(data.cursoTipo,function(index,cursotipo)
                            {    
                                if(editar_cursoTipo != cursotipo.id)
                                {
                                    $('#nuevoCursoTipo').append('<option value="'+cursotipo.id+'">'+cursotipo.cursotipo+'</option>');
                                }
                                else
                                {
                                    $('#nuevoCursoTipo').append('<option value="'+cursotipo.id+'" selected>'+cursotipo.cursotipo+'</option>');
                                }
                            });   
                        }
                    }
                });
        }

        function agregarCursoVista()
        {
            $('.editar').hide();
            $('.eliminar').hide();
            $('#agregarCurso').hide();

            editar_cursoTipo = 0;
            //editar_plan = 0;
            //editar_curso = 0;

            $('#curso').append('<tr>'+
                                    '<td>'+numerador+
                                    '</td>'+
                                    '<td><select id="nuevoCursoTipo" class="form-control llenar"></select></td>'+
                                    '<td><input type="text" class="llenar" id="nuevoCodigo"></td>'+
                                    '<td><input type="text" class="llenar" id="nuevoCurso"></td>'+
                                    '<td><input type="text" class="llenar" id="nuevaDescripcion"></td>'+
                                    '<td><input type="text" class="llenar" id="nuevoSemestre"></td>'+
                                    '<td><input type="text" class="llenar" id="nuevoHTeoria"></td>'+
                                    '<td><input type="text" class="llenar" id="nuevoHPractica"></td>'+
                                    '<td><input type="text" class="llenar" id="nuevoCreditos"></td>'+
                                    '<td><input type="button" id="guardarCurso" value="Guardar"><input type="button" id="noGuardarCurso" value="Cancelar"></td>'+
                                    '</tr>');           
            numerador +=1;

            cargaCursoTipo();
                    
        };

        function guardarCursoVista()
        {
            var plan_id = $('#plan').val();
            var cursotipo_id = $('#nuevoCursoTipo').val();
            var codigo = $('#nuevoCodigo').val();
            var curso = $('#nuevoCurso').val();
            var descripcion = $('#nuevaDescripcion').val();
            var semestre = $('#nuevoSemestre').val();
            var horasteoria = $('#nuevoHTeoria').val();
            var horaspractica = $('#nuevoHPractica').val();
            var creditos = $('#nuevoCreditos').val();

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.insertarcurso') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        plan_id:plan_id,
                        cursotipo_id:cursotipo_id,
                        codigo:codigo,
                        curso:curso,
                        descripcion:descripcion,
                        semestre:semestre,
                        horasteoria:horasteoria,
                        horaspractica:horaspractica,
                        creditos:creditos
                    },
                    success:function() 
                    {
                        cargacursos();
                    }
                    
                });

            $('#agregarCurso').show();

        };

        function cancelarGuardarCursoVista(e)
        {
            $(e.target ).closest( "tr" ).remove();
            $('#agregarCurso').show();
            $('.editar').show();
            $('.eliminar').show();
            numerador -=1;
        };

        function editarCursoVista(e)
        {
            $('.editar').hide();
            $('.eliminar').hide();
            $('#agregarCurso').hide();

            datosPrevios = $(e.target ).closest( "tr" ).html();
            editarId = e.target.id;

            var myTr = $(e.target ).closest( "tr" );

            console.log("editar1 "+editar_cursoTipo);
            
            editar_cursoTipo = myTr.find('td:eq(1)').attr('id');
            var codigo = myTr.find('td:eq(2)').text();
            var curso = myTr.find('td:eq(3)').text();
            var descripcion = myTr.find('td:eq(4)').text();
            var semestre = myTr.find('td:eq(5)').text();
            var horasteoria = myTr.find('td:eq(6)').text();
            var horaspractica = myTr.find('td:eq(7)').text();
            var creditos = myTr.find('td:eq(8)').text();

            console.log("editar2 "+editar_cursoTipo);

            $(e.target ).closest( "td" ).append('<input type="button" id="guardarCursoEditado" value="Guardar"><input type="button" id="noGuardarCursoEditado" class="noEditar" value="Cancelar">');

            myTr.find('td:eq(1)').empty();
            //myTr.find('td:eq(1)').append('<input type="text" id="cursoTipoIdEditado" value="'+cursotipo_id+'">');
            myTr.find('td:eq(1)').append('<select id="nuevoCursoTipo" class="form-control"></select>');
            cargaCursoTipo();

            myTr.find('td:eq(2)').empty();
            myTr.find('td:eq(2)').append('<input type="text" id="codigoEditado" class="llenar" value="'+codigo+'">');

            myTr.find('td:eq(3)').empty();
            myTr.find('td:eq(3)').append('<input type="text" id="cursoEditado" class="llenar" value="'+curso+'">');

            myTr.find('td:eq(4)').empty();
            myTr.find('td:eq(4)').append('<input type="text" id="descripcionEditado" class="llenar" value="'+descripcion+'">');

            myTr.find('td:eq(5)').empty();
            myTr.find('td:eq(5)').append('<input type="text" id="semestreEditado" class="llenar" value="'+semestre+'">');
            
            myTr.find('td:eq(6)').empty();
            myTr.find('td:eq(6)').append('<input type="text" id="horasTEditado" class="llenar" value="'+horasteoria+'">');

            myTr.find('td:eq(7)').empty();
            myTr.find('td:eq(7)').append('<input type="text" id="horasPEditado" class="llenar" value="'+horaspractica+'">');

            myTr.find('td:eq(8)').empty();
            myTr.find('td:eq(8)').append('<input type="text" id="creditosEditado" class="llenar" value="'+creditos+'">');
        };

        function guardarEditarCursoVista(e)
        {
            var id = editarId;
            var cursotipo_id = $('#nuevoCursoTipo').val();
            var codigo = $('#codigoEditado').val();
            var curso = $('#cursoEditado').val();
            var descripcion = $('#descripcionEditado').val();
            var semestre = $('#semestreEditado').val();
            var horasteoria = $('#horasTEditado').val();
            var horaspractica = $('#horasPEditado').val();
            var creditos = $('#creditosEditado').val();

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.editarcurso') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        id:id,
                        cursotipo_id:cursotipo_id,
                        codigo:codigo,
                        curso:curso,
                        descripcion:descripcion,
                        semestre:semestre,
                        horasteoria:horasteoria,
                        horaspractica:horaspractica,
                        creditos:creditos
                    },
                    success:function() 
                    {
                        cargacursos();
                        //poner notificacion de editado exitoso
                    }
                    
                });

            $('#agregarCurso').show();
        };

        function cancelarEditarCursoVista(e)
        {
            var myTr = $(e.target ).closest( "tr" );
            myTr.empty();
            myTr.append(datosPrevios);
            $('.editar').show();
            $('.eliminar').show();
            $('#agregarCurso').show();
        };

        function eliminarCursoVista(e)
        {
            var myId = e.target.id;
            $.ajax(
            {
                type:'POST',
                url:"{{ route('ajax.eliminarcurso') }}",
                data:
                {
                    "_token": "{{ csrf_token() }}",
                    id:myId
                },
                success:function(resp) 
                {

                    $('.alert').remove();
                    if(resp != 0){
                        $('.card').prepend('<div class="alert alert-danger col-md-4">No se puede eliminar curso porque tiene relacion con '+ resp +' registros</div>');
                        $('.alert').fadeOut(7000);
                    }else{
                        $('.card').prepend('<div class="alert alert-success col-md-4">Se elimino exitosamente</div>');
                        $('.alert').fadeOut(7000);
                    }
                    cargacursos();
                    //Notificacion de eliminacion                     
                }    
            });
        };

        //termina


        $('#facultad').on('change', cargaescuelas);
        $('#escuela').on('change', cargaplanes);
        $('#plan').on('change', cargacursos);

        $('#agregarCurso').on('click', agregarCursoVista);
        $('body').on('click','#guardarCurso',guardarCursoVista);
        $('body').on('click','#noGuardarCurso',cancelarGuardarCursoVista);

        $('body').on('click','#guardarCursoEditado',guardarEditarCursoVista);
        $('body').on('click','.editar',editarCursoVista);
        $('body').on('click','.noEditar',cancelarEditarCursoVista);

        $('body').on('click','.eliminar',eliminarCursoVista);

    });

    


</script>

@endsection
