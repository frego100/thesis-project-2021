@extends('layouts.adminlayout')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">Plan de Estudios</div>
                    <div class="card-body">
                        <form method="GET" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            <div class="form-group {{ $errors->has('facultad') ? 'has-error' : '' }}">
                                <label for="facultad" class="control-label">{{ 'Facultad:' }}</label>
                                <select name="facultad" class="form-control" id="facultad">
                                    @foreach ($facultades as $item)
                                        <option value="{{ $item->id }}"
                                            {{ isset($item->id) ? ($item->id == $selected_facultad ? 'selected' : '') : '' }}>
                                            {{ $item->facultad }}</option>
                                    @endforeach
                                </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <br>
                            <div class="form-group {{ $errors->has('escuela') ? 'has-error' : '' }}">
                                <label for="escuela" class="control-label">{{ 'Escuela' }}</label>
                                <select name="escuela" class="form-control" id="escuela">
                                    @foreach ($escuelas as $item)
                                        <option value="{{ $item->id }} "
                                            {{ isset($item->id) ? ($item->id == $selected_escuela ? 'selected' : '') : '' }}>
                                            {{ $item->escuela }}</option>
                                    @endforeach
                                </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <br>
                            <div class="form-group {{ $errors->has('plan') ? 'has-error' : '' }}">
                                <label for="plan" class="control-label">{{ 'Plan de Estudio' }}</label>
                                <select name="plan" class="form-control" id="plan">
                                    @foreach ($planes as $item)
                                        <option value="{{ $item->id }}">{{ $item->anio }}</option>
                                    @endforeach


                                </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <br>
                            <div>
                                <div class="btn-group col-md-3">
                                    <input class="btn btn-sm boton-color" type="submit" value="Curso"
                                        formaction="{{ url('/admin/curso') }}">
                                </div>

                                <div class="btn-group col-md-3">
                                    <input class="btn btn-sm boton-color" type="submit" value="Competencia"
                                        formaction="{{ url('/admin/competencia') }}">
                                </div>

                                <div class="btn-group col-md-3">
                                    <input class="btn btn-sm boton-color" type="submit" value="Resultado del estudiante"
                                        formaction="{{ url('/admin/resultadoEstudiante') }}">
                                </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <input class="btn boton-color2" type="submit" value="Atrás"
                                    formaction="{{ url('/admin') }}">
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function() {
            function cargaescuelas() {
                var facultad_id = $('#facultad').val();
                if (facultad_id) {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('ajax.escuelas') }}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            facultad_id: facultad_id
                        },
                        success: function(data) {
                            $('#escuela').empty();
                            $.each(data.escuelas[0].escuelas, function(index, escuela) {

                                $('#escuela').append('<option value="' + escuela.id + '">' +
                                    escuela.escuela + '</option>');
                            });
                            cargaplanes();
                        }
                    });
                } else {
                    $('#escuela').empty();
                    cargaplanes();
                }
            };

            function cargaplanes() {
                var escuela_id = $('#escuela').val();
                if (escuela_id) {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('ajax.planes') }}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            escuela_id: escuela_id
                        },
                        success: function(data) {
                            $('#plan').empty();
                            $.each(data.planes[0].plans, function(index, plan) {
                                $('#plan').append('<option value="' + plan.id + '">' + plan
                                    .anio + '</option>');
                            });
                            //cargacompetencias();
                        }
                    });
                } else {
                    $('#plan').empty();
                    //cargacompetencias();
                }
            }

            $('#facultad').on('change', cargaescuelas);
            $('#escuela').on('change', cargaplanes);
        });
    </script>

@endsection
