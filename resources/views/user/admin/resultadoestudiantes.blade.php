@extends('layouts.adminlayout')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{ __('Admin') }}</div>
                    <div class="card-body">
                    <form method="GET" action="{{ url('/admin/resultadoECompetencia') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        <div class="form-group {{ $errors->has('facultad') ? 'has-error' : '' }}">
                            <label for="facultad" class="control-label">{{ 'Facultad' }}</label>
                            <select name="facultad" class="form-control" id="facultad">
                                @foreach ($facultades as $item)
                                    <option value="{{ $item->id }}"
                                        {{ isset($item->id) ? ($item->id == $selected_facultad ? 'selected' : '') : '' }}>
                                        {{ $item->facultad }}</option>
                                @endforeach
                            </select>
                            <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                        </div>
                        <br>
                        <div class="form-group {{ $errors->has('escuela') ? 'has-error' : '' }}">
                            <label for="escuela" class="control-label">{{ 'Escuela' }}</label>
                            <select name="escuela" class="form-control" id="escuela">
                                @foreach ($escuelas as $item)
                                    <option value="{{ $item->id }} "
                                        {{ isset($item->id) ? ($item->id == $selected_escuela ? 'selected' : '') : '' }}>
                                        {{ $item->escuela }}</option>
                                @endforeach

                            </select>
                            <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                        </div>
                        <br>
                        <div class="form-group {{ $errors->has('plan') ? 'has-error' : '' }}">
                            <label for="plan" class="control-label">{{ 'Plan' }}</label>
                            <select name="plan" class="form-control" id="plan">
                                @foreach ($planes as $item)
                                    <option value="{{ $item->id }} "
                                        {{ isset($item->id) ? ($item->id == $selected_plan ? 'selected' : '') : '' }}>
                                        {{ $item->anio }}</option>
                                @endforeach

                            </select>
                            <!--{!! $errors->first('plan', '<p class="help-block">:message</p>') !!}-->
                        </div>
                        <br>
                        <div class="form-group">
                            <input class="btn boton-color" type="button" value="Agregar" id="agregarResultado">
                        </div>
                        <div class="content-tabla">
                        <table class="display table-re" name="resultadosestudiante" id="resultadosestudiante">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Codigo</th>
                                    <th scope="col">Resultado Estudiante</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($resultadosestudiantes as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->codigore }}</td>
                                        <td><p id="{{ $item->id }}">{{ $item->re }}</p></td>
                                        <td><input type="button" id="{{ $item->id }}" class="btn boton-criterio criterio" value="Criterio"><input type="button" id="{{ $item->id }}" class="btn editar" value="Editar"><input type="button" id="{{ $item->id }}" class="btn eliminar" value="Eliminar"></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                        <br>
                        <div class="form-group">
                            <input class="btn boton-color2" type="submit" value="Atrás"
                                    formaction="{{ url('/admin/planEstudio') }}">
                        </div>
                        <br>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var numerador = {{ sizeof($resultadosestudiantes) }} + 1;

    $(document).ready(function ()
    {
        function cargaescuelas() 
        {

            var facultad_id = $('#facultad').val();
            if(facultad_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.escuelas') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        facultad_id: facultad_id
                    },  
                    success:function (data) 
                    {
                        $('#escuela').empty();
                        $.each(data.escuelas[0].escuelas,function(index,escuela)
                        {
                                    
                            $('#escuela').append('<option value="'+escuela.id+'">'+escuela.escuela+'</option>');
                        });
                        cargaplanes();
                    }
                });
            }
            else
            {
                $('#escuela').empty();
                cargaplanes();
            }

        };

        function cargaplanes() 
        {
            var escuela_id = $('#escuela').val();
            if(escuela_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.planes') }}",
                    data: 
                    {
                        "_token": "{{ csrf_token() }}",
                        escuela_id: escuela_id
                    },
                    success:function (data) 
                    {
                        $('#plan').empty();
                        $.each(data.planes[0].plans,function(index,plan)
                        {    
                            $('#plan').append('<option value="'+plan.id+'">'+plan.anio+'</option>');
                        });
                        cargaresultados();
                    }
                });
            }
            else
            {
                $('#plan').empty();
                cargaresultados();
            }
                    
        };

        function cargaresultados()
        { 
            numerador = 1;
            var plan_id = $('#plan').val();
            if(plan_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.resultadosestudiantes') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        plan_id: plan_id
                    },
                    success:function (data)
                    {
                        $('#resultadosestudiante').empty();
                        $('#resultadosestudiante').append('<thead class="thead-light"><tr><th scope="col">#</th><th scope="col">Codigo</th><th scope="col">Resultado Estudiante</th><th scope="col">Acciones</th></tr></thead><tbody>');

                        $.each(data.resultadosestudiantes[0].resultadosestudiantes,function(index,resultadosestudiante)
                        {    
                            $('#resultadosestudiante').append('<tr><td>'+numerador+'</td><td>'+resultadosestudiante.codigore+'</td><td><p id="'+resultadosestudiante.id+'">'+resultadosestudiante.re+'</p></td><td><input type="button" id="'+resultadosestudiante.id+'" class="criterio" value="Criterio"><input type="button" id="'+resultadosestudiante.id+'" class="editar" value="Editar"><input type="button" id="'+resultadosestudiante.id+'" class="eliminar" value="Eliminar"></td></td></tr>');

                            numerador+=1;
                        });
                        $('#resultadosestudiante').append('</tbody>');
                    }
                });
            }
            else
            {
                $('#resultadosestudiante').empty();
                $('#resultadosestudiante').append('<thead class="thead-light">'+
                    '<tr>'+
                        '<th scope="col">#</th>'+
                        '<th scope="col">Codigo</th>'+
                        '<th scope="col">Resultado Estudiante</th>'+
                    '</tr></thead><tbody>');
            }
        }

        function IrVistaResultadoespecifico(e)
        {
            console.log(e.target.nodeName);
            var facultad_id = $('#facultad').val();
            var escuela_id = $('#escuela').val();
            var plan_id = $('#plan').val();
            var resultadoID = e.target.id; 
            $('#reID').attr('value', e.target.id);
            console.log(e.target.id);
            $('form').attr('action', '{{ url('/admin/resultadoECompetencia') }}');
            $( "form" ).submit();
        };

        function IrVistaCriterio(e)
        {
            console.log(e.target.nodeName);
            var facultad = $('#facultad').val();
            var escuela = $('#escuela').val();
            var plan = $('#plan').val();
            var resultadoID = e.target.id; 
            $('#reID').attr('value', e.target.id);
            console.log(e.target.id);
            $('form').attr('action', '{{ url('/admin/resultadoEstudianteEsp') }}');
            $( "form" ).submit();
        };

        function agregarResultadoVista()
        {
            $('.criterio').hide();
            $('.editar').hide();
            $('.eliminar').hide();
            $('#agregarResultado').hide();

            //editar_plan = 0;
            //editar_curso = 0;

            $('#resultadosestudiante').append('<tr>'+
                                                    '<td>'+numerador+'</td>'+
                                                    '<td><input type="text" class="llenar" id="nuevoCodigo"></td>'+
                                                    '<td><input type="text" class="llenar" id="nuevoResultado"></td>'+
                                                    '<td><input type="button" id="guardarResultado" value="Guardar">'+
                                                        '<input type="button" id="noGuardarResultado" value="Cancelar">'+
                                                    '</td>'+
                                                    '</tr>');

            numerador +=1;
                    
        };

        function guardarResultadoVista()
        {
            var plan_id = $('#plan').val();
            var codigore = $('#nuevoCodigo').val();
            var re = $('#nuevoResultado').val();
            console.log(re);

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.insertarresultado') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        plan_id:plan_id,
                        codigore:codigore,
                        re:re
                    },
                    success:function() 
                    {
                        cargaresultados();
                    }
                    
                });

            $('#agregarResultado').show();
        };

        function cancelarGuardarResultadoVista(e)
        {
            $(e.target ).closest( "tr" ).remove();
            $('#agregarResultado').show();
            $('.criterio').show();
            $('.editar').show();
            $('.eliminar').show();
            numerador -=1;
        };

        function editarResultadoVista(e)
        {
            $('.editar').hide();
            $('.eliminar').hide();
            $('.criterio').hide();
            $('#agregarResultado').hide();

            datosPrevios = $(e.target ).closest( "tr" ).html();
            editarId = e.target.id;

            var myTr = $(e.target ).closest( "tr" );
            
            codigore = myTr.find('td:eq(1)').text();
            re = myTr.find('td:eq(2)').text();

            $(e.target ).closest( "td" ).append('<input type="button" id="guardarResultadoEditado" value="Guardar"><input type="button" id="noGuardarResultadoEditado" class="noEditar" value="Cancelar">');

            myTr.find('td:eq(1)').empty();
            myTr.find('td:eq(1)').append('<input type="text" id="codigoEditado" value="'+codigore+'">');

            myTr.find('td:eq(2)').empty();
            myTr.find('td:eq(2)').append('<input type="text" id="ResultadoEditado" value="'+re+'">');
        };

        function guardarEditarResultadoVista(e)
        {
            var id = editarId;
            var codigore = $('#codigoEditado').val();
            var re = $('#ResultadoEditado').val();

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.editarresultado') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        id:id,
                        codigore:codigore,
                        re:re
                    },
                    success:function() 
                    {
                        cargaresultados();
                        //poner notificacion de editado exitoso
                    }
                    
                });

            $('#agregarResultado').show();
        };

        function cancelarEditarResultadoVista(e)
        {
            var myTr = $(e.target ).closest( "tr" );
            myTr.empty();
            myTr.append(datosPrevios);
            $('.criterio').show();
            $('.editar').show();
            $('.eliminar').show();
            $('#agregarResultado').show();
        };

        function eliminarResultadoVista(e)
        {
            var myId = e.target.id;
            $.ajax(
            {
                type:'POST',
                url:"{{ route('ajax.eliminarresultado') }}",
                data:
                {
                    "_token": "{{ csrf_token() }}",
                    id:myId
                },
                success:function(resp) 
                {
                    
                    $('.alert').remove();
                    if(resp != 0){
                        $('.card').prepend('<div class="alert alert-danger col-md-4">No se puede eliminar resultado de estudiante porque tiene relacion con '+ resp +' registros</div>');
                        $('.alert').fadeOut(7000);
                    }else{
                        $('.card').prepend('<div class="alert alert-success col-md-4">Se elimino exitosamente</div>');
                        $('.alert').fadeOut(7000);
                    }
                    cargaresultados();                     
                }    
            });
        };

        $('body').on('click','p',IrVistaResultadoespecifico);
        $('body').on('click','.criterio',IrVistaCriterio);
        $('#facultad').on('change', cargaescuelas);
        $('#escuela').on('change', cargaplanes);
        $('#plan').on('change',cargaresultados);

        $('#agregarResultado').on('click', agregarResultadoVista);
        $('body').on('click','#guardarResultado',guardarResultadoVista);
        $('body').on('click','#noGuardarResultado',cancelarGuardarResultadoVista);

        $('body').on('click','#guardarResultadoEditado',guardarEditarResultadoVista);
        $('body').on('click','.editar',editarResultadoVista);
        $('body').on('click','.noEditar',cancelarEditarResultadoVista);

        $('body').on('click','.eliminar',eliminarResultadoVista);

    });


</script>
@endsection
