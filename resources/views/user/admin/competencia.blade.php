@extends('layouts.adminlayout')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Competencias') }}</div>

                <div class="card-body">
                    <form method="GET" action="{{ url('/admin/competenciaEsp') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            <div class="form-group {{ $errors->has('facultad') ? 'has-error' : ''}}">
                                <label for="facultad" class="control-label">{{ 'Facultad' }}</label>
                                <select name="facultad" class="form-control" id="facultad" >
                                @foreach($facultades as $item)
                                <option value="{{ $item->id }}" {{ isset($item->id) ?  ($item->id == $selected_facultad ? 'selected' : '') : '' }}>{{ $item->facultad }}</option>
                                @endforeach   
                                </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <br>
                           <div class="form-group {{ $errors->has('escuela') ? 'has-error' : ''}}">
                                <label for="escuela" class="control-label">{{ 'Escuela' }}</label>
                                <select name="escuela" class="form-control" id="escuela" >
                                @foreach($escuelas as $item)
                                <option value="{{ $item->id }}" {{ isset($item->id) ?  ($item->id == $selected_escuela ? 'selected' : '') : '' }}>{{ $item->escuela }}</option>
                                @endforeach   
                                </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div> 
                            
                            <br>
                           <div class="form-group {{ $errors->has('plan') ? 'has-error' : ''}}">
                                <label for="plan" class="control-label">{{ 'Plan de Estudio' }}</label>
                                <select name="plan" class="form-control" id="plan" >
                                @foreach($planes as $item)
                                <option value="{{ $item->id }}" {{ isset($item->id) ?  ($item->id == $selected_plan ? 'selected' : '') : '' }}>{{ $item->anio }}</option>
                                @endforeach
                                </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div> 
                                   
                            <br>
                            <div class="form-group">
                                <input class="btn boton-color" type="button" value="Añadir Competencia" id="agregarCompetencia">
                            </div>
                            <br>
                            <table class="table table-bordered" name="competencia" id="competencia">
                                <thead class="thead-light">
                                    <tr>
                                        <th >#</th>
                                        <th >Código</th>
                                        <th >Competencia</th>
                                        <th >Acciones</th>  
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($competencias as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->codigo }}</td>
                                            <td><p id="{{ $item->id }}">{{ $item->competencia }}</p></td>
                                            <td><input type="button" id="{{ $item->id }}" class="editar" value="Editar"><input type="button" id="{{ $item->id }}" class="eliminar" value="Eliminar"></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                
                            <div class="form-group">
                            <a href="/admin/planEstudio" class="btn boton-color2">Atrás</a>
                            </div>
                            <input type="hidden" id="competenciaID" name="competenciaID" value="0">
                       </form>                                                                                         
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var numerador = {{ sizeof($competencias) }} + 1;

    $(document).ready(function ()
    {
         
        


        var datosPrevios;
        var editarId = 0;

        function cargaescuelas() 
        {
            var facultad_id = $('#facultad').val();
            
            if(facultad_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.escuelas') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        facultad_id: facultad_id
                    },  
                    success:function (data) 
                    {
                        $('#escuela').empty();
                        $.each(data.escuelas[0].escuelas,function(index,escuela)
                        {
                                    
                            $('#escuela').append('<option value="'+escuela.id+'">'+escuela.escuela+'</option>');
                        });
                        cargaplanes();
                    }
                });
            }
            else
            {
                $('#escuela').empty();
                cargaplanes();
            }

        };

        function cargaplanes() 
        {
            var escuela_id = $('#escuela').val();
            if(escuela_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.planes') }}",
                    data: 
                    {
                        "_token": "{{ csrf_token() }}",
                        escuela_id: escuela_id
                    },
                    success:function (data) 
                    {
                        $('#plan').empty();
                        $.each(data.planes[0].plans,function(index,plan)
                        {    
                            $('#plan').append('<option value="'+plan.id+'">'+plan.anio+'</option>');
                        });
                        cargacompetencias();
                    }
                });
            }
            else
            {
                $('#plan').empty();
                cargacompetencias();
            }
                    
        };

        function cargacompetencias()
        { 
            numerador = 1;
            var plan_id = $('#plan').val();
            if(plan_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.competencias') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        plan_id: plan_id
                    },
                    success:function (data)
                    {
                        $('#competencia').empty();
                        $('#competencia').append('<thead>'+
                                                    '<tr>'+
                                                        '<th>#</th>'+
                                                        '<th>Codigo</th>'+
                                                        '<th>Competencia</th>'+
                                                        '<th>Acciones</th>'+
                                                    '</tr>'+
                                                    '</thead><tbody>');

                        $.each(data.competencias[0].competencias,function(index,competencia)
                        {    
                            $('#competencia').append('<tr><td>'+numerador+'</td><td>'+competencia.codigo+'</td><td><p id="'+competencia.id+'">'+competencia.competencia+'</p></td><td><input type="button" id="'+competencia.id+'" class="btn editar" value="Editar"><input type="button" id="'+competencia.id+'" class="btn eliminar" value="Eliminar"></td></tr>');
                            numerador += 1;
                        });
                        $('#competencia').append('</tbody>');
                    }
                });
            }
            else
            {
                $('#competencia').empty();
                $('#competencia').append('<thead class="thead-light"><tr><th scope="col">#</th><th scope="col">Codigo</th><th scope="col">Competencia</th></tr></thead><tbody>');
            }
        }


        function IrVistaCompetenciaespecifica(e)
        {
            console.log('click detectado');
            console.log(e.target.nodeName);
            console.log(e.target.name);

            var facultad_id = $('#facultad').val();
            var escuela_id = $('#escuela').val();
            var escuelasemestre_id = $('#escuelasemestre').val();
            var curso_id = e.target.id; 
            $('#competenciaID').attr('value', e.target.id);
            console.log(e.target.id);
            $( "form" ).submit();
        };
        //comienza
        function agregarCompetenciaVista()
        {
            $('.editar').hide();
            $('.eliminar').hide();
            $('#agregarCompetencia').hide();

            //editar_plan = 0;
            //editar_curso = 0;

            $('#competencia').append('<tr><td>'+numerador+'</td><td><input type="text" id="nuevoCodigo"></td><td><input type="text" id="nuevaCompetencia"></td><td><input class="btn boton-color" type="button" id="guardarCompetencia" value="Guardar"><input class="btn btn-danger" type="button" id="noGuardarCompetencia" value="Cancelar"></td></tr>');

            numerador +=1;
                    
        };

        function guardarCompetenciaVista()
        {
            var plan_id = $('#plan').val();
            var codigo = $('#nuevoCodigo').val();
            var competencia = $('#guardarCompetencia').val();

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.insertarcompetencia') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        plan_id:plan_id,
                        codigo:codigo,
                        competencia:competencia
                    },
                    success:function() 
                    {
                        cargacompetencias();
                    }
                    
                });

            $('#agregarCompetencia').show();
        };

        function cancelarGuardarCompetenciaVista(e)
        {
            $(e.target ).closest( "tr" ).remove();
            $('#agregarCompetencia').show();
            $('.editar').show();
            $('.eliminar').show();
            numerador -=1;
        };

        function editarCompetenciaVista(e)
        {
            $('.editar').hide();
            $('.eliminar').hide();
            $('#agregarCompetencia').hide();

            datosPrevios = $(e.target ).closest( "tr" ).html();
            editarId = e.target.id;

            var myTr = $(e.target ).closest( "tr" );
            
            codigo = myTr.find('td:eq(1)').text();
            competencia = myTr.find('td:eq(2)').text();

            $(e.target ).closest( "td" ).append('<input type="button" id="guardarCompetenciaEditada" value="Guardar"><input type="button" id="noGuardarCompetenciaEditada" class="noEditar" value="Cancelar">');

            myTr.find('td:eq(1)').empty();
            myTr.find('td:eq(1)').append('<input type="text" id="codigoEditado" value="'+codigo+'">');

            myTr.find('td:eq(2)').empty();
            myTr.find('td:eq(2)').append('<input type="text" id="competenciaEditada" value="'+competencia+'">');
        };

        function guardarEditarCompetenciaVista(e)
        {
            var id = editarId;
            var codigo = $('#codigoEditado').val();
            var competencia = $('#competenciaEditada').val();

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.editarcompetencia') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        id:id,
                        codigo:codigo,
                        competencia:competencia
                    },
                    success:function() 
                    {
                        cargacompetencias();
                        //poner notificacion de editado exitoso
                    }
                    
                });

            $('#agregarCompetencia').show();
        };

        function cancelarEditarCompetenciaVista(e)
        {
            var myTr = $(e.target ).closest( "tr" );
            myTr.empty();
            myTr.append(datosPrevios);
            $('.editar').show();
            $('.eliminar').show();
            $('#agregarCompetencia').show();
        };

        function eliminarCompetenciaVista(e)
        {
            var myId = e.target.id;
            $.ajax(
            {
                type:'POST',
                url:"{{ route('ajax.eliminarcompetencia') }}",
                data:
                {
                    "_token": "{{ csrf_token() }}",
                    id:myId
                },
                success:function(resp) 
                {

                    $('.alert').remove();
                    if(resp != 0){
                        $('.card').prepend('<div class="alert alert-danger col-md-4">No se puede eliminar competencia porque tiene relacion con '+ resp +' registros</div>');
                        $('.alert').fadeOut(7000);
                    }else{
                        $('.card').prepend('<div class="alert alert-success col-md-4">Se elimino exitosamente</div>');
                        $('.alert').fadeOut(7000);
                    }
                    cargacompetencias();                     
                }    
            });
        };

        //termina

        $('#facultad').on('change', cargaescuelas);
        $('#escuela').on('change', cargaplanes);
        $('#plan').on('change',cargacompetencias);
        $('body').on('click','p',IrVistaCompetenciaespecifica);

        $('#agregarCompetencia').on('click', agregarCompetenciaVista);
        $('body').on('click','#guardarCompetencia',guardarCompetenciaVista);
        $('body').on('click','#noGuardarCompetencia',cancelarGuardarCompetenciaVista);

        $('body').on('click','#guardarCompetenciaEditada',guardarEditarCompetenciaVista);
        $('body').on('click','.editar',editarCompetenciaVista);
        $('body').on('click','.noEditar',cancelarEditarCompetenciaVista);

        $('body').on('click','.eliminar',eliminarCompetenciaVista);

    });


</script>

@endsection