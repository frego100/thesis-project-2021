@extends('layouts.adminlayout')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Resultado Especifico') }}</div>

                <div class="card-body">

                            <div class="form-group {{ $errors->has('facultad') ? 'has-error' : ''}}">
                                <label for="facultad" class="control-label">{{ 'Facultad' }}</label>
                                <select name="facultad" class="form-control" id="facultad" >
                                @foreach($facultades as $item)
                                <option value="{{ $item->id }}"{{ isset($item->id) ?  ($item->id == $selected_facultad ? 'selected' : '') : '' }}>{{ $item->facultad }}</option>
                                @endforeach   

                                 </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <br>

                           <div class="form-group {{ $errors->has('escuela') ? 'has-error' : ''}}">
                                <label for="escuela" class="control-label">{{ 'Escuela' }}</label>
                                <select name="escuela" class="form-control" id="escuela" >
                                @foreach($escuelas as $item)
                                <option value="{{ $item->id }}"{{ isset($item->id) ?  ($item->id == $selected_escuela ? 'selected' : '') : '' }}>{{ $item->escuela }}</option>
                                @endforeach   
                                </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div> 
                            <br>
                            
                           <div class="form-group {{ $errors->has('plan') ? 'has-error' : ''}}">
                                <label for="plan" class="control-label">{{ 'Plan de Estudio' }}</label>
                                <select name="plan" class="form-control" id="plan" >
                                @foreach($planes as $item)
                                <option value="{{ $item->id }}"{{ isset($item->id) ?  ($item->id == $selected_plan ? 'selected' : '') : '' }}>{{ $item->anio }}</option>
                                @endforeach
                                 </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>   
                            <br>
                            <div class="form-group {{ $errors->has('re') ? 'has-error' : ''}}">
                                <label for="re" class="control-label">{{ 'Resultado del Estudiante' }}</label>
                                <select name="re" class="form-control" id="re" >
                                @forelse ($resultadosestudiantes as $item)
                                <option value="{{ $item->id }}"{{ isset($item->id) ?  ($item->id == $selected_re ? 'selected' : '') : '' }}>{{ $item->re }}</option>
                                @empty
                                <option></option>
                                @endforelse
                                </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div> 
                            <br>
                            <div class="form-group" >
                                <input class="btn boton-color" type="button" value="Añadir R.E" id="agregarCriterio">
                            </div>
                            <br>
                            <div class="content-table">
                            <table class="display table-re" name="criterio" id="criterio">
                            <thead class="thead-light">
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Codigo</th>
                                <th scope="col">Criterio</th>  
                                <th scope="col">Insatisfactorio</th>  
                                <th scope="col">En Proceso</th>  
                                <th scope="col">Satisfactorio</th>  
                                <th scope="col">Sobresaliente</th>                                                               
                                <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($criterios as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->codigo }}</td>
                                        <td>{{ $item->criterio }}</td>
                                        <td>{{ $item->insatisfactorio }}</td>
                                        <td>{{ $item->enproceso }}</td>
                                        <td>{{ $item->satisfactorio }}</td>
                                        <td>{{ $item->sobresaliente }}</td>
                                        <td>
                                            
                                            <!-- <i class="fa fa-pencil-square-o" aria-hidden="true"></i> -->
                                            <input type="submit" id="{{ $item->id }}" class="btn editar" value="Editar" ><br>                                      
                                            <!-- <i class="fa fa-trash-o" aria-hidden="true"></i> -->
                                            <input type="submit" id="{{ $item->id }}" class="btn eliminar" value="Eliminar">
                                           
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            </table>
                            </div>
                            <div class="form-group">
                            <a href="/admin/resultadoEstudiante" class="btn boton-color2">Atrás</a>
                            </div>
                            
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var numerador = {{ sizeof($criterios) }} + 1;

    $(document).ready(function ()
    {
        var datosPrevios;
        var editarId;

        function cargaescuelas() 
        {
            var facultad_id = $('#facultad').val();
            if(facultad_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.escuelas') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        facultad_id: facultad_id
                    },  
                    success:function (data) 
                    {
                        $('#escuela').empty();
                        $.each(data.escuelas[0].escuelas,function(index,escuela)
                        {
                                    
                            $('#escuela').append('<option value="'+escuela.id+'">'+escuela.escuela+'</option>');
                        });
                        cargaplanes();
                    }
                });
            }
            else
            {
                $('#escuela').empty();
                cargaplanes();
            }

        };

        function cargaplanes() 
        {
            var escuela_id = $('#escuela').val();
            if(escuela_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.planes') }}",
                    data: 
                    {
                        "_token": "{{ csrf_token() }}",
                        escuela_id: escuela_id
                    },
                    success:function (data) 
                    {
                        $('#plan').empty();
                        $.each(data.planes[0].plans,function(index,plan)
                        {    
                            $('#plan').append('<option value="'+plan.id+'">'+plan.anio+'</option>');
                        });
                        cargaResultadoEstudiante();
                    }
                });
            }
            else
            {
                $('#plan').empty();
                cargaResultadoEstudiante();
            }
                    
        };

        function cargaResultadoEstudiante(){
            var plan_id = $('#plan').val();
            if(plan_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.re') }}",
                    data: 
                    {
                        "_token": "{{ csrf_token() }}",
                        plan_id: plan_id
                    },
                    success:function (data) 
                    {
                        $('#re').empty();
                        $.each(data.resultadosestudiantes[0].resultadosestudiantes,function(index,re)
                        {    
                            $('#re').append('<option value="'+re.id+'">'+re.re+'</option>');
                            var maxlength=130;
                            $('#re>option').text(function(i,text){
                                if(text.length > maxlength){
                                    return text.substr(0, maxlength)+ '...';
                                }
                            });
                        });
                        cargaCriterio();
                    }
                });
            }
            else
            {
                $('#re').empty();
                cargaCriterio();
            }
            
        };  

        
        function cargaCriterio()
        { 
            $('#agregarCompetencia').show();
            numerador = 1;
            var resultadoestudiante_id = $('#re').val();
            if(resultadoestudiante_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.criterio') }}", 
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        resultadoestudiante_id: resultadoestudiante_id
                    },
                    success:function (data)
                    {
                        $('#criterio').empty();
                        $('#criterio').append('<thead class="thead-light"><tr><th scope="col">#</th><th scope="col">Codigo</th><th scope="col">Criterio</th><th scope="col">Insatisfactorio</th><th scope="col">En Proceso</th><th scope="col">Satisfactorio</th><th scope="col">Sobresaliente</th></tr></thead><tbody>');

                        $.each(data.criterios,function(index,criterio)
                        {    
                            $('#criterio').append('<tr><td>'+numerador+'</td><td>'+criterio.codigo+'</td><td>'+criterio.criterio+'</td><td>'+criterio.insatisfactorio+'</td><td>'+criterio.enproceso+'</td><td>'+criterio.satisfactorio+'</td><td>'+criterio.sobresaliente+'</td><td><input type="button" id="'+criterio.id+'" class="btn editar" value="Editar"><input type="button" id="'+criterio.id+'" class="btn eliminar" value="Eliminar"></td></tr>');
                            numerador+=1;
                        });
                        $('#criterio').append('</tbody>');
                    }
                });
            }
            else
            {
                $('#criterio').empty();
                $('#criterio').append('<thead class="thead-light"><tr><th scope="col">#</th><th scope="col">Codigo</th><th scope="col">Criterio</th></tr></thead><tbody>');
            }
        }

        //comienza
        function agregarCriterioVista()
        {
            $('.editar').hide();
            $('.eliminar').hide();
            $('#agregarCriterio').hide();

            //editar_plan = 0;
            //editar_curso = 0;

            $('#criterio').append('<tr><td>'+numerador+'</td><td><input type="text" id="nuevoCodigo"></td><td><input type="text" id="nuevaCriterio"></td><td><input type="text" id="nuevoInsatisfactorio"></td><td><input type="text" id="nuevoEnProgreso"></td><td><input type="text" id="nuevoSatisfactorio"></td><td><input type="text" id="nuevoSobresaliente"></td><td><input class="btn boton-color" type="button" id="guardarCriterio" value="Guardar"><input class="btn btn-danger" type="button" id="noGuardarCriterio" value="Cancelar"></td></tr>');

            numerador +=1;
                    
        };

        function guardarCriterioVista()
        {
            var resultadoestudiante_id = $('#re').val();
            var codigo = $('#nuevoCodigo').val();
            var criterio = $('#nuevaCriterio').val();
            var insatisfactorio = $('#nuevoInsatisfactorio').val();
            var enproceso = $('#nuevoEnProgreso').val();
            var satisfactorio = $('#nuevoSatisfactorio').val();
            var sobresaliente = $('#nuevoSobresaliente').val();

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.insertarcriterio') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        resultadoestudiante_id:resultadoestudiante_id,
                        codigo:codigo,
                        criterio:criterio,
                        insatisfactorio:insatisfactorio,
                        enproceso:enproceso,
                        satisfactorio:satisfactorio,
                        sobresaliente:sobresaliente
                    },
                    success:function() 
                    {
                        cargaCriterio();
                    }
                    
                });

            $('#agregarCriterio').show();
        };

        function cancelarGuardarCriterioVista(e)
        {
            $(e.target ).closest( "tr" ).remove();
            $('#agregarCriterio').show();
            $('.editar').show();
            $('.eliminar').show();
            numerador -=1;
        };

        function editarCriterioVista(e)
        {
            $('.editar').hide();
            $('.eliminar').hide();
            $('#agregarCriterio').hide();

            datosPrevios = $(e.target ).closest( "tr" ).html();
            editarId = e.target.id;

            var myTr = $(e.target ).closest( "tr" );
            
            var codigo = myTr.find('td:eq(1)').text();
            var criterio = myTr.find('td:eq(2)').text();
            var insatisfactorio = myTr.find('td:eq(3)').text();
            var enproceso = myTr.find('td:eq(4)').text();
            var satisfactorio = myTr.find('td:eq(5)').text();
            var sobresaliente = myTr.find('td:eq(6)').text();

            $(e.target ).closest( "td" ).append('<input class="btn boton-color" type="button" id="guardarCriterioEditado" value="Guardar"><input type="button" id="noGuardarCriterioEditado" class="noEditar" value="Cancelar">');

            myTr.find('td:eq(1)').empty();
            myTr.find('td:eq(1)').append('<input type="text" id="nuevoCodigo" value="'+codigo+'">');

            myTr.find('td:eq(2)').empty();
            myTr.find('td:eq(2)').append('<input type="text" id="nuevaCriterio" value="'+criterio+'">');

            myTr.find('td:eq(3)').empty();
            myTr.find('td:eq(3)').append('<input type="text" id="nuevoInsatisfactorio" value="'+insatisfactorio+'">');

            myTr.find('td:eq(4)').empty();
            myTr.find('td:eq(4)').append('<input type="text" id="nuevoEnProgreso" value="'+enproceso+'">');

            myTr.find('td:eq(5)').empty();
            myTr.find('td:eq(5)').append('<input type="text" id="nuevoSatisfactorio" value="'+satisfactorio+'">');

            myTr.find('td:eq(6)').empty();
            myTr.find('td:eq(6)').append('<input type="text" id="nuevoSobresaliente" value="'+sobresaliente+'">');
        };

        function guardarEditarCriterioVista(e)
        {
            var id = editarId;
            var codigo = $('#nuevoCodigo').val();
            var criterio = $('#nuevaCriterio').val();
            var insatisfactorio = $('#nuevoInsatisfactorio').val();
            var enproceso = $('#nuevoEnProgreso').val();
            var satisfactorio = $('#nuevoSatisfactorio').val();
            var sobresaliente = $('#nuevoSobresaliente').val();

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.editarcriterio') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        id:id,
                        codigo:codigo,
                        criterio:criterio,
                        insatisfactorio:insatisfactorio,
                        enproceso:enproceso,
                        satisfactorio:satisfactorio,
                        sobresaliente:sobresaliente
                    },
                    success:function() 
                    {
                        cargaCriterio();
                        //poner notificacion de editado exitoso
                    }
                    
                });

            $('#agregarCriterio').show();
        };

        function cancelarEditarCriterioVista(e)
        {
            var myTr = $(e.target ).closest( "tr" );
            myTr.empty();
            myTr.append(datosPrevios);
            $('.editar').show();
            $('.eliminar').show();
            $('#agregarCriterio').show();
        };

        function eliminarCriterioVista(e)
        {
            var myId = e.target.id;
            $.ajax(
            {
                type:'POST',
                url:"{{ route('ajax.eliminarcriterio') }}",
                data:
                {
                    "_token": "{{ csrf_token() }}",
                    id:myId
                },
                success:function(resp) 
                {

                    $('.alert').remove();
                    if(resp != 0){
                        $('.card').prepend('<div class="alert alert-danger col-md-4">No se puede eliminar esquema porque tiene relacion con '+ resp +' registros</div>');
                        $('.alert').fadeOut(7000);
                    }else{
                        $('.card').prepend('<div class="alert alert-success col-md-4">Se elimino exitosamente</div>');
                        $('.alert').fadeOut(7000);
                    }
                    cargaCriterio();
                    //Notificacion de eliminacion                     
                }    
            });
        };

        //termina

        $('#facultad').on('change', cargaescuelas);
        $('#escuela').on('change', cargaplanes);
        $('#plan').on('change', cargaResultadoEstudiante);
        $('#re').on('change', cargaCriterio);

        $('#agregarCriterio').on('click', agregarCriterioVista);
        $('body').on('click','#guardarCriterio',guardarCriterioVista);
        $('body').on('click','#noGuardarCriterio',cancelarGuardarCriterioVista);

        $('body').on('click','#guardarCriterioEditado',guardarEditarCriterioVista);
        $('body').on('click','.editar',editarCriterioVista);
        $('body').on('click','.noEditar',cancelarEditarCriterioVista);

        $('body').on('click','.eliminar',eliminarCriterioVista);
        
    });
</script>

@endsection