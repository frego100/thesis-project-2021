@extends('layouts.docentelayout')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @if(session()->has('success'))
                        <div class="alert alert-success col-md-4">{{ session()->get('success') }}</div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger col-md-4">{{ session()->get('error') }}</div>
                    @endif
                    <div class="card-header">
                        <div class="row justify-content-around">
                            <div class="col">Esquemas de Trabajo</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/files_Docente/docente-esquema.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ url('/exportaresquema') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            <div class="form-group {{ $errors->has('semestre') ? 'has-error' : '' }}">
                                <label for="semestre" class="control-label">{{ 'Semestre' }}</label>
                                <select name="semestre" class="form-control" id="semestre">
                                    @foreach ($semestres as $item)
                                        <option value="{{ $item->id }} "
                                            {{ isset($item->id) ? ($item->id == $selected_semestre ? 'selected' : '') : '' }}>
                                            {{ $item->anio }}-{{ $item->semestre }}</option>
                                    @endforeach
                                </select>
                                <!--{!! $errors->first('semestre', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <div class="form-group {{ $errors->has('curso') ? 'has-error' : '' }}">
                                <label for="curso" class="control-label">{{ 'Curso' }}</label>
                                <select name="curso" class="form-control" id="curso">
                                    @foreach ($cursos as $item)
                                        <option value="{{ $item->id }}"{{ isset($item->id) ? ($item->id == $selected_curso ? 'selected' : '') : '' }}>{{ $item->curso }}</option>
                                    @endforeach
                                </select>
                                <!--{!! $errors->first('curso', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <br>
                            <div class="form-group">
                                <input class="btn boton-color" type="button" value="Agregar" id="agregarEsquema">
                            </div>
                            <div class="content-tabla">
                                <table class="display" name="esquemas" id="esquemas">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Codigo</th>
                                            <th scope="col">Detalle</th>
                                            <th colspan ="2">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($esquemas as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->codigo }}</td>
                                                <td>{{ $item->detalle }}</td>
                                                <td>
                                                    <input type="button" id="{{ $item->id }}" class="btn btn-primary btn-sm editar" value="Editar">
                                                    <input type="button" id="{{ $item->id }}" class="btn btn-danger btn-sm eliminar" value="Eliminar">
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        <br>

                        <div class="form-group">
                            <div class="btn">
                            <input class="btn boton-color" type="button" value="Exportar" id="exportarEsquema">
                            </div>
                        </div>

                        <div class="file-field input-field">
                                <div class="btn">
                                    <!-- <span>Importar Excel<i class="material-icons left"></i></span><> -->
                                    <input class="btn boton-color" type="submit" value="Importar excel" id="importar" formaction="{{ url('/importaresquema') }}">
                                    <input type="file" name="esquemasexcel" id="selectimport" accept=".xls, .xlsx">
                                    {{ csrf_field() }}
                                </div>
                        </div>

                        <input type="hidden" value={{$cursosemestres->id}} name="cursoSemestre" id="cursoSemestre">
                        <input type="hidden" value={{$selected_semestre}} name="semestreE" id="semestreE">
                        <input type="hidden" value={{$selected_curso}} name="cursoE" id="cursoE">

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var datosPrevios;
        var editarId;

        var numerador = {{ sizeof($esquemas) }} + 1;
        var mi_curso_semestre = {{ $cursosemestres->id }};
        $('#importar').attr('disabled',true);

        $(document).ready(function() {

        function cargacursosdocente() 
        {
            var semestre_id = $('#semestre').val();
            if(semestre_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cursosdocenteespecifico') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        semestre_id: semestre_id,
                        coordinador: '1'
                    },  
                    success:function (data) 
                    {
                        $('#curso').empty();
                        $.each(data.cursos,function(index,curso)
                        {         
                            $('#curso').append('<option value="'+curso.id+'">'+curso.curso+'</option>');
                        });
                        cargaesquemasdocente();
                    }
                });
            }
            else
            {
                $('#escuela').empty();
                cargaesquemasdocente();
            }

        };

        function cargaesquemasdocente() 
        {
            numerador = 1;
            var semestre_id = $('#semestre').val();
            var curso_id = $('#curso').val();
            if(curso_id)
            {
                console.log("Se inicia el ajax cargaesquemasdocente");
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.esquemacurso') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        semestre_id: semestre_id,
                        curso_id: curso_id
                    },  
                    success:function (data) 
                    {
                        mi_curso_semestre = data.cursosemestre;
                        $('#cursoSemestre').attr('value',data.cursosemestre);
                        $('#semestreE').attr('value',semestre_id);
                        $('#cursoE').attr('value',curso_id);

                        $('#esquemas').empty();
                        console.log("Se corraron los datos");
                        console.log($('#cursoE').val());
                        $('#esquemas').append('<thead class="thead-light"><tr><th scope="col">#</th><th scope="col">Codigo</th><th scope="col">Detalle</th><th colspan ="2">Acciones</th></tr></thead><tbody>');
                        

                        $.each(data.cursoesquemas,function(index,esquema)
                        {       
                            $('#esquemas').append('<tr><td>'+numerador+'</td><td>'+esquema.codigo+'</td><td>'+esquema.detalle+'</td><td><input type="button" id="'+esquema.id+'" class="btn btn-primary btn-sm editar" value="Editar"></td><td><input type="button" id="'+esquema.id+'" class="btn btn-danger btn-sm eliminar" value="Eliminar"></td></tr>');

                            numerador += 1;
                        });
                        $('#esquemas').append('</tbody>');
                    }
                });
            }

        };

        //comienza

        function agregarEsquemaVista()
        {
            $('.editar').hide();
            $('.eliminar').hide();
            $('#agregarEsquema').hide();

            //editar_cursoTipo = 0;
            //editar_plan = 0;
            //editar_curso = 0;

            $('#esquemas').append('<tr><td>'+numerador+'</td><td><input type="text" id="nuevoCodigo" maxlength="20"></td><td><input type="text" id="nuevoDetalle"  ></td><td><input type="button" id="guardarEsquema" value="Guardar"></td><td><input type="button" id="noGuardarEsquema" value="Cancelar"></td>');

            numerador +=1;            
        };

        function exportarEsquema(e)
        {
            console.log(e.target.nodeName);
            var semestre_id = $('#semestre').val();
            var curso_id = $('#curso').val();
            var cursosemestre_id = $('#cursoSemestre').val();
            console.log(cursosemestre_id);
            $( "form" ).submit();
        };

        function guardarEsquemaVista()
        {
            var codigo = $('#nuevoCodigo').val();
            var detalle = $('#nuevoDetalle').val();

            var cursosemestre_id = mi_curso_semestre;

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.insertarcursoesquema') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        codigo:codigo,
                        detalle:detalle,
                        cursosemestre_id:cursosemestre_id
                    },
                    success:function() 
                    {
                        cargaesquemasdocente();
                    }
                    
                });

            $('#agregarEsquema').show();

        };

        function cancelarGuardarEsquemaVista(e)
        {
            $(e.target ).closest( "tr" ).remove();
            $('#agregarEsquema').show();
            $('.editar').show();
            $('.eliminar').show();
            numerador -=1;
        };

        function editarEsquemaVista(e)
        {
            $('.editar').hide();
            $('.eliminar').hide();
            $('#agregarEsquema').hide();

            datosPrevios = $(e.target ).closest( "tr" ).html();
            editarId = e.target.id;

            var myTr = $(e.target ).closest( "tr" );
            
            var codigo = myTr.find('td:eq(1)').text();
            var detalle = myTr.find('td:eq(2)').text();

            $(e.target ).closest( "td" ).append('<input type="button" id="guardarEsquemaEditado" value="Guardar"><input type="button" id="noGuardarEsquemaEditado" class="noEditar" value="Cancelar">');

            myTr.find('td:eq(1)').empty();
            myTr.find('td:eq(1)').append('<input type="text" id="codigoEditado" value="'+codigo+'" maxlength="10">');

            myTr.find('td:eq(2)').empty();
            myTr.find('td:eq(2)').append('<input type="text" id="detalleEditado" value="'+detalle+ ' " maxlength="10">');
        };

        function guardarEditarEsquemaVista(e)
        {
            var id = editarId;
            var codigo = $('#codigoEditado').val();
            var detalle = $('#detalleEditado').val();

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.editarcursoesquema') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        id:id,
                        codigo:codigo,
                        detalle:detalle
                    },
                    success:function() 
                    {
                        cargaesquemasdocente();
                        $('#agregarEsquema').show();
                        //poner notificacion de editado exitoso
                    }
                    
                });

            
        };

        function cancelarEditarEsquemaVista(e)
        {
            var myTr = $(e.target ).closest( "tr" );
            myTr.empty();
            myTr.append(datosPrevios);
            $('.editar').show();
            $('.eliminar').show();
            $('#agregarEsquema').show();
        };

        function eliminarEsquemaVista(e)
        {
            var myId = e.target.id;

            $.ajax(
            {
                type:'POST',
                url:"{{ route('ajax.eliminarcursoesquema') }}",
                data:
                {
                    "_token": "{{ csrf_token() }}",
                    id:myId
                },
                success:function(resp) 
                {
                    $('.alert').remove();
                    if(resp != 0){
                        $('.card').prepend('<div class="alert alert-danger col-md-4">No se puede eliminar esquema porque tiene relacion con '+ resp +' registros</div>');
                        $('.alert').fadeOut(7000);
                    }else{
                        $('.card').prepend('<div class="alert alert-success col-md-4">Se elimino exitosamente</div>');
                        $('.alert').fadeOut(7000);
                    }
                    cargaesquemasdocente();
                    //Notificacion de eliminacion                     
                }    
            });
        };

        function importarEsquema(e)
        {
            var lenArchivo = e.currentTarget.files[0];
            console.log(lenArchivo);
            $('#importar').attr('disabled',false);
            
        };

        $('#semestre').on('change', cargacursosdocente);
        $('#curso').on('change', cargaesquemasdocente);

        $('#agregarEsquema').on('click', agregarEsquemaVista);
        $('body').on('click','#guardarEsquema',guardarEsquemaVista);
        $('body').on('click','#noGuardarEsquema',cancelarGuardarEsquemaVista);

        $('body').on('click','#guardarEsquemaEditado',guardarEditarEsquemaVista);
        $('body').on('click','.editar',editarEsquemaVista);
        $('body').on('click','.noEditar',cancelarEditarEsquemaVista);

        $('body').on('click','.eliminar',eliminarEsquemaVista);

        // $('#exportarEsquema').on('click', exportarEsquema2);

        $('body').on('click','#exportarEsquema',exportarEsquema);

        $('#selectimport').on('change',importarEsquema);

        });
    </script>
@endsection
