@extends('layouts.docentelayout')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="row justify-content-around">
                            <div class="col">Inicio</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/files_Docente/docente.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">

                        <form method="GET" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            <div class="form-group {{ $errors->has('cargo') ? 'has-error' : '' }}">
                                <label for="cargo" class="control-label" id="cargo"><b>{{ 'Cargo:' }} {{ $cargo }}</b></label>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <br>
                            <div class="form-group {{ $errors->has('semestre') ? 'has-error' : '' }}">
                                <label for="semestre" class="control-label">{{ 'Semestre' }}</label>
                                <select name="semestre" class="form-control" id="semestre">
                                    @foreach ($semestres as $item)
                                        <option value="{{ $item->id }} ">
                                            {{ $item->anio }}-{{ $item->semestre }}</option>
                                    @endforeach
                                </select>
                                <!--{!! $errors->first('semestre', '<p class="help-block">:message</p>') !!}-->
                            </div>

                            <div class="form-group {{ $errors->has('curso') ? 'has-error' : '' }}">
                                <label for="curso" class="control-label">{{ 'Curso' }}</label>
                                <select name="curso" class="form-control" id="curso">
                                    @foreach ($cursos as $item)
                                        <option value="{{ $item->id }}">{{ $item->curso }}</option>
                                    @endforeach
                                </select>
                                <!--{!! $errors->first('curso', '<p class="help-block">:message</p>') !!}-->
                            </div>


                            <div class="form-group {{ $errors->has('grupo') ? 'has-error' : '' }}">
                                <label for="grupo" class="control-label">{{ 'Grupo' }}</label>
                                <select name="grupo" class="form-control" id="grupo">
                                    @foreach ($cursogrupos as $item)
                                        <option value="{{ $item->id }}">{{ $item->grupo }}</option>
                                    @endforeach
                                </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <br>
                            <div class="form-group">
                                <input class="btn boton-color" type="submit" value="Alumnos"
                                    formaction="{{ url('docente/alumnos') }}">
                         
                                <input class="btn boton-color" type="submit" value="Registro"
                                    formaction="{{ url('/docente/matriz') }}">
                
                                <input class="btn boton-color" type="submit" value="Esquemas" id="irEsquemas" formaction="{{ url('/docente/esquemas') }}" style="{{ $cargo == "Docente" ? 'display: none;' : ''}}">
            
                                <input class="btn boton-color" type="submit" value="Matrices" id="irMatrizCabecera" formaction="{{ url('/docente/matrizcabecera') }}" style="{{ $cargo == "Docente" ? 'display: none;' : ''}}">
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">

                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var cargo = "{{ $cargo }}";

        $(document).ready(function() {

        function cargacursosdocente() 
        {
            var semestre_id = $('#semestre').val();
            if(semestre_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cursosdocenteespecifico') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        semestre_id: semestre_id
                    },  
                    success:function (data) 
                    {
                        $('#curso').empty();
                        $.each(data.cursos,function(index,curso)
                        {         
                            $('#curso').append('<option value="'+curso.id+'">'+curso.curso+'</option>');
                        });
                        cargagruposdocente();
                    }
                });
            }
            else
            {
                $('#escuela').empty();
                cargagruposdocente();
                cargo = "Sin cargo";
            }

        };

        function cargagruposdocente() 
        {
            var curso_id = $('#curso').val();
            var semestre_id = $('#semestre').val();
            if(curso_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.gruposdocenteespecifico') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        curso_id: curso_id,
                        semestre_id: semestre_id
                    },  
                    success:function (data) 
                    {
                        $('#grupo').empty();
                        $.each(data.grupos,function(index,grupo)
                        {       
                            $('#grupo').append('<option value="'+grupo.id+'">'+grupo.grupo+'</option>');
                        });
                        cargo = data.cargo;
                        console.log("Nuevo cargo: "+cargo);
                        verificarcargo();
                    }
                });
            }
            else
            {
                $('#grupo').empty();
            }

        };

        function verificarcargo()
        {
            if(cargo == 'Coordinador')
            {
                $('#irEsquemas').show();
                $('#irMatrizCabecera').show();
            }
            else
            {
                $('#irEsquemas').hide();
                $('#irMatrizCabecera').hide();
            }
            $('#cargo').empty();
            $('#cargo').text("Cargo: "+cargo);
        }


        $('#semestre').on('change', cargacursosdocente);
        $('#curso').on('change', cargagruposdocente);

        });
    </script>

@endsection
