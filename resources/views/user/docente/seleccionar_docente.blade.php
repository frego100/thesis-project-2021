@extends('layouts.docentelayout')

@section('content')

<br><br>
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">                                
                <div class="card-header">{{ __('Seleccionar docente') }}</div>
            
                    <div class="card-body">

                            <div class="form-group {{ $errors->has('facultad') ? 'has-error' : ''}}">
                                <label for="facultad" class="control-label">{{ 'Semestre:' }}</label>
                                <select name="facultad" class="form-control" id="facultad" >
                                    

                            </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>

                            <div class="form-group {{ $errors->has('facultad') ? 'has-error' : ''}}">
                                <label for="facultad" class="control-label">{{ 'Curso:' }}</label>
                                <select name="facultad" class="form-control" id="facultad" >
                                    

                            </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>

                            <div class="form-group {{ $errors->has('facultad') ? 'has-error' : ''}}">
                                <label for="facultad" class="control-label">{{ 'Grupo:' }}</label>
                                <select name="facultad" class="form-control" id="facultad" >
                                    

                            </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>

                            <div class="form-group {{ $errors->has('facultad') ? 'has-error' : ''}}">
                                <label for="facultad" class="control-label">{{ 'Docentes:' }}</label>
                                <select name="facultad" class="form-control" id="facultad" >
                                    

                            </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>


                            <br>

                            <div class="form-group">
                                <input class="btn btn-primary" type="submit" value="Seleccionar"
                                    formaction="{{ url('/admin/curso') }}">
                            </div>
                            <br>

                            <div class="form-group">
                                <input class="btn btn-primary" type="submit" value="Crear"
                                    formaction="{{ url('/admin/competencia') }}">
                            </div>                

            </div>
        </div>
    </div>
</div>

@endsection