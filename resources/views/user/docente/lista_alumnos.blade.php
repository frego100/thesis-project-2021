@extends('layouts.docentelayout')
@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            @if(session()->has('success'))
                <div class="alert alert-success col-md-4">{{ session()->get('success') }}</div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-danger col-md-4">{{ session()->get('error') }}</div>
            @endif                            
            <div class="card-header">
                     <div class="row justify-content-around">
                            <div class="col">Matricular Alumnos</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/files_Docente/docente-alumnos.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                    </div>
            </div>
                    <div class="card-body">
                    <form method="POST" action="{{ url('/exportaralumno') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            <div class="row ">
                                <div class="form-group {{ $errors->has('cargo') ? 'has-error' : '' }}">
                                    <label for="cargo" class="control-label" id="cargo">{{ 'Cargo:' }} {{ $cargo }}</label>
                                    <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                                </div>
                                <div class="form-group {{ $errors->has('docente') ? 'has-error' : '' }}">
                                    <label for="docente" class="control-label" id="docente">{{ 'Docente:' }} {{ $docente[0]->nombre }}</label>
                                    <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                                </div>
                            </div>
                            
                            
                            <div class="form-group {{ $errors->has('semestre') ? 'has-error' : '' }}">
                                <label for="semestre" class="control-label">{{ 'Semestre' }}</label>
                                <select name="semestre" class="form-control" id="semestre">
                                    @foreach ($semestres as $item)
                                        <option value="{{ $item->id }} "
                                            {{ isset($item->id) ? ($item->id == $selected_semestre ? 'selected' : '') : '' }}>
                                            {{ $item->anio }}-{{ $item->semestre }}</option>
                                    @endforeach
                                </select>
                                <!--{!! $errors->first('semestre', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <div class="form-group {{ $errors->has('curso') ? 'has-error' : '' }}">
                                <label for="curso" class="control-label">{{ 'Curso' }}</label>
                                <select name="curso" class="form-control" id="curso">
                                    @foreach ($cursos as $item)
                                        <option value="{{ $item->id }}" {{ isset($item->id) ? ($item->id == $selected_curso ? 'selected' : '') : '' }}>{{ $item->curso }}</option>
                                    @endforeach
                                </select>
                                <!--{!! $errors->first('curso', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <div class="form-group {{ $errors->has('grupo') ? 'has-error' : '' }}">
                                <label for="grupo" class="control-label">{{ 'Grupo' }}</label>
                                <select name="grupo" class="form-control" id="grupo">
                                    @foreach ($cursogrupos as $item)
                                        <option value="{{ $item->id }}" {{ isset($item->id) ? ($item->id == $selected_grupo ? 'selected' : '') : '' }}>{{ $item->grupo }}</option>
                                    @endforeach
                                </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <br>
                            <div class="form-group">
                                <input class="btn boton-color" type="button" value="Matricular Alumnos" id="agAlumnos">
                            </div>
                            <div class="content-tabla">
                            <table class="display" id='tablaAlumnos'>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Estudiante</th>
                                        <th>CUI</th>
                                        <th>Email</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>

                        
                                <tbody>
                                    @foreach ($grupoalumnos as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->estudiante->estudiante }}</td>
                                            <td>{{ $item->estudiante->cui }}</td>
                                            <td>{{ $item->estudiante->email }}</td>
                                            <td><input type="button" id="{{ $item->id }}" class="btn btn-danger btn-sm eliminar" value="Eliminar"></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>


                            <br>

                            <!-- <div class="form-group">
                                <div class="btn">
                                <input class="btn  boton-color" type="button" value="Exportar" id="exportarAlumno">
                                </div>
                            </div> -->

                            <div class="file-field input-field">
                                    <div class="btn">
                                        <!-- <span>Importar Excel<i class="material-icons left"></i></span><> -->  
                                        <input class="btn boton-color" type="submit" value="Importar excel" id="importar" disabled="false" formaction="{{ url('/importaralumno') }}">
                                        <input type="file" name="alumnosexcel" id="selectimport" accept=".xls, .xlsx">
                                        {{ csrf_field() }}
                                    </div>
                            </div>

                            <input type="hidden" value={{$selected_semestre}} name="semestreI" id="semestreI">
                            <input type="hidden" value={{$selected_curso}} name="cursoI" id="cursoI">
                            <input type="hidden" value={{$selected_grupo}} name="grupoI" id="grupoI">
                            <input type="hidden" value={{$docente[0]->id}} name="docenteI" id="docenteI">

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var cargo = "{{ $cargo }}";
        var numerador = {{ sizeof($grupoalumnos) }} + 1;
        var lAlumnos;

        $('.alert').fadeOut(7000);
        $('#importar').attr('disabled',true);

        $(document).ready(function() {
        function cargacursosdocente() 
        {
            var semestre_id = $('#semestre').val();
            if(semestre_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cursosdocenteespecifico') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        semestre_id: semestre_id
                    },  
                    success:function (data) 
                    {
                        $('#curso').empty();
                        $.each(data.cursos,function(index,curso)
                        {         
                            $('#curso').append('<option value="'+curso.id+'">'+curso.curso+'</option>');
                        });
                        cargagruposdocente();
                    }
                });
            }
            else
            {
                $('#escuela').empty();
                cargagruposdocente();
            }

        };

        function cargagruposdocente() 
        {
            var curso_id = $('#curso').val();
            var semestre_id = $('#semestre').val();
            if(curso_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.gruposdocenteespecifico') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        curso_id: curso_id,
                        semestre_id: semestre_id
                    },  
                    success:function (data) 
                    {
                        $('#grupo').empty();
                        $.each(data.grupos,function(index,grupo)
                        {       
                            $('#grupo').append('<option value="'+grupo.id+'">'+grupo.grupo+'</option>');
                        });
                        cargo = data.cargo;
                        cargadocente();
                        verificarcargo();
                    }
                });
            }
            else
            {
                $('#grupo').empty();
                cargadocente();
            }

        };

        function verificarcargo()
        {
            if(cargo == 'Coordinador')
            {
                $('#irMatrices').show();
            }
            else
            {
                $('#irMatrices').hide();
            }
            $('#cargo').empty();
            $('#cargo').text("Cargo: "+cargo);
        }

        function cargadocente() 
        {
            var curso_id = $('#curso').val();
            var semestre_id = $('#semestre').val();
            var grupo_id = $('#grupo').val();
            if(grupo_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.docenteespecifico') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        grupo_id: grupo_id
                    },  
                    success:function (data) 
                    {
                        $('#cursoI').attr('value',curso_id);
                        $('#semestreI').attr('value',semestre_id);
                        $('#grupoI').attr('value',grupo_id);
                        $('#docenteI').attr('value',data.docente.id);
                        console.log("Se corraron los datos");
                        console.log($('#cursoI').val());
                        $('#docente').empty();
                        $('#docente').text("Profesor: "+data.docente.nombre);
                        actualizarListaAlumno();
                    }
                });
            }
            else
            {
                $('#grupo').empty();
                actualizarListaAlumno();
            }

        };
        function actualizarListaAlumno()
        {
            numerador = 1;
            var grupo_id = $('#grupo').val();
            if(grupo_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.acalumno') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        grupo_id: grupo_id
                    },  
                    success:function (data) 
                    {
                        $('#tablaAlumnos').empty();
                        $('#tablaAlumnos').append('<thead class="thead-light"><tr><th scope="col">#</th><th scope="col">Estudiante</th><th scope="col">CUI</th><th scope="col">Email</th><th colspan ="1">Acciones</th></tr></thead><tbody>');
                        
                        $.each(data.grupoalumnos,function(index,galumnos)
                        {       
                            $('#tablaAlumnos').append('<tr><td>'+numerador+'</td><td>'+galumnos.estudiante.estudiante+'</td><td>'+galumnos.estudiante.cui+'</td><<td>'+galumnos.estudiante.email+'</td><td><input type="button" id="'+galumnos.id+'" class="btn btn-danger btn-sm eliminar" value="Eliminar"></td></tr>');

                            numerador += 1;
                        });
                        $('#tablaAlumnos').append('</tbody>');
                    }
                });
            }
            else
            {
                $('#grupo').empty();
            }

        }
            // CRUD
        function agregarAlumnos(){
            $('.eliminar').hide();
            $('#agAlumnos').hide();

            

            $('#tablaAlumnos').append('<tr><td>'+numerador+'</td><td><select name="nuevo_estudiante" class="form-control" id="nuevoEstudiante"></select></td><td></td><td></td><td><input class="btn btn-info btn-sm" type="button" id="guardarEstudiante" value="Guardar"></td><td><input class="btn btn-danger btn-sm" type="button" id="noGuardarEstudiante" value="Cancelar"></td></tr>');

            cargaAlumnos();
            numerador +=1;            
        };
        function cargaAlumnos(){
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.listaAlumnos') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}"
                    },  
                    success:function (data) 
                    {
                            lAlumnos=data.listaAlumnos;
                            $.each(data.listaAlumnos,function(index,alumnos)
                            {   
                                $('#nuevoEstudiante').append('<option value="'+alumnos.id+'">'+alumnos.estudiante+'</option>');
                                
                            });
                            var myTr = $('#nuevoEstudiante').closest( "tr" );
                                myTr.find('td:eq(2)').empty();
                                myTr.find('td:eq(2)').append(data.listaAlumnos[0].cui);
                                myTr.find('td:eq(3)').empty();
                                myTr.find('td:eq(3)').append(data.listaAlumnos[0].email);
                    }
                });
        }
        function cambiarDatosTabla(e)
        {
            var myTr = $(e.target ).closest( "tr" );
            var myAlumno = $('#nuevoEstudiante').val();

            $.each(lAlumnos,function(index,alumno)
            {   
                if(alumno.id == myAlumno)
                {
                    myTr.find('td:eq(2)').empty();
                    myTr.find('td:eq(2)').append(alumno.cui);
                    myTr.find('td:eq(3)').empty();
                    myTr.find('td:eq(3)').append(alumno.email);
                }
            });
        }
        function guardarAlumno()
        {
            var alumno_id = $('#nuevoEstudiante').val();
            var cursogrupo_id =$('#grupo').val();
            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.matricularAlumno') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        alumno_id:alumno_id,
                        cursogrupo_id:cursogrupo_id
                    },
                    success:function() 
                    {
                        actualizarListaAlumno();                        
                    }
                });
            $('#agAlumnos').show();
        };
        function eliminarAlumno(e)
        {
            var myId = e.target.id;
            $.ajax(
            {
                type:'POST',
                url:"{{ route('ajax.eliminarAlumno') }}",
                data:
                {
                    "_token": "{{ csrf_token() }}",
                    id:myId
                },
                success:function(resp) 
                {

                    $('.alert').remove();
                    if(resp != 0){
                        $('.card').prepend('<div class="alert alert-danger col-md-4">No se puede eliminar alumno porque tiene relacion con '+ resp +' registros</div>');
                        $('.alert').fadeOut(7000);
                    }else{
                        $('.card').prepend('<div class="alert alert-success col-md-4">Se elimino exitosamente</div>');
                        $('.alert').fadeOut(7000);
                    }
                    actualizarListaAlumno();                     
                }    
            });
        };

        function exportarAlumno(e)
        {
            console.log(e.target.nodeName);
            var semestre_id = $('#semestre').val();
            var curso_id = $('#curso').val();
            var grupo_id = $('#grupo').val();
            console.log(semestre_id);
            $( "form" ).submit();
        };

        function importarAlumno(e)
        {
            var lenArchivo = e.currentTarget.files[0];
            console.log(lenArchivo);
            $('#importar').attr('disabled',false);
            
        };

        function cancelarAgregarAlumno(e)
        {
            $(e.target ).closest( "tr" ).remove();
            $('#agAlumnos').show();
            $('.eliminar').show();
            numerador -=1;
        };

        $('#semestre').on('change', cargacursosdocente);
        $('#curso').on('change', cargagruposdocente);
        $('#grupo').on('change',cargadocente);
        $('#agAlumnos').on('click', agregarAlumnos);
        $('body').on('change','#nuevoEstudiante',cambiarDatosTabla);
        $('body').on('click','#guardarEstudiante',guardarAlumno);
        $('body').on('click','#noGuardarEstudiante',cancelarAgregarAlumno);
        $('body').on('click','.eliminar',eliminarAlumno);
        $('body').on('click','#exportarAlumno',exportarAlumno);
        // $('body').on('click','#selectimport',importarAlumno);
        $('#selectimport').on('change',importarAlumno);
        });
    </script>
@endsection