@extends('layouts.docentelayout')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    @if(session()->has('error'))
                        <div class="alert alert-danger col-md-4">{{ session()->get('error') }}</div>
                    @endif
                    <div class="card-header">
                        <div class="row justify-content-around">
                            <div class="col">Matriz</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/files_Docente/docente-registro.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="GET" action=" {{ url('/docente/notasalumno') }}" id="myForm" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            <center> 
                            <div class="form-group {{ $errors->has('cargo') ? 'has-error' : '' }}">
                                <label for="cargo" class="control-label" id="cargo">{{ 'Cargo:' }} {{ $cargo }}</label>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <div class="form-group {{ $errors->has('docente') ? 'has-error' : '' }}">
                                <label for="docente" class="control-label" id="docente">{{ 'Docente:' }} {{ $docente[0]->nombre }}</label>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            </center>
                            <div class="form-group {{ $errors->has('semestre') ? 'has-error' : '' }}">
                                <label for="semestre" class="control-label">{{ 'Semestre' }}</label>
                                <select name="semestre" class="form-control" id="semestre">
                                    @foreach ($semestres as $item)
                                        <option value="{{ $item->id }} "
                                            {{ isset($item->id) ? ($item->id == $selected_semestre ? 'selected' : '') : '' }}>
                                            {{ $item->anio }}-{{ $item->semestre }}</option>
                                    @endforeach
                                </select>
                                <!--{!! $errors->first('semestre', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <div class="form-group {{ $errors->has('estado') ? 'has-error' : '' }}" id="estadosemestre">
                                <label for="estado" class="control-label" >{{ 'Estado Semestre: ' }} {{$escuelasemestre->estadosemestre}}</label>
                                <!--{!! $errors->first('plan', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <div class="form-group {{ $errors->has('curso') ? 'has-error' : '' }}">
                                <label for="curso" class="control-label">{{ 'Curso' }}</label>
                                <select name="curso" class="form-control" id="curso">
                                    @foreach ($cursos as $item)
                                        <option value="{{ $item->id }}" {{ isset($item->id) ? ($item->id == $selected_curso ? 'selected' : '') : '' }}>{{ $item->curso }}</option>
                                    @endforeach
                                </select>
                                <!--{!! $errors->first('curso', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <div class="form-group {{ $errors->has('grupo') ? 'has-error' : '' }}">
                                <label for="grupo" class="control-label">{{ 'Grupo' }}</label>
                                <select name="grupo" class="form-control" id="grupo">
                                    @foreach ($cursogrupos as $item)
                                        <option value="{{ $item->id }}" {{ isset($item->id) ? ($item->id == $selected_grupo ? 'selected' : '') : '' }}>{{ $item->grupo }}</option>
                                    @endforeach
                                </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>

                            <div class="form-group {{ $errors->has('matriz') ? 'has-error' : '' }}">
                                <label for="matriz" class="control-label">{{ 'Matriz' }}</label>
                                <select name="matriz" class="form-control" id="matriz">
                                    <option value="0" >TODAS LAS MATRICES</option>
                                    @foreach ($cursomatrizs as $item)
                                        <option value="{{ $item->id }}" >{{ $item->matriz }}</option>
                                    @endforeach

                                    
                                </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <br>
                            <div class="content-tabla">
                            <table class="display" name="notas" id="notas">
                                <thead class="thead-light">
                                    <tr id="notas-cabecera">
                                        <th scope="col">#</th>
                                        <th scope="col">Alumno</th>
                                        @foreach ($cursomatrizs as $item)
                                        <th scope="col">{{ $item->matriz }}<button type="button" class="btn btn-sm pdf" id="{{ $item->id }}" title="Imprimir {{ $item->matriz }}"><i id="{{ $item->id }}" class="fa fa-print" aria-hidden="true"></i></button>
                                        @endforeach
                                        <th scope="col">Promedio final</th>
                                    </tr>
                                </thead>
                                <tbody id="notas-detalle">
                                    @foreach ($cursoestudiantes as $ce)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td id="{{ $ce->id }}" class="mialumno">{{ $ce->estudiante->estudiante }}</td>
                                        @foreach ($ce->promedios as $p)
                                        <td>{{ number_format(round($p,2),2) }}</td>
                                        @endforeach    
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>

                        <input type="hidden" value={{$selected_semestre}} name="semestreR" id="semestreR">
                        <input type="hidden" value={{$selected_curso}} name="cursoR" id="cursoR">
                        <input type="hidden" value={{$selected_grupo}} name="grupoR" id="grupoR">
                        <input type="hidden" value={{$docente[0]->id}} name="docenteR" id="docenteR">
                        <input type="hidden" value={{$escuelasemestre->escuela_id}} name="escuela" id="escuela">

                        <input type="hidden" id="AlumnoID" name="AlumnoID" value="0">
                        <input type="hidden" value="0" id="matrizid" name="matrizid">

                            <br>

                            <div class="form-group">
                                <input class="btn boton-color" type="submit" value="Editar Matrices" id="irMatrices" formaction="{{ url('/docente/crearMatriz') }}" {{ $cargo == "Docente" ? 'hidden' : ''}}>
                           
                            <input class="btn boton-color" type="button" value="Reporte excel" id="reporte">
                            {{ csrf_field() }}
                            </div>
                            <br>
                            <div class="form-group">
                            <input class="btn btn-primary pdf" type="button" name="reporte" value="Reporte" id="0" style="display: none;">
                            {{ csrf_field() }}
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var numerador;
        var cargo = "{{ $cargo }}";
        $('#mensajealerta').hide();
        $('.alert').fadeOut(7000);

        $(document).ready(function() {

        function cargacursosdocente() 
        {
            var semestre_id = $('#semestre').val();
            var escuela_id = $('#escuela').val();
            if(semestre_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cursosdocenteespecifico') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        semestre_id: semestre_id,
                        escuela_id:escuela_id
                    },  
                    success:function (data) 
                    {
                        $('#curso').empty();
                        $.each(data.cursos,function(index,curso)
                        {         
                            $('#curso').append('<option value="'+curso.id+'">'+curso.curso+'</option>');
                        });
                        cargagruposdocente();
                        $('#estadosemestre').empty();
                        $('#estadosemestre').append('<label for="estado" class="control-label">Estado Semestre: ' + data.escuelasemestre.estadosemestre  + '</label>');
                    }
                });
            }
            else
            {
                cargagruposdocente();
            }

        };



        function cargagruposdocente() 
        {
            var curso_id = $('#curso').val();
            var semestre_id = $('#semestre').val();
            if(curso_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.gruposdocenteespecifico') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        curso_id: curso_id,
                        semestre_id: semestre_id
                    },  
                    success:function (data) 
                    {
                        $('#grupo').empty();
                        $.each(data.grupos,function(index,grupo)
                        {       
                            $('#grupo').append('<option value="'+grupo.id+'">'+grupo.grupo+'</option>');
                        });
                        cargo = data.cargo;
                        cargadocente();
                        cargacursomatriz();
                        verificarcargo();
                    }
                });
            }
            else
            {
                $('#grupo').empty();
                cargadocente();
                cargacursomatriz();
            }

        };

        function verificarcargo()
        {
            if(cargo == 'Coordinador')
            {
                $('#irMatrices').show();
            }
            else
            {
                $('#irMatrices').hide();
            }
            $('#cargo').empty();
            $('#cargo').text("Cargo: "+cargo);
        }

        function cargadocente() 
        {
            var grupo_id = $('#grupo').val();
            var semestre_id = $('#semestre').val();
            var curso_id = $('#curso').val();
            var cursogrupo_id = $('#grupo').val();
            if(grupo_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.docenteespecifico') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        grupo_id: grupo_id
                    },  
                    success:function (data) 
                    {
                        $('#semestreR').attr('value',semestre_id);
                        $('#cursoR').attr('value',curso_id);
                        $('#grupoR').attr('value',cursogrupo_id);
                        $('#docenteR').attr('value',data.docente.id);
                        console.log(cursogrupo_id);
                        $('#docente').empty();
                        $('#docente').text("Profesor a cargo: "+data.docente.nombre);
                    }
                });
            }
            else
            {
                $('#grupo').empty();
            }

        };

        function cargacursomatriz()
        {
            var grupo_id = $('#grupo').val();
            if(grupo_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cargacursomatriz') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        grupo_id: grupo_id
                    },  
                    success:function (data) 
                    {
                        $('#matriz').empty();
                        $('#matriz').append('<option value="0">TODAS LAS MATRICES</option>');
                        $.each(data.cursomatrizs,function(index,matriz)
                        {       
                            $('#matriz').append('<option value="'+matriz.id+'">'+matriz.matriz+'</option>');
                        });
                        carganotascabecera()

                    }
                });
            }
            else
            {
                $('#matriz').empty();
                carganotascabecera()
            }
        }

        //carga la cabecera de la tabla
        function carganotascabecera()
        {
            var cursomatriz_id = $('#matriz').val();
            if(cursomatriz_id!=0)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cargamatrizdetalle') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        cursomatriz_id: cursomatriz_id
                    },  
                    success:function (data) 
                    {
                        console.log("Carga de cabecera");
                        console.log(data.matrizdetalles);
                        $('#notas-cabecera').empty();
                        $('#notas-cabecera').append('<th scope="col">#</th><th scope="col">Alumno</th>');
                        $.each(data.matrizdetalles,function(index,matriz)
                        {       
                            $('#notas-cabecera').append('<th scope="col">'+matriz.cursoesquema.detalle+'<br>'+matriz.criterio.codigo+'</th>');
                        });
                        $('#notas-cabecera').append('<th scope="col">Promedio</th>');
                        carganotasdetalle();
                    }
                });
            }
            else
            {
                carganotasdetalle();
            }
        }

        //carga los alumnos con sus correspondintes notas en una matriz especifica
        function carganotasdetalle()
        {
            numerador = 1;
            var cursogrupo_id = $('#grupo').val();
            var cursomatriz_id = $('#matriz').val();
            if(cursomatriz_id && cursogrupo_id && cursomatriz_id!=0)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.carganotasdetalle') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        cursogrupo_id: cursogrupo_id,
                        cursomatriz_id: cursomatriz_id
                    },  
                    success:function (data) 
                    {
                        $('#notas-detalle').empty();
                        $.each(data.cursoestudiantes,function(index,ce)
                        {       
                            var misNotas='';
                            $.each(data.matrizdetalles_n,function(index,mdn)
                            {
                                if(typeof ce.notas[mdn] != 'undefined')
                                {
                                    let nota = Number(ce.notas[mdn].nota).toFixed(2);
                                    misNotas = misNotas+'<td>'+nota+'</td>';
                                    console.log(' '+ce.notas[mdn].nota);
                                }
                                else
                                {
                                    misNotas = misNotas+'<td>Sin Nota</td>';
                                }
                            });
                            console.log(' '+misNotas);
                            $('#notas-detalle').append('<tr><td>'+numerador+'</td><td id="'+ce.id+'" class="mialumno">'+ce.estudiante.estudiante+'</td>'+misNotas+'<td>'+Number(ce.promedio_matriz).toFixed(2)+'</td></tr>');
                            numerador = numerador +1;
                        });
                    }
                });
            }
            else
            {
                if(cursomatriz_id==0)
                    cargamatrizpromedios();
            }
        }

        //carga los alumnos con sus correspondintes promedios de todas las matrices
        function cargamatrizpromedios()
        {
            numerador = 1;
            var cursogrupo_id = $('#grupo').val();

            $.ajax(
            {
                type:'POST',
                url:"{{ route('ajax.cargamatrizpromedios') }}",
                data:
                {
                    "_token": "{{ csrf_token() }}",
                    cursogrupo_id: cursogrupo_id
                },  
                success:function (data) 
                {
                    $('#notas-cabecera').empty();
                    $('#notas-cabecera').append('<th scope="col">#</th><th scope="col">Alumno</th>');
                    $.each(data.cursomatrizs,function(index,cm)
                    {
                        $('#notas-cabecera').append('<th>'+cm.matriz+'<button type="button" class="btn btn-sm pdf" id="'+cm.id+'" title="Imprimir '+cm.matriz+'"><i id="'+cm.id+'" class="fa fa-print" aria-hidden="true"></i></button></th>');
                    });
                    $('#notas-cabecera').append('<th>Promedio final</th>');

                    $('#notas-detalle').empty();
                    $.each(data.cursoestudiantes,function(index,ce)
                    {
                        var promedios = '';
                        $.each(ce.promedios,function(index,promedio)
                        {
                            promedios = promedios+'<td>'+Number(promedio).toFixed(2)+'</td>';
                        });
                        $('#notas-detalle').append('<tr><td>'+numerador+'</td><td id="'+ce.id+'" class="mialumno">'+ce.estudiante.estudiante+'</td>'+promedios+'</tr>');
                        numerador = numerador +1;
                    });
                }
            });
        }

        function modificargrupo()
        {
            carganotasdetalle();
            cargadocente();
        }

        //carga la cabecera de la tabla
        function IrVistaAlumnoNotas(e)
        {
            $('#AlumnoID').attr('value', e.target.id);
            var semestre_id = $('#semestre').val();
            var curso_id = $('#curso').val();
            var form = document.getElementById("myForm");

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.verificarestadosemestre') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        semestre_id: semestre_id,
                        curso_id:curso_id
                    },  
                    success:function (data) 
                    {
                        if(data.escuelasemestre.estadosemestre == "Abierto"){
                            $( "form" ).submit();
                            form.method = "GET";
                            console.log(form);
                        }else{
                            console.log("NOPASO");
                            $('#mensajealerta').show(1000);
                        }
                    }
                });
                form.method = "GET";
        }

            //controla las salidas de form
        function controladorFormExcel(e)
        {
            //var form = $("form");
            $('#matrizid').attr('value', e.target.id);
            var form = document.getElementById("myForm");
            form.method = "POST";
            form.action = '/exportarlista';

            $( "form" ).submit();

            form.method = "GET";
            form.action = '/docente/notasalumno';
            console.log(form);
        }

        function controladorFormPdf(e)
        {
            //var form = $("form");
            $('#matrizid').attr('value', e.target.id);
            var form = document.getElementById("myForm");
            form.method = "POST";
            form.action = '/matrizgrupopdf';

            $( "form" ).submit();

            form.method = "GET";
            form.action = '/docente/notasalumno';
            console.log(form);
        }

        function actualizarBoton()
        {
            if($('#matriz').val() != '0')
            {
                $('.pdf[name="reporte"]').prop('id',$('#matriz').val());
                $('.pdf[name="reporte"]').prop('value','Reporte '+$('#matriz').find('option:selected').text());
                $('.pdf[name="reporte"]').show();
            }
            else
            {
                $('.pdf[name="reporte"]').hide();
            }
        }

        $('#semestre').on('change', cargacursosdocente);
        $('#curso').on('change', cargagruposdocente);
        $('#grupo').on('change',modificargrupo);
        $('#matriz').on('change',carganotascabecera);
        $('#matriz').on('change',actualizarBoton);
        $('body').on('click','.mialumno',IrVistaAlumnoNotas);
        $('body').on('click','#reporte',controladorFormExcel);
        $('body').on('click','.pdf',controladorFormPdf);

        });
    </script>
@endsection
