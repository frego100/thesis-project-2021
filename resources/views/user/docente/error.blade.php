@extends('layouts.docentelayout')

@section('content')

<br><br>
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">                                
                <div class="card-header">{{ __('Error docente') }}</div>
            
                    <div class="card-body">

                            <p>Actualmente usted no se encuentra a cargo de ningún curso.</p>

            </div>
        </div>
    </div>
</div>

@endsection