@extends('layouts.docentelayout')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Admin') }}</div>
                    <div class="card-body">
                    <form method="GET" action="{{ url('/admin/resultadoECompetencia') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">

                        <div class="form-group {{ $errors->has('semestre') ? 'has-error' : '' }}">
                            <label for="semestre" class="control-label">{{ 'Semestre' }}</label>
                            <select name="semestre" class="form-control" id="semestre" required>
                                @foreach ($semestres as $item)
                                    <option value="{{ $item->id }} "
                                        {{ isset($item->id) ? ($item->id == $selected_semestre ? 'selected' : '') : '' }}>
                                        {{ $item->anio }}-{{ $item->semestre }}</option>
                                @endforeach
                            </select>
                            <!--{!! $errors->first('semestre', '<p class="help-block">:message</p>') !!}-->
                        </div>

                        <div class="form-group {{ $errors->has('curso') ? 'has-error' : '' }}">
                            <label for="curso" class="control-label">{{ 'Curso' }}</label>
                            <select name="curso" class="form-control" id="curso" required>
                                @foreach ($cursos as $item)
                                    <option value="{{ $item->id }} "
                                        {{ isset($item->id) ? ($item->id == $selected_curso ? 'selected' : '') : '' }}>
                                        {{ $item->curso }}</option>
                                @endforeach
                            </select>
                            <!--{!! $errors->first('curso', '<p class="help-block">:message</p>') !!}-->
                        </div>

                        <div class="form-group {{ $errors->has('matriz') ? 'has-error' : '' }}">
                            <label for="matriz" class="control-label">{{ 'Matriz' }}</label>
                            <select name="matriz" class="form-control" id="matriz">
                                @foreach ($matrizs as $item)
                                    <option value="{{ $item->id }} "
                                        {{ isset($item->id) ? ($item->id == $selected_matriz ? 'selected' : '') : '' }}>
                                        {{ $item->matriz }}</option>
                                @endforeach
                            </select>
                            <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                        </div>

                        <br>
                            <div class="form-group" id="matriz">
                                <input class="btn boton-color" type="button" value="Agregar matriz" id="agregarMatriz">
                            </div>
                        <br>
                        <input type="hidden" id="reID" name="reID" value="0">

                        <table id="tablat"><tr><th>Matriz</th><th>Ponderacion</th><th>CursoSemestre</th><th>Semestre</th><th>Curso</th><th>FechaInicio</th><th>FechaFin</th><th>Acciones</th></tr></table>

                        <table class="table table-bordered" >
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" rowspan="2">#</th>
                                    <th scope="col" rowspan="2">Elemento de evaluacion</th>
                                    <th scope="col" rowspan="2">Criterio</th>
                                    <th scope="col">Ponderacion</th>
                                </tr>
                                <tr><td id="sumaPonderacion">Total: {{ $suma_ponderacion }}</td></tr>
                            </thead>
                            <tbody name="esquema" id="esquema">
                                @foreach ($cursoesquemas as $item)
                                    @php
                                    $contador=0;
                                    $criterio=0;
                                    foreach($matrizdetalle_general as $cursoesquema){
                                        if($item->id == $cursoesquema->cursoesquema->id){
                                            $criterio=$cursoesquema->criterio_id;
                                            break;
                                        }
                                    }
                                    @endphp
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td value = {{$item->matrizdetalle_id}} cursoesq = {{$item->id}}>{{ $item->detalle }}</td>
                                        <td><select name="criterio" class="form-control criterio-tabla">
                                            <option value=0>SIN CRITERIO</option>
                                            @foreach ($criterios as $itemesquema)
                                                <option value="{{ $itemesquema->id }}"
                                                    {{ isset($itemesquema->id) ? ($itemesquema->id == $criterio ? 'selected' : '') : '' }}>
                                                    {{ $itemesquema->codigo }}</option>
                                            @endforeach
                                        </select>
                                        </td>
                                        <td value = "{{$item->id}}" class="miPonderacion">{{ $item->ponderacion }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#tablat').hide();

    var numerador = {{ sizeof($cursoesquemas) }} + 1;
    var enCambio = 0;
    let suma_ponderacion = 0;

    $(document).ready(function ()
    {
        var editar_cursoSemestre = 0;

        function agregarMatrizVista()
        {
            $('#agregarMatriz').hide();
            $('#tablat').show();

            editar_cursoSemestre = 0;
            //editar_plan = 0;
            //editar_curso = 0;

            $('#tablat').append('<tr><td><input type="text" id="nuevaDesMatriz"></td><td><input type="number" step="0.01" id="nuevaPonderacion"></td><td><select id="nuevoSemestre" class="form-control"></select></td><td><select id="nuevoCurso" class="form-control"></select></td><td><input type="date" id="nuevaFechaInicio"></td><td><input type="date" id="nuevaFechaFin"></td><br><td><input type="button" id="guardarMatriz" value="Guardar"></td><td><input type="button" id="noGuardarMatriz" value="Cancelar"></td></tr>');

            cargasemestre();
                    
        };

        function guardarMatrizVista()
        {
            var matriz = $('#nuevaDesMatriz').val();
            var ponderacion = $('#nuevaPonderacion').val();
            var cursosemestre_id = $('#nuevoCursoSemestre').val();
            var semestre_id = $('#nuevoSemestre').val();
            var curso_id = $('#nuevoCurso').val();
            var fechacalificacioninicio = $('#nuevaFechaInicio').val();
            var fechacalificacionfin = $('#nuevaFechaFin').val();

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.insertarmatriz') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        matriz:matriz,
                        ponderacion:ponderacion,
                        cursosemestre_id:cursosemestre_id,
                        semestre_id:semestre_id,
                        curso_id:curso_id,
                        fechacalificacioninicio:fechacalificacioninicio,
                        fechacalificacionfin:fechacalificacionfin
                    },
                    success:function() 
                    {
                        $('#nuevaDesMatriz').remove();
                        $('#nuevaPonderacion').remove();
                        $('#nuevoCursoSemestre').remove();
                        $('#nuevoSemestre').remove();
                        $('#nuevoCurso').remove();
                        $('#nuevaFechaInicio').remove();
                        $('#nuevaFechaFin').remove();
                        $('#guardarMatriz').remove();
                        $('#noGuardarMatriz').remove();
                        $('#tablat').hide();
                        cargacursosdocente();
                        cargamatriz();
                    }                   
                });

            $('#agregarMatriz').show();

        };

        function cancelarGuardarMatrizVista(e)
        {
            $('#nuevaDesMatriz').remove();
            $('#nuevaPonderacion').remove();
            $('#nuevoCursoSemestre').remove();
            $('#nuevoSemestre').remove();
            $('#nuevoCurso').remove();
            $('#nuevaFechaInicio').remove();
            $('#nuevaFechaFin').remove();
            $('#guardarMatriz').remove();
            $('#noGuardarMatriz').remove();
            $('#tablat').hide();
            $('#agregarMatriz').show();
        };

        function cargacursosdocente() 
        {
            var semestre_id = $('#semestre').val();
            if(semestre_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cursosdocenteespecificomatriz') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        semestre_id: semestre_id
                    },  
                    success:function (data) 
                    {
                        $('#curso').empty();
                        $.each(data.cursos,function(index,curso)
                        {         
                            $('#curso').append('<option value="'+curso.id+'">'+curso.curso+'</option>');
                        });
                        cargamatriz();
                    }
                });
            }
            else
            {
                $('#escuela').empty();
                cargamatriz();
                cargacriterios();
                cargo = "Sin cargo";
            }

        };

        function cargagruposdocente() 
        {
            var curso_id = $('#curso').val();
            var semestre_id = $('#semestre').val();
            if(curso_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.gruposdocenteespecifico') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        curso_id: curso_id,
                        semestre_id: semestre_id
                    },  
                    success:function (data) 
                    {
                        $('#grupo').empty();
                        $.each(data.grupos,function(index,grupo)
                        {       
                            $('#grupo').append('<option value="'+grupo.id+'">'+grupo.grupo+'</option>');
                        });
                        cargamatriz();
                    }
                });
            }
            else
            {
                $('#grupo').empty();
            }

        };

        function cargamatriz() 
        {
            var curso_id = $('#curso').val();
            var semestre_id = $('#semestre').val();
            if(curso_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cargamatriz') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        curso_id: curso_id,
                        semestre_id: semestre_id
                    },  
                    success:function (data) 
                    {
                        $('#matriz').empty();
                        $.each(data.matrizs,function(index,matriz)
                        {       
                            $('#matriz').append('<option value="'+matriz.id+'">'+matriz.matriz+'</option>');
                        });

                        cargacriterios();
                        cargaesquemas();
                    }
                });
            }
            else
            {
                $('#grupo').empty();
            }

        };

        function cargasemestre() 
        {

            $.ajax(
            {
                type:'POST',
                url:"{{ route('ajax.cargasemestredocente') }}",
                data:
                {
                    "_token": "{{ csrf_token() }}"
                },  
                success:function (data) 
                {
                    $.each(data.semestres,function(index,semestre)
                    {       
                        $('#nuevoSemestre').append('<option value="'+semestre.id+'">'+semestre.anio + "-" + semestre.semestre +'</option>');
                    });
                    cargacurso();
                }
            });
        };

        function cargacurso() 
        {
            var semestre_id = $('#nuevoSemestre').val();
            console.log(semestre_id);

            $.ajax(
            {
                type:'POST',
                url:"{{ route('ajax.cursosdocenteespecificomatriz') }}",
                data:
                {
                    "_token": "{{ csrf_token() }}",
                    semestre_id:semestre_id,
                },  
                success:function (data) 
                {
                    $('#nuevoCurso').empty();
                    $.each(data.cursos,function(index,curso)
                    {       
                        $('#nuevoCurso').append('<option value="'+curso.id+'">'+curso.curso +'</option>');
                    });
                }
            });
        };

        function cargacriterios() 
        {
            var matriz_id = $('#matriz').val();
            var curso_id = $('#curso').val();

            $.ajax(
            {
                type:'POST',
                url:"{{ route('ajax.cargaresultados') }}",
                data:
                {
                    "_token": "{{ csrf_token() }}",
                    matriz_id:matriz_id,
                    curso_id:curso_id
                },  
                success:function (data) 
                {
                    $('.criterio-tabla').empty();
                    $('.criterio-tabla').append('<option value=0>SIN CRITERIO</option>');
                    $.each(data.criterios,function(index,criterio)
                    {       
                        $('.criterio-tabla').append('<option value="'+criterio.id+'">'+criterio.codigo +'</option>');
                    });
                }
            });
        };

        function cargaesquemas()
        { 
            numerador = 1;
            var matriz_id = $('#matriz').val();
            var curso_id = $('#curso').val();
            var semestre_id = $('#semestre').val();

            suma_ponderacion = 0;
            if(matriz_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cargaesquemas') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        matriz_id: matriz_id,
                        semestre_id:semestre_id,
                        curso_id:curso_id
                    },
                    success:function (data)
                    {
                        $('#esquema').empty();
                        /*
                        $('#esquema').append('<thead class="thead-light"><tr><th scope="col">#</th><th scope="col">Elemento de evaluacion</th><th scope="col">Criterio</th><th scope="col">Ponderacion</th></tr></thead><tbody>');*/
                        
                        $.each(data.esquemas,function(index,esquema)
                        {    
                            $('#esquema').append('<tr><td>'+numerador+'</td><td value= '+ esquema.matrizdetalle_id +' cursoesq = '+ esquema.id +'>'+esquema.detalle+'</td><td><select name="criterio" id='+ esquema.id + ' class="form-control criterio-tabla" ></select></td><td class="miPonderacion">'+esquema.ponderacion+'</td></tr>');
                            console.log("mi suma ponderacion: "+suma_ponderacion);

                            criterio_selected=0;
                            $.each(data.matrizdetalle_general,function(index,matrizdetalle)
                            {
                                if(esquema.id == matrizdetalle.cursoesquema.id)
                                {
                                    criterio_selected=matrizdetalle.criterio_id;
                                    suma_ponderacion = suma_ponderacion + parseFloat(esquema.ponderacion);
                                }
                            });

                            $('#'+esquema.id).empty();
                            $('#'+esquema.id).append('<option value=0>SIN CRITERIO</option>');
                            $.each(data.criterios,function(index,criterio)
                            {
                                if(criterio.id == criterio_selected){
                                    $('#'+esquema.id).append('<option value="'+criterio.id+'" selected>'+criterio.codigo+'</option>');
                                }else{
                                    $('#'+esquema.id).append('<option value="'+criterio.id+'">'+criterio.codigo+'</option>');
                                }
                            });
                            $('#sumaPonderacion').empty();
                            $('#sumaPonderacion').append('Total: '+suma_ponderacion);
                            numerador+=1;

                        });

                        $('#esquema').append('</tbody>');

                    }
                });
            }
            else
            {
                $('#esquema').empty();
                $('#esquema').append('<thead class="thead-light"><tr><th scope="col">#</th><th scope="col">Elemento de evaluacion</th><th scope="col">Resultado Estudiante</th><th scope="col">Ponderacion</th></tr></thead><tbody>');
            }
        }

        function editarMatriz(e)
        {

            var matriz_id = $('#matriz').val();
            var datosPrevios = $(e.target ).closest( "tr" ).html();
            var select = (e.target);

            var myTr = $(e.target ).closest( "tr" );
            
            var matrizdet_id=myTr.find('td:eq(1)').attr('value');
            var cursoesquema=myTr.find('td:eq(1)').attr('cursoesq');
            var esq = myTr.find('td:eq(1)').text();
            var pond = myTr.find('td:eq(3)').text();

            var criterio_valor=$('option:selected',select).attr('value');

            console.log('evento ' +matrizdet_id);
            console.log('valor ' +cursoesquema);
            console.log(pond);

            $.ajax(
            {
                type:'POST',
                url:"{{ route('ajax.editaresquema') }}",
                data:
                {
                    "_token": "{{ csrf_token() }}",
                    matrizdet_id:matrizdet_id,
                    cursoesquema:cursoesquema,
                    matriz_id:matriz_id,
                    criterio_valor:criterio_valor,
                    pond:pond
                },  
                success:function (data) 
                {
                    cargaesquemas();
                }
            });


        };

        function activarPonderacion(e)
        {
            console.log("tag de doble click: "+e.target.nodeName);

            var myTr = $(e.target ).closest( "tr" );
            var criterio=myTr.find('td:eq(2)');
            var matrizdet_id=myTr.find('td:eq(1)').attr('value');
            var cursoesquema=myTr.find('td:eq(1)').attr('cursoesq');

            var matriz_id = $('#matriz').val();

            var criterio_valor=$('option:selected',criterio).attr('value');

            console.log("criterio: " + cursoesquema);

            if(criterio_valor!=0){
                if(enCambio == 0 && e.target.nodeName == "TD")
                {
                    enCambio = 1;
                    var tdText = $(this).text();
                    console.log("prueba tdtext " + tdText);
                    var estudiante_matriz_id = e.target.id;

                    $(e.target ).empty();

                    if(criterio_valor != 0)
                    {
                        $(e.target).append('<input type="number" step="0.01" id="'+criterio_valor+'" value="'+tdText+'">');
                    }
                    else
                    {
                        $(e.target).append('<input type="number" step="0.01" id="0" value="0.0">');
                    }
                }
                if(enCambio == 1 && e.target.nodeName == "INPUT")
                {
                    var myTr = $(e.target ).closest( "tr" );


                    var pond = $(e.target).val();

                    console.log("prueba pond " + pond);
                    
                    $.ajax(
                    {
                        type:'POST',
                        url:"{{ route('ajax.editaresquema') }}",
                        data:
                        {
                            "_token": "{{ csrf_token() }}",
                            matrizdet_id:matrizdet_id,
                            cursoesquema:cursoesquema,
                            matriz_id:matriz_id,
                            criterio_valor:criterio_valor,
                            pond:pond
                        },
                        success:function() 
                        {
                            cargaesquemas();
                        }
                        
                    });
                    
                    enCambio = 0;
                }
            }

            
        }

        function verificarcargo()
        {
            if(cargo == 'Coordinador')
            {
                $('#irEsquemas').show();
            }
            else
            {
                $('#irEsquemas').hide();
            }
            $('#cargo').empty();
            $('#cargo').text("Cargo: "+cargo);
        }

        $('#matriz').on('change',cargacriterios);
        $('#semestre').on('change', cargacursosdocente);
        $('#curso').on('change', cargamatriz);
        $('body').on('change','#nuevoSemestre',cargacurso);

        $('#agregarMatriz').on('click', agregarMatrizVista);
        $('body').on('click','#guardarMatriz',guardarMatrizVista);
        $('body').on('click','#noGuardarMatriz',cancelarGuardarMatrizVista);
        $('body').on('change','#matriz',cargaesquemas);

        $('body').on('change','.criterio-tabla',editarMatriz);
        $('body').on('dblclick','.miPonderacion',activarPonderacion);

    });


</script>
@endsection
