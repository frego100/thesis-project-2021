@extends('layouts.docentelayout')

@section('content')

<br><br>
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">                                
                <div class="card-header">{{ __('Docentes') }}</div>
            
                    <div class="card-body">
                                                                                                 
                            <br>

                            <div class="form-group">
                                <input class="btn btn-primary" type="submit" value="Agregar Docentes"
                                    formaction="{{ url('/alumno/agregar') }}">
                            </div>
                            <br>
                           

                            <table class="table table-bordered" name="resultadosECompetencia" id="resultadosECompetencia">
                            <thead class="thead-light">
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Codigo</th>
                                <th scope="col">Apellidos y Nombres</th>
                                <th scope="col">Email</th>
                                <th scope="col">Acciones</th>                                                           
                                </tr>
                            </thead>
                            <tbody>
                                
                                    <tr>
                                        <td>1</td>
                                        <td>20210982</td>
                                        <td>Victor Cornejo</td>
                                        <td>vcornejo@gmail.com</td>
                                        <td><input type="button" class="editar" value="Editar"><input type="button" class="eliminar" value="Eliminar"></td>
                                    </tr>
                                
                            </tbody>
                            </table>        
                
            </div>
        </div>
    </div>
</div>

@endsection