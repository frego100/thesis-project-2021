@extends('layouts.docentelayout')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        <div class="row justify-content-around">
                            <div class="col">Matrices de Evaluación</div>
                            <div class="col-1">
                                <a target="_blank" href="{{ asset('files/files_Docente/docente-matriz.pdf') }}"
                                    class="btn boton-color btn-sm" title="Info">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="GET" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            <div class="form-group {{ $errors->has('semestre') ? 'has-error' : '' }}">
                                <label for="semestre" class="control-label">{{ 'Semestre' }}</label>
                                <select name="semestre" class="form-control" id="semestre">
                                    @foreach ($semestres as $item)
                                        <option value="{{ $item->id }} "
                                            {{ isset($item->id) ? ($item->id == $selected_semestre ? 'selected' : '') : '' }}>
                                            {{ $item->anio }}-{{ $item->semestre }}</option>
                                    @endforeach
                                </select>
                                <!--{!! $errors->first('semestre', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <div class="form-group {{ $errors->has('curso') ? 'has-error' : '' }}">
                                <label for="curso" class="control-label">{{ 'Curso' }}</label>
                                <select name="curso" class="form-control" id="curso">
                                    @foreach ($cursos as $item)
                                        <option value="{{ $item->id }}"{{ isset($item->id) ? ($item->id == $selected_curso ? 'selected' : '') : '' }}>{{ $item->curso }}</option>
                                    @endforeach
                                </select>
                                <!--{!! $errors->first('curso', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <br>
                            <div class="content-tabla">
                                <table class="display" name="matrizs" id="matrizs">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Matriz</th>
                                            <th scope="col">Ponderacion</th>
                                            <th scope="col">Fecha inicio</th>
                                            <th scope="col">Fecha fin</th>
                                            <th colspan ="2">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($cursomatrizs as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->matriz }}</td>
                                                <td>{{ $item->ponderacion }}</td>
                                                <td>{{ $item->fechacalificacioninicio }}</td>
                                                <td>{{ $item->fechacalificacionfin }}</td>
                                                <td><input type="button" id="{{ $item->id }}" class="btn btn-primary btn-sm editar" value="Editar"></td><td><input type="button" id="{{ $item->id }}" class="btn btn-danger btn-sm eliminar" value="Eliminar"></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        <br>
                        <div class="form-group">
                            <input class="btn boton-color" type="button" value="Agregar" id="agregarMatriz">
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var datosPrevios;
        var editarId;

        var numerador = {{ sizeof($cursomatrizs) }} + 1;
        var mi_curso_semestre = {{ $cursosemestres->id }};

        $(document).ready(function() {

        function cargacursosdocente() 
        {
            var semestre_id = $('#semestre').val();
            if(semestre_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cursosdocenteespecifico') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        semestre_id: semestre_id,
                        coordinador: '1'
                    },  
                    success:function (data) 
                    {
                        $('#curso').empty();
                        $.each(data.cursos,function(index,curso)
                        {         
                            $('#curso').append('<option value="'+curso.id+'">'+curso.curso+'</option>');
                        });
                        cargamatrizsdocente();
                    }
                });
            }
            else
            {
                $('#escuela').empty();
                cargamatrizsdocente();
            }

        };

        function cargamatrizsdocente() 
        {
            numerador = 1;
            var semestre_id = $('#semestre').val();
            var curso_id = $('#curso').val();
            if(curso_id)
            {
                console.log("Se inicia el ajax cargamatrizsdocente");
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.matrizcurso') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        semestre_id: semestre_id,
                        curso_id: curso_id
                    },  
                    success:function (data) 
                    {
                        mi_curso_semestre = data.cursosemestre;

                        $('#matrizs').empty();
                        console.log("Se corraron los datos");
                        $('#matrizs').append('<thead class="thead-light"><tr><th scope="col">#</th><th scope="col">Matriz</th><th scope="col">Ponderacion</th><th scope="col">Fecha inicio</th><th scope="col">Fecha fin</th><th colspan ="2">Acciones</th></tr></thead><tbody>');
                        

                        $.each(data.cursomatrizs,function(index,matriz)
                        {       
                            $('#matrizs').append('<tr><td>'+numerador+'</td><td>'+matriz.matriz+'</td><td>'+matriz.ponderacion+'</td><td>'+matriz.fechacalificacioninicio+'</td><td>'+matriz.fechacalificacionfin+'</td><td><input type="button" id="'+matriz.id+'" class="btn btn-primary btn-sm editar" value="Editar"></td><td><input type="button" id="'+matriz.id+'" class="btn btn-danger btn-sm eliminar" value="Eliminar"></td></tr>');

                            numerador += 1;
                        });
                        $('#matrizs').append('</tbody>');
                    }
                });
            }

        };

        //comienza

        function agregarMatrizVista()
        {
            $('.editar').hide();
            $('.eliminar').hide();
            $('#agregarMatriz').hide();

            //editar_cursoTipo = 0;
            //editar_plan = 0;
            //editar_curso = 0;

            $('#matrizs').append('<tr><td>'+numerador+'</td><td><input type="text" id="nuevaMatriz"><td><input type="number" minlength="0" maxlength="100" step="0.01"  id="nuevaPonderacion"></td><td><input type="date" id="nuevaFechaInico"></td><td><input type="date" id="nuevaFechaFin"></td><td><input type="button" id="guardarMatriz" value="Guardar"></td><td><input type="button" id="noGuardarMatriz" value="Cancelar"></td>');

            numerador +=1;            
        };

        function guardarMatrizVista()
        {
            var matriz = $('#nuevaMatriz').val();
            var ponderacion = $('#nuevaPonderacion').val();
            var fechainicio = $('#nuevaFechaInico').val();
            var fechafin = $('#nuevaFechaFin').val();

            var cursosemestre_id = mi_curso_semestre;

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.insertarcursomatriz') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        matriz:matriz,
                        ponderacion:ponderacion,
                        fechainicio:fechainicio,
                        fechafin:fechafin,
                        cursosemestre_id:cursosemestre_id
                    },
                    success:function() 
                    {
                        cargamatrizsdocente();
                    }
                    
                });

            $('#agregarMatriz').show();

        };

        function cancelarGuardarMatrizVista(e)
        {
            $(e.target ).closest( "tr" ).remove();
            $('#agregarMatriz').show();
            $('.editar').show();
            $('.eliminar').show();
            numerador -=1;
        };

        function editarMatrizVista(e)
        {
            $('.editar').hide();
            $('.eliminar').hide();
            $('#agregarMatriz').hide();

            datosPrevios = $(e.target ).closest( "tr" ).html();
            editarId = e.target.id;

            var myTr = $(e.target ).closest( "tr" );
            
            var matriz = myTr.find('td:eq(1)').text();
            var ponderacion = myTr.find('td:eq(2)').text();
            var fechainicio = myTr.find('td:eq(3)').text();
            var fechafin = myTr.find('td:eq(4)').text();

            $(e.target ).closest( "td" ).append('<input type="button" id="guardarMatrizEditado" value="Guardar"><input type="button" id="noGuardarMatrizEditado" class="noEditar" value="Cancelar">');

            myTr.find('td:eq(1)').empty();
            myTr.find('td:eq(1)').append('<input type="text" id="matrizEditado" value="'+matriz+'">');

            myTr.find('td:eq(2)').empty();
            myTr.find('td:eq(2)').append('<input type="number" minlength="0" maxlength="100" step="0.01" id="ponderacionEditado" value="'+ponderacion+'">');

            myTr.find('td:eq(3)').empty();
            myTr.find('td:eq(3)').append('<input type="date" id="fechainicioEditado" value="'+fechainicio+'">');

            myTr.find('td:eq(4)').empty();
            myTr.find('td:eq(4)').append('<input type="date" id="fechafinEditado" value="'+fechafin+'">');
        };

        function guardarEditarMatrizVista(e)
        {
            var id = editarId;
            var matriz = $('#matrizEditado').val();
            var ponderacion = $('#ponderacionEditado').val();
            var fechainicio = $('#fechainicioEditado').val();
            var fechafin = $('#fechafinEditado').val();

            $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.editarcursomatriz') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        id:id,
                        matriz:matriz,
                        ponderacion:ponderacion,
                        fechainicio:fechainicio,
                        fechafin:fechafin
                    },
                    success:function() 
                    {
                        cargamatrizsdocente();
                        $('#agregarMatriz').show();
                        //poner notificacion de editado exitoso
                    }
                    
                });

            
        };

        function cancelarEditarMatrizVista(e)
        {
            var myTr = $(e.target ).closest( "tr" );
            myTr.empty();
            myTr.append(datosPrevios);
            $('.editar').show();
            $('.eliminar').show();
            $('#agregarMatriz').show();
        };

        function eliminarMatrizVista(e)
        {
            var myId = e.target.id;
            $.ajax(
            {
                type:'POST',
                url:"{{ route('ajax.eliminarcursomatriz') }}",
                data:
                {
                    "_token": "{{ csrf_token() }}",
                    id:myId
                },
                success:function(resp) 
                {
                    $('.alert').remove();
                    if(resp != 0){
                        $('.card').prepend('<div class="alert alert-danger col-md-4">No se puede eliminar matriz porque tiene relacion con '+ resp +' registros</div>');
                        $('.alert').fadeOut(7000);
                    }else{
                        $('.card').prepend('<div class="alert alert-success col-md-4">Se elimino exitosamente</div>');
                        $('.alert').fadeOut(7000);
                    }
                    cargamatrizsdocente();
                    //Notificacion de eliminacion                     
                }    
            });
        };

        $('#semestre').on('change', cargacursosdocente);
        $('#curso').on('change', cargamatrizsdocente);
        $('#agregarMatriz').on('click', agregarMatrizVista);
        $('body').on('click','#guardarMatriz',guardarMatrizVista);
        $('body').on('click','#noGuardarMatriz',cancelarGuardarMatrizVista);

        $('body').on('click','#guardarMatrizEditado',guardarEditarMatrizVista);
        $('body').on('click','.editar',editarMatrizVista);
        $('body').on('click','.noEditar',cancelarEditarMatrizVista);

        $('body').on('click','.eliminar',eliminarMatrizVista);

        });
    </script>

@endsection
