@extends('layouts.docentelayout')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Notas Alumno</div>
                    <div class="card-body">
                        <form method="POST" action=" {{ url('/matrizalumnopdf') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                             <div class="form-group {{ $errors->has('cargo') ? 'has-error' : '' }}">
                                <label for="cargo" class="control-label" id="cargo">{{ 'Cargo:' }} {{ $cargo }}</label>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <div class="form-group {{ $errors->has('docente') ? 'has-error' : '' }}">
                                <label for="docente" class="control-label" id="docente">{{ 'Profesor a cargo:' }} {{ $docente[0]->nombre }}</label>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <div class="form-group {{ $errors->has('semestre') ? 'has-error' : '' }}">
                                <label for="semestre" class="control-label">{{ 'Semestre' }}</label>
                                <select name="semestre" class="form-control" id="semestre">
                                    @foreach ($semestres as $item)
                                        <option value="{{ $item->id }} "
                                            {{ isset($item->id) ? ($item->id == $selected_semestre ? 'selected' : '') : '' }}>
                                            {{ $item->anio }}-{{ $item->semestre }}</option>
                                    @endforeach
                                </select>
                                <!--{!! $errors->first('semestre', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <div class="form-group {{ $errors->has('curso') ? 'has-error' : '' }}">
                                <label for="curso" class="control-label">{{ 'Curso' }}</label>
                                <select name="curso" class="form-control" id="curso">
                                    @foreach ($cursos as $item)
                                        <option value="{{ $item->id }}" {{ isset($item->id) ? ($item->id == $selected_curso ? 'selected' : '') : '' }}>{{ $item->curso }}</option>
                                    @endforeach
                                </select>
                                <!--{!! $errors->first('curso', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <div class="form-group {{ $errors->has('grupo') ? 'has-error' : '' }}">
                                <label for="grupo" class="control-label">{{ 'Grupo' }}</label>
                                <select name="grupo" class="form-control" id="grupo">
                                    @foreach ($cursogrupos as $item)
                                        <option value="{{ $item->id }}" {{ isset($item->id) ? ($item->id == $selected_grupo ? 'selected' : '') : '' }}>{{ $item->grupo }}</option>
                                    @endforeach
                                </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>

                            <div class="form-group {{ $errors->has('alumno') ? 'has-error' : '' }}">
                                <label for="alumno" class="control-label">{{ 'Alumno' }}</label>
                                <select name="alumno" class="form-control" id="alumno">
                                    @foreach ($cursoestudiantes as $item)
                                        <option value="{{ $item->id }}" {{ isset($item->id) ? ($item->id == $selected_alumno ? 'selected' : '') : '' }}>{{ $item->estudiante->estudiante }}</option>
                                    @endforeach

                                    
                                </select>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>

                            <div id="datosMatriz" class="form-group {{ $errors->has('matriz') ? 'has-error' : '' }}">
                                <label for="matriz" class="control-label">{{ 'Matriz' }}</label>
                                <select name="matriz" class="form-control" id="matriz">
                                    @foreach ($cursomatrizs as $item)
                                        <option value="{{ $item->id }}">{{ $item->matriz }}</option>
                                    @endforeach
                                </select>
                                <br>
                                <table class="table">
                                    <tbody>
                                        <tr><td style="width: 30%;">Titulo</td><td colspan="2"><input name="titulo" class="dato" type="text" value="{{ isset($datomatrizs[0]) ? $datomatrizs[0]->titulo : '' }}" style="width: 70%;" readonly></td></tr>

                                        <tr><td style="width: 30%;">Resumen de la Propuesta</td><td colspan="2"><textarea name="resumen" class="dato" rows="5" style="width: 70%; resize:none;" readonly>{{ isset($datomatrizs[0]) ? $datomatrizs[0]->resumen : '' }}</textarea></td>

                                        <tr>
                                            <td style="width: 30%;">Pertinencia a la carrera</td>
                                            <td><input id="pertinencia1" class="dato" type="radio" name="pertinencia" value="1" {{ isset($datomatrizs[0]) ? ( $datomatrizs[0]->pertinencia == "1" ? 'checked' : '' ) : '' }}><label for="pertinencia1">&nbsp;Es pertinente</label></td>
                                            <td><input id="pertinencia0" class="dato" type="radio" name="pertinencia" value="0" {{ isset($datomatrizs[0]) ? ( $datomatrizs[0]->pertinencia == "0" ? 'checked' : '' ) : '' }}><label for="pertinencia0">&nbspNo es pertinente</label></td></tr>

                                        <tr>
                                            <td style="width: 30%;">Trivialidad</td>
                                            <td><input id="trivial0" class="dato" type="radio" name="trivial" value="0" {{ isset($datomatrizs[0]) ? ( $datomatrizs[0]->trivial == "0" ? 'checked' : '' ) : '' }}><label for="trivial0">&nbspNo es trivial</label></td>
                                            <td><input id="trivial1" class="dato" type="radio" name="trivial" value="1" {{ isset($datomatrizs[0]) ? ( $datomatrizs[0]->trivial == "1" ? 'checked' : '' ) : '' }}><label for="trivial1">&nbsp;Es trivial</label></td></tr>
                                        @php
                                        error_log("DATO MATRIZS desde la vista");
                                        error_log($datomatrizs);
                                        @endphp

                                    </tbody>
                                </table>
                                <!--{!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}-->
                            </div>
                            <br>

                            <table class="table table-bordered" name="notas" id="notas">
                            <thead class="thead-light" id="matrices">
                                <tr>
                                    <th scope="col" rowspan="2">#</th>
                                    <th scope="col" rowspan="2">Esquemas</th>
                                    @foreach ($cursomatrizs as $item)
                                    <th scope="col">{{ $item->matriz }}<button type="button" class="btn btn-sm pdf" id="{{ $item->id }}" title="Imprimir {{ $item->matriz }}"><i id="{{ $item->id }}" class="fa fa-print" aria-hidden="true"></i></button></th>
                                    @endforeach
                                </tr>
                                <tr id="promedios">
                                    @foreach ($promedios as $promedio)
                                    <td scope="col">Prom: {{ number_format(round($promedio,2),2) }}</td>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody id="esquema-notas">
                                @php
                                    $contador_f = 0;
                                    $contador_c = 0;
                                @endphp
                                @foreach ($cursoesquemas as $ce)
                                    @php
                                        $contador_c = 0;
                                    @endphp
                                <tr>
                                    <td>{{ $loop->iteration }}</td><td>{{ $ce->detalle }}</td>
                                    @foreach ($cursomatrizs_n as $cm)
                                        <td id="{{ isset($ce->notas[$cm][0]->nota) ? $ce->notas[$cm][0]->id : '0' }}"  class="{{ isset($ce->notas[$cm])  ? 'miNota' : 'noCalificar' }}" mf="{{ $contador_f }}" mc="{{ $contador_c }}">{{ isset($ce->notas[$cm]) ? ( isset($ce->notas[$cm][0]->nota) ? number_format(round($ce->notas[$cm][0]->nota, 2),2) : 'Sin nota') : '' }}</td>
                                        @php
                                            $contador_c++;
                                        @endphp

                                    @endforeach

                                    @php
                                        $contador_f++;
                                    @endphp
                                </tr>
                                @endforeach
                                @php
                                    $filas = sizeof($cursomatrizs_n)+1;
                                @endphp
                                <tr><td colspan="{{ $filas }}" style="text-align: right;">PROMEDIO FINAL&nbsp&nbsp</td><td>{{ number_format(round($promedio_final,2),2) }}</td></tr>
                            </tbody>
                        </table>

                        <input type="hidden" value="0" id="matrizid" name="matrizid">
                        <input type="hidden" value="{{ isset($datomatrizs[0]) ? $datomatrizs[0]->id : 0 }}" id="datomatrizid" name="datomatrizid">

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var numerador;

        var mf;
        var mc;
        var nota_estado;

        var enCambio = 0;
        var promedioAlumno = 0;
        var cargo = "{{ $cargo }}";
        var matriz_n = {{ sizeof($cursomatrizs) }};
        var alumnos_n = {{ sizeof($cursoestudiantes) }};

        var arr_esquemas_n = {!! json_encode($cursoesquemas_n, JSON_HEX_TAG) !!};
        var arr_matriz_n = {!! json_encode($cursomatrizs_n, JSON_HEX_TAG) !!};

        $(document).ready(function() {

        function cargacursosdocente() 
        {
            var semestre_id = $('#semestre').val();
            if(semestre_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cursosdocenteespecifico') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        semestre_id: semestre_id
                    },  
                    success:function (data) 
                    {
                        $('#curso').empty();
                        $.each(data.cursos,function(index,curso)
                        {         
                            $('#curso').append('<option value="'+curso.id+'">'+curso.curso+'</option>');
                        });
                        cargagruposdocente();
                    }
                });
            }
            else
            {
                $('#escuela').empty();
                cargagruposdocente();
            }

        };

        function cargagruposdocente() 
        {
            var curso_id = $('#curso').val();
            var semestre_id = $('#semestre').val();
            if(curso_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.gruposdocenteespecifico') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        curso_id: curso_id,
                        semestre_id: semestre_id
                    },  
                    success:function (data) 
                    {
                        $('#grupo').empty();
                        $.each(data.grupos,function(index,grupo)
                        {       
                            $('#grupo').append('<option value="'+grupo.id+'">'+grupo.grupo+'</option>');
                        });
                        cargo = data.cargo;
                        modificaciongrupos();
                        cargacursomatriz();
                    }
                });
            }
            else
            {
                $('#grupo').empty();
                
                modificaciongrupos();
            }

        };

        function verificarcargo()
        {
            if(cargo == 'Coordinador')
            {
                $('#irMatrices').show();
            }
            else
            {
                $('#irMatrices').hide();
            }
            $('#cargo').empty();
            $('#cargo').text("Cargo: "+cargo);
        }

        function modificaciongrupos()
        {
            cargamatrizcabecera();
            cargadocente();
            cargaalumnos();
            verificarcargo();
        }

        function cargadocente() 
        {
            var grupo_id = $('#grupo').val();
            if(grupo_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.docenteespecifico') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        grupo_id: grupo_id
                    },  
                    success:function (data) 
                    {
                        $('#docente').empty();
                        $('#docente').text("Profesor a cargo: "+data.docente.nombre);
                    }
                });
            }
            else
            {
                $('#grupo').empty();
            }

        };

        function cargaalumnos() 
        {
            var grupo_id = $('#grupo').val();
            if(grupo_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.acalumno') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        grupo_id: grupo_id
                    },  
                    success:function (data) 
                    {
                        $('#alumno').empty();
                        $.each(data.grupoalumnos,function(index,alumno)
                        {       
                            $('#alumno').append('<option value="'+alumno.id+'">'+alumno.estudiante.estudiante+'</option>');
                        });
                        alumnos_n = data.grupoalumnos.length;
                        console.log("Cantidad de alumnos: "+alumnos_n);
                        carganotasalumno();
                    }
                });
            }
            else
            {
                $('#grupo').empty();
                carganotasalumno();
            }

        };

        function cargacursomatriz()
        {
            var grupo_id = $('#grupo').val();
            if(grupo_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cargacursomatriz') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        grupo_id: grupo_id
                    },  
                    success:function (data) 
                    {
                        $('#matriz').empty();
                        $.each(data.cursomatrizs,function(index,matriz)
                        {       
                            $('#matriz').append('<option value="'+matriz.id+'">'+matriz.matriz+'</option>');
                        });
                    }
                });
            }
            else
            {
                $('#matriz').empty();
            }
        }

        function cargadatosmatriz()
        {
            console.log("Se entro a caga de dato matriz");
            var curso_matriz_id = $('#matriz').val();
            var curso_estudiante_id = $('#alumno').val();

            if(curso_matriz_id && curso_estudiante_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cargadatomatriz') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        curso_matriz_id: curso_matriz_id,
                        curso_estudiante_id: curso_estudiante_id
                    },  
                    success:function (data) 
                    {
                        console.log("Ajax exitoso de carga dato matriz");
                        vaciarDatosMAtriz();
                        if(data.datomatriz.length > 0)
                        {
                            console.log("Mi titulo: "+data.datomatriz[0].titulo);
                            $('.dato[name="titulo"]').prop('value',data.datomatriz[0].titulo);
                            $('.dato[name="resumen"]').prop('value',data.datomatriz[0].resumen);
                            if(data.datomatriz[0].trivial == '1' || data.datomatriz[0].trivial == '0')
                            {
                                $('#trivial'+data.datomatriz[0].trivial).prop('checked',true);
                            }
                            if(data.datomatriz[0].pertinencia == '1' || data.datomatriz[0].pertinencia == '0')
                            {
                                $('#pertinencia'+data.datomatriz[0].pertinencia).prop('checked',true);
                            }
                            $('#datomatrizid').prop('value',data.datomatriz[0].id);
                        }
                    }
                });
            }
            else
            {
                vaciarDatosMAtriz();
            }
        }

        function vaciarDatosMAtriz()
        {
            console.log("se borran los datos matriz");
            $('.dato[name="titulo"]').prop('value','');
            $('.dato[name="resumen"]').prop('value','');
            $('.dato[name="trivial"]:checked').prop('checked',false);
            $('.dato[name="pertinencia"]:checked').prop('checked',false);
            $('#datomatrizid').prop('value',0);
        }

        //carga las columnas de la taba
        function cargamatrizcabecera()
        {
            var grupo_id = $('#grupo').val();
            if(grupo_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cargacursomatriz') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        grupo_id: grupo_id
                    },  
                    success:function (data) 
                    {
                        $('#matrices').empty();
                        var mat = '<tr><th scope="col" rowspan="2">#</th><th scope="col" rowspan="2">Esquemas</th>';
                        $.each(data.cursomatrizs,function(index,matriz)
                        {       
                            mat = mat+'<th scope="col">'+matriz.matriz+'<button type="button" class="btn btn-sm pdf" id="'+matriz.id+'" title="Imprimir '+matriz.matriz+'"><i id="'+matriz.id+'" class="fa fa-print" aria-hidden="true"></i></button></th>';
                        });

                        mat = mat+'</tr>';
                        $('#matrices').append(mat+'<tr id="promedios"></tr>');
                        arr_matriz_n = data.cursomatrizs_n;

                        matriz_n = data.cursomatrizs.length;
                        cargamatrizpromedio();
                    }
                });
            }
            else
            {
                matriz_n = 0;
                $('#matrices').empty();
                cargamatrizpromedio();
            }
        }

        //carga los promedios por matriz
        function cargamatrizpromedio()
        {
            var curso_estudiante_id = $('#alumno').val();
            var grupo_id = $('#grupo').val();
            if(grupo_id && curso_estudiante_id)
            {
                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.cargamatrizpromedio') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        grupo_id: grupo_id,
                        curso_estudiante_id:curso_estudiante_id
                    },  
                    success:function (data) 
                    {
                        $('#promedios').empty();
                        $.each(data.promedios,function(index,promedio)
                        {     
                            $('#promedios').append('<td>Prom: '+Number(promedio).toFixed(2)+'</td>');
                        });
                        promedioAlumno = data.promedio_final;
                    }
                });
            }
            else
            {
                $('#promedios').empty();
                $('#promedios').append('<td>Prom: 0.00</td><td>Prom: 0.00</td><td>Prom: 0.00</td>');
                promedioAlumno = 0;
            }

        }


        //carga las notas del alumnos elegido en la tabla
        function carganotasalumno()
        {
            $('#esquema-notas').empty();
            console.log("Carganotas cantidad alumnos: " + alumnos_n);
            //if(alumnos_n>0 && matriz_n>0)
            if(alumnos_n>0 && matriz_n>0)
            {
                console.log("Carganotas Entro if con: " + alumnos_n);
                numerador = 1;

                var cursomatriz_id = arr_matriz_n[0];
                var cursoestudiante_id = $('#alumno').val();
                if(cursomatriz_id && cursomatriz_id)
                {
                    cargamatrizpromedio();
                    $.ajax(
                    {
                        type:'POST',
                        url:"{{ route('ajax.cargamatrizalumno') }}",
                        data:
                        {
                            "_token": "{{ csrf_token() }}",
                            cursomatriz_id: cursomatriz_id,
                            cursoestudiante_id: cursoestudiante_id
                        },  
                        success:function (data) 
                        {
                            arr_esquemas_n = data.cursoesquemas_n;
                            var misNotas='';
                            if(data.cursoesquemas_n.length == 0)
                            {
                                console.log("No hay esquemas");
                                $('#esquema-notas').append('<tr><td colspan ="2">No hay elementos de evaluación en la matriz seleccionada.</td></tr>');
                            }
                            else
                            {
                                var fila = 0;
                                var columna = 0;
                                $.each(data.cursoesquemas,function(index,ce)
                                {
                                    columna = 0;
                                    misNotas = '<td>'+numerador+'</td><td>'+ce.detalle+'</td>';
                                    $.each(arr_matriz_n,function(index,cm)
                                    {
                                        //define la clase del <td>
                                        if(typeof ce.notas[cm] != 'undefined')
                                        {
                                            misNotas = misNotas+'<td class="miNota" mf="'+fila+'" mc="'+columna+'" ';
                                            if(typeof ce.notas[cm][0] != 'undefined')
                                            {
                                                let nota = Number(ce.notas[cm][0].nota).toFixed(2);
                                                misNotas = misNotas+'id="'+ce.notas[cm][0].id+'">'+nota+'</td>';
                                            }
                                            else
                                            {
                                                misNotas = misNotas+'id="0">'+'Sin nota</td>';
                                            }
                                        }
                                        else
                                        {
                                            misNotas = misNotas+'<td id="0" class="noCalificar"></td>';
                                        }
                                        columna = columna+1;
                                    });
                                    fila = fila + 1;
                                    numerador = numerador + 1;

                                    $('#esquema-notas').append('<tr>'+misNotas+'</tr>');
                                });
                                console.log("columna: "+columna);
                                columna = columna +1;
                                $('#esquema-notas').append('<tr><td colspan="'+columna+'" style="text-align: right;">PROMEDIO FINAL&nbsp&nbsp</td><td>'+Number(promedioAlumno).toFixed(2)+'</td></tr>');
                                
                            }
                            /*
                            if(data.matrizdetalles.length == 0)
                            {
                                console.log("No hay esquemas");
                                $('#esquema-notas').append('<tr><td colspan ="3">No hay elementos de evaluación en la matriz seleccionada.</td></tr>');
                            }
                            else
                            {
                                $.each(data.matrizdetalles,function(index,matrizdetalle)
                                {
                                    console.log(matrizdetalle.estudiantematrizs);
                                    if (typeof matrizdetalle.estudiantematrizs[0] != 'undefined')
                                    {
                                        console.log("Se entro a IF de carga alumnos");
                                        $('#esquema-notas').append('<tr id="'+matrizdetalle.id+'"><td>'+numerador+'</td><td>'+matrizdetalle.cursoesquema.detalle+'</td><td id="'+matrizdetalle.estudiantematrizs[0].id+'" class ="miNota">'+matrizdetalle.estudiantematrizs[0].nota+'</td></tr>');
                                    }
                                    else
                                    {
                                        console.log("Se entro a ELSE de carga alumnos");
                                        $('#esquema-notas').append('<tr id="'+matrizdetalle.id+'"><td>'+numerador+'</td><td>'+matrizdetalle.cursoesquema.detalle+'</td><td id="0" class ="miNota">Sin Nota</td></tr>');
                                    }

                                    numerador = numerador +1; 
                                });
                            }
                            */

                        }
                    });
                }
                $('#datosMatriz').show();
            }
            else
            {
                $('#datosMatriz').hide();
                if(alumnos_n == 0)
                {
                    var col = matriz_n+2;
                    console.log("Se entro a ELSE por falta de alumnos");
                    $('#esquema-notas').append('<tr><td colspan ="'+col+'">No hay alumnos matriculados en este grupo.</td></tr>');
                    cargamatrizpromedio();
                }
                
                if(matriz_n == 0)
                {
                    console.log("Se entro a ELSE por falta de matrices");
                    $('#esquema-notas').append('<tr><td colspan ="2">No hay matrices registradas en este curso.</td></tr>');
                }
            }
        }

        function activarNota(e)
        {
            console.log("tag de doble click: "+e.target.nodeName);

            if(enCambio == 0 && e.target.nodeName == "TD")
            {
                enCambio = 1;
                var tdText = $(this).text();
                var estudiante_matriz_id = e.target.id;

                console.log("Dato impreso: "+tdText);

                mf = $(e.target ).attr("mf");
                mc = $(e.target ).attr("mc");

                console.log("Mis datos: "+mf+" "+mc);

                $(e.target ).empty();

                if(!(tdText=='Sin nota'))
                {
                    nota_estado = 0;
                    $(e.target).append('<input type="number" step="0.01" id="'+estudiante_matriz_id+'" value="'+tdText+'">');

                }
                else
                {
                    nota_estado = 1;
                    $(e.target).append('<input type="number" step="0.01" id="0" value="0.0">');
                }
            }
            if(enCambio == 1 && e.target.nodeName == "INPUT")
            {
                var estudiante_matriz_id = e.target.id;
                var curso_estudiante_id = $('#alumno').val();
                let nota = $(e.target).val();

                var cursomatriz_id = arr_matriz_n[mc];
                var cursoesquema_id = arr_esquemas_n[mf];

                console.log("Mis datos: "+cursomatriz_id+" "+cursoesquema_id);

                myTd = $(e.target).closest( "td" );

                $.ajax(
                {
                    type:'POST',
                    url:"{{ route('ajax.modificarnotaalumno') }}",
                    data:
                    {
                        "_token": "{{ csrf_token() }}",
                        cursomatriz_id:cursomatriz_id,
                        cursoesquema_id:cursoesquema_id,
                        estudiante_matriz_id:estudiante_matriz_id,
                        curso_estudiante_id:curso_estudiante_id,
                        nota:nota
                    },
                    success:function (data)
                    {
                        myTd.empty();
                        nota = Number(nota).toFixed(2);
                        myTd.append(nota);

                        cargamatrizpromedio();
                    }
                    
                });
                
                enCambio = 0;
            }
        }

        function exportarMatrizAlumno(e)
        {
            console.log(e.target.nodeName);
            $('#matrizid').attr('value', e.target.id);
            $( "form" ).submit();
        };

        function activarDato(e)
        {
            console.log("Activando dato");

            if(enCambio == 0 && $(e.target ).prop("readonly"))
            {
                $(e.target ).prop("readonly", false);
                enCambio = 1;
                console.log("Activando dato : 1");

            }
            else
            {
                if(enCambio == 1 && !$(e.target ).prop("readonly"))
                {
                    $(e.target ).prop("readonly", true);
                    guardarDato(e);

                    enCambio = 0;
                    console.log("Activando dato: 0");
                }
            }
        }

        function guardarDato(e)
        {
            dato_matriz_id = $('#datomatrizid').val();
            curso_matriz_id = $('#matriz').val();
            curso_estudiante_id = $('#alumno').val();
            dato = $(e.target).val();
            ndato = $(e.target).prop('name');

            console.log("mi ndato: "+ndato);
            
            $.ajax(
            {
                type:'POST',
                url:"{{ route('ajax.modificardatomatriz') }}",
                data:
                {
                    "_token": "{{ csrf_token() }}",
                    dato_matriz_id: dato_matriz_id,
                    curso_matriz_id:curso_matriz_id,
                    curso_estudiante_id:curso_estudiante_id,
                    dato:dato,
                    ndato:ndato
                },
                success:function (data)
                {
                    console.log("datos: "+data);
                    console.log("mi resultado: "+data.resultado);
                    if(data.resultado == "insertar")
                    {
                        console.log("mi id: "+data.nid);
                        console.log("Se creo un nuevo dato");
                        $('#datomatrizid').prop('value', data.nid);
                    }
                    console.log("Se guardo exitosamente");
                }
                
            });
        }

        function verificarDato(e)
        {
            console.log("Se modifico algun dato");
            guardarDato(e);
        }

        function quitarCheck()
        {
            $('.dato').prop("checked",false);
        }

        $('#semestre').on('change', cargacursosdocente);
        $('#curso').on('change', cargagruposdocente);
        $('#grupo').on('change',modificaciongrupos);
        //$('#matriz').on('change',carganotasalumno);
        $('#alumno').on('change',carganotasalumno);
        $('#alumno').on('change',cargadatosmatriz);
        $('#matriz').on('change',cargadatosmatriz);

        $('body').on('dblclick','.miNota',activarNota);
        //$('body').on('dblclick','.miNota',activarNota);
        //$('body').on('click','#descargaPDF',exportarMatrizAlumno);
        $('body').on('click','.pdf',exportarMatrizAlumno);
        $('body').on('dblclick','.dato',activarDato);
        $('body').on('change','.dato[type="radio"]', verificarDato);

        });
    </script>
    <style>
      td.noCalificar {
        background: #808080;
      }
      .dato:read-only {
        background: #ccc;
      }
    </style>
@endsection
