@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Nuevo Usuario</div>

                <div class="card-body">
                    <form method="POST" action="{{url('usuarios')}}" >
                        @csrf
                        <div class="form-group">
                            <label for="nombre">Nombre:</label>
                            <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror"
                                name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>
                            @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror   
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input id="email" type="text" class="form-control @error('email') is-invalid @enderror"
                                name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror   
                        </div>
                        <div class="form-group">
                            <label for="imail2">Ingrese nuevamente Email</label>
                            <input id="imail2" type="text" class="form-control @error('imail2') is-invalid @enderror"
                                name="imail2" value="{{ old('imail2') }}" required autocomplete="imail2" autofocus>
                            @error('imail2')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror   
                        </div>
                        <div class="form-group">
                            <label for="rol">Rol</label>
                            <input id="rol" type="text" class="form-control @error('rol') is-invalid @enderror"
                                name="rol" value="{{ old('rol') }}" required autocomplete="rol" autofocus>
                            @error('rol')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror   
                        </div>

                        <div class="form-group">
                            <label for="usuario">Usuario</label>
                            <input id="usuario" type="text" class="form-control @error('usuario') is-invalid @enderror"
                                name="usuario" value="{{ old('usuario') }}" required autocomplete="usuario" autofocus>
                            @error('usuario')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror   
                        </div>

                        <div class="form-group">
                            <label for="clave">Clave</label>
                            <input id="clave" type="text" class="form-control @error('clave') is-invalid @enderror"
                                name="clave" value="{{ old('clave') }}" required autocomplete="clave" autofocus>
                            @error('clave')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror   
                        </div>

                        <div class="form-group">
                            <label for="estado">Estado</label>
                            <input id="estado" type="text" class="form-control @error('estado') is-invalid @enderror"
                                name="estado" value="{{ old('estado') }}" required autocomplete="estado" autofocus>
                            @error('estado')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror   
                        </div>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection