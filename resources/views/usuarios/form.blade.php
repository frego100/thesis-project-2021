<div class="form-group {{ $errors->has('usuario') ? 'has-error' : ''}}">
    <label for="usuario" class="control-label">{{ 'usuario' }}</label>
    <input class="form-control" name="usuario" type="text" id="usuario" value="{{ isset($post->id) ? $post->id : ''}}" >
    {!! $errors->first('usuario', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>