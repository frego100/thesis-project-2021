<div class="form-group {{ $errors->has('detalle') ? 'has-error' : ''}}">
    <label for="detalle" class="control-label">{{ 'MatrizDetalle' }}</label>
    <select name="detalle" class="form-control" id="detalle" required>
        @foreach($matrizdetalles as $item)              
        <option value="{{ $item->id }}" {{ isset($estudiantematriz->matrizdetalle_id) ?  ($item->id == $estudiantematriz->matrizdetalle_id ? 'selected' : '') : '' }}>
            {{ $item->detalle }}
        </option>                      
    @endforeach  
</select>
    {!! $errors->first('plan', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('curso') ? 'has-error' : ''}}">
    <label for="curso" class="control-label">{{ 'Curso' }}</label>
    <select name="curso" class="form-control" id="curso" required>
        @foreach($cursoestudiantes as $item)              
        <option value="{{ $item->id }}" {{ isset($estudiantematriz->cursoestudiante_id) ?  ($item->id == $estudiantematriz->cursoestudiante_id ? 'selected' : '') : '' }}>
            {{ $item->curso }}
        </option>                      
    @endforeach  
</select>
    {!! $errors->first('plan', '<p class="help-block">:message</p>') !!}
</div>
<br>
<div class="form-group">
    <input class="btn boton-color btn-sm" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
</div>

