<div class="form-group {{ $errors->has('anio') ? 'has-error' : ''}}">

    <label for="anio" class="control-label">{{ 'Año' }}</label>

    <input class="form-control" name="anio" type="number" id="anio" value="{{ isset($plan->anio) ? $plan->anio : ''}}" required>
    {!! $errors->first('anio', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('escuela') ? 'has-error' : ''}}">
    <label for="escuela" class="control-label">{{ 'Escuela' }}</label>
    <select name="escuela" class="form-control" id="escuela" required>
        @foreach($escuelas as $item)              
        <option value="{{ $item->id }}" {{ isset($plan->escuela_id) ?  ($item->id == $plan->escuela_id ? 'selected' : '') : '' }}>
            {{ $item->escuela }}
        </option>                      
    @endforeach  
</select>
    {!! $errors->first('escuela', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn boton-color btn-sm" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Crear' }}">
</div>

