<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function(){return view('googleLogin');});
Route::get('auth/google',[App\Http\Controllers\Auth\LoginController::class, 'redirectToGoogle']);
Route::get('auth/google/callback',[App\Http\Controllers\Auth\LoginController::class, 'handleGoogleCallback']);
Route::get('cerrarsesion',[App\Http\Controllers\Auth\LoginController::class, 'logout']);
Route::get('verificarsesion',[App\Http\Controllers\Auth\LoginController::class, 'verifySession']);

//RUTAS_SUPERADMIN
Route::get('semestres/create',[\App\Http\Controllers\SemestreController::class,'create'])->middleware('superadmin');
Route::get('semestres',[\App\Http\Controllers\SemestreController::class,'index'])->middleware('superadmin');
Route::get('semestres/{semestre}',[\App\Http\Controllers\SemestreController::class,'show'])->middleware('superadmin');
Route::post('semestres',[\App\Http\Controllers\SemestreController::class,'store'])->middleware('superadmin');
Route::put('semestres/{semestre}',[App\Http\Controllers\SemestreController::class, 'edit'])->middleware('superadmin');
Route::put('semestres/{semestre}/edit',[App\Http\Controllers\SemestreController::class, 'update'])->middleware('superadmin');
Route::post('semestres/delete/{semestre}',[\App\Http\Controllers\SemestreController::class,'delete'])->middleware('superadmin');

Route::get('competencias/create',[\App\Http\Controllers\CompetenciaController::class,'create'])->middleware('superadmin');
Route::get('competencias',[App\Http\Controllers\CompetenciaController::class, 'index'])->middleware('superadmin');
Route::get('competencias/{competencia}',[App\Http\Controllers\CompetenciaController::class, 'show'])->middleware('superadmin');
Route::post('competencias',[App\Http\Controllers\CompetenciaController::class, 'store'])->middleware('superadmin');
Route::put('competencias/{competencia}/edit',[App\Http\Controllers\CompetenciaController::class, 'update'])->middleware('superadmin');
Route::put('competencias/{competencia}',[App\Http\Controllers\CompetenciaController::class, 'edit'])->middleware('superadmin');
Route::post('competencias/delete/{competencia}',[App\Http\Controllers\CompetenciaController::class, 'delete'])->middleware('superadmin');

Route::get('resultadosestudiantes/create',[\App\Http\Controllers\ResultadosEstudianteController::class,'create'])->middleware('superadmin');
Route::get('/resultadosestudiantes',[App\Http\Controllers\ResultadosEstudianteController::class, 'index'])->middleware('superadmin');
Route::get('resultadosestudiantes/{resultadosestudiante}',[App\Http\Controllers\ResultadosEstudianteController::class, 'show'])->middleware('superadmin');
Route::post('resultadosestudiantes',[App\Http\Controllers\ResultadosEstudianteController::class, 'store'])->middleware('superadmin');
Route::put('resultadosestudiantes/{resultadosestudiante}/edit',[App\Http\Controllers\ResultadosEstudianteController::class, 'update'])->middleware('superadmin');
Route::put('resultadosestudiantes/{resultadosestudiante}',[App\Http\Controllers\ResultadosEstudianteController::class, 'edit'])->middleware('superadmin');
Route::post('resultadosestudiantes/delete/{resultadosestudiante}',[App\Http\Controllers\ResultadosEstudianteController::class, 'delete'])->middleware('superadmin');

Route::get('cursos/create',[\App\Http\Controllers\CursoController::class,'create'])->middleware('superadmin');
Route::get('cursos',[App\Http\Controllers\CursoController::class, 'index'])->middleware('superadmin');
Route::get('cursos/{curso}',[App\Http\Controllers\CursoController::class, 'show'])->middleware('superadmin');
Route::post('cursos',[App\Http\Controllers\CursoController::class, 'store'])->middleware('superadmin');
Route::put('cursos/{curso}',[App\Http\Controllers\CursoController::class, 'edit'])->middleware('superadmin');
Route::put('cursos/{curso}/edit',[App\Http\Controllers\CursoController::class, 'update'])->middleware('superadmin');
Route::post('cursos/delete/{curso}',[\App\Http\Controllers\CursoController::class,'delete'])->middleware('superadmin');

Route::get('facultades/create',[\App\Http\Controllers\FacultadController::class,'create'])->middleware('superadmin');
Route::get('facultades',[\App\Http\Controllers\FacultadController::class,'index'])->middleware('superadmin');
Route::get('facultades/{facultad}',[\App\Http\Controllers\FacultadController::class,'show'])->middleware('superadmin');
Route::post('facultades',[\App\Http\Controllers\FacultadController::class,'store'])->middleware('superadmin');
Route::put('facultades/{facultad}/edit',[App\Http\Controllers\FacultadController::class, 'update'])->middleware('superadmin');
Route::put('facultades/{facultad}',[App\Http\Controllers\FacultadController::class, 'edit'])->middleware('superadmin');
Route::post('facultades/delete/{facultad}',[\App\Http\Controllers\FacultadController::class,'delete'])->middleware('superadmin');

Route::get('escuelas/create',[\App\Http\Controllers\EscuelaController::class,'create'])->middleware('superadmin');
Route::get('escuelas',[\App\Http\Controllers\EscuelaController::class,'index'])->middleware('superadmin');
Route::get('escuelas/{escuela}',[\App\Http\Controllers\EscuelaController::class,'show'])->middleware('superadmin');
Route::post('escuelas',[\App\Http\Controllers\EscuelaController::class,'store'])->middleware('superadmin');
Route::put('escuelas/{escuela}/edit',[App\Http\Controllers\EscuelaController::class, 'update'])->middleware('superadmin');
Route::put('escuelas/{escuela}',[App\Http\Controllers\EscuelaController::class, 'edit'])->middleware('superadmin');
Route::post('escuelas/delete/{escuela}',[\App\Http\Controllers\EscuelaController::class,'delete'])->middleware('superadmin');

Route::get('planes/create',[\App\Http\Controllers\PlanController::class,'create'])->middleware('superadmin');
Route::get('planes',[\App\Http\Controllers\PlanController::class,'index'])->middleware('superadmin');
Route::get('planes/{plan}',[\App\Http\Controllers\PlanController::class,'show'])->middleware('superadmin');
Route::post('planes',[\App\Http\Controllers\PlanController::class,'store'])->middleware('superadmin');
Route::put('planes/{plan}/edit',[App\Http\Controllers\PlanController::class, 'update'])->middleware('superadmin');
Route::put('planes/{plan}',[App\Http\Controllers\PlanController::class, 'edit'])->middleware('superadmin');
Route::post('planes/delete/{plan}',[\App\Http\Controllers\PlanController::class,'delete'])->middleware('superadmin');

Route::get('cursoTipos/create',[\App\Http\Controllers\CursoTipoController::class,'create'])->middleware('superadmin');
Route::get('cursoTipos',[\App\Http\Controllers\CursoTipoController::class,'index'])->middleware('superadmin');
Route::get('cursoTipos/{cursoTipo}',[\App\Http\Controllers\CursoTipoController::class,'show'])->middleware('superadmin');
Route::post('cursoTipos',[\App\Http\Controllers\CursoTipoController::class,'store'])->middleware('superadmin');
Route::put('cursoTipos/{cursoTipo}/edit',[App\Http\Controllers\CursoTipoController::class, 'update'])->middleware('superadmin');
Route::put('cursoTipos/{cursoTipo}',[App\Http\Controllers\CursoTipoController::class, 'edit'])->middleware('superadmin');
Route::post('cursoTipos/delete/{cursoTipo}',[\App\Http\Controllers\CursoTipoController::class,'delete'])->middleware('superadmin');

Route::get('estudiantes/create',[\App\Http\Controllers\EstudianteController::class,'create'])->middleware('superadmin');
Route::get('estudiantes',[\App\Http\Controllers\EstudianteController::class,'index'])->middleware('superadmin');
Route::post('estudiantes',[\App\Http\Controllers\EstudianteController::class,'store'])->middleware('superadmin');
Route::get('estudiantes/{estudiante}',[\App\Http\Controllers\EstudianteController::class,'show'])->middleware('superadmin');
Route::put('estudiantes/{estudiante}',[App\Http\Controllers\EstudianteController::class, 'edit'])->middleware('superadmin');
Route::put('estudiantes/{estudiante}/edit',[App\Http\Controllers\EstudianteController::class, 'update'])->middleware('superadmin');
Route::post('estudiantes/delete/{estudiante}',[\App\Http\Controllers\EstudianteController::class,'delete'])->middleware('superadmin');

Route::get('usuarios/create',[\App\Http\Controllers\UsuarioController::class,'create'])->middleware('superadmin');
Route::get('usuarios',[\App\Http\Controllers\UsuarioController::class,'index'])->middleware('superadmin');
Route::post('usuarios',[\App\Http\Controllers\UsuarioController::class,'store'])->middleware('superadmin');
Route::get('usuarios/{usuario}',[\App\Http\Controllers\UsuarioController::class,'show'])->middleware('superadmin');
Route::put('usuarios/{usuario}',[App\Http\Controllers\UsuarioController::class, 'edit'])->middleware('superadmin');
Route::put('usuarios/{usuario}/edit',[App\Http\Controllers\UsuarioController::class, 'update'])->middleware('superadmin');
Route::post('usuarios/delete/{usuario}',[\App\Http\Controllers\UsuarioController::class,'delete'])->middleware('superadmin');

Route::get('matrizdetalles/create',[\App\Http\Controllers\MatrizDetalleController::class,'create'])->middleware('superadmin');
Route::get('matrizdetalles',[\App\Http\Controllers\MatrizDetalleController::class,'index'])->middleware('superadmin');
Route::post('matrizdetalles',[\App\Http\Controllers\MatrizDetalleController::class,'store'])->middleware('superadmin');
Route::get('matrizdetalles/{matrizdetalle}',[\App\Http\Controllers\MatrizDetalleController::class,'show'])->middleware('superadmin');
Route::put('matrizdetalles/{matrizdetalle}',[App\Http\Controllers\MatrizDetalleController::class, 'edit'])->middleware('superadmin');
Route::put('matrizdetalles/{matrizdetalle}/edit',[App\Http\Controllers\MatrizDetalleController::class, 'update'])->middleware('superadmin');
Route::post('matrizdetalles/delete/{matrizdetalle}',[\App\Http\Controllers\MatrizDetalleController::class,'delete'])->middleware('superadmin');

Route::get('cursoestudiantes/create',[\App\Http\Controllers\CursoEstudianteController::class,'create'])->middleware('superadmin');
Route::get('cursoestudiantes',[\App\Http\Controllers\CursoEstudianteController::class,'index'])->middleware('superadmin');
Route::post('cursoestudiantes',[\App\Http\Controllers\CursoEstudianteController::class,'store'])->middleware('superadmin');
Route::get('cursoestudiantes/{cursoestudiante}',[\App\Http\Controllers\CursoEstudianteController::class,'show'])->middleware('superadmin');
Route::put('cursoestudiantes/{cursoestudiante}',[App\Http\Controllers\CursoEstudianteController::class, 'edit'])->middleware('superadmin');
Route::put('cursoestudiantes/{cursoestudiante}/edit',[App\Http\Controllers\CursoEstudianteController::class, 'update'])->middleware('superadmin');
Route::post('cursoestudiantes/delete/{cursoestudiante}',[\App\Http\Controllers\CursoEstudianteController::class,'delete'])->middleware('superadmin');

Route::get('estudiantematrizs/create',[\App\Http\Controllers\EstudianteMatrizController::class,'create'])->middleware('superadmin');
Route::get('estudiantematrizs',[\App\Http\Controllers\EstudianteMatrizController::class,'index'])->middleware('superadmin');
Route::post('estudiantematrizs',[\App\Http\Controllers\EstudianteMatrizController::class,'store'])->middleware('superadmin');
Route::get('estudiantematrizs/{estudiantematriz}',[\App\Http\Controllers\EstudianteMatrizController::class,'show'])->middleware('superadmin');
Route::put('estudiantematrizs/{estudiantematriz}',[App\Http\Controllers\EstudianteMatrizController::class, 'edit'])->middleware('superadmin');
Route::put('estudiantematrizs/{estudiantematriz}/edit',[App\Http\Controllers\EstudianteMatrizController::class, 'update'])->middleware('superadmin');
Route::post('estudiantematrizs/delete/{estudiantematriz}',[\App\Http\Controllers\EstudianteMatrizController::class,'delete'])->middleware('superadmin');

Route::get('competenciasres/create',[\App\Http\Controllers\CompetenciasREController::class,'create'])->middleware('superadmin');
Route::get('/competenciasres',[App\Http\Controllers\CompetenciasREController::class, 'index'])->middleware('superadmin');
Route::post('competenciasres',[App\Http\Controllers\CompetenciasREController::class, 'store'])->middleware('superadmin');
Route::get('competenciasres/{competenciasre}',[App\Http\Controllers\CompetenciasREController::class, 'show'])->middleware('superadmin');
Route::put('competenciasres/{competenciasre}',[App\Http\Controllers\CompetenciasREController::class, 'edit'])->middleware('superadmin');
Route::put('competenciasres/{competenciasre}/edit',[App\Http\Controllers\CompetenciasREController::class, 'update'])->middleware('superadmin');
Route::post('competenciasres/delete/{competenciasre}',[App\Http\Controllers\CompetenciasREController::class, 'delete'])->middleware('superadmin');

Route::get('/escuelasemestres/create',[App\Http\Controllers\EscuelaSemestreController::class, 'create'])->middleware('superadmin');
Route::get('/escuelasemestres',[App\Http\Controllers\EscuelaSemestreController::class, 'index'])->middleware('superadmin');
Route::post('escuelasemestres',[App\Http\Controllers\EscuelaSemestreController::class, 'store'])->middleware('superadmin');
Route::get('escuelasemestres/{escuelasemestre}',[App\Http\Controllers\EscuelaSemestreController::class, 'show'])->middleware('superadmin');
Route::put('escuelasemestres/{escuelasemestre}',[App\Http\Controllers\EscuelaSemestreController::class, 'edit'])->middleware('superadmin');
Route::put('escuelasemestres/{escuelasemestre}/edit',[App\Http\Controllers\EscuelaSemestreController::class, 'update'])->middleware('superadmin');
Route::post('escuelasemestres/delete/{escuelasemestre}',[App\Http\Controllers\EscuelaSemestreController::class, 'delete'])->middleware('superadmin');

Route::get('criterios/create',[\App\Http\Controllers\CriterioController::class,'create'])->middleware('superadmin');
Route::get('criterios',[App\Http\Controllers\CriterioController::class, 'index'])->middleware('superadmin');
Route::get('criterios/{criterio}',[App\Http\Controllers\CriterioController::class, 'show'])->middleware('superadmin');
Route::post('criterios',[App\Http\Controllers\CriterioController::class, 'store'])->middleware('superadmin');
Route::put('criterios/{criterio}',[App\Http\Controllers\CriterioController::class, 'edit'])->middleware('superadmin');
Route::put('criterios/{criterio}/edit',[App\Http\Controllers\CriterioController::class, 'update'])->middleware('superadmin');
Route::post('criterios/delete/{criterio}',[\App\Http\Controllers\CriterioController::class,'delete'])->middleware('superadmin');

Route::get('/cursosemestres/create',[App\Http\Controllers\CursoSemestreController::class, 'create'])->middleware('superadmin');
Route::get('/cursosemestres',[App\Http\Controllers\CursoSemestreController::class, 'index'])->middleware('superadmin');
Route::post('cursosemestres',[App\Http\Controllers\CursoSemestreController::class, 'store'])->middleware('superadmin');
Route::get('cursosemestres/{cursosemestre}',[App\Http\Controllers\CursoSemestreController::class, 'show'])->middleware('superadmin');
Route::put('cursosemestres/{cursosemestre}',[App\Http\Controllers\CursoSemestreController::class, 'edit'])->middleware('superadmin');
Route::put('cursosemestres/{cursosemestre}/edit',[App\Http\Controllers\CursoSemestreController::class, 'update'])->middleware('superadmin');
Route::post('cursosemestres/delete/{cursosemestre}',[App\Http\Controllers\CursoSemestreController::class, 'delete'])->middleware('superadmin');

Route::get('docentes/create',[\App\Http\Controllers\DocenteController::class,'create'])->middleware('superadmin');
Route::get('docentes',[\App\Http\Controllers\DocenteController::class,'index'])->middleware('superadmin');
Route::get('docentes/{docente}',[\App\Http\Controllers\DocenteController::class,'show'])->middleware('superadmin');
Route::post('docentes',[\App\Http\Controllers\DocenteController::class,'store'])->middleware('superadmin');
Route::put('docentes/{docente}/edit',[App\Http\Controllers\DocenteController::class, 'update'])->middleware('superadmin');
Route::put('docentes/{docente}',[App\Http\Controllers\DocenteController::class, 'edit'])->middleware('superadmin');
Route::post('docentes/delete/{docente}',[\App\Http\Controllers\DocenteController::class,'delete'])->middleware('superadmin');

Route::get('cursomatrizs/create',[\App\Http\Controllers\CursoMatrizController::class,'create'])->middleware('superadmin');
Route::get('/cursomatrizs',[App\Http\Controllers\CursoMatrizController::class, 'index'])->middleware('superadmin');
Route::get('cursomatrizs/{cursomatriz}',[App\Http\Controllers\CursoMatrizController::class, 'show'])->middleware('superadmin');
Route::post('cursomatrizs',[App\Http\Controllers\CursoMatrizController::class, 'store'])->middleware('superadmin');
Route::put('cursomatrizs/{cursomatriz}',[App\Http\Controllers\CursoMatrizController::class, 'update'])->middleware('superadmin');
Route::put('cursomatrizs/{cursomatriz}',[App\Http\Controllers\CursoMatrizController::class, 'edit'])->middleware('superadmin');
Route::post('cursomatrizs/delete/{cursomatriz}',[App\Http\Controllers\CursoMatrizController::class, 'delete'])->middleware('superadmin');

Route::get('cursoesquemas/create',[\App\Http\Controllers\EscuelaController::class,'create'])->middleware('superadmin');
Route::get('/cursoesquemas',[App\Http\Controllers\CursoEsquemaController::class, 'index'])->middleware('superadmin');
Route::get('cursoesquemas/{cursoesquema}',[App\Http\Controllers\CursoEsquemaController::class, 'show'])->middleware('superadmin');
Route::post('cursoesquemas',[App\Http\Controllers\CursoEsquemaController::class, 'store'])->middleware('superadmin');
Route::put('cursoesquemas/{cursoesquema}',[App\Http\Controllers\CursoEsquemaController::class, 'update'])->middleware('superadmin');
Route::put('cursoesquemas/{cursoesquema}',[App\Http\Controllers\EscuelaController::class, 'edit'])->middleware('superadmin');
Route::post('cursoesquemas/delete/{cursoesquema}',[App\Http\Controllers\CursoEsquemaController::class, 'delete'])->middleware('superadmin');

Route::get('cursogrupos/create',[\App\Http\Controllers\CursoGrupoController::class,'create'])->middleware('superadmin');
Route::get('/cursogrupos',[App\Http\Controllers\CursoGrupoController::class, 'index'])->middleware('superadmin');
Route::get('cursogrupos/{cursogrupo}',[App\Http\Controllers\CursoGrupoController::class, 'show'])->middleware('superadmin');
Route::post('cursogrupos',[App\Http\Controllers\CursoGrupoController::class, 'store'])->middleware('superadmin');
Route::put('cursogrupos/{cursogrupo}',[App\Http\Controllers\CursoGrupoController::class, 'update'])->middleware('superadmin');
Route::put('cursogrupos/{cursogrupo}',[App\Http\Controllers\CursoGrupoController::class, 'edit'])->middleware('superadmin');
Route::post('cursogrupos/delete/{cursogrupo}',[App\Http\Controllers\CursoGrupoController::class, 'delete'])->middleware('superadmin');

//RUTAS_ADMIN
Route::get('/admin',[App\Http\Controllers\AdminController::class, 'index'])->middleware('admin');
Route::get('/admin/competencia',[App\Http\Controllers\AdminController::class, 'competencia'])->middleware('admin');
Route::get('/admin/resultadoEstudianteEsp',[App\Http\Controllers\AdminController::class, 'reEsp'])->middleware('admin');
Route::get('/admin/cursoEsp',[App\Http\Controllers\AdminController::class, 'cursoEsp'])->name('ajax.vistaCursoEsp')->middleware('admin');
Route::get('/admin/resultadoEstudiante',[App\Http\Controllers\AdminController::class, 'resultadoEstudiante'])->middleware('admin');
Route::get('/admin/semestreCurso',[App\Http\Controllers\AdminController::class, 'semestreCurso'])->middleware('admin');
Route::get('/admin/competenciaEsp',[App\Http\Controllers\AdminController::class, 'competenciaEsp'])->middleware('admin');
Route::get('/admin/curso',[App\Http\Controllers\AdminController::class, 'curso'])->middleware('admin');
Route::get('/admin/planEstudio',[App\Http\Controllers\AdminController::class, 'planEstudio'])->middleware('admin');
Route::get('/admin/resultadoECompetencia',[App\Http\Controllers\AdminController::class, 'resultadoECompetencia'])->middleware('admin');

Route::post('cargaescuelas',[App\Http\Controllers\AdminController::class, 'cargaEscuelas'])->name('ajax.escuelas')->middleware('admin');
Route::post('cargaplanes',[App\Http\Controllers\AdminController::class, 'cargaPlanes'])->name('ajax.planes')->middleware('admin');
Route::post('cargacompetencias',[App\Http\Controllers\AdminController::class, 'cargaCompetencias'])->name('ajax.competencias')->middleware('admin');
Route::post('cargare',[App\Http\Controllers\AdminController::class, 'cargaRE'])->name('ajax.re')->middleware('admin');
Route::post('cargacursos',[App\Http\Controllers\AdminController::class, 'cargaCursos'])->name('ajax.cursos')->middleware('admin');
Route::post('cargacriterios',[App\Http\Controllers\AdminController::class, 'cargaCriterios'])->name('ajax.criterio')->middleware('admin');
Route::post('cargaresultadosestudiantes',[App\Http\Controllers\AdminController::class, 'cargaResultadosEstudiantes'])->name('ajax.resultadosestudiantes')->middleware('admin');
Route::post('cargaescuelasemestres',[App\Http\Controllers\AdminController::class, 'cargaEscuelasemestres'])->name('ajax.escuelasemestre')->middleware('admin');
Route::post('cargacursosemestre',[App\Http\Controllers\AdminController::class, 'cargaCursosemestre'])->name('ajax.cursosemestre')->middleware('admin');
Route::post('cargasemestre',[App\Http\Controllers\AdminController::class, 'cargaCursosemestre'])->name('ajax.semestrecurso')->middleware('admin');
Route::post('cargacursodocente',[App\Http\Controllers\AdminController::class, 'cargaCursodocente'])->name('ajax.cursodocente')->middleware('admin');
Route::post('cargaResultadosEsp',[App\Http\Controllers\AdminController::class, 'cargaResultadosEsp'])->name('ajax.competenciasEspecifico')->middleware('admin');
Route::post('cargacompetencias',[App\Http\Controllers\AdminController::class, 'cargaCompetencias'])->name('ajax.competencias')->middleware('admin');
Route::post('cargadocentes',[App\Http\Controllers\AdminController::class, 'cargaDocentes'])->name('ajax.docentes')->middleware('admin');
Route::post('cargaResultadoECompetencia',[App\Http\Controllers\AdminController::class, 'cargaResultadoECompetencia'])->name('ajax.resultadoECompetencia')->middleware('admin');
Route::post('cargacursotipo',[App\Http\Controllers\AdminController::class, 'cargaCursoTipo'])->name('ajax.cursotipo')->middleware('admin');
Route::post('editarestadosemestre',[App\Http\Controllers\AdminController::class, 'editarEstadoSemestre'])->name('ajax.editarestadosemestre')->middleware('admin');

//AJAX_ADMIN
Route::post('insertarcompetencia',[App\Http\Controllers\CompetenciaController::class, 'insertarCompetencia'])->name('ajax.insertarcompetencia')->middleware('admin');
Route::post('editarcompetencia',[App\Http\Controllers\CompetenciaController::class, 'editarCompetencia'])->name('ajax.editarcompetencia')->middleware('admin');
Route::post('eliminarcompetencia',[App\Http\Controllers\CompetenciaController::class, 'eliminarCompetencia'])->name('ajax.eliminarcompetencia')->middleware('admin');
Route::post('insertarcursogrupo',[App\Http\Controllers\CursoGrupoController::class, 'insertarCursoGrupo'])->name('ajax.insertarcursogrupo')->middleware('admin');
Route::post('editarcursogrupo',[App\Http\Controllers\CursoGrupoController::class, 'editarCursoGrupo'])->name('ajax.editarcursogrupo')->middleware('admin');
Route::post('editarcursogrupocoordinador',[App\Http\Controllers\CursoGrupoController::class, 'editarCursoGrupoCoordinador'])->name('ajax.editarcursogrupocoordinador')->middleware('admin');
Route::post('eliminarcursogrupo',[App\Http\Controllers\CursoGrupoController::class, 'eliminarCursoGrupo'])->name('ajax.eliminarcursogrupo')->middleware('admin');
Route::post('insertarcursosemestre',[App\Http\Controllers\CursoSemestreController::class, 'insertarCursoSemestre'])->name('ajax.insertarcursosemestre')->middleware('admin');
Route::post('editarcursosemestre',[App\Http\Controllers\CursoSemestreController::class, 'editarCursoSemestre'])->name('ajax.editarcursosemestre')->middleware('admin');
Route::post('eliminarcursosemestre',[App\Http\Controllers\CursoSemestreController::class, 'eliminarCursoSemestre'])->name('ajax.eliminarcursosemestre')->middleware('admin');
Route::post('insertarcriterio',[App\Http\Controllers\CriterioController::class, 'insertarCriterio'])->name('ajax.insertarcriterio')->middleware('admin');
Route::post('editarcriterio',[App\Http\Controllers\CriterioController::class, 'editarCriterio'])->name('ajax.editarcriterio')->middleware('admin');
Route::post('eliminarcriterio',[App\Http\Controllers\CriterioController::class, 'eliminarCriterio'])->name('ajax.eliminarcriterio')->middleware('admin');
Route::post('insertarcompetenciare',[App\Http\Controllers\CompetenciasREController::class, 'insertarCompetenciaRE'])->name('ajax.insertarcompetenciare')->middleware('admin');
Route::post('eliminarcompetenciare',[App\Http\Controllers\CompetenciasREController::class, 'eliminarCompetenciaRE'])->name('ajax.eliminarcompetenciare')->middleware('admin');
Route::post('insertarresultado',[App\Http\Controllers\ResultadosEstudianteController::class, 'insertarResultado'])->name('ajax.insertarresultado')->middleware('admin');
Route::post('editarresultado',[App\Http\Controllers\ResultadosEstudianteController::class, 'editarResultado'])->name('ajax.editarresultado')->middleware('admin');
Route::post('eliminarresultado',[App\Http\Controllers\ResultadosEstudianteController::class, 'eliminarResultado'])->name('ajax.eliminarresultado')->middleware('admin');
Route::post('insertarcurso',[App\Http\Controllers\CursoController::class, 'insertarCurso'])->name('ajax.insertarcurso')->middleware('admin');
Route::post('editarcurso',[App\Http\Controllers\CursoController::class, 'editarCurso'])->name('ajax.editarcurso')->middleware('admin');
Route::post('eliminarcurso',[App\Http\Controllers\CursoController::class, 'eliminarCurso'])->name('ajax.eliminarcurso')->middleware('admin');

//RUTAS_DOCENTES
Route::get('/docente',[App\Http\Controllers\VistasDocenteController::class, 'cargaIndexDocentes'])->middleware('docente');
Route::get('docente/crearMatriz',[\App\Http\Controllers\VistasDocenteController::class,'crearMatriz'])->middleware('docente');
Route::post('insertarmatriz',[App\Http\Controllers\VistasDocenteController::class, 'insertarMatriz'])->name('ajax.insertarmatriz')->middleware('docente');
Route::post('cargacursosemestredocente',[App\Http\Controllers\VistasDocenteController::class, 'cargaCursoSemestre'])->name('ajax.cargacursosemestredocente')->middleware('docente');
Route::post('cargamatrizcrear',[App\Http\Controllers\VistasDocenteController::class, 'cargaMatriz'])->name('ajax.cargamatriz')->middleware('docente');
Route::post('cargasemestredocente',[App\Http\Controllers\VistasDocenteController::class, 'cargaSemestre'])->name('ajax.cargasemestredocente')->middleware('docente');
Route::post('cursosdocenteespecificomatriz',[App\Http\Controllers\VistasDocenteController::class, 'cargaCursosDocenteEspecificoW'])->name('ajax.cursosdocenteespecificomatriz')->middleware('docente');
Route::post('cargacursodocentematriz',[App\Http\Controllers\VistasDocenteController::class, 'cargaCurso'])->name('ajax.cargacursodocente')->middleware('docente');
Route::post('cargaresultadomatriz',[App\Http\Controllers\VistasDocenteController::class, 'cargaResultado'])->name('ajax.cargaresultados')->middleware('docente');
Route::post('cargaesquemamatriz',[App\Http\Controllers\VistasDocenteController::class, 'cargaEsquema'])->name('ajax.cargaesquemas')->middleware('docente');
Route::post('editaresquemacrear',[App\Http\Controllers\VistasDocenteController::class, 'editarEsquema'])->name('ajax.editaresquema')->middleware('docente');
Route::post('exportaresquema',[App\Http\Controllers\VistasDocenteController::class, 'exportarEsquema'])->name('ajax.exportaresquema')->middleware('docente');
Route::post('importaresquema',[App\Http\Controllers\VistasDocenteController::class, 'importarEsquema'])->name('ajax.importaresquema')->middleware('docente');
Route::post('exportaralumno',[App\Http\Controllers\VistasDocenteController::class, 'exportarAlumno'])->name('ajax.exportaralumno')->middleware('docente');
Route::post('importaralumno',[App\Http\Controllers\VistasDocenteController::class, 'importarAlumno'])->name('ajax.importaralumno')->middleware('docente');
Route::post('exportarlista',[App\Http\Controllers\VistasDocenteController::class, 'exportarLista'])->name('ajax.exportarlista')->middleware('docente');

Route::get('/docente/alumnos',[App\Http\Controllers\VistasDocenteController::class, 'cargalistaAlumnos'])->middleware('docente');
Route::post('cargacursosdocenteespecificoA',[App\Http\Controllers\VistasDocenteController::class, 'cargaCursosDocenteEspecificoC'])->name('ajax.cursosdocenteespecificoC')->middleware('docente');
Route::post('cargagrupoestudiantes',[App\Http\Controllers\VistasDocenteController::class, 'cargaGrupoEstudiantes'])->name('ajax.acalumno')->middleware('docente');
Route::post('cargalistaestudiantes',[App\Http\Controllers\VistasDocenteController::class, 'cargaListaEstudiantes'])->name('ajax.listaAlumnos')->middleware('docente');
Route::post('guardaestudiante',[App\Http\Controllers\CursoEstudianteController::class, 'insertarAlumnoGrupo'])->name('ajax.matricularAlumno');
Route::post('eliminaestudiante',[App\Http\Controllers\CursoEstudianteController::class, 'eliminarAlumnoGrupo'])->name('ajax.eliminarAlumno');

Route::post('verificarestadosemestre',[App\Http\Controllers\VistasDocenteController::class, 'verificarEstadoSemestre'])->name('ajax.verificarestadosemestre')->middleware('docente');

Route::get('/docente/matriz',[App\Http\Controllers\VistasDocenteController::class, 'cargaVistaMatriz'])->middleware('docente');
Route::post('cargacursosdocenteespecifico',[App\Http\Controllers\VistasDocenteController::class, 'cargaCursosDocenteEspecifico'])->name('ajax.cursosdocenteespecifico')->middleware('docente');
Route::post('cargagruposdocenteespecifico',[App\Http\Controllers\VistasDocenteController::class, 'cargaGruposDocenteEspecifico'])->name('ajax.gruposdocenteespecifico')->middleware('docente');
Route::post('cargadocenteespecifico',[App\Http\Controllers\VistasDocenteController::class, 'cargaDocenteEspecifico'])->name('ajax.docenteespecifico')->middleware('docente');
Route::get('/docente/esquemas',[App\Http\Controllers\VistasDocenteController::class, 'cargaEsquemaTrabajo'])->middleware('docente');
Route::post('cargaesquemascurso',[App\Http\Controllers\VistasDocenteController::class, 'cargaEsquemasCurso'])->name('ajax.esquemacurso')->middleware('docente');
Route::post('insertarcursoesquema',[App\Http\Controllers\CursoEsquemaController::class, 'insertarCursoEsquema'])->name('ajax.insertarcursoesquema');
Route::post('editarcursoesquema',[App\Http\Controllers\CursoEsquemaController::class, 'editarCursoEsquema'])->name('ajax.editarcursoesquema');
Route::post('eliminarcursoesquema',[App\Http\Controllers\CursoEsquemaController::class, 'eliminarCursoEsquema'])->name('ajax.eliminarcursoesquema');
Route::post('cargacursomatriz',[App\Http\Controllers\VistasDocenteController::class, 'cargaCursoMatriz'])->name('ajax.cargacursomatriz')->middleware('docente');
Route::post('cargamatrizdetalle',[App\Http\Controllers\VistasDocenteController::class, 'cargaMatrizDetalle'])->name('ajax.cargamatrizdetalle')->middleware('docente');
Route::get('/docente/matrizcabecera',[App\Http\Controllers\VistasDocenteController::class, 'cargaMatrizCabecera'])->middleware('docente');
Route::post('cargamatrizscurso',[App\Http\Controllers\VistasDocenteController::class, 'cargaMatrizsCurso'])->name('ajax.matrizcurso')->middleware('docente');
Route::post('insertarcursomatriz',[App\Http\Controllers\CursoMatrizController::class, 'insertarCursoMatriz'])->name('ajax.insertarcursomatriz');
Route::post('editarcursomatriz',[App\Http\Controllers\CursoMatrizController::class, 'editarCursoMatriz'])->name('ajax.editarcursomatriz');

Route::post('eliminarcursomatriz',[App\Http\Controllers\CursoMatrizController::class, 'eliminarCursoMatriz'])->name('ajax.eliminarcursomatriz');
Route::get('/docente/notasalumno',[App\Http\Controllers\VistasDocenteController::class, 'cargaNotasAlumno'])->middleware('docente');
Route::post('cargamatrizalumno',[App\Http\Controllers\VistasDocenteController::class, 'cargaMatrizAlumno'])->name('ajax.cargamatrizalumno')->middleware('docente');
Route::post('modificarnotaalumno',[App\Http\Controllers\EstudianteMatrizController::class, 'modificarNotaAlumno'])->name('ajax.modificarnotaalumno');
Route::post('carganotasdetalle',[App\Http\Controllers\VistasDocenteController::class, 'cargaNotasDetalle'])->name('ajax.carganotasdetalle')->middleware('docente');
Route::post('cargamatrizpromedio',[App\Http\Controllers\VistasDocenteController::class, 'cargaMatrizPromedio'])->name('ajax.cargamatrizpromedio')->middleware('docente');
Route::post('cargamatrizpromedios',[App\Http\Controllers\VistasDocenteController::class, 'cargaMatrizPromedios'])->name('ajax.cargamatrizpromedios')->middleware('docente');
Route::post('exportarmatrizgrupo',[App\Http\Controllers\VistasDocenteController::class, 'exportarMatrizGrupo'])->name('ajax.exportarmatrizgrupo')->middleware('docente');
Route::post('matrizalumnopdf',[App\Http\Controllers\VistasDocenteController::class,'matrizAlumnoPDF'])->middleware('docente');
Route::post('matrizgrupopdf',[App\Http\Controllers\VistasDocenteController::class,'matrizGrupoPDF'])->middleware('docente');
Route::post('modificardatomatriz',[App\Http\Controllers\DatoMatrizController::class, 'modificarDatoMatriz'])->name('ajax.modificardatomatriz');
Route::post('cargadatomatriz',[App\Http\Controllers\VistasDocenteController::class, 'cargaDatoMatriz'])->name('ajax.cargadatomatriz')->middleware('docente');

Auth::routes();
?>