<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCursoGruposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curso_grupos', function (Blueprint $table) {
            $table->id();
            $table->string('grupo');
            $table->char('coordinador', 1)->default(0);
            $table->char('estado',1)->default('A');
            $table->timestamps();

            //relaciones
            $table->unsignedBigInteger('cursosemestre_id');
            $table->foreign('cursosemestre_id')
                ->references('id')
                ->on('curso_semestres')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedBigInteger('docente_id');
            $table->foreign('docente_id')
                ->references('id')
                ->on('docentes')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curso_grupos');
    }
}
