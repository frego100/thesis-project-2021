<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEscuelaSemestresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('escuela_semestres', function (Blueprint $table) {
            $table->id();
            $table->char('estado',1)->default('A');
            $table->char('estadosemestre',7)->default('Abierto');
            $table->timestamps();

            //relaciones
            $table->unsignedBigInteger('escuela_id');
            $table->foreign('escuela_id')
                ->references('id')
                ->on('escuelas')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedBigInteger('semestre_id');
            $table->foreign('semestre_id')
                ->references('id')
                ->on('semestres')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('escuela_semestres');
    }
}
