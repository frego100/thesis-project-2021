<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCriteriosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('criterios', function (Blueprint $table) {
            $table->id();
            $table->char('codigo',8);
            $table->text('criterio')->nullable();
            $table->text('insatisfactorio')->nullable();
            $table->text('enproceso')->nullable();
            $table->text('satisfactorio')->nullable();
            $table->text('sobresaliente')->nullable();
            $table->char('estado',1)->default('A');
            $table->timestamps();

            //relaciones
            $table->unsignedBigInteger('resultadoestudiante_id');
            $table->foreign('resultadoestudiante_id')
                ->references('id')
                ->on('resultados_estudiantes')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('criterios');
    }
}
