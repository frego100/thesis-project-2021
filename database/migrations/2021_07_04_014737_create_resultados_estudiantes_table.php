<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultadosEstudiantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resultados_estudiantes', function (Blueprint $table) {
            $table->id();
            $table->char('codigore',4);
            $table->text('re');
            $table->char('estado',1)->default('A');

            $table->unsignedBigInteger('plan_id');
            $table->foreign('plan_id')
                        ->references('id')
                        ->on('plans')
                        ->onUpdate('cascade')
                        ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resultados_estudiantes');
    }
}
