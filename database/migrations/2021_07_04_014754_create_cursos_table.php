<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursos', function (Blueprint $table) {
            $table->id();
            $table->char('codigo',7);
            $table->string('curso');
            $table->string('descripcion');
            $table->char('semestre',2);
            $table->integer('horasteoria');
            $table->integer('horaspractica');
            $table->integer('creditos');
            $table->char('estado',1)->default('A');

            $table->unsignedBigInteger('plan_id');
            $table->foreign('plan_id')
                            ->references('id')
                            ->on('plans')
                            ->onUpdate('cascade')
                            ->onUpdate('cascade');

            $table->unsignedBigInteger('cursotipo_id');
            $table->foreign('cursotipo_id')
                            ->references('id')
                            ->on('curso_tipos')
                            ->onUpdate('cascade')
                            ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cursos');
    }
}
