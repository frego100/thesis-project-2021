<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatrizDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matriz_detalles', function (Blueprint $table) {
            $table->id();
            $table->decimal('ponderacion', $precision = 5, $scale = 2);
            $table->char('estado',1)->default('A');

            $table->unsignedBigInteger('criterio_id'); 
            $table->foreign('criterio_id')
                            ->references('id')
                            ->on('criterios')
                            ->onUpdate('cascade')
                            ->onDelete('cascade');

            $table->unsignedBigInteger('cursomatriz_id'); 
            $table->foreign('cursomatriz_id')
                            ->references('id')
                            ->on('curso_matrizs');
            
            $table->unsignedBigInteger('cursoesquema_id'); 
            $table->foreign('cursoesquema_id')
                            ->references('id')
                            ->on('curso_esquemas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matriz_detalles');
    }
}
