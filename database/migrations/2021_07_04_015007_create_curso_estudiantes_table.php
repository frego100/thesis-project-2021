<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCursoEstudiantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curso_estudiantes', function (Blueprint $table) {
            $table->id();
            $table->char('estado',1)->default('A');
            $table->timestamps();

             //relacion
             $table->unsignedBigInteger('cursogrupo_id'); 
             $table->foreign('cursogrupo_id')
                             ->references('id')
                             ->on('curso_grupos')
                             ->onUpdate('cascade')
                             ->onDelete('cascade');
            
             $table->unsignedBigInteger('estudiante_id'); 
             $table->foreign('estudiante_id')
                             ->references('id')
                             ->on('estudiantes')
                             ->onUpdate('cascade')
                             ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curso_estudiantes');
    }
}
