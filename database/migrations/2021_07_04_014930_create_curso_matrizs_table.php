<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCursoMatrizsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curso_matrizs', function (Blueprint $table) {
            $table->id();
            $table->string('matriz');
            $table->decimal('ponderacion', $precision = 5, $scale = 2);
            $table->date('fechacalificacioninicio');
            $table->date('fechacalificacionfin');
            $table->char('estado',1)->default('A');
            $table->timestamps();

            //relaciones
            $table->unsignedBigInteger('cursosemestre_id');
            $table->foreign('cursosemestre_id')
                ->references('id')
                ->on('curso_semestres')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curso_matrizs');
    }
}
