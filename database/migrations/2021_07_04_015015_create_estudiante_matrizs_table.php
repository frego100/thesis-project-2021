<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstudianteMatrizsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudiante_matrizs', function (Blueprint $table) {
            $table->id();
            $table->decimal('nota', $precision = 4, $scale = 2);
            $table->char('estado',1)->default('A');
            $table->timestamps();

            //relaciones
            $table->unsignedBigInteger('matriz_detalle_id');
            $table->foreign('matriz_detalle_id')
                ->references('id')
                ->on('matriz_detalles')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedBigInteger('curso_estudiante_id');
            $table->foreign('curso_estudiante_id')
                ->references('id')
                ->on('curso_estudiantes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudiante_matrizs');
    }
}
