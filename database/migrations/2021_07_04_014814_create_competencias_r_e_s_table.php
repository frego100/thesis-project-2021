<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompetenciasRESTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competencias_res', function (Blueprint $table) {
            $table->id();
            $table->char('estado',1)->default('A');
            $table->timestamps();

            //relaciones
            $table->unsignedBigInteger('resultadoestudiante_id');
            $table->foreign('resultadoestudiante_id')
                ->references('id')
                ->on('resultados_estudiantes')
                ->onUpdate('cascade')
                ->onDelete('cascade');
                
            $table->unsignedBigInteger('competencia_id');
            $table->foreign('competencia_id')
                ->references('id')
                ->on('competencias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competencias_res');
    }
}
