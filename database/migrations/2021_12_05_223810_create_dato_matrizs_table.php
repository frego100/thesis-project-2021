<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatoMatrizsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dato_matrizs', function (Blueprint $table) {
            $table->id();
            $table->text('titulo')->default('Titulo')->nullable();
            $table->text('resumen')->default('Resumen')->nullable();
            $table->boolean('trivial')->nullable();
            $table->boolean('pertinencia')->nullable();
            $table->char('estado',1)->default('A');
            $table->timestamps();

            //relaciones
            $table->unsignedBigInteger('curso_matriz_id');
            $table->foreign('curso_matriz_id')
                ->references('id')
                ->on('curso_matrizs')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedBigInteger('curso_estudiante_id');
            $table->foreign('curso_estudiante_id')
                ->references('id')
                ->on('curso_estudiantes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dato_matrizs');
    }
}
