<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCursoEsquemasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curso_esquemas', function (Blueprint $table) {
            $table->id();
            $table->string('detalle');
            $table->char('codigo',10);
            $table->char('estado',1)->default('A');
            $table->timestamps();

            //relaciones
            $table->unsignedBigInteger('cursosemestre_id');
            $table->foreign('cursosemestre_id')
                ->references('id')
                ->on('curso_semestres')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curso_esquemas');
    }
}
