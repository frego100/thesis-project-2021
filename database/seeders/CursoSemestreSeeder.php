<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CursoSemestre;

class CursoSemestreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->inicializarCursoSemestre("1","1","A");
        $this->inicializarCursoSemestre("2","2","A");
        $this->inicializarCursoSemestre("3","3","A");
        $this->inicializarCursoSemestre("4","4","A");
        $this->inicializarCursoSemestre("5","5","A");
        $this->inicializarCursoSemestre("6","6","A");
        $this->inicializarCursoSemestre("7","1","A");
        $this->inicializarCursoSemestre("8","2","A");
    }

    private function inicializarCursoSemestre($curso_id,$escuelasemestre_id,$estado)
    {
    	CursoSemestre::create([
    		'curso_id' => $curso_id,
    		'escuelasemestre_id' => $escuelasemestre_id,
    		'estado' => $estado
    	]);
    }
}