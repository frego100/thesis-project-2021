<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CursoMatriz;

class CursoMatrizSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CursoMatriz::factory()->count(10)->create();
    }
}
