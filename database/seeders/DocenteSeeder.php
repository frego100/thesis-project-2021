<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Docente;

class DocenteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->inicializarDocentes("ADRIAZOLA/HERRERA, ELIANA MARIA","email1",0,0,1,"A");
		$this->inicializarDocentes("AEDO/LOPEZ, MARCO WILFREDO","email2",0,0,1,"A");
		$this->inicializarDocentes("ALFARO/CASAS, LUIS ALBERTO","email3",0,0,1,"A");
		$this->inicializarDocentes("APAZA/ACEITUNO, RONI GUILLERMO","email4",0,0,1,"A");
		$this->inicializarDocentes("ARISACA/MAMANI, ROBERT EDISON","email5",0,0,1,"A");
		$this->inicializarDocentes("ATENCIO/TORRES, CARLOS EDUARDO","email6",0,0,1,"A");
		$this->inicializarDocentes("BALUARTE/ARAYA, CESAR BASILIO","email7",0,0,1,"A");
		$this->inicializarDocentes("BEDREGAL/ALPACA, NORKA NORALI","email8",0,0,1,"A");
		$this->inicializarDocentes("BENIQUE/RUELAS, LILIAN MAGNOLIA","email9",0,0,1,"A");
		$this->inicializarDocentes("CABRERA/MÁLAGA, GIOVANNI ROLANDO","email10",0,0,1,"A");
		$this->inicializarDocentes("CANO/MAMANI, EDITH GIOVANNA","email11",0,0,1,"A");
		$this->inicializarDocentes("CÁRDENAS/TALAVERA, ROLANDO JESÚS","email12",0,0,1,"A");
		$this->inicializarDocentes("CASTRO/GUTIERREZ, EVELING GLORIA","ecastro@unsa.edu.pe",1,1,1,"A");
		$this->inicializarDocentes("CHAMBY/DIAZ, JORGE CHRISTIAN","email14",0,0,1,"A");
		$this->inicializarDocentes("CORNEJO/APARICIO, VICTOR MANUEL","vcornejo@unsa.edu.pe",1,1,1,"A");
		$this->inicializarDocentes("CORRALES/DELGADO, CARLO JOSE LUIS","email16",0,0,1,"A");
		$this->inicializarDocentes("CUADROS/LINARES, OSCAR ALONSO","email17",0,0,1,"A");
		$this->inicializarDocentes("CUADROS/VALDIVIA, ANA MARIA","email18",0,0,1,"A");
		$this->inicializarDocentes("DELGADO/BARRA, LUCY ANGELA","email19",0,0,1,"A");
		$this->inicializarDocentes("DELGADO/POLAR, CHRISTIAN JORGE","email20",0,0,1,"A");
		$this->inicializarDocentes("ESCOBEDO/QUISPE, RICHART SMITH","email21",0,0,1,"A");
		$this->inicializarDocentes("FLORES/QUISPE, ROXANA","email22",0,0,1,"A");
		$this->inicializarDocentes("GALARZA/FLORES, MARISOL CRISTEL","email23",0,0,1,"A");
		$this->inicializarDocentes("GONZALES/SAJI, FREDDY ORLANDO","email24",0,0,1,"A");
		$this->inicializarDocentes("GUEVARA/PUENTE DE LA VEGA, KARIM","email25",0,0,1,"A");
		$this->inicializarDocentes("GUTIERREZ/CACERES, JUAN CARLOS","email26",0,0,1,"A");
		$this->inicializarDocentes("HINOJOSA/CARDENAS, EDWARD","email27",0,0,1,"A");
		$this->inicializarDocentes("HUERTAS/NIQUEN, PERCY OSCAR","email28",0,0,1,"A");
		$this->inicializarDocentes("IQUIRA/BECERRA, DIEGO ALONSO","email29",0,0,1,"A");
		$this->inicializarDocentes("JUAREZ/BUENO, JUAN CARLOS","email30",0,0,1,"A");
		$this->inicializarDocentes("LAURA/OCHOA, LETICIA MARISOL","email31",0,0,1,"A");
		$this->inicializarDocentes("LOPEZ/DEL ALAMO, CRISTIAN JOSE","email32",0,0,1,"A");
		$this->inicializarDocentes("LUQUE/MAMANI, EDSON FRANCISCO","email33",0,0,1,"A");
		$this->inicializarDocentes("MACHACA/ARCEDA, VICENTE ENRIQUE","email34",0,0,1,"A");
		$this->inicializarDocentes("MAMANI/ALIAGA, ALVARO HENRY","email35",0,0,1,"A");
		$this->inicializarDocentes("MOLINA/BARRIGA, MARIBEL","email36",0,0,1,"A");
		$this->inicializarDocentes("ORDOÑEZ/RAMOS, ERECH","email37",0,0,1,"A");
		$this->inicializarDocentes("PAZ/VALDERRAMA, ALFREDO","email38",0,0,1,"A");
		$this->inicializarDocentes("PEREZ/VERA, YASIEL","email39",0,0,1,"A");
		$this->inicializarDocentes("QUISPE/SAJI, GUADALUPE DEL ROSARIO","email40",0,0,1,"A");
		$this->inicializarDocentes("RAMIREZ/VALDEZ, OSCAR ALBERTO","email41",0,0,1,"A");
		$this->inicializarDocentes("RAMOS/LOVON, WILBER ROBERTO","email42",0,0,1,"A");
		$this->inicializarDocentes("REVILLA/ARROYO, CHRISTIAN ALAIN","email43",0,0,1,"A");
		$this->inicializarDocentes("RIVERO/TUPAC, EDITH PAMELA","email44",0,0,1,"A");
		$this->inicializarDocentes("RODRIGUEZ/GONZALEZ, PEDRO ALEX","email45",0,0,1,"A");
		$this->inicializarDocentes("SAIRE/PERALTA, EDWAR ABRIL","email46",0,0,1,"A");
		$this->inicializarDocentes("SARMIENTO/CALISAYA, EDGAR","email47",0,0,1,"A");
		$this->inicializarDocentes("SHARHORODSKA/, OLHA","email48",0,0,1,"A");
		$this->inicializarDocentes("SILVA/FERNANDEZ, JESUS MARTIN","email49",0,0,1,"A");
		$this->inicializarDocentes("SORIANO/VARGAS, AUREA ROSSY","email50",0,0,1,"A");
		$this->inicializarDocentes("SUAREZ/LOPEZ, ERNESTO MAURO","email51",0,0,1,"A");
		$this->inicializarDocentes("SULLA/TORRES, JOSE ALFREDO","email52",0,0,1,"A");
		$this->inicializarDocentes("SUNI/LOPEZ, FRANCI","email53",0,0,1,"A");
		$this->inicializarDocentes("VELAZCO/PAREDES, YUBER ELMER","email54",0,0,1,"A");
		$this->inicializarDocentes("VIDAL/DUARTE, ELIZABETH ENRIQUETA","email55",0,0,1,"A");
		$this->inicializarDocentes("YARI/RAMOS, YESSENIA DEYSI","email56",0,0,1,"A");
		$this->inicializarDocentes("ZARATE/CARLOS, EDWARD VICENTE","email57",0,0,1,"A");
		$this->inicializarDocentes("mi profesor","cuentas.ax@gmail.com",1,1,1,"A");
		$this->inicializarDocentes("ORIHUELA/TRUJILLLO, WILLIAN EDUARDO","airis7495@gmail.com",1,1,1,"A");
		$this->inicializarDocentes("KARI/NINACANSAYA, DIEGO JUNIOR","dkari@unsa.edu.pe",1,1,1,"A");
		$this->inicializarDocentes("ZAMATA/FLORES, PEDRO","pzamata@unsa.edu.pe",1,1,1,"A");
		$this->inicializarDocentes("RONDÁN/HUAPAYA, CRISTIAN","crondanh@unsa.edu.pe@unsa.edu.pe",1,1,1,"A");
    }

    private function inicializarDocentes($nombre,$email,$superadmin,$admin,$docente,$estado)
    {
    	Docente::create([
    		'nombre' => $nombre,
    		'email' => $email,
    		'superadmin' => $superadmin,
    		'admin' => $admin,
    		'docente' => $docente,
    		'estado' => $estado
    	]);
    }
}
