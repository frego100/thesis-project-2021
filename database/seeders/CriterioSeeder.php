<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Criterio;
use DB;

class CriterioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Criterio::create(array(
            'resultadoestudiante_id' => '1',
            'codigo' => 'RE.1.1.',
            'criterio' => 'identifica problemas complejos de ingeniería aplicando conocimientos de matemáticas, ciencias e ingeniería',
            'insatisfactorio' => 'No identifica problemas',
            'enproceso' => 'Identifica problema en ingeniería sin definir su complejidad',
            'satisfactorio' => 'Identifica problemas complejos parcialmente',
            'sobresaliente' => 'Identifica problemas complejos'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '1',
            'codigo' => 'RE.1.2.',
            'criterio' => 'Representa apropiadamente problemas complejos de ingeniería, usando herramientas de matemáticas, ciencia e ingenieria',
            'insatisfactorio' => 'No representa  problemas',
            'enproceso' => 'Genera representaciones básicas de los problemas',
            'satisfactorio' => 'Construye representaciones complejas de problemas propuestos',
            'sobresaliente' => 'Construye representaciones formales de problemas complejos'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '1',
            'codigo' => 'RE.1.3.',
            'criterio' => 'Formula soluciones a los problemas complejos de ingeniería aplicando conocimientos de matemáticas, ciencia e ingeniería',
            'insatisfactorio' => 'No  formula soluciones',
            'enproceso' => 'Formula soluciones básicas',
            'satisfactorio' => 'Formula soluciones complejas a problemas propuestos',
            'sobresaliente' => 'Formula soluciones apropiadas a problemas complejos'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '1',
            'codigo' => 'RE.1.4.',
            'criterio' => 'Resuelve problemas complejos de ingeniería aplicando conocimientos de matemáticas, ciencia e ingeniería',
            'insatisfactorio' => 'No resuelve problemas',
            'enproceso' => 'Resuelve problemas básicos',
            'satisfactorio' => 'Resuelve problemas complejos propuestos',
            'sobresaliente' => 'Resuelve problemas complejos'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '2',
            'codigo' => 'RE.2.1.',
            'criterio' => 'Identifica problemas en ingeniería con consideraciones realistas en salud pública, seguridad y bienestar; factores globales, culturales, sociales, económicos y ambientales',
            'insatisfactorio' => 'No identifica problemas',
            'enproceso' => 'Identifica problemas sin definir sus restricciones',
            'satisfactorio' => 'Identifica problemas considerando parcialmente sus restricciones',
            'sobresaliente' => 'Identifica problemas  en ingeniería y sus restricciones'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '2',
            'codigo' => 'RE.2.2.',
            'criterio' => 'Plantea soluciones a los problemas en ingeniería con consideraciones realistas en salud pública, seguridad y bienestar; factores globales, culturales, sociales, económicos y ambientales',
            'insatisfactorio' => 'No plantea soluciones',
            'enproceso' => 'Plantea soluciones parciales no considerando restricciones',
            'satisfactorio' => 'Plantea soluciones considerando parcialmente restricciones',
            'sobresaliente' => 'Plantea soluciones considerando todas las restricciones necesarias'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '2',
            'codigo' => 'RE.2.3.',
            'criterio' => 'Diseña sistemas, componentes o procesos como solución adecuada al problema en ingeniería considerando restricciones en el contexto en salud pública, seguridad y bienestar; factores globales, culturales, sociales, económicos y ambientales',
            'insatisfactorio' => 'No diseña sistemas, componentes o procesos',
            'enproceso' => 'Diseña de manera parcial sistemas, componentes o procesos',
            'satisfactorio' => 'Diseña sistemas, componentes o procesos',
            'sobresaliente' => 'Diseña eficientemente sistemas, componentes o procesos'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '2',
            'codigo' => 'RE.2.4.',
            'criterio' => 'Implementa sistemas, componentes o procesos acorde a la solución planteada al problema en ingeniería, considerando restricciones en el contexto en salud pública, seguridad y bienestar; factores globales, culturales, sociales, económicos y ambientales',
            'insatisfactorio' => 'No implementa sistemas, componentes o procesos',
            'enproceso' => 'Implementa de manera parcial sistemas, componentes o procesos',
            'satisfactorio' => 'Implementa sistemas, componentes o procesos',
            'sobresaliente' => 'Implementa eficientemente sistemas, componentes o procesos'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '2',
            'codigo' => 'RE.2.5.',
            'criterio' => 'Evalúa los sistemas, componentes o procesos implementados, considerando restricciones en el contexto en salud pública, seguridad y bienestar; factores globales, culturales, sociales, económicos y ambientales',
            'insatisfactorio' => 'No evalúa los sistemas, componentes o procesos implementados.',
            'enproceso' => 'Evalúa los sistemas, componentes o procesos implementados de manera parcial sin formular conclusiones',
            'satisfactorio' => 'Evalúa los sistemas, componentes o procesos implementados generando conclusiones',
            'sobresaliente' => 'Evalúa los sistemas, componentes o procesos implementados formulando conclusiones y recomendaciones'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '3',
            'codigo' => 'RE.3.1.',
            'criterio' => 'Utiliza apropiadamente los recursos disponibles',
            'insatisfactorio' => 'No utiliza los recursos',
            'enproceso' => 'Utiliza recursos básicos',
            'satisfactorio' => 'Utiliza recursos propuestos por el profesor',
            'sobresaliente' => 'Utiliza apropiadamente recursos disponibles'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '3',
            'codigo' => 'RE.3.2.',
            'criterio' => 'Organiza apropiadamente la información para expresar sus ideas de manera efectiva',
            'insatisfactorio' => 'No organiza la información',
            'enproceso' => 'Organiza información sin jerarquizarla',
            'satisfactorio' => 'Organiza la información jerarquizando los contenidos',
            'sobresaliente' => 'Organiza apropiadamente la información para expresar sus ideas de manera efectiva'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '3',
            'codigo' => 'RE.3.3.',
            'criterio' => 'Redacta y/o comprende correctamente informes técnicos',
            'insatisfactorio' => 'No redacta y/o comprende informes',
            'enproceso' => 'Redacta y/o comprende  informes de manera incompleta',
            'satisfactorio' => 'Redacta y/o comprende  informes según lo solicitado',
            'sobresaliente' => 'Redacta y/o comprende correctamente  informes técnicos'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '4',
            'codigo' => 'RE.4.1.',
            'criterio' => 'Reconoce los principios éticos  e identifica responsabilidades profesionales en la práctica de la ingeniería',
            'insatisfactorio' => 'No reconoce los principios ni identifica responsabilidades',
            'enproceso' => 'Reconoce los principios éticos parcilamente',
            'satisfactorio' => 'Reconoce los principios éticos',
            'sobresaliente' => 'Reconoce los principios e identifica responsabilidades'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '4',
            'codigo' => 'RE.4.2.',
            'criterio' => 'plica los principios éticos y asume responsabilidades profesionales en la práctica de la ingeniería haciendo juicios informados que consideren el impacto de las soluciones de ingeniería',
            'insatisfactorio' => 'No aplica los principios ni asume responsabilidades',
            'enproceso' => 'Aplica los principios éticos parcilamente',
            'satisfactorio' => 'Aplica los principios éticos',
            'sobresaliente' => 'Aplica los principios y asume responsabilidades'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '4',
            'codigo' => 'RE.4.3.',
            'criterio' => 'Evalúa el impacto de la solución a los problemas  de ingeniería en un contexto global, económico, ambiental y social',
            'insatisfactorio' => 'No evalua el impacto de la solución',
            'enproceso' => 'Evalúa el impacto parcialmente',
            'satisfactorio' => 'Evalúa el impacto',
            'sobresaliente' => 'Evalúa el impacto de la solución a los problemas  de ingeniería emitiendo juicios de valor'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '5',
            'codigo' => 'RE.5.1.',
            'criterio' => 'Se integra eficazmente en equipos de trabajo',
            'insatisfactorio' => 'No se integra',
            'enproceso' => 'Se integra en equipos de trabajo sin participar',
            'satisfactorio' => 'Se integra en equipos de trabajo asignados y participa',
            'sobresaliente' => 'Se integra en equipos de trabajo y participa por iniciativa propia'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '5',
            'codigo' => 'RE.5.2.',
            'criterio' => 'Ejerce liderazgo en un ambiente colaborativo e incluyente',
            'insatisfactorio' => 'No ejerce liderazgo',
            'enproceso' => 'No ejerce liderazgo pero reconoce el rol en algún miembro del equipo',
            'satisfactorio' => 'Ejerce liderazgo sin propiciar un ambiente colaborativo',
            'sobresaliente' => 'Ejerce liderazgo en un ambiente colaborativo e incluyente'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '5',
            'codigo' => 'RE.5.3.',
            'criterio' => 'Planifica tareas, metas y trabaja para el logro de los objetivos',
            'insatisfactorio' => 'No planifica',
            'enproceso' => 'No planifica pero reconoce la planificación de otros miembros del equipo',
            'satisfactorio' => 'Planifica tareas y metas sin llegar al logro de objetivos',
            'sobresaliente' => 'Planifica tareas, metas y trabaja para el logro de los objetivos'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '6',
            'codigo' => 'RE.6.1.',
            'criterio' => 'Formula el desarrollo experimental a partir de hipótesis basándose en métodos apropiados',
            'insatisfactorio' => 'No formula el desarrollo experimental',
            'enproceso' => 'Formula el desarrollo experimental sin usar  métodos apropiados',
            'satisfactorio' => 'Formula un desarrollo experimental usando  métodos apropiados',
            'sobresaliente' => 'Formula un desarrollo experimental eficiente usando métodos apropiados'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '6',
            'codigo' => 'RE.6.2.',
            'criterio' => 'Experimenta, recopila, analiza e interpreta los datos obtenidos',
            'insatisfactorio' => 'No experimenta, recopila, analiza ni interpreta',
            'enproceso' => 'Experimenta, pero no recopila los datos',
            'satisfactorio' => 'Experimenta, recopila pero no interpreta apropiadamente los datos',
            'sobresaliente' => 'Experimenta, recopila e interpreta los datos con claridad'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '6',
            'codigo' => 'RE.6.3.',
            'criterio' => 'Genera conclusiones válidas a partir del juicio de ingeniería',
            'insatisfactorio' => 'No genera conclusiones',
            'enproceso' => 'Genera conclusiones no consistentes',
            'satisfactorio' => 'Genera conclusiones parciales o incompletas',
            'sobresaliente' => 'Genera conclusiones válidas a partir del juicio de ingeniería'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '7',
            'codigo' => 'RE.7.1.',
            'criterio' => 'Reconoce la necesidad de mantener sus conocimientos y habilidades actualizadas en la especialidad',
            'insatisfactorio' => 'No reconoce la necesidad del autoaprendizaje',
            'enproceso' => 'Reconoce la necesidad del autoaprendizaje de forma parcial',
            'satisfactorio' => 'Reconoce la necesidad del autoaprendizaje como un compromiso personal',
            'sobresaliente' => 'Reconoce la necesidad de mantener sus conocimientos y habilidades actualizadas en la especialidad'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '7',
            'codigo' => 'RE.7.2.',
            'criterio' => 'Afronta procesos de aprendizaje individual en áreas de conocimiento nuevas usando estrategias apropiadas',
            'insatisfactorio' => 'No afronta procesos de aprendizaje individual',
            'enproceso' => 'participa en procesos de aprendizaje a sugerencia de otros',
            'satisfactorio' => 'participa en procesos de aprendizaje por iniciativa propia',
            'sobresaliente' => 'Afronta procesos de aprendizaje individual en áreas de conocimiento nuevas usando estrategias apropiadas'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '7',
            'codigo' => 'RE.7.3.',
            'criterio' => 'Aporta creatividad e innovación en temas de ciencia y tecnología usando estrategias apropiadas',
            'insatisfactorio' => 'No aporta creatividad ni innovación',
            'enproceso' => 'Aporta creatividad e innovación  por exigencias externos',
            'satisfactorio' => 'Aporta creatividad e innovación por iniciativa propia',
            'sobresaliente' => 'Aporta creatividad e innovación en temas de ciencia y tecnología usando estrategias apropiadas'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '8',
            'codigo' => 'RE.8.1.',
            'criterio' => 'Usa herramientas, habilidades, técnicas, métodos y/o metodologías adecuadas, propias de la disciplina entendiendo sus limitaciones',
            'insatisfactorio' => 'No usa herramientas, habilidades, técnicas, métodos y/o metodologías',
            'enproceso' => 'Usa herramientas, habilidades, técnicas, métodos y/o metodologías',
            'satisfactorio' => 'Usa adecuadamente herramientas, habilidades, técnicas, métodos y/o metodologías',
            'sobresaliente' => 'Usa las herramientas, habilidades, técnicas, métodos y/o metodologías más apropiadas entendiendo sus limitaciones'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '8',
            'codigo' => 'RE.8.2.',
            'criterio' => 'Selecciona herramientas, habilidades, técnicas, métodos y/o metodologías adecuadas, propias de la disciplina entendiendo sus limitaciones',
            'insatisfactorio' => 'No selecciona herramientas, habilidades, técnicas, métodos y/o metodologías',
            'enproceso' => 'Usa adecuadamente herramientas, habilidades, técnicas, métodos y/o metodologías',
            'satisfactorio' => 'Selecciona adecuadamente herramientas, habilidades, técnicas, métodos y/o metodologías',
            'sobresaliente' => 'Selecciona las herramientas, habilidades, técnicas, métodos y/o metodologías más apropiadas entendiendo sus limitaciones'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '8',
            'codigo' => 'RE.8.3.',
            'criterio' => 'Desarrolla herramientas, habilidades, técnicas, métodos y/o metodologías adecuadas, propias de la disciplina entendiendo sus limitaciones',
            'insatisfactorio' => 'No desarrolla herramientas, habilidades, técnicas, métodos y/o metodologías',
            'enproceso' => 'Selecciona adecuadamente herramientas, habilidades, técnicas, métodos y/o metodologías',
            'satisfactorio' => 'Desarrolla adecuadamente herramientas, habilidades, técnicas, métodos y/o metodologías',
            'sobresaliente' => 'Desarrolla las herramientas, habilidades, técnicas, métodos y/o metodologías más apropiadas entendiendo sus limitaciones'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '9',
            'codigo' => 'RE.9.1.',
            'criterio' => 'Formula los objetivos, alcances y restricciones de un proyecto de Software y/o Tecnologías de Información',
            'insatisfactorio' => 'No formula objetivos, alcances y restricciones',
            'enproceso' => 'Formula objetivos y/o alcances y/o restricciones parcialmente',
            'satisfactorio' => 'Formula objetivos, alcances y restricciones parcialmente',
            'sobresaliente' => 'Formula objetivos, alcances y restricciones de un proyecto de Software y/o Tecnologías de Información'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '9',
            'codigo' => 'RE.9.2.',
            'criterio' => 'Planifica actividades, recursos e indicadores necesarios para desarrollar el proyecto',
            'insatisfactorio' => 'No planifica actividades, recursos e indicadores',
            'enproceso' => 'Planifica actividades y/o recursos y/o indicadores parcialmente',
            'satisfactorio' => 'Planifica actividades, recursos e indicadores parcialmente',
            'sobresaliente' => 'Planifica actividades, recursos e indicadores para desarrollar el proyecto, evaluando la factibilidad'
        ));
        Criterio::create(array(
            'resultadoestudiante_id' => '9',
            'codigo' => 'RE.9.3.',
            'criterio' => 'Realiza el seguimiento del desarrollo de un proyecto de Software y/o Tecnologías de Información según la planificación',
            'insatisfactorio' => 'No realiza el seguimiento',
            'enproceso' => 'Realiza el seguimiento sin considerar la planificación',
            'satisfactorio' => 'Realiza el seguimiento  considerando la planificación de manera parcial',
            'sobresaliente' => 'Realiza el seguimiento del desarrollo de un proyecto de Software y/o Tecnologías de Información según la planificación'
        ));
    }
}