<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Competencia;
use DB;

class CompetenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *s
     * @return void
     */
    public function run()
    {
        Competencia::create(array(
            "plan_id" => '2',
            "codigo" => 'C.a.',
            "competencia" => 'Aplica de forma transformadora conocimientos de matemática, computación e ingeniería    como herramienta para evaluar, sintetizar y mostrar información como fundamento de sus ideas y perspectivas para la resolución de problemas'
        ));

        Competencia::create(array(
            "plan_id" => '2',
            "codigo" => 'C.b.',
            "competencia" => 'Genera de forma responsable prototipos, experimentos y modelos, con el fin de analizar e interpretar información para la toma de decisiones fundamentadas y objetivas'
        ));

        Competencia::create(array(
            "plan_id" => '2',
            "codigo" => 'C.c.',
            "competencia" => 'Diseña responsablemente sistemas, componentes o procesos para satisfacer necesidades dentro de restricciones realistas: económicas, medio ambientales, sociales, políticas, éticas, de salud, de seguridad, manufacturación y sostenibilidad'
        ));

        Competencia::create(array(
            "plan_id" => '2',
            "codigo" => 'C.d.',
            "competencia" => 'Trabaja éticamente, de manera en efectiva, en equipos multidisciplinarios, adaptándose a diferentes entornos laborales y nuevas situaciones, con colaboradores de diversa formación y cultura'
        ));

        Competencia::create(array(
            "plan_id" => '2',
            "codigo" => 'C.e.',
            "competencia" => 'Identifica de forma reflexiva y responsable, necesidades a ser resueltas usando tecnologías de información y/o desarrollo de software en los ámbitos local, nacional o internacional, utilizando técnicas, herramientas, metodologías, estándares y principios de la ingeniería'
        ));

        Competencia::create(array(
            "plan_id" => '2',
            "codigo" => 'C.f.',
            "competencia" => 'Asume su responsabilidad profesional y ética, en el desempeño de las tareas o actividades de su profesión, contribuyendo de manera competente y con prácticas innovadoras a la industria, la academia y/o el sector público'
        ));

        Competencia::create(array(
            "plan_id" => '2',
            "codigo" => 'C.g.',
            "competencia" => 'Comunica con tolerancia, a público de diferentes especialidades, culturas y jerarquías de forma clara y efectiva ideas, resultados y productos relacionados al desarrollo de su ejercicio profesional'
        ));

        Competencia::create(array(
            "plan_id" => '2',
            "codigo" => 'C.h.',
            "competencia" => 'Asume una posición reflexiva sobre el impacto de las soluciones de software y tecnología de la información, en un contexto local, nacional, global, económico, ambiental y sustenta mediante la evaluación de evidencia relevante a dicha posición, evaluando las implicancias y/o consecuencias'
        ));

        Competencia::create(array(
            "plan_id" => '2',
            "codigo" => 'C.i.',
            "competencia" => 'Practica el aprendizaje permanente, como herramienta para adaptarse a los rápidos cambios tecnológicos, organizacionales y sociales, con una actitud proactiva y de liderazgo y como agente de cambio'
        ));

        Competencia::create(array(
            "plan_id" => '2',
            "codigo" => 'C.j.',
            "competencia" => 'Conoce la problemática social, histórica, política, económica, cultural, y medioambiental de la región y del país y su interacción con la realidad mundial contemporánea, a fin de aportar en el desarrollo del país por medio de una práctica profesional responsable e integral'
        ));
    }
}
