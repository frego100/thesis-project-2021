<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CompetenciasRE;

class CompetenciasRESeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CompetenciasRE::factory()->count(10)->create();
    }
}