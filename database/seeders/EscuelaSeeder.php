<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Escuela;
use DB;

class EscuelaSeeder extends Seeder
{
    
    public function run()
    {
        Escuela::create(array(
            'escuela' => 'Ingeniería de Sistemas',
            'facultad_id' => '1'
        ));

        Escuela::create(array(
            'escuela' => 'Ingeniería Industrial',
            'facultad_id' => '1'
        ));

        Escuela::create(array(
            'escuela' => 'Ingeniería Electrónica',
            'facultad_id' => '1'
        ));

        Escuela::create(array(
            'escuela' => 'Ingeniería Eléctrica',
            'facultad_id' => '1'
        ));

        Escuela::create(array(
            'escuela' => 'Ingeniería Mecánica',
            'facultad_id' => '1'
        ));

        Escuela::create(array(
            'escuela' => 'Ingeniería de Telecomunicaciones',
            'facultad_id' => '1'
        ));

        Escuela::create(array(
            'escuela' => 'Ciencias de la Computación',
            'facultad_id' => '1'
        ));

        Escuela::create(array(
            'escuela' => 'Ingeniería Quimica',
            'facultad_id' => '2'
        ));

        Escuela::create(array(
            'escuela' => 'Ingeniería Ambiental',
            'facultad_id' => '2'
        ));

        Escuela::create(array(
            'escuela' => 'Ingeniería de Materiales',
            'facultad_id' => '2'
        ));
    }
}
