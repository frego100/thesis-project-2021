<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Curso;
use DB;

class CursoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->inicializarCursos("2","1","1701102","RAZONAMIENTO LOGICO MATEMATICO","0","1","2","2","3");
        $this->inicializarCursos("2","1","1701103","MATEMATICA BASICA","0","1","2","4","4");
        $this->inicializarCursos("2","1","1701104","ESTRUCTURAS DISCRETAS 1","0","1","2","2","3");
        $this->inicializarCursos("2","1","1701105","INTRODUCCION A LA COMPUTACION","0","1","1","2","2");
        $this->inicializarCursos("2","1","1701106","FUNDAMENTOS DE LA PROGRAMACION 1","0","1","2","2","5");
        $this->inicializarCursos("2","1","1701114","METODOLOGIA DEL TRABAJO INTELECTUAL UNIVERSITARIO","0","1","0","4","2");
        $this->inicializarCursos("2","1","1701145","RELACIONES HUMANAS EN EMPRESAS DE DESARROLLO DE SOFTWARE Y BASE TECNOLÓGICA","0","1","1","2","2");
        $this->inicializarCursos("2","1","1701210","CALCULO EN UNA VARIABLE","0","2","1","6","4");
        $this->inicializarCursos("2","1","1701211","ESTRUCTURAS DISCRETAS 2","0","2","2","2","3");
        $this->inicializarCursos("2","1","1701212","PROGRAMACION WEB 1","0","2","2","0","4");
        $this->inicializarCursos("2","1","1701213","FUNDAMENTOS DE PROGRAMACION 2","0","2","2","2","5");
        $this->inicializarCursos("2","1","1701216","COMUNICACION INTEGRAL","0","2","2","2","3");
        $this->inicializarCursos("2","1","1701217","REALIDAD NACIONAL","0","2","1","2","2");
        $this->inicializarCursos("2","1","1702118","TALLERES DE PSICOLOGIA","0","3","2","0","2");
        $this->inicializarCursos("2","1","1702119","CALCULO EN VARIAS VARIABLES","0","3","1","6","4");
        $this->inicializarCursos("2","1","1702120","TALLER DE LIDERAZGO Y COLABORACION","0","3","2","6","2");
        $this->inicializarCursos("2","1","1702121","INNOVACION Y CREATIVIDAD","0","3","2","2","3");
        $this->inicializarCursos("2","2","1702122","PROGRAMACION WEB 2","0","3","2","0","4");
        $this->inicializarCursos("2","1","1702123","REDACCION DE ARTICULOS E INFORMES DE INVESTIGACION","0","3","2","0","2");
        $this->inicializarCursos("2","1","1702124","ESTRUCTURA DE DATOS Y ALGORITMOS","0","3","2","2","4");
        $this->inicializarCursos("2","1","1702125","CIUDADANIA E INTERCULTURALIDAD","0","3","1","2","2");
        $this->inicializarCursos("2","1","1702226","ESTADISTICA MATEMATICA, PROBABILIDADES Y METODOS EMPIRICOS","0","4","2","2","4");
        $this->inicializarCursos("2","1","1702227","ARQUITECTURA DE COMPUTADORAS","0","4","1","2","3");
        $this->inicializarCursos("2","1","1702228","METODOS DE INVESTIGACION Y REDACCION","0","4","1","2","2");
        $this->inicializarCursos("2","1","1702229","INTERACCION HUMANO COMPUTADOR","0","4","2","2","4");
        $this->inicializarCursos("2","1","1702230","ENFOQUE EMPRESARIAL","0","4","1","4","3");
        $this->inicializarCursos("2","2","1702231","ANALISIS Y DISENO DE ALGORITMOS","0","4","2","2","4");
        $this->inicializarCursos("2","1","1702278","ECOLOGIA Y MEDIO AMBIENTE","0","4","1","2","2");
        $this->inicializarCursos("2","1","1703133","BASE DE DATOS","0","5","2","2","4");
        $this->inicializarCursos("2","1","1703134","PROGRAMACION DE SISTEMAS","0","5","2","0","4");
        $this->inicializarCursos("2","2","1703135","INGENIERIA Y PROCESOS DE SOFTWARE","0","5","1","4","3");
        $this->inicializarCursos("2","1","1703136","TEORIA DE LA COMPUTACION","0","5","1","2","3");
        $this->inicializarCursos("2","1","1703137","ORGANIZACION Y METODOS","0","5","1","2","3");
        $this->inicializarCursos("2","1","1703138","INVESTIGACION DE OPERACIONES","0","5","2","2","4");
        $this->inicializarCursos("2","1","1703239","REDES Y COMUNICACION DE DATOS","0","6","2","2","4");
        $this->inicializarCursos("2","1","1703240","TECNOLOGIA DE OBJETOS","0","6","2","2","4");
        $this->inicializarCursos("2","1","1703241","SISTEMAS OPERATIVOS","0","6","2","2","4");
        $this->inicializarCursos("2","1","1703242","FUNDAMENTOS DE SISTEMAS DE INFORMACION","0","6","1","4","3");
        $this->inicializarCursos("2","2","1703243","CONSTRUCCION DE SOFTWARE","0","6","2","0","4");
        $this->inicializarCursos("2","1","1703244","METODOS NUMERICOS","0","6","1","2","3");
        $this->inicializarCursos("2","1","1704146","FISICA COMPUTACIONAL","0","7","2","0","3");
        $this->inicializarCursos("2","1","1704147","TECNOLOGIAS DE INFORMACION","0","7","2","2","4");
        $this->inicializarCursos("2","1","1704148","INTELIGENCIA ARTIFICIAL","0","7","1","4","3");
        $this->inicializarCursos("2","2","1704149","INGENIERIA DE REQUERIMIENTOS","0","7","2","2","4");
        $this->inicializarCursos("2","1","1704150","SISTEMAS DISTRIBUIDOS","0","7","2","2","4");
        $this->inicializarCursos("2","1","1704151","PRUEBAS DE SOFTWARE","0","7","2","0","3");
        $this->inicializarCursos("2","1","1704153","INGLES","0","7","1","2","2");
        $this->inicializarCursos("2","1","1704252","GESTION DE PROYECTOS DE SOFTWARE","0","8","2","0","3");
        $this->inicializarCursos("2","1","1704254","CALIDAD DE SOFTWARE","0","8","2","0","3");
        $this->inicializarCursos("2","1","1704255","AUDITORIA DE SISTEMAS","0","8","1","4","3");
        $this->inicializarCursos("2","2","1704256","DISEÑO Y ARQUITECTURA DE SOFTWARE","0","8","2","2","4");
        $this->inicializarCursos("2","1","1704257","NEGOCIOS ELECTRONICOS (E)","0","8","3","0","4");
        $this->inicializarCursos("2","1","1704258","INTRODUCCION AL DESARROLLO DE SOFTWARE DE ENTRETENIMIENTO (E)","0","8","3","0","4");
        $this->inicializarCursos("2","1","1704259","INTRODUCCION AL DESARROLLO DE NUEVAS PLATAFORMAS (E)","0","8","3","0","4");
        $this->inicializarCursos("2","1","1704260","ASPECTOS FORMALES DE ESPECIFICACION Y VERIFICACION","0","8","2","0","3");
        $this->inicializarCursos("2","3","1705161","PROYECTO DE INGENIERIA DE SOFTWARE 1","0","9","2","0","3");
        $this->inicializarCursos("2","3","1705162","SEMINARIO DE TESIS 1","0","9","2","2","3");
        $this->inicializarCursos("2","1","1705163","GESTION DE EMPRENDIMIENTO DE SOFTWARE","0","9","1","2","2");
        $this->inicializarCursos("2","1","1705164","SEGURIDAD INFORMATICA","0","9","1","2","3");
        $this->inicializarCursos("2","1","1705165","MANTENIMIENTO, CONFIGURACION Y EVOLUCION DE SOFTWARE","0","9","1","2","3");
        $this->inicializarCursos("2","1","1705166","ETICA GENERAL Y PROFESIONAL","0","9","1","2","2");
        $this->inicializarCursos("2","1","1705167","TOPICOS AVANZADOS EN BASES DE DATOS (E)","0","9","3","2","4");
        $this->inicializarCursos("2","1","1705168","COMPUTACION GRAFICA, VISION COMPUTACIONAL Y MULTIMEDIA (E)","0","9","3","0","4");
        $this->inicializarCursos("2","1","1705169","DESARROLLO AVANZADO EN NUEVAS PLATAFORMAS (E)","0","9","3","0","4");
        $this->inicializarCursos("2","1","1705270","TOPICOS AVANZADOS EN INGENIERIA DE SOFTWARE","0","10","1","2","3");
        $this->inicializarCursos("2","1","1705271","PRACTICAS PREPROFESIONALES","0","10","2","2","3");
        $this->inicializarCursos("2","3","1705272","SEMINARIO DE TESIS 2","0","10","2","4","4");
        $this->inicializarCursos("2","3","1705273","PROYECTO DE INGENIERIA DE SOFTWARE 2","0","10","2","0","3");
        $this->inicializarCursos("2","1","1705274","GESTION DE SISTEMAS Y TECNOLOGIAS DE INFORMACION (E)","0","10","2","0","3");
        $this->inicializarCursos("2","1","1705275","DESARROLLO DE SOFTWARE PARA JUEGOS (E)","0","10","2","0","3");
        $this->inicializarCursos("2","1","1705276","PLATAFORMAS EMERGENTES (E)","0","10","2","0","3");
    }

    private function inicializarCursos($plan_id,$cursotipo_id,$codigo,$curso,$descripcion,$semestre,$horasteoria,$horaspractica,$creditos)
    {
        Curso::create([
            'plan_id' => $plan_id,
            'cursotipo_id' => $cursotipo_id,
            'codigo' => $codigo,
            'curso' => $curso,
            'descripcion' => $descripcion,
            'semestre' => $semestre,
            'horasteoria' => $horasteoria,
            'horaspractica' => $horaspractica,
            'creditos' => $creditos,
        ]);
    }
}
