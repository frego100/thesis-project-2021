<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call(FacultadSeeder::class);
        $this->call(EscuelaSeeder::class);
        $this->call(PlanSeeder::class);
        $this->call(CursoTipoSeeder::class);
        $this->call(SemestreSeeder::class);
        $this->call(CompetenciaSeeder::class);
        $this->call(ResultadosEstudianteSeeder::class);
        $this->call(CursoSeeder::class);
        $this->call(EscuelaSemestreSeeder::class);
        $this->call(CompetenciasRESeeder::class);
        $this->call(CriterioSeeder::class);
        //$this->call(CursoSemestreSeeder::class);
        $this->call(DocenteSeeder::class);
        //$this->call(CursoMatrizSeeder::class);
        //$this->call(CursoEsquemaSeeder::class);
        //$this->call(CursoGrupoSeeder::class);
        $this->call(EstudianteSeeder::class);
        //$this->call(MatrizDetalleSeeder::class);
        //$this->call(CursoEstudianteSeeder::class);
        //$this->call(EstudianteMatrizSeeder::class);
        
        
    }
}
