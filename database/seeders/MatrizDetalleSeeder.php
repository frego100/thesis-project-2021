<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MatrizDetalle;

class MatrizDetalleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MatrizDetalle::factory()->count(10)->create();
    }
}
