<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ResultadosEstudiante;
use DB;

class ResultadosEstudianteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ResultadosEstudiante::create(array(
            'plan_id' => '2',
            'codigore' => 'RE1',
            're' => 'La capacidad de aplicar conocimientos de matemáticas, ciencias e ingeniería en la identificación, formulación y resolución de problemas complejos de ingeniería.'
        ));

        ResultadosEstudiante::create(array(
            'plan_id' => '2',
            'codigore' => 'RE2',
            're' => 'La capacidad de aplicar diseño de ingeniería para producir soluciones a problemas y diseñar sistemas, componentes o procesos para satisfacer necesidades específicas dentro de consideraciones realistas en los aspectos de salud pública, seguridad y bienestar; factores globales, culturales, sociales, económicos y ambientales.'
        ));

        ResultadosEstudiante::create(array(
            'plan_id' => '2',
            'codigore' => 'RE3',
            're' => 'La capacidad de comunicarse efectivamente ante audiencias variadas, mediante la comprensión y redacción de informes, así como la realización de exposiciones.'
        ));

        ResultadosEstudiante::create(array(
            'plan_id' => '2',
            'codigore' => 'RE4',
            're' => 'La capacidad para reconocer y aplicar principios éticos y asumir responsabilidades profesionales en la práctica de la ingeniería, haciendo juicios informados que consideren el impacto de las soluciones de ingeniería en contextos globales, económicos, ambientales y sociales.'
        ));

        ResultadosEstudiante::create(array(
            'plan_id' => '2',
            'codigore' => 'RE5',
            're' => 'La capacidad de desenvolverse eficazmente como miembro o líder en diversos equipos, y en entornos multidisciplinarios, creando un entorno colaborativo e inclusivo, estableciendo metas, planificando apropiadamente tareas y logrando los objetivos planteados.'
        ));

        ResultadosEstudiante::create(array(
            'plan_id' => '2',
            'codigore' => 'RE6',
            're' => 'La capacidad de desarrollar y conducir experimentos, análisis, interpretación de datos y síntesis de información para producir conclusiones válidas usando el juicio de ingeniería.'
        ));

        ResultadosEstudiante::create(array(
            'plan_id' => '2',
            'codigore' => 'RE7',
            're' => 'El reconocimiento de la necesidad del aprendizaje permanente y la capacidad de adquirir y aplicar nuevos conocimientos usando las estrategias apropiadas, en el más amplio contexto de los cambios tecnológicos.'
        ));

        ResultadosEstudiante::create(array(
            'plan_id' => '2',
            'codigore' => 'RE8',
            're' => 'La capacidad de crear, seleccionar y utilizar técnicas, habilidades, recursos y herramientas modernas de ingeniería y tecnologías de la información, incluyendo la predicción y el modelamiento, con una comprensión de las limitaciones.'
        ));

        ResultadosEstudiante::create(array(
            'plan_id' => '2',
            'codigore' => 'RE9',
            're' => 'La capacidad de demostrar el conocimiento y comprensión de los principios de gestión en ingeniería y la toma de decisiones, así como su respectiva aplicación.'
        ));
    }
}
