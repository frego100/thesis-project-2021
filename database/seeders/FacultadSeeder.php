<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Facultad;
use DB;

class FacultadSeeder extends Seeder
{
   
    public function run()
    {
        Facultad::create(array(
            'facultad' => 'Ingeniería de Producción y Sevicios',
        ));
        
        Facultad::create(array(
            'facultad' => 'Ingeniería de Procesos',
        ));

        Facultad::create(array(
            'facultad' => 'Enfermería',
        ));

        Facultad::create(array(
            'facultad' => 'Medicina',
        ));

        Facultad::create(array(
            'facultad' => 'Arquitectura y Urbanismo',
        ));

        Facultad::create(array(
            'facultad' => 'Ciencias Naturales y Formales',
        ));

        Facultad::create(array(
            'facultad' => 'Geología, Geofísica y Minas',
        ));

        Facultad::create(array(
            'facultad' => 'Ingeniería Civil',
        ));

        Facultad::create(array(
            'facultad' => 'Ciencias Biológicas',
        ));
        
        Facultad::create(array(
            'facultad' => 'Filosofía y Humanidades',
        ));
    }
}
