<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CursoEsquema;

class CursoEsquemaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CursoEsquema::factory()->count(10)->create();
    }
}
