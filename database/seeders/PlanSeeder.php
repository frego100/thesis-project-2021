<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Plan;
use DB;

class PlanSeeder extends Seeder
{
    
    public function run()
    {
        Plan::create(array(
            'anio' => '2013',
            'escuela_id' => '1'
        ));

        Plan::create(array(
            'anio' => '2017',
            'escuela_id' => '1'
        ));
    }
}
