<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EstudianteMatriz;

class EstudianteMatrizSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EstudianteMatriz::factory()->count(10)->create();
    }
}
