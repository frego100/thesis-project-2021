<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Semestre;
use DB;

class SemestreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Semestre::create(array(
            'semestre' => 'A',
            'anio' => 2021,
            'inicio' => '2020-04-01',
            'fin' => '2020-08-01'
        ));

        Semestre::create(array(
            'semestre' => 'B',
            'anio' => 2021,
            'inicio' => '2020-08-02',
            'fin' => '2020-12-30'
        ));

        Semestre::create(array(
            'semestre' => 'C',
            'anio' => 2021,
            'inicio' => '2021-01-08',
            'fin' => '2021-03-30'
        ));
        Semestre::create(array(
            'semestre' => 'A',
            'anio' => 2022,
            'inicio' => '2021-01-08',
            'fin' => '2021-03-30'
        ));
        Semestre::create(array(
            'semestre' => 'B',
            'anio' => 2022,
            'inicio' => '2021-01-08',
            'fin' => '2021-03-30'
        ));
    }
}
