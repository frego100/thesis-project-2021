<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EscuelaSemestre;

class EscuelaSemestreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->inicializarEscuelaSmestre("1","1","Abierto","A");
        $this->inicializarEscuelaSmestre("1","2","Abierto","A");
        $this->inicializarEscuelaSmestre("1","3","Abierto","A");
        $this->inicializarEscuelaSmestre("2","1","Abierto","A");
        $this->inicializarEscuelaSmestre("2","2","Abierto","A");
        $this->inicializarEscuelaSmestre("2","3","Abierto","A");
        $this->inicializarEscuelaSmestre("1","4","Abierto","A");
        $this->inicializarEscuelaSmestre("1","5","Abierto","A");
    }

    private function inicializarEscuelaSmestre($escuela_id,$semestre_id,$estadosemestre,$estado)
    {
    	EscuelaSemestre::create([
    		'escuela_id' => $escuela_id,
    		'semestre_id' => $semestre_id,
    		'estadosemestre' => $estadosemestre,
    		'estado' => $estado
    	]);
    }
}