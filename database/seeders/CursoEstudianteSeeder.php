<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CursoEstudiante;

class CursoEstudianteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CursoEstudiante::factory()->count(10)->create();
    }
}