<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CursoTipo;
use DB;

class CursoTipoSeeder extends Seeder
{
    
    public function run()
    {
        CursoTipo::create(array(
            'cursotipo' => 'Regular' 
        ));

        CursoTipo::create(array(
            'cursotipo' => 'Control' 
        ));

        CursoTipo::create(array(
            'cursotipo' => 'Capstone'
        ));
    }
}
