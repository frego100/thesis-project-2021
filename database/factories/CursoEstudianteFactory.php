<?php

namespace Database\Factories;

use App\Models\CursoEstudiante;
use Illuminate\Database\Eloquent\Factories\Factory;

class CursoEstudianteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CursoEstudiante::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'cursogrupo_id' => rand(1,4),
            'estudiante_id' => rand(1,100),
            'estado' => 'A',
        ];
    }
}
