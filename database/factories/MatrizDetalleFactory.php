<?php

namespace Database\Factories;

use App\Models\MatrizDetalle;
use Illuminate\Database\Eloquent\Factories\Factory;

class MatrizDetalleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MatrizDetalle::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'ponderacion' => 1,
            'estado' => 'A',
            'criterio_id' => 1,
            'cursomatriz_id' => 1,
            'cursoesquema_id' => 1,
        ];
    }
}
