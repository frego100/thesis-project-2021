<?php

namespace Database\Factories;

use App\Models\CompetenciasRE;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompetenciasREFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CompetenciasRE::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'estado' => 'A',
            'resultadoestudiante_id' => rand(1,4),
            'competencia_id'=>rand(1,4),
        ];
    }
}