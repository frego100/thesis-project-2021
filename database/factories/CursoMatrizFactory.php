<?php

namespace Database\Factories;

use App\Models\CursoMatriz;
use Illuminate\Database\Eloquent\Factories\Factory;

class CursoMatrizFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CursoMatriz::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'matriz' => $this->faker->word(),
            'ponderacion' => 0.3,
            'fechacalificacioninicio' => '2020-08-02',
            'fechacalificacionfin' => '2020-12-30',
            'estado' => 'A',
            'cursosemestre_id' => 1,
        ];
    }
}
