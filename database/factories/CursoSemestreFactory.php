<?php

namespace Database\Factories;

use App\Models\CursoSemestre;
use Illuminate\Database\Eloquent\Factories\Factory;

class CursoSemestreFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CursoSemestre::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'estado' => 'A',
            'curso_id' => rand(1,4),
            'escuelasemestre_id'=>rand(1,3),
        ];
    }
}