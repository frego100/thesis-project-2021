<?php

namespace Database\Factories;

use App\Models\EstudianteMatriz;
use Illuminate\Database\Eloquent\Factories\Factory;

class EstudianteMatrizFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EstudianteMatriz::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nota' => rand(0,20),
            'estado' => 'A',
            'matriz_detalle_id' => 1,
            'curso_estudiante_id' => 1,
        ];
    }
}
