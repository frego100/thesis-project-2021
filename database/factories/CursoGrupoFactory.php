<?php

namespace Database\Factories;

use App\Models\CursoGrupo;
use Illuminate\Database\Eloquent\Factories\Factory;

class CursoGrupoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CursoGrupo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'grupo' => $this->faker->word(),
            'coordinador' => '0',
            'estado' => 'A',
            'cursosemestre_id' => 1,
            'docente_id' => 1,
        ];
    }
}
