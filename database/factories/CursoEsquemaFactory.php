<?php

namespace Database\Factories;

use App\Models\CursoEsquema;
use Illuminate\Database\Eloquent\Factories\Factory;

class CursoEsquemaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CursoEsquema::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'detalle' => $this->faker->word(),
            'codigo' => 'ab',
            'estado' => 'A',
            'cursosemestre_id' => 1,
        ];
    }
}
