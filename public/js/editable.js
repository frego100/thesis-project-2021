var editor; // use a global for the submit and return data rendering in the examples

$(document).ready(function() {


    // Activate an inline edit on click of a table cell
    $('#example').on('click', 'tbody td:not(:first-child)', function(e) {
        // Focus on the input in the cell that was clicked when Editor opens
        editor.one('open', () => {
            $('input', this).focus();
        });

        editor.inline(table.cells(this.parentNode, '*').nodes());
    });

    var table = $('#example').DataTable({
        dom: "Bfrtip",
        ajax: "../php/staff.php",
        order: [
            [1, 'asc']
        ],
        columns: [{
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            { data: "first_name" },
            { data: "last_name" },
            { data: "position" },
            { data: "office" },
            { data: "start_date" },
            { data: "salary", render: $.fn.dataTable.render.number(',', '.', 0, '$') }
        ],
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
        buttons: [
            { extend: "create", editor: editor },
            { extend: "edit", editor: editor },
            { extend: "remove", editor: editor }
        ]
    });
});