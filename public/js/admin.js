$(function() {
    $(".menu").on('click', function() {
        $(".page-wrapper").toggleClass("toggled");
        $(".keep").toggleClass("width");
        $(".navbar").toggleClass("toggled");
    });
});

$('ul li').on('click', function() {
    $(this).parent().addClass('active');
});